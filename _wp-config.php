<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'w9_vnist_1');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0lZ|Ll`6]ze<rv~y1{fjLC3]?h*b+=:iWYN9gdz,2jX5pwRUy76D&[ScU7 LbNq#');
define('SECURE_AUTH_KEY',  ' (.mR~`D#wnE0FFY3,cWb`clKK6dtZ4n-%x)&7%Z;=j0th0(aYk Wju|G4j|52+N');
define('LOGGED_IN_KEY',    'bNfI5V|C!J_&o?~e%GaJX#;#?Rt`8 ;2AIwP?Jt|TaQ[LYb/QuyZ YsTe4!7]:F}');
define('NONCE_KEY',        'xC.}POC3CtZ_/LNEy408eb=i.O16h7Ggr+8)T l%eS.o-QrETimDYw~QNz1=>o`g');
define('AUTH_SALT',        '?5PZ_Npl)/w+X8iK/!LGFsA:^ENQ=k3u h~Zb#|Xy[M$XwK;Y<G9hE0iv_o[h}Ha');
define('SECURE_AUTH_SALT', 'D/E`JUn9Ze*[q*rj5p{N=_SY}cgi|>cMCl`D]phRJX^0&ayNB{rJ%w:aXzjQwYH?');
define('LOGGED_IN_SALT',   '[hF9Rq/}eFlm0MJek`J)MT;mJvj+G4$nw/~%[|QhW.*!>~o.TJ-d{JP#sm:pqcK0');
define('NONCE_SALT',       'MMKG(9&|Xi0PW3T!xz2F*bh:->d[Va@Slo]p>y]$M8^j^,[G:)H^~ZM~5|7p!d[.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

define('W9SME_DEV_MODE', true);

define('FS_METHOD','direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
