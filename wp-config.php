<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'w9_vnist_4');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';(9IIUM3Fz,5&|Fu0ur~f$Q@El(pUz7Wj)(6?q>k87 HmMx[>u BrWO300Tye5Fb');
define('SECURE_AUTH_KEY',  '4MF[Is2ES$ q]AIv&,HE,/:R8Q^q>G01~*)%0Y7RYT5veJ3O|1J.H5mO$qH7?r11');
define('LOGGED_IN_KEY',    'P]_Z7IU8a3g!3M/-nDh2ktPO9(kz~IWfa?+w1=uYV,bu2.6=M8&R]t1`IgEh3Z)Q');
define('NONCE_KEY',        '-^yKl!evN8Oz,}M1JJXKlBTMrDng|/atFyS^VQ!>j9, |8?CjYys_r?%=A_MhyYx');
define('AUTH_SALT',        'ouv9yE,*HoT6L&Dn5twL2S%,Dk*-3D{_vFcj}Z^Cq!:rG[0N{9K;nlR$Tk3B{O;A');
define('SECURE_AUTH_SALT', 'SJAW Q`zIy5y/*yTW,6m-,mWWwP:x&d8pu1kQ=ocHxj_~YU^HheHyE]{QKazZ|0v');
define('LOGGED_IN_SALT',   'L6e06b<-x(r+fxTZMPU}(|N}1lB}SA(/!$uzLe]a {kjz6T&FX8$[G323ktCbO~N');
define('NONCE_SALT',       'hPlOa$Y}F fC:5A3P[ HEy/5h!5i?gWNN|j#,T?GEmVg/I%#g!V9[M{b9kPmF2Ej');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('W9SME_DEV_MODE', true);

define('FS_METHOD','direct');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
