=== W9 W9sme Addon ===
* Designed & Developed by 9WPThemes

== Description ==

* Just an addon of W9sme Premium WordPress Theme.
* The plugin requires W9sme Premium WordPress Theme to be activated.

== Changelog ==
= Version 1.0.0

* Initial Release!