<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: service-meta.php
 * @time    : 9/28/17 1:39 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
?>
<div class="service-meta-wrapper text-link-color">
	<ul class="service-meta list-unstyled no-mb">
		<?php if ( $enable_date ): ?>
			<li class="entry-meta entry-meta-date">
				<span class="__title fw-semibold"><?php esc_html_e( 'Date', 'w9sme' ); ?>:</span>
				<span class="__entry date"><a href="<?php echo get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php echo get_the_date( get_option( 'date_format' ) ); ?> </a></span>
			</li>
		<?php endif; ?>
		
		<?php if ( $enable_categories ):
			$terms = wp_get_post_terms( get_the_ID(), array( W9sme_CPT_Service::TAX_SLUG ) );
			$cat = array();
			foreach ( $terms as $term ) {
				$cat[] = '<a href="' . get_term_link( $term, W9sme_CPT_Service::TAX_SLUG ) . '">' . $term->name . '</a>';
			}
			
			?>
			<li class="entry-meta entry-meta-category">
				<span class="__title fw-semibold"><?php esc_html_e( 'Departments', 'w9sme' ); ?>:</span>
				<span class="__entry category text-link-color"><?php echo join( ", ", $cat ); ?></span>
			</li>
		<?php endif; ?>
		
		<?php
		$price = get_post_meta(get_the_ID(),'meta-service-price', true);
		if ( $enable_price && $price): ?>
			<li class="entry-meta entry-meta-price">
				<span class="__title fw-semibold"><?php esc_html_e( 'Price', 'w9sme' ); ?>:</span>
				<span class="__entry price p-color fw-semibold"><?php echo $price; ?></span>
			</li>
		<?php endif; ?>
		
		<?php
		$time = get_post_meta(get_the_ID(),'meta-service-time', true);
		if ( $enable_time && $time): ?>
			<li class="entry-meta entry-meta-time">
				<span class="__title fw-semibold"><?php esc_html_e( 'Time', 'w9sme' ); ?>:</span>
				<span class="__entry time fs-italic"><?php echo $time; ?></span>
			</li>
		<?php endif; ?>
		
		<?php if ( $enable_social_share ): ?>
			<li  class="entry-meta entry-meta-social-share">
				<span class="__title fw-semibold"><?php esc_html_e( 'Share', 'w9sme' ); ?>:</span>
				<span class="__entry social-share">
					<?php w9sme_get_template_part( 'blog/parts/post', 'social-share' ); ?>
				</span>
			</li>
		<?php endif; ?>
	</ul>
</div>
