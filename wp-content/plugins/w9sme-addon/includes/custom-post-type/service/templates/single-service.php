<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: single-service.php
 * @time    : 12/14/2016 8:53 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

//
// Service single layout
//
$service_single_layout = isset( $_GET['layout'] ) ? $_GET['layout'] : '';
if ( ! in_array( $service_single_layout, array( 'fullwidth', 'container', 'container-xlg', 'container-fluid' ) ) ) {
	$service_single_layout = w9sme_get_meta_option( 'single-layout', get_the_ID(), '', 'container' );
	if ( w9sme_is_meta_default_value( $service_single_layout ) ) {
		$service_single_layout = w9sme_get_option( 'service-single-layout' );
	}
}
//
// Widget title style
//

$widget_title_style = w9sme_get_meta_option( 'single-widget-title-style', get_the_ID() );
if ( w9sme_is_meta_default_value( $widget_title_style ) ) {
	$template_general_widget_title_style = w9sme_get_option( 'service-single-widget-title-style' );
	if ( w9sme_is_meta_default_value( $template_general_widget_title_style ) ) {
		$widget_title_style = 'widget-title_default';
	} else {
		$widget_title_style = 'widget-title_' . $template_general_widget_title_style;
	}
} else {
	$widget_title_style = 'widget-title_' . $widget_title_style;
}


//
// Service single sidebar
//
$service_single_sidebar = isset( $_GET['sidebar'] ) ? $_GET['sidebar'] : '';
if ( ! in_array( $service_single_sidebar, array( 'left', 'right', 'both', 'none' ) ) ) {
	$service_single_sidebar = w9sme_get_meta_option( 'single-sidebar', get_the_ID() );
	if ( w9sme_is_meta_default_value( $service_single_sidebar ) ) {
		$service_single_sidebar = w9sme_get_option( 'service-single-sidebar' );
	}
}
$service_single_sidebar_width = isset( $_GET['sidebar-width'] ) ? $_GET['sidebar-width'] : '';
if ( ! in_array( $service_single_sidebar_width, array( 'small', 'large' ) ) ) {
	$service_single_sidebar_width = w9sme_get_meta_option( 'single-sidebar-width', get_the_ID() );
	if ( w9sme_is_meta_default_value( $service_single_sidebar_width ) ) {
		$service_single_sidebar_width = w9sme_get_option( 'service-single-sidebar-width' );
	}
}

$service_single_sidebar_left = w9sme_get_meta_option( 'single-sidebar-left', get_the_ID() );
if ( w9sme_is_meta_default_value( $service_single_sidebar_left ) ) {
	$service_single_sidebar_left = w9sme_get_option( 'service-single-sidebar-left' );
}

$service_single_sidebar_right = w9sme_get_meta_option( 'single-sidebar-right', get_the_ID() );
if ( w9sme_is_meta_default_value( $service_single_sidebar_right ) ) {
	$service_single_sidebar_right = w9sme_get_option( 'service-single-sidebar-right' );
}

//
// Which sidebar to display
//
$display_left  = ( $service_single_sidebar == 'left' || $service_single_sidebar == 'both' ) && is_active_sidebar( $service_single_sidebar_left );
$display_right = ( $service_single_sidebar == 'right' || $service_single_sidebar == 'both' ) && is_active_sidebar( $service_single_sidebar_right );

//
// Calculate sidebar columns
//
$left_col   = 0;
$right_col  = 0;
$center_col = 12;
if ( $display_left ) {
	if ( $service_single_sidebar_width == 'small' ) {
		$left_col = 3;
	} else {
		$left_col = 4;
	}
}
if ( $display_right ) {
	if ( $service_single_sidebar_width == 'small' ) {
		$right_col = 3;
	} else {
		$right_col = 4;
	}
}
$center_col -= ( $left_col + $right_col );

$left_col   = 'col-md-' . $left_col;
$right_col  = 'col-md-' . $right_col;
$center_col = 'col-md-' . $center_col;

//
// Header
//

$enable_feature_image = w9sme_get_option( 'service-single-feature-image', '', 0 );

//
// Meta
//
$service_single_post_meta = w9sme_get_option( 'service-single-post-meta' );
$enable_date              = $enable_categories = $enable_price = $enable_time = $enable_social_share = 0;
if ( is_array( $service_single_post_meta ) && ! empty( $service_single_post_meta ) ) {
	$enable_date         = ! ( empty( $service_single_post_meta['date'] ) ) ? 1 : 0;
	$enable_categories   = ! ( empty( $service_single_post_meta['categories'] ) ) ? 1 : 0;
	$enable_price        = ! ( empty( $service_single_post_meta['price'] ) ) ? 1 : 0;
	$enable_time         = ! ( empty( $service_single_post_meta['time'] ) ) ? 1 : 0;
	$enable_social_share = ! ( empty( $service_single_post_meta['social-share'] ) ) ? 1 : 0;
}

/*-------------------------------------
	Related Services
---------------------------------------*/
$show_related_service = w9sme_get_option( 'service-single-related' );
$related_amount       = $related_col = '';
if ( $show_related_service ) {
	$related_amount = w9sme_get_option( 'service-single-related-amount' );
	$related_col    = w9sme_get_option( 'service-single-related-col', '', 4 );
}

/*-------------------------------------
	CONTENT LAYOUT
---------------------------------------*/
$content_layout = w9sme_get_meta_or_option( 'service-single-content-layout' );


/**
 * Include header
 */
get_header();

/**
 * Include title
 */
w9sme_get_template_part( 'page-title' );
?>
	<main id="site-main-single" class="site-main-single service-single">
		<div id="primary" class="content-area <?php w9sme_the_clean_html_classes( array(
			$service_single_layout,
			$widget_title_style
		) ); ?>">
			<div class="row clearfix">
				<?php
				if ( $display_left ): ?>
					<div class="sidebar sidebar-left hidden-sm hidden-xs <?php w9sme_the_clean_html_classes( $left_col ); ?>">
						<?php dynamic_sidebar( $service_single_sidebar_left ); ?>
					</div>
				<?php endif; ?>

				<div class="single-content-wrapper <?php w9sme_the_clean_html_classes( $center_col ); ?>">
					<div class="single-content-inner">
						<?php
						while ( have_posts() ) : the_post(); ?>
							<div class="entry-content-wrapper">
								<div class="entry-content">
									<?php
									if ( ! empty( $content_layout ) ):
										echo w9sme_get_post_content_by_name( $content_layout, 'content-template' );
									else:
										the_content();
									endif;
									?>
								</div>
							</div>
							<?php
							if ( $show_related_service ) {
								include( W9sme_Addon::plugin_dir( __FILE__ ) . 'parts/single-related-item.php' );
							}
							?>
						<?php endwhile; ?>
					</div>

				</div>
				
				<?php if ( $display_right ): ?>
					<div class="sidebar sidebar-right hidden-sm hidden-xs <?php w9sme_the_clean_html_classes( $right_col ); ?>">
						<?php dynamic_sidebar( $service_single_sidebar_right ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div><!-- #primary -->
	</main><!-- #main -->

<?php

/**
 * Include footer
 */
get_footer();