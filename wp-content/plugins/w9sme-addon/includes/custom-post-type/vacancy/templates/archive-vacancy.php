<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: archive-vacancy.php
 * @time    : 8/26/16 12:39 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

//
// TP archive layout
//
$vacancy_archive_layout = isset( $_GET['layout'] ) ? $_GET['layout'] : '';
if ( ! in_array( $vacancy_archive_layout, array( 'fullwidth', 'container', 'container-xlg', 'container-fluid' ) ) ) {
	$vacancy_archive_layout = w9sme_get_option( 'vacancy-archive-layout', '', 'container' );
}

//
// Widget title style
//

$widget_title_style = w9sme_get_option( 'vacancy-archive-widget-title-style' );
if ( w9sme_is_meta_default_value( $widget_title_style ) ) {
	$widget_title_style = 'widget-title_default';
} else {
	$widget_title_style = 'widget-title_' . $widget_title_style;
}

//
// TP archive sidebar
//
$vacancy_archive_sidebar = isset( $_GET['sidebar'] ) ? $_GET['sidebar'] : '';
if ( ! in_array( $vacancy_archive_sidebar, array( 'left', 'right', 'both', 'none' ) ) ) {
	$vacancy_archive_sidebar = w9sme_get_option( 'vacancy-archive-sidebar', '', 'left' );
}

$vacancy_archive_sidebar_width = isset( $_GET['sidebar-width'] ) ? $_GET['sidebar-width'] : '';
if ( ! in_array( $vacancy_archive_sidebar_width, array( 'small', 'large' ) ) ) {
	$vacancy_archive_sidebar_width = w9sme_get_option( 'vacancy-archive-sidebar-width', '', 'small' );
}

$vacancy_archive_sidebar_left  = w9sme_get_option( 'vacancy-archive-sidebar-left', '', 'sidebar-1' );
$vacancy_archive_sidebar_right = w9sme_get_option( 'vacancy-archive-sidebar-right', '', 'sidebar-2' );

//
// Which sidebar to display
//
$display_left  = ( $vacancy_archive_sidebar == 'left' || $vacancy_archive_sidebar == 'both' ) && is_active_sidebar( $vacancy_archive_sidebar_left );
$display_right = ( $vacancy_archive_sidebar == 'right' || $vacancy_archive_sidebar == 'both' ) && is_active_sidebar( $vacancy_archive_sidebar_right );

//
// Calculate sidebar columns
//
$left_col   = 0;
$right_col  = 0;
$center_col = 12;
if ( $display_left ) {
	if ( $vacancy_archive_sidebar_width == 'small' ) {
		$left_col = 3;
	} else {
		$left_col = 4;
	}
}
if ( $display_right ) {
	if ( $vacancy_archive_sidebar_width == 'small' ) {
		$right_col = 3;
	} else {
		$right_col = 4;
	}
}
$center_col -= ( $left_col + $right_col );

$left_col   = 'col-md-' . $left_col;
$right_col  = 'col-md-' . $right_col;
$center_col = 'col-md-' . $center_col;


$archive_gutter = isset( $_GET['parst-gutter'] ) ? $_GET['parst-gutter'] : w9sme_get_option( 'vacancy-archive-gutter' );

if ( $archive_gutter !== '' ) {
	$center_col .= ' w9sme-gutter-' . intval( $archive_gutter );
} else {
	$center_col .= ' w9sme-gutter-30';
}
//
// Archive navigation
//
$archive_paging_style = isset( $_GET['parst-paging-style'] ) ? $_GET['parst-paging-style'] : w9sme_get_option( 'vacancy-archive-paging-style', '', 'default' );

$vacancy_loop_class   = array();
$vacancy_loop_class[] = 'paging-' . $archive_paging_style;
//
// Archive Display Style, Blog Loop
//
$archive_display_type = isset( $_GET['display-type'] ) ? $_GET['display-type'] : '';
if ( ! in_array( $archive_display_type, array( 'grid', 'masonry' ) ) ) {
	$archive_display_type = w9sme_get_option( 'vacancy-archive-display-type', '', 'masonry' );
}

$vacancy_loop_class[] = 'blog-type-' . $archive_display_type;
if ( ! $display_left && ! $display_right ) {
	$vacancy_loop_class[] = 'no-sidebar';
}


//
// Portfolio post class
//
$vacancy_post_class = array( 'w9sme-vacancy-classic-default' );

// target term id
$target_term_slug = '';
if ( is_tax( W9sme_CPT_Vacancy::TAX_SLUG ) ) {
	$target_term_slug = get_queried_object()->slug;
}

// item per page
$item_per_page = w9sme_get_option( 'vacancy-archive-items-per-page', '', '10' );
$item_per_page = intval( $item_per_page );
if ( empty( $item_per_page ) ) {
	$item_per_page = 10;
}

$length_menu_option   = [ 10, 25, 50, 100 ];
$length_menu_option[] = $item_per_page;

$length_menu_option = array_unique( $length_menu_option );
sort( $length_menu_option, SORT_NUMERIC );

// table widgets
$dom_position                            = "lfrtip";
$vacancy_archive_search_enable           = w9sme_get_option( 'vacancy-archive-search-enable' );
$vacancy_archive_page_length_menu_enable = w9sme_get_option( 'vacancy-archive-page-length-menu-enable' );
$vacancy_archive_result_info_enable      = w9sme_get_option( 'vacancy-archive-result-info-enable' );

if ( empty( $vacancy_archive_search_enable ) ) {
	$dom_position = str_replace( 'f', '', $dom_position );
}

if ( empty( $vacancy_archive_page_length_menu_enable ) ) {
	$dom_position = str_replace( 'l', '', $dom_position );
}

if ( empty( $vacancy_archive_result_info_enable ) ) {
	$dom_position = str_replace( 'i', '', $dom_position );
}


/**
 * Include header
 */
get_header();
/**
 * Include title
 */
w9sme_get_template_part( 'page-title' );

wp_enqueue_style( W9sme_Enqueue::STYLE_PREFIX . 'datatables' );
wp_enqueue_script( W9sme_Enqueue::SCRIPT_PREFIX . 'datatables' );
?>
	<main id="site-main-archive vacancy-archive" class="site-main-archive vacancy-archive" role="main">
		<div id="primary" class="content-area <?php w9sme_the_clean_html_classes( array(
			$vacancy_archive_layout,
			$widget_title_style
		) ); ?>">
			<div class="row clearfix">
				<?php
				if ( $display_left ): ?>
					<div class="sidebar sidebar-left hidden-sm hidden-xs <?php w9sme_the_clean_html_classes( $left_col ); ?>">
						<?php dynamic_sidebar( $vacancy_archive_sidebar_left ); ?>
					</div>
				<?php endif; ?>

				<div class="main-archive-inner <?php w9sme_the_clean_html_classes( $center_col ); ?>">
					<div class="vacancy-table-wrapper">
						<table class="vacancy-table" id="vacancy-table">
							<thead>
							<th class="all text-center" width="30">#</th>
							<th class="all"><?php _e( "Job Title", "w9sme" ) ?></th>
							<th class="min-tablet-l"><?php _e( "Location", "w9sme" ) ?></th>
							<th class="min-tablet-l"><?php _e( "Departments", "w9sme" ) ?></th>
							<th class="min-tablet-l"><?php _e( "Date", "w9sme" ) ?></th>
							</thead>
						</table>
					</div>
					<script>
						(function ($) {
							$(document).on('ready', function () {
								$('.vacancy-table').DataTable({
									"processing" : true,
									"serverSide" : true,
									"responsive" : true,
									"autoWidth"  : false,
									"language"   : {
										"processing": '<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> ' + "<?php _e( "Loading Data...", "w9sme" ) ?>",
										"lengthMenu": "<?php _e( "Rows: _MENU_", "w9sme" ); ?>",
										"paginate"  : {
											"first"   : '<i class="fa fa-step-backward" aria-hidden="true"></i>',
											"last"    : '<i class="fa fa-step-forward" aria-hidden="true"></i>',
											"next"    : '<i class="fa fa-caret-right" aria-hidden="true"></i>',
											"previous": '<i class="fa fa-caret-left" aria-hidden="true"></i>'
										},
										"info"      : "<?php _e( "Results _START_ - _END_ of _TOTAL_", "w9sme" ); ?>",
										"infoEmpty" : "<?php _e( "Results 0 - 0 of 0", "w9sme" ); ?>",
										"search"    : "<?php _e( "Search:", "w9sme" ); ?>"
									},
									"ajax"       : {
										"url" : w9sme_main_vars.ajax_url,
										"type": "GET",
										"data": {
											"action"          : 'w9sme_load_vacancy_data',
											"target_term_slug": '<?php echo $target_term_slug; ?>'
										}
									},
									"columns"    : [
										{
											"data"              : "no",
											"orderable"         : false,
											"searchable"        : false,
											"responsivePriority": 1
										},
										{
											"data"              : "title",
											"orderable"         : true,
											"searchable"        : true,
											"responsivePriority": 2
										},
										{
											"data"              : "location",
											"orderable"         : true,
											"searchable"        : false,
											"responsivePriority": 3
										},
										{
											"data"              : "cat",
											"orderable"         : false,
											"searchable"        : false,
											"responsivePriority": 4
										},
										{
											"data"              : "date",
											"orderable"         : true,
											"searchable"        : false,
											"responsivePriority": 6
										}
									],
									"order"      : [[1, 'desc']],
									"orderMulti" : false,
									"searchDelay": 500,
									"pageLength" : '<?php echo $item_per_page; ?>',
									"dom"        : '<?php echo $dom_position; ?>',
									"lengthMenu" : <?php echo '[' . join( ', ', $length_menu_option ) . ']' ?>
								});
							});
						})(jQuery);
					</script>
				</div>
				
				
				<?php if ( $display_right ): ?>
					<div class="sidebar sidebar-right hidden-sm hidden-xs <?php w9sme_the_clean_html_classes( $right_col ); ?>">
						<?php dynamic_sidebar( $vacancy_archive_sidebar_right ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div><!-- #primary -->
	</main>
<?php

/**
 * Include footer
 */
get_footer();
