<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: single-vacancy.php
 * @time    : 8/26/16 12:39 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

//
// Vacancy single layout
//
$vacancy_single_layout = isset( $_GET['layout'] ) ? $_GET['layout'] : '';
if ( ! in_array( $vacancy_single_layout, array( 'fullwidth', 'container', 'container-xlg', 'container-fluid' ) ) ) {
	$vacancy_single_layout = w9sme_get_meta_option( 'single-layout', get_the_ID(), '', 'container' );
	if ( w9sme_is_meta_default_value( $vacancy_single_layout ) ) {
		$vacancy_single_layout = w9sme_get_option( 'vacancy-single-layout' );
	}
}

//
// Widget title style
//

$widget_title_style = w9sme_get_meta_option( 'single-widget-title-style' );
if ( w9sme_is_meta_default_value( $widget_title_style ) ) {
	$template_general_widget_title_style = w9sme_get_option( 'vacancy-single-widget-title-style' );
	if ( w9sme_is_meta_default_value( $template_general_widget_title_style ) ) {
		$widget_title_style = 'widget-title_default';
	} else {
		$widget_title_style = 'widget-title_' . $template_general_widget_title_style;
	}
} else {
	$widget_title_style = 'widget-title_' . $widget_title_style;
}


//
// Vacancy single sidebar
//
$vacancy_single_sidebar = isset( $_GET['sidebar'] ) ? $_GET['sidebar'] : '';
if ( ! in_array( $vacancy_single_sidebar, array( 'left', 'right', 'both', 'none' ) ) ) {
	$vacancy_single_sidebar = w9sme_get_meta_option( 'single-sidebar', get_the_ID() );
	if ( w9sme_is_meta_default_value( $vacancy_single_sidebar ) ) {
		$vacancy_single_sidebar = w9sme_get_option( 'vacancy-single-sidebar' );
	}
}
$vacancy_single_sidebar_width = isset( $_GET['sidebar-width'] ) ? $_GET['sidebar-width'] : '';
if ( ! in_array( $vacancy_single_sidebar_width, array( 'small', 'large' ) ) ) {
	$vacancy_single_sidebar_width = w9sme_get_meta_option( 'single-sidebar-width', get_the_ID() );
	if ( w9sme_is_meta_default_value( $vacancy_single_sidebar_width ) ) {
		$vacancy_single_sidebar_width = w9sme_get_option( 'vacancy-single-sidebar-width' );
	}
}

$vacancy_single_sidebar_left = w9sme_get_meta_option( 'single-sidebar-left', get_the_ID() );
if ( w9sme_is_meta_default_value( $vacancy_single_sidebar_left ) ) {
	$vacancy_single_sidebar_left = w9sme_get_option( 'vacancy-single-sidebar-left' );
}

$vacancy_single_sidebar_right = w9sme_get_meta_option( 'single-sidebar-right', get_the_ID() );
if ( w9sme_is_meta_default_value( $vacancy_single_sidebar_right ) ) {
	$vacancy_single_sidebar_right = w9sme_get_option( 'vacancy-single-sidebar-right' );
}

//
// Which sidebar to display
//
$display_left  = ( $vacancy_single_sidebar == 'left' || $vacancy_single_sidebar == 'both' ) && is_active_sidebar( $vacancy_single_sidebar_left );
$display_right = ( $vacancy_single_sidebar == 'right' || $vacancy_single_sidebar == 'both' ) && is_active_sidebar( $vacancy_single_sidebar_right );

//
// Calculate sidebar columns
//
$left_col   = 0;
$right_col  = 0;
$center_col = 12;
if ( $display_left ) {
	if ( $vacancy_single_sidebar_width == 'small' ) {
		$left_col = 3;
	} else {
		$left_col = 4;
	}
}
if ( $display_right ) {
	if ( $vacancy_single_sidebar_width == 'small' ) {
		$right_col = 3;
	} else {
		$right_col = 4;
	}
}
$center_col -= ( $left_col + $right_col );

$left_col   = 'col-md-' . $left_col;
$right_col  = 'col-md-' . $right_col;
$center_col = 'col-md-' . $center_col;

/*-------------------------------------
	CONTENT LAYOUT
---------------------------------------*/
$content_layout = w9sme_get_meta_or_option( 'vacancy-single-content-layout' );


/**
 * Include header
 */
get_header();

/**
 * Include title
 */
w9sme_get_template_part( 'page-title' );
?>
	<main id="site-main-single" class="site-main-single vacancy-single">
		<div id="primary" class="content-area <?php w9sme_the_clean_html_classes( array(
			$vacancy_single_layout,
			$widget_title_style
		) ); ?>">
			<div class="row clearfix">
				<?php
				if ( $display_left ): ?>
					<div class="sidebar sidebar-left hidden-sm hidden-xs <?php w9sme_the_clean_html_classes( $left_col ); ?>">
						<?php dynamic_sidebar( $vacancy_single_sidebar_left ); ?>
					</div>
				<?php endif; ?>

				<div class="single-content-wrapper <?php w9sme_the_clean_html_classes( $center_col ); ?>">
					<div class="single-content-inner">
						<?php
						while ( have_posts() ) : the_post(); ?>
							<div class="entry-content-wrapper">
								<div class="entry-content">
									<?php
									if ( ! empty( $content_layout ) ):
										echo w9sme_get_post_content_by_name( $content_layout, 'content-template' );
									else:
										the_content();
									endif;
									?>
								</div>
							</div>
							
							<?php
							$comment_enabled = w9sme_get_option( 'vacancy-single-comment-enabled' );
							if ( $comment_enabled ) {
								comments_template();
							}
							?>
						<?php endwhile; ?>
					</div>
				</div>
				
				<?php if ( $display_right ): ?>
					<div class="sidebar sidebar-right hidden-sm hidden-xs <?php w9sme_the_clean_html_classes( $right_col ); ?>">
						<?php dynamic_sidebar( $vacancy_single_sidebar_right ); ?>
					</div>
				<?php endif; ?>
			</div>
		</div><!-- #primary -->
	</main><!-- #main -->

<?php

/**
 * Include footer
 */
get_footer();