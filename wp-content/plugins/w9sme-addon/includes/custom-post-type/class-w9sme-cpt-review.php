<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: class-w9sme-cpt-review.php
 * @time    : 8/26/16 12:38 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class W9sme_CPT_Review extends W9sme_CPT_BASE {
	const CPT_SLUG = 'review';
	const CPT_DEFAULT_NAME = 'Review';
	
	const TAX_SLUG = 'review-category';
	const TAX_DEFAULT_NAME = 'Review Category';
	
	function __construct() {
		/*
		 * LOAD OPTIONS
		 */
		$review_enable = w9sme_get_option( 'review-enable' );
		if ( empty( $review_enable ) ) {
			return;
		}
		/*
		 * CONFIG
		 */
		$postype_args  = array(
			'slug' => self::CPT_SLUG,
			'name' => self::CPT_DEFAULT_NAME,
			'args' => array(
				'has_archive' => true,
				'public'      => false,
				'supports'    => 'no-support'
			)
		);
		$taxonomy_args = array(
//            'slug' => self::TAX_SLUG,
//            'name' => self::TAX_DEFAULT_NAME
		);
		/*
		 * SETUP
		 */
		parent::__construct( $postype_args, $taxonomy_args );
		
		add_action( 'wp_ajax_submit_user_feed_back', array( $this, 'submit_user_feed_back' ) );
		add_action( 'wp_ajax_nopriv_submit_user_feed_back', array( $this, 'submit_user_feed_back' ) );
		add_action( 'admin_notices', array( $this, 'print_review_statistic' ), 9999 );
		add_action( 'wp_ajax_print_review_statistic', array( $this, 'print_review_statistic' ) );
	}
	
	function calc_avg_rating( $rating ) {
		$avg_rating = 0;
		$counting   = 0;
		if ( is_array( $rating ) ) {
			foreach ( $rating as $item ) {
				if ( is_numeric( $item ) && $item > 0 ) {
					$avg_rating += $item;
					$counting ++;
				}
			}
		}
		
		if ( $counting > 0 ) {
			return round( $avg_rating / $counting, 2 );
		} else {
			return 0;
		}
	}
	
	function do_show_hide_review_type() {
		$hide_items = get_option( 'w9sme-hidden-review-types' );
		
		$show_item = isset( $_GET['show-item'] ) ? $_GET['show-item'] : '';
		$do_save   = false;
		if ( ! empty( $show_item ) ) {
			foreach ( $hide_items as $id => $item ) {
				if ( $show_item == $item ) {
					unset( $hide_items[ $id ] );
					$do_save = true;
				}
			}
		}
		
		$hide_item = isset( $_GET['hide-item'] ) ? $_GET['hide-item'] : '';
		if ( ! empty( $hide_item ) ) {
			if ( ! in_array( $hide_item, $hide_items ) ) {
				$hide_items[] = $hide_item;
				$do_save      = true;
			}
		}
		
		if ( $do_save ) {
			update_option( 'w9sme-hidden-review-types', $hide_items );
		}
		
		return $hide_items;
	}
	
	function print_review_statistic() {
		if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) {
			if ( w9sme_get_current_post_type() !== W9sme_CPT_Review::CPT_SLUG ) {
				return;
			}
			
			$screen = get_current_screen();
			if ( $screen->id !== 'edit-review' ) {
				return;
			}
		}
		
		
		$hide_items = $this->do_show_hide_review_type();
		
		/*-------------------------------------
			CALC RATING
		---------------------------------------*/
		$rating     = $this->get_meta_values( 'meta-review-rating', W9sme_CPT_Review::CPT_SLUG, 'publish', false );
		$avg_rating = $this->calc_avg_rating( $rating );
		
		
		/*-------------------------------------
			FETCHING TYPES
		---------------------------------------*/
		$review_type = $this->get_meta_values( 'meta-review-type', W9sme_CPT_Review::CPT_SLUG );
		
		/*-------------------------------------
			OTHERS
		---------------------------------------*/
		$total_reviews = wp_count_posts( W9sme_CPT_Review::CPT_SLUG )->publish;
		
		ob_start();
		if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) {
			echo '<div style="border-left: 4px solid rgba(0,162,227,1)!important;" class="updated redux-notice notice redux-notice review-statistic wp-clearfix">';
		}
		
		?>

		<p><strong><?php _e( "Reviews Statistic" ) ?></strong></p>
		<hr>
		<p>
			<strong class="state-label" style="display:inline-block; width: 200px"><?php _e( 'Total reviews', 'w9sme-addon' ) ?>:</strong>
			<strong><?php echo $total_reviews; ?></strong>
		</p>
		<p>
			<strong class="state-label" style="display:inline-block; width: 200px"><?php _e( 'Reviewed category', 'w9sme-addon' ) ?>:</strong>
			<strong><?php echo count( $review_type ); ?></strong>
		</p>
		<p>
			<strong class="state-label" style="display:inline-block; width: 200px"><?php _e( 'Average Rating', 'w9sme-addon' ) ?>:</strong>
			<strong style="color: red"><?php echo $avg_rating; ?></strong>
		</p>
		<hr>
		<div class="left-col statistic-wrapper">
			<?php foreach ( $review_type as $type ) :
				if ( in_array( $type, $hide_items ) ) {
					continue;
				}
				/*-------------------------------------
					REVIEW BY TYPE
				---------------------------------------*/
				$args  = array(
					'post_type'           => W9sme_CPT_Review::CPT_SLUG,
					'post_status'         => 'publish',
					'ignore_sticky_posts' => 1,
					'orderby'             => 'date',
					'order'               => 'ASC',
					'posts_per_page'      => - 1,
					'meta_query'          => array(
						array(
							'key'     => 'meta-review-type',
							'value'   => $type,
							'compare' => '=',
						)
					)
				);
				$query = new WP_Query( $args );
				
				$post_count = $query->post_count;
				$percent    = $post_count * 100 / $total_reviews;
				
				/*-------------------------------------
					RATING BY TYPE
				---------------------------------------*/
				$type_rating         = $this->get_meta_values_by_review_type( 'meta-review-rating', $type, W9sme_CPT_Review::CPT_SLUG );
				$avg_type_rating     = $this->calc_avg_rating( $type_rating );
				$type_rating_percent = $avg_type_rating * 100 / 5;
				
				?>
				<div class="review-type-wrapper">
					<div class="type-name">
						<a href="javascript:void(0);" data-hide-item="<?php echo $type; ?>" class="do-hide-item">
							<span>x</span>
							<?php echo $type; ?>
						</a>
					</div>
					<div class="statistic">
					<span class="state total-review" style="width: <?php echo $percent . '%'; ?>">
						<?php echo $query->post_count . ' ' . __( 'review(s)', 'w9sme-addon' ); ?>
					</span>
						<span class="state rating" style="width: <?php echo $type_rating_percent . '%'; ?>">
						<?php echo $avg_type_rating . ' ' . __( 'point(s)', 'w9sme-addon' ) ?>
					</span>
					</div>
				</div>
				<?php
				wp_reset_postdata();
			endforeach; ?>
		</div>
		<?php if ( count( $hide_items ) > 0 ): ?>
			<div class="right-col" style="float: right">
				<h3><?php _e( 'Hidden Reviews', 'w9sme-addon' ) ?></h3>
				<ul style="list-style: disc">
					<?php foreach ( $hide_items as $item ):
						if ( empty( $item ) ) {
							continue;
						}
						?>
						<li>
							<a href="javascript:void(0);"
							   data-show-item="<?php echo $item; ?>"
							   class="do-show-item"
							   title="Show this item">
								<?php echo $item; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php endif; ?>
		
		<?php
		if ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) {
			echo '</div>';
		}
		?>

		<script>
			(function ($) {
				$(document).ready(function () {
					var $review_statistic = $('.review-statistic');

					$('.do-hide-item').on('click', function () {
						var $this = $(this);

						$.ajax({
							type   : 'get',
							url    : ajaxurl,
							data   : {
								'action'   : 'print_review_statistic',
								'hide-item': $this.attr('data-hide-item')
							},
							success: function (response) {
								$review_statistic.empty();
								$review_statistic.append(response);
							},
							error  : function () {
								console.error('Could not load data');
							}
						});
					});


					$('.do-show-item').on('click', function () {
						var $this = $(this);

						$.ajax({
							type   : 'get',
							url    : ajaxurl,
							data   : {
								'action'   : 'print_review_statistic',
								'show-item': $this.attr('data-show-item')
							},
							success: function (response) {
								$review_statistic.empty();
								$review_statistic.append(response);
							},
							error  : function () {
								console.error('Could not load data');
							}
						});
					});
				});
			})(jQuery);
		</script>
		<?php
		
		echo ob_get_clean();
		
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
			die();
		}
	}
	
	function get_meta_values( $key = '', $type = 'post', $status = 'publish', $is_distinct = true ) {
		
		global $wpdb;
		
		if ( empty( $key ) ) {
			return;
		}
		
		$distinct = '';
		if ( $is_distinct ) {
			$distinct = 'DISTINCT';
		}
		
		
		$r = $wpdb->get_col( $wpdb->prepare( "
        SELECT {$distinct} pm.meta_value FROM {$wpdb->postmeta} pm
        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
        WHERE pm.meta_key = '%s'
        AND p.post_status = '%s'
        AND p.post_type = '%s'
    ", $key, $status, $type ) );
		
		return $r;
	}
	
	function get_meta_values_by_review_type( $key = '', $review_type, $type = 'post', $status = 'publish' ) {
		
		global $wpdb;
		
		if ( empty( $key ) ) {
			return;
		}
		
		$r = $wpdb->get_col( $wpdb->prepare( "
        SELECT pm.meta_value FROM {$wpdb->postmeta} pm
        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
        WHERE pm.meta_key = '%s'
        AND p.post_status = '%s'
        AND p.post_type = '%s'
        AND p.ID IN (SELECT p.ID FROM {$wpdb->posts} p
        LEFT JOIN {$wpdb->postmeta} pm ON p.ID = pm.post_id
        WHERE pm.meta_key = '%s' AND pm.meta_value = '%s' AND p.post_status = '%s' AND p.post_type = '%s'
        )
    ", $key, $status, $type, 'meta-review-type', $review_type, $status, $type ) );
		
		return $r;
	}
	
	function submit_user_feed_back() {
		$type   = $_POST['feedback-type'];
		$status = 'DANGER';
		
		if ( ! empty( $type ) ) {
			$rating = $_POST['rating'];
			
			if ( empty( $rating ) ) {
				$rating = 0;
			}
			
			$content   = $_POST['message'];
			$name      = $_POST['name'];
			$email     = $_POST['email'];
			$job_title = $_POST['job-title'];
			
			$postarr = array(
				'post_type'    => self::CPT_SLUG,
				'post_status'  => 'publish',
				'post_title'   => 'Review For: ' . $type . ' By ' . $name,
				'post_content' => '',
			);
			
			$post_id = wp_insert_post( $postarr );
			
			update_post_meta( $post_id, 'meta-review-type', $type );
			update_post_meta( $post_id, 'meta-review-content', $content );
			update_post_meta( $post_id, 'meta-review-rating', $rating );
			update_post_meta( $post_id, 'meta-review-reviewer-name', $name );
			update_post_meta( $post_id, 'meta-review-reviewer-email', $email );
			update_post_meta( $post_id, 'meta-review-reviewer-job', $job_title );
			$status = 'SUCCESS';
			$msg    = __( 'Thank you very much, you feedback has been submitted successfully.', 'w9sme-addon' );
		} else {
			$msg = __( 'Please specify feedback category and try again.', 'w9sme-addon' );
		}
		
		echo json_encode( array(
			'status'  => $status,
			'message' => $msg
		) );
		die();
	}
	
	function manage_admin_columns( $columns ) {
		unset( $columns['title'] );
		unset( $columns['cb'] );
		
		$columns = array_merge( array(
			'cb'               => '',
			'id'               => esc_html__( 'ID', 'w9sme-addon' ),
			'review-type'      => esc_html__( 'Review Type', 'w9sme-addon' ),
			'reviewer-name'    => esc_html__( 'Reviewer name', 'w9sme-addon' ),
			'reviewer-job'     => esc_html__( 'Reviewer job title', 'w9sme-addon' ),
			'reviewer-email'   => esc_html__( 'Reviewer email', 'w9sme-addon' ),
			'reviewer-content' => esc_html__( 'Review content', 'w9sme-addon' ),
			'reviewer-rating'  => esc_html__( 'Rating', 'w9sme-addon' )
		), $columns );
		
		return $columns;
	}
	
	function manage_admin_columns_values( $column, $post_id ) {
		switch ( $column ) {
			case 'id':
				edit_post_link( sprintf( esc_html__( 'Review ID: %s', 'w9sme-addon' ), $post_id ), '', '', $post_id, 'row-title' );
				
				break;
			case 'reviewer-name':
				$name = get_post_meta( $post_id, 'meta-review-reviewer-name', true );
				if ( ! empty( $name ) ) {
					echo $name;
				}
				
				break;
			case 'review-type':
				$name = get_post_meta( $post_id, 'meta-review-type', true );
				
				echo '<strong>' . $name . '</strong>';
				break;
			case 'reviewer-email':
				$name = get_post_meta( $post_id, 'meta-review-reviewer-email', true );
				
				echo $name;
				break;
			case 'reviewer-job':
				$job = get_post_meta( $post_id, 'meta-review-reviewer-job', true );
				if ( ! empty( $job ) ) {
					echo $job;
				}
				
				break;
			case 'reviewer-content':
				$content = get_post_meta( $post_id, 'meta-review-content', true );
				echo $content;
				
				break;
			case 'reviewer-rating':
				$avatar_rating = get_post_meta( $post_id, 'meta-review-rating', true );
				if ( ! empty( $avatar_rating ) ) {
					if ( $avatar_rating == '' ) {
						echo esc_html__( 'No rating', 'w9sme-addon' );
					} else {
						echo sprintf( esc_html__( '%s Point(s)', 'w9sme-addon' ), $avatar_rating );
					}
				}
				
				break;
		}
	}
	
}

new W9sme_CPT_Review();

