<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: class-w9sme-cpt-service.php
 * @time    : 12/13/2016 6:20 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class W9sme_CPT_Service extends W9sme_CPT_BASE {
	const CPT_SLUG = 'service';
	const CPT_DEFAULT_NAME = 'Service';
	
	const TAX_SLUG = 'service-category';
	const TAX_DEFAULT_NAME = 'Service Category';
	
	function __construct() {
		/*
		 * LOAD OPTIONS
		 */
		$service_enable = w9sme_get_option( 'cpt-service-enable' );
		if ( ! $service_enable ) {
			return;
		}
		
		$service_slug     = W9sme_CPT_BASE::uglify( w9sme_get_option( 'cpt-service-slug' ) );
		$service_tax_slug = W9sme_CPT_BASE::uglify( w9sme_get_option( 'cpt-service-tax-slug' ) );
		
		$service_name     = W9sme_CPT_BASE::beautify( w9sme_get_option( 'cpt-service-name' ) );
		$service_tax_name = W9sme_CPT_BASE::beautify( w9sme_get_option( 'cpt-service-tax-name' ) );
		
		/*
		 * CONFIG
		 */
		$postype_args = array(
			'slug' => self::CPT_SLUG,
			'name' => ( ! empty( $service_name ) ) ? $service_name : self::CPT_DEFAULT_NAME,
			'args' => array(
				'has_archive' => true,
				'supports'    => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author' ),
				'menu_icon'   => 'dashicons-share-alt'
			)
		);
		
		if ( ! empty( $service_slug ) ) {
			$postype_args['args']['rewrite'] = array( 'slug' => $service_slug, 'with_front' => true );
		}
		
		// tax
		
		$taxonomy_args = array(
			'slug' => self::TAX_SLUG,
			'name' => ( ! empty( $service_tax_name ) ) ? $service_tax_name : self::TAX_DEFAULT_NAME,
		);
		
		if ( ! empty( $service_tax_slug ) ) {
			$taxonomy_args['args']['rewrite'] = array( 'slug' => $service_tax_slug );
		}
		/*
		 * SETUP
		 */
		parent::__construct( $postype_args, $taxonomy_args );
		add_action( 'pre_get_posts', array( $this, 'set_items_per_page' ), 5 );
	}
	
	/**
	 * Register single template
	 *
	 * @param $single_template
	 *
	 * @return string
	 */
	function register_single_template( $single_template ) {
		$post_type = get_post_type();
		if ( $post_type == self::CPT_SLUG ) {
			return W9sme_Addon::plugin_dir( __FILE__ ) . 'service/templates/single-service.php';
		}
		
		return $single_template;
	}
	
	/**
	 * Register archive template
	 *
	 * @param $archive_template
	 *
	 * @return string
	 */
	function register_archive_template( $archive_template ) {
		if ( is_archive() && self::CPT_SLUG == get_post_type() ) {
			$service_override_default_archive_id = w9sme_get_option( 'service-archive-template' );
			if ( ! empty( $service_override_default_archive_id ) ) {
				wp_redirect( get_permalink( $service_override_default_archive_id ) );
			} else {
				$template = W9sme_Addon::plugin_dir( __FILE__ ) . 'service/templates/archive-service.php';
				if ( file_exists( $template ) ) {
					return $template;
				}
			}
		}
		
		return $archive_template;
	}
	
	/**
	 * Add admin columns
	 *
	 * @param $columns
	 *
	 * @return array
	 */
	function manage_admin_columns( $columns ) {
		$new = array();
		
		foreach ( $columns as $key => $value ) {
			$new[ $key ] = $value;
			if ( $key == 'title' ) {
				$new['thumbnail'] = __( 'Service Thumbnail', 'w9sme-addon' );
				$new['cats']      = __( 'Service Categories', 'w9sme-addon' );
			}
		}
		
		return $new;
	}
	
	/**
	 * Manage admin columns values
	 *
	 * @param $column
	 * @param $post_id
	 */
	function manage_admin_columns_values( $column, $post_id ) {
		if ( $column == 'thumbnail' ) {
			$post_thumnail_id = get_post_thumbnail_id( $post_id );
			if ( $post_thumnail_id ) {
				echo W9sme_Image::get_image( $post_thumnail_id );
			} else {
				echo __( 'No Image', 'w9sme-addon' );
			}
		}
		
		if ( $column == 'cats' ) {
			$terms = wp_get_post_terms( get_the_ID(), array( self::TAX_SLUG ) );
			$cat   = '<ul style="margin-top: 0">';
			foreach ( $terms as $term ) {
				$cat .= '<li><a href="' . get_term_link( $term, self::TAX_SLUG ) . '">' . $term->name . '<a/></li>';
			}
			$cat .= '</ul>';
			echo wp_kses_post( $cat );
		}
	}
	
	/**
	 * set_items_per_page
	 *
	 * @param $query WP_Query
	 */
	function set_items_per_page( $query ) {
		if ( ( is_tax( self::TAX_SLUG ) || is_post_type_archive( self::CPT_SLUG ) ) && $query->is_main_query() && ! is_admin() ) {
			
			$service_items_per_page = w9sme_get_option( 'service-archive-items-per-page' );
			if ( empty( $service_items_per_page ) ) {
				$service_items_per_page = 8;
			}
			// show 50 posts on custom taxonomy pages
			$query->set( 'posts_per_page', $service_items_per_page );
		}
	}
	
	function excerpt_length( $length ) {
		$excerpt_length = w9sme_get_option( 'service-archive-excerpt-length' );
		$excerpt_length = intval( $excerpt_length );
		if ( empty( $excerpt_length ) ) {
			$excerpt_length = 50;
		}
		
		global $post;
		if ( $post->post_type == self::CPT_SLUG ) {
			return $excerpt_length;
		}
		
		return $length;
	}
	
	static function append_service_name( $booking_page_url, $service_name = false ) {
		$auto_select_service = w9sme_get_option( 'service-booking-auto-select-service' );
		if ( ! empty( $auto_select_service ) && $booking_page_url !== '#' ) {
			if ( ! $service_name ) {
				$service_name = get_the_title();
			}
			
			$booking_page_url .= '?sv-name=' . urlencode( $service_name );
		}
		
		return $booking_page_url;
	}
}

new W9sme_CPT_Service();
