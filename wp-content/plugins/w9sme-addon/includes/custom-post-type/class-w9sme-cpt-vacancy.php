<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: class-w9sme-cpt-vacancy.php
 * @time    : 8/26/16 12:39 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class W9sme_CPT_Vacancy extends W9sme_CPT_BASE {
	const CPT_SLUG = 'vacancy';
	const CPT_DEFAULT_NAME = 'Vacancy';
	
	const TAX_SLUG = 'vacancy-category';
	const TAX_DEFAULT_NAME = 'Vacancy Category';
	
	function __construct() {
		/*
		 * GET OPTIONS FROM THEME OPTIONS
		 */
		$vacancy_enable = w9sme_get_option( 'cpt-vacancy-enable' );
		if ( ! $vacancy_enable ) {
			return;
		}
		
		$vacancy_slug     = W9sme_CPT_BASE::uglify( w9sme_get_option( 'cpt-vacancy-slug' ) );
		$vacancy_tax_slug = W9sme_CPT_BASE::uglify( w9sme_get_option( 'cpt-vacancy-tax-slug' ) );
		
		$vacancy_name     = W9sme_CPT_BASE::beautify( w9sme_get_option( 'cpt-vacancy-name' ) );
		$vacancy_tax_name = W9sme_CPT_BASE::beautify( w9sme_get_option( 'cpt-vacancy-tax-name' ) );
		
		
		/*
		 * CONFIG
		 */
		$postype_args = array(
			'slug' => self::CPT_SLUG,
			'name' => ( ! empty( $vacancy_name ) ) ? $vacancy_name : self::CPT_DEFAULT_NAME,
			'args' => array(
				'has_archive' => true,
//                'taxonomies'  => array( 'post_tag', 'category' ),
				'supports'    => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author', 'comments' ),
				'menu_icon'   => 'dashicons-awards',
			)
		);
		// custom slug
		if ( ! empty( $vacancy_slug ) ) {
			$postype_args['args']['rewrite'] = array( 'slug' => $vacancy_slug, 'with_front' => true );
		}
		
		$taxonomy_args = array(
			'slug' => self::TAX_SLUG,
			'name' => ( ! empty( $vacancy_tax_name ) ) ? $vacancy_tax_name : self::TAX_DEFAULT_NAME,
		);
		
		// custom slug
		if ( ! empty( $vacancy_tax_slug ) ) {
			$taxonomy_args['args']['rewrite'] = array( 'slug' => $vacancy_tax_slug );
		}
		/*
		 * SETUP
		 */
		parent::__construct( $postype_args, $taxonomy_args );
		add_action( 'pre_get_posts', array( $this, 'set_items_per_page' ), 5 );
		add_action( 'wp_ajax_nopriv_w9sme_load_vacancy_data', array( $this, 'load_vacancy_data' ) );
		add_action( 'wp_ajax_w9sme_load_vacancy_data', array( $this, 'load_vacancy_data' ) );
	}
	
	/**
	 * Register single template
	 *
	 * @param $single_template
	 *
	 * @return string
	 */
	function register_single_template( $single_template ) {
		$post_type = get_post_type();
		if ( $post_type == self::CPT_SLUG ) {
			return W9sme_Addon::plugin_dir( __FILE__ ) . 'vacancy/templates/single-vacancy.php';
		}
		
		return $single_template;
	}
	
	/**
	 * Register archive template
	 *
	 * @param $archive_template
	 *
	 * @return string
	 */
	function register_archive_template( $archive_template ) {
		if ( is_archive() && self::CPT_SLUG == get_post_type() ) {
			$vacancy_override_default_archive_id = w9sme_get_option( 'vacancy-archive-template' );
			if ( ! empty( $vacancy_override_default_archive_id ) ) {
				wp_redirect( get_permalink( $vacancy_override_default_archive_id ) );
			} else {
				$template = W9sme_Addon::plugin_dir( __FILE__ ) . 'vacancy/templates/archive-vacancy.php';
				if ( file_exists( $template ) ) {
					return $template;
				}
			}
		}
		
		return $archive_template;
	}
	
	/**
	 * Add admin columns
	 *
	 * @param $columns
	 *
	 * @return array
	 */
	function manage_admin_columns( $columns ) {
		$new = array();
		
		foreach ( $columns as $key => $value ) {
			$new[ $key ] = $value;
			if ( $key == 'title' ) {
				$new['thumbnail'] = __( 'Vacancy Thumbnail', 'w9sme-addon' );
				$new['cats']      = __( 'Vacancy Categories', 'w9sme-addon' );
			}
		}
		
		return $new;
	}
	
	/**
	 * Manage admin columns values
	 *
	 * @param $column
	 * @param $post_id
	 */
	function manage_admin_columns_values( $column, $post_id ) {
		if ( $column == 'thumbnail' ) {
			$post_thumnail_id = get_post_thumbnail_id( $post_id );
			if ( $post_thumnail_id ) {
				echo W9sme_Image::get_image( $post_thumnail_id );
			} else {
				echo __( 'No Image', 'w9sme-addon' );
			}
		}
		
		if ( $column == 'cats' ) {
			$terms = wp_get_post_terms( get_the_ID(), array( self::TAX_SLUG ) );
			$cat   = '<ul style="margin-top: 0">';
			foreach ( $terms as $term ) {
				$cat .= '<li><a href="' . get_term_link( $term, self::TAX_SLUG ) . '">' . $term->name . '</a></li>';
			}
			$cat .= '</ul>';
			echo wp_kses_post( $cat );
		}
	}
	
	/**
	 * set_items_per_page
	 *
	 * @param $query WP_Query
	 */
	function set_items_per_page( $query ) {
		if ( is_post_type_archive( self::CPT_SLUG ) && $query->is_main_query() && ! is_admin() ) {
			
			$vacancy_items_per_page = w9sme_get_option( 'vacancy-archive-items-per-page' );
			
			if ( empty( $vacancy_items_per_page ) ) {
				$vacancy_items_per_page = 8;
			}
			// show 50 posts on custom taxonomy pages
			$query->set( 'posts_per_page', $vacancy_items_per_page );
		}
	}
	
	function parsing_ordering() {
		// ordering
		// columns
		$columns = array();
		if ( ! empty( $_GET["columns"] ) ) {
			$columns = $_GET["columns"];
		}
		
		$order_dir = "desc";
		
		$order_col_num = "0";
		if ( ! empty( $_GET["order"][0] ) ) {
			$order_col_num = $_GET["order"][0]["column"];
			$order_dir     = $_GET["order"][0]["dir"];
		}
		
		$order_col = $columns[ $order_col_num ];
		
		$order_by = array();
		if ( $order_col["data"] == "title" ) {
			$order_by = array(
				"orderby" => $order_col["data"]
			);
		} else if ( $order_col["data"] == "location" ) {
			$order_by = array(
				"orderby"  => "meta_value",
				"meta_key" => "meta-vacancy-location"
			);
		} else if ( $order_col["data"] == "date" ) {
			$order_by = array(
				"meta_type" => "DATE",
				"orderby"   => "meta_value_date",
				"meta_key"  => "meta-vacancy-date"
			);
		}
		
		$order_by['order'] = $order_dir;
		
		return $order_by;
	}
	
	function load_vacancy_data() {
		$data = array();
		
		$args = array(
			"post_type" => self::CPT_SLUG,
		);
		
		$args = array_merge(
			$args,
			$this->parsing_ordering()
		);
		
		
		add_filter( 'posts_join', array( $this, 'alter_posts_join_clause' ) );
		add_filter( 'posts_where', array( $this, 'alter_posts_where_clause' ) );
		add_filter( 'post_limits', array( $this, 'limiting_the_posts' ) );
		add_filter( 'posts_distinct', array( $this, 'posts_distinct' ) );
		
		if ( ! empty( $_GET["target_term_slug"] ) ) {
			$target_term_slug = explode("||", $_GET["target_term_slug"]);
			
			$args = array_merge(
				$args,
				array(
					'tax_query' => array(
						array(
							'taxonomy' => self::TAX_SLUG,
							'field'    => 'slug',
							'terms'    => $target_term_slug
						)
					),
				)
			);
		}
		
		
		$the_query = new WP_Query( $args );
		
		if ( $the_query->have_posts() ):
			$i = $_GET["start"] ? $_GET["start"] : 0;;
			while ( $the_query->have_posts() ) : $the_query->the_post();
				$terms = wp_get_post_terms( get_the_ID(), array( self::TAX_SLUG ) );
				$cat   = array();
				foreach ( $terms as $term ) {
					$cat[] = '<a href="' . get_term_link( $term, self::TAX_SLUG ) . '">' . $term->name . '</a>';
				}
				
				
				$date = false;
				try {
					$date = DateTime::createFromFormat( W9SME_DEFAULT_DATE_FORMAT, get_post_meta( get_the_ID(), "meta-vacancy-date", true ) )->format( get_option( "date_format" ) );
				} catch ( Exception $e ) {
					error_log( $e->getMessage() );
				}
				
				if ( ! $date ) {
					$date = '';
				}
				
				$data[] = array(
					"no"       => ++$i,
					"title"    => "<a href='" . get_permalink() . "'>" . get_the_title() . "</a>",
					"location" => get_post_meta( get_the_ID(), "meta-vacancy-location", true ),
					"cat"      => join( ", ", $cat ),
					"date"     => $date
				);
			endwhile;
		endif;
		
		$recordsFiltered = $the_query->found_posts;
		$recordsTotal    = wp_count_posts( self::CPT_SLUG )->publish;
		
		wp_reset_postdata();
		
		echo json_encode( [
			"data"            => $data,
			"args"            => $args,
			"get"             => $_GET,
			"recordsFiltered" => $recordsFiltered,
			"recordsTotal"    => $recordsTotal
		] );
		die();
	}
	
	function alter_posts_where_clause( $where ) {
		global $wpdb;
		
		$search_query = "";
		if ( ! empty( $_GET["search"]["value"] ) ) {
			$search_query = $_GET["search"]["value"];
		}
		
		$term = filter_var( trim( $search_query ), FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE );
		if ( empty( $term ) ) {
			return $where;
		}
		
		$term = $wpdb->esc_like( $term );
		
		// Append to the WHERE clause:
		$where .= $wpdb->prepare( " AND ({$wpdb->posts}.post_title LIKE '%s' OR (meta.meta_key = '%s' AND meta.meta_value LIKE '%s') )", "%{$term}%", "meta-vacancy-location", "%{$term}%" );
		
		return $where;
	}
	
	function alter_posts_join_clause( $join ) {
		global $wpdb;
		$join .= " LEFT JOIN {$wpdb->postmeta} as meta ON meta.post_id = {$wpdb->posts}.ID";
		
		return $join;
	}
	
	function posts_distinct( $distinct ) {
		return "DISTINCT";
	}
	
	function limiting_the_posts( $limit ) {
		// start & length
		$start  = $_GET["start"] ? $_GET["start"] : 0;
		$length = $_GET["length"] ? $_GET["length"] : 10;
		
		return " LIMIT " . $start . ", " . $length;
	}
	
	static function append_vacancy_title( $apply_page_url, $vacancy_title = false ) {
		$auto_select_vacancy = w9sme_get_option( 'vacancy-auto-select' );
		if ( ! empty( $auto_select_vacancy ) && $apply_page_url !== '#' ) {
			if ( ! $vacancy_title ) {
				$vacancy_title = get_the_title();
			}
			
			$apply_page_url .= '?job-name=' . urlencode( $vacancy_title );
		}
		
		return $apply_page_url;
	}
	
	
}

new W9sme_CPT_Vacancy();

