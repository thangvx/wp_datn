<?php

/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: class-w9sme-widget-download.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class W9sme_Widget_Download extends W9sme_Widget_Base {
	/**
	 * W9sme_Widget_Download constructor.
	 */
	public function __construct() {
		$args = array(
			'id'      => 'w9sme-widget-download',
			'name'    => esc_html__( 'W9sme Download', 'w9sme-addon' ),
			'options' => array(
				'classname'   => 'w9sme-widget-download',
				'description' => esc_html__( 'Widget to get a link to download.', 'w9sme-addon' )
			),
			'fields'  => array(
				array(
					'id'      => 'title',
					'type'    => 'text',
					'title'   => esc_html__( 'Title', 'w9sme-addon' ),
					'default' => ''
				),
				array(
					'id'    => 'file',
					'type'  => 'file-selector',
					'title' => esc_html__( 'Select File', 'w9sme-addon' ),
				),
				
				array(
					'id'      => 'display_text',
					'type'    => 'text',
					'title'   => esc_html__( 'Display Text', 'w9sme-addon' ),
					'default' => __( 'DOWNLOAD', 'w9sme-addon' )
				),
				
				array(
					'id'      => 'file_type',
					'type'    => 'select',
					'title'   => esc_html__( 'File Type', 'w9sme-addon' ),
					'options' => array(
						'DOC'    => esc_html__( 'DOC', 'w9sme-addon' ),
						'XLS'    => esc_html__( 'XLS', 'w9sme-addon' ),
						'PDF'    => esc_html__( 'PDF', 'w9sme-addon' ),
						'PPT'    => esc_html__( 'PPT', 'w9sme-addon' ),
						'custom' => esc_html__( 'Custom', 'w9sme-addon' ),
					),
					'default' => 'DOC'
				),
			)
		
		);
		parent::__construct( $args );
	}
	
	public function widget_content( $args, $instance ) {
		$file = $display_text = $file_type = '';
		extract( $instance, EXTR_IF_EXISTS );
		if ( empty( $file ) ) {
			$file = '#';
		}
		
		$icon = '';
		
		switch ( $file_type ) {
			case 'DOC':
				$icon = 'fa fa-file-text-o';
				break;
			case 'XLS':
				$icon = 'fa fa-file-excel-o';
				break;
			case 'PDF':
				$icon = 'fa fa-file-pdf-o';
				break;
			case 'PPT':
				$icon = 'fa fa-file-powerpoint-o';
				break;
			default:
				$icon = 'fa fa-file-o';
		}
		
		$display_text = ( ! empty( $display_text ) ) ? $display_text : __( 'DOWNLOAD', 'w9sme-addon' );
		
		$this->the_widget_title( $args, $instance );
		ob_start();
		?>
		
		<div class="w9sme-download">
			<a target="_blank" href="<?php echo esc_url( $file ); ?>" class="__inner">
				<div class="__icon">
					<i class="<?php echo esc_attr( $icon ); ?>"></i>
				</div>
				<div class="__text ls-005">
					<?php echo esc_html( $display_text ); ?>
				</div>
			</a>
		</div>
		<?php
		echo ob_get_clean();
	}
}
