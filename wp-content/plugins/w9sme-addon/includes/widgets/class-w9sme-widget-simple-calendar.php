<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: class-w9sme-widget-simple-calendar.php
 * @time    : 5/11/2017 11:16 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class W9sme_Widget_Simple_Calendar extends W9sme_Widget_Base {
	/**
	 * W9sme_Widget_Logo constructor.
	 */
	public function __construct() {
		$args = array(
			'id'      => 'w9sme-widget-simple-calendar',
			'name'    => esc_html__( 'W9sme Simple Calendar', 'w9sme-addon' ),
			'options' => array(
				'classname'   => 'w9sme-widget-simple-calendar',
				'description' => esc_html__( 'Display a simple calendar.', 'w9sme-addon' )
			),
			'fields'  => array(
				array(
					'id'      => 'title',
					'type'    => 'text',
					'title'   => esc_html__( 'Title', 'w9sme-addon' ),
					'default' => ''
				),
			)
		
		);
		parent::__construct( $args );
	}
	
	public function widget_content( $args, $instance ) {
//		$a ='';
//		extract( $instance, EXTR_IF_EXISTS );
		$this->the_widget_title( $args, $instance );
//		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui-datepicker' );
		$uni_id = uniqid( 'w9sme-calendar-' );
		ob_start();
		$days = array (
			__( 'Sun', 'w9sme-addon' ),
			__( 'Mon', 'w9sme-addon' ),
			__( 'Tue', 'w9sme-addon' ),
			__( 'Wed', 'w9sme-addon' ),
			__( 'Thu', 'w9sme-addon' ),
			__( 'Fri', 'w9sme-addon' ),
			__( 'Sat', 'w9sme-addon' ),
		);
		?>
		<div class="w9sme-calendar" id="<?php echo $uni_id;?>"></div>
		<script>
			!function(a){"use strict";a(document).ready(function(){a(<?php echo '\'#' . $uni_id . '\'' ?>).datepicker({inline:!0,firstDay:1,showOtherMonths:!0,dayNamesMin:<?php echo json_encode($days);?>})})}(jQuery);
//			$(<?php //echo '\'#' . $uni_id . '\'' ?>//).datepicker({
//				inline: true,
//				firstDay: 1,
//				showOtherMonths: true,
//				dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
//			});
		</script>
		<?php
		echo ob_get_clean();
	}
}