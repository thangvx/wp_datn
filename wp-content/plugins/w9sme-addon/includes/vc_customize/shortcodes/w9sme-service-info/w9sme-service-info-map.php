<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-service-info-map.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( is_admin() && ( ! defined( 'DOING_AJAX' ) && ( ! post_type_exists( W9sme_CPT_Service::CPT_SLUG ) ) ) ) {
	return;
}


$using_default_booking_page_url = array(
	'type'        => 'switcher',
	'heading'     => esc_html__( 'Use service booking page url', 'w9sme-addon' ),
	'param_name'  => 'info_df_booking_url',
	'description' => __( 'The url is setup in the service archive theme options.', 'w9sme-addon' ),
	'group'       => __( 'Button', 'w9sme-addon' ),
	'value'       => '1',
	'dependency'  => array(
		'element' => 'add_button',
		'value'   => 'yes'
	)
);

$using_default_booking_text = array(
	'type'        => 'switcher',
	'heading'     => esc_html__( 'Use service booking button text', 'w9sme-addon' ),
	'param_name'  => 'info_df_booking_text',
	'description' => __( 'The button text is setup in the single service.', 'w9sme-addon' ),
	'group'       => __( 'Button', 'w9sme-addon' ),
	'value'       => '1',
	'dependency'  => array(
		'element' => 'add_button',
		'value'   => 'yes'
	)
);

$button_param = vc_map_integrate_shortcode( W9sme_SC_Button::SC_BASE, '', __( 'Button', 'w9sme-addon' ), array(
	'exclude' => array(
		'css',
		'el_class',
		'animation_css',
		'animation_duration',
		'animation_delay',
		'css_inline',
	),
),
	
	array(
		'element' => 'add_button',
		'value'   => 'yes'
	)
);

foreach ( $button_param as $key => $single_param ) {
	if ( isset( $single_param['param_name'] ) && $single_param['param_name'] === 'btn_text' ) {
		$button_param[ $key ]['std'] = __( 'Book', 'w9sme-addon' );
//		$button_param[ $key ]['dependency'] = array( 'element' => 'info_df_booking_text', 'value' => '0' );
		
		array_splice( $button_param, $key, 0, array( $using_default_booking_text ) );
	}
	
	if ( isset( $single_param['param_name'] ) && $single_param['param_name'] === 'btn_link' ) {
//		$button_param[ $key ]['dependency'] = array( 'element' => 'info_df_booking_url', 'value' => '0' );
		
		array_splice( $button_param, $key + 1, 0, array( $using_default_booking_page_url ) );
	}
}

vc_map( array(
	'name'           => __( 'W9sme Service Info', 'w9sme-addon' ),
	'base'           => W9sme_SC_Service_Info::SC_BASE,
	'php_class_name' => 'W9sme_SC_Service_Info',
	'icon'           => 'w9 w9-ico-software-layout-8boxes',
	'category'       => array( __( 'Content', 'w9sme-addon' ) ),
	'description'    => __( 'Get common meta info from single service', 'w9sme-addon' ),
	'post_type'      => W9sme_CPT_Service::CPT_SLUG,
	'params'         => array_merge(
		array(
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Style', 'w9sme-addon' ),
				'param_name'  => 'info_style',
				'admin_label' => true,
				'value'       => array(
					__( 'Style 1', 'w9sme-addon' ) => 'style-1',
					__( 'Style 2', 'w9sme-addon' ) => 'style-2',
					__( 'Style 3', 'w9sme-addon' ) => 'style-3',
				),
				'default'     => 'style-1',
			),
			array(
				'type'        => 'param_group',
				'heading'     => __( 'Info fields', 'w9sme-addon' ),
				'param_name'  => 'info_list',
				'description' => __( 'Select info use in this shortcode.', 'w9sme-addon' ),
				'value'       => urlencode( json_encode( array(
					array(
						'info'       => 'date',
						'info_label' => __( 'DATE:', 'w9sme-addon' ),
					),
					array(
						'info'       => 'categories',
						'info_label' => __( 'CATEGORY:', 'w9sme-addon' ),
					),
					array(
						'info'       => 'price',
						'info_label' => __( 'PRICE:', 'w9sme-addon' ),
					),
					array(
						'info'       => 'time',
						'info_label' => __( 'TIME:', 'w9sme-addon' ),
					),
					array(
						'info' => 'addition-info',
					),
					array(
						'info'       => 'share-this',
						'info_label' => __( 'SHARE:', 'w9sme-addon' ),
					),
				) ) ),
				'params'      => array(
					array(
						'type'        => 'dropdown',
						'heading'     => __( 'Info', 'w9sme-addon' ),
						'param_name'  => 'info',
						'admin_label' => true,
						'value'       => array(
							__( 'Categories', 'w9sme-addon' )      => 'categories',
							__( 'Date', 'w9sme-addon' )            => 'date',
							__( 'Price', 'w9sme-addon' )           => 'price',
							__( 'Time', 'w9sme-addon' )            => 'time',
							__( 'Additional info', 'w9sme-addon' ) => 'addition-info',
							__( 'Single Additional info', 'w9sme-addon' ) => 'single-additional-info',
							__( 'Share This', 'w9sme-addon' )      => 'share-this',
						),
					),
					array(
						'type'        => 'textfield',
						'heading'     => __( 'Label', 'w9sme-addon' ),
						'param_name'  => 'info_label',
						'admin_label' => true,
						'dependency'  => array(
							'element'            => 'info',
							'value_not_equal_to' => array( 'addition-info' )
						)
					),
					array(
						'type'        => 'textfield',
						'heading'     => __( 'Additional Label Name', 'w9sme-addon' ),
						'param_name'  => 'additional_label_name',
						'admin_label' => true,
						'dependency'  => array(
							'element' => 'info',
							'value'   => array( 'single-additional-info' )
						)
					)
				),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Add button', 'w9sme-addon' ),
				'param_name'  => 'add_button',
				'description' => __( 'Add button link to Project URL below service info.', 'w9sme-addon' ),
				'admin_label' => true,
				'value'       => array(
					__( 'Yes, Please!', 'w9sme-addon' ) => 'yes'
				),
				'default'     => 'true',
			),
			W9sme_Map_Helpers::extra_class(),
		),
		$button_param,
		array(
			W9sme_Map_Helpers::design_options(),
			W9sme_Map_Helpers::animation_css(),
			W9sme_Map_Helpers::animation_duration(),
			W9sme_Map_Helpers::animation_delay()
		)
	)
) );
