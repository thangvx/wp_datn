<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-service.php
 * @time    : 4/20/2017 3:02 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( !post_type_exists( W9sme_CPT_Service::CPT_SLUG ) ) {
	return;
}

class W9sme_SC_Service extends WPBakeryShortCode {
	const SC_BASE = 'w9sme_shortcode_service';
	
	public function __construct($settings) {
		parent::__construct($settings);
	}
	
	
}