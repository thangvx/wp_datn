<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: vc-text-separator-map.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$icons_params = vc_map_integrate_shortcode( 'vc_icon', 'i_', __( 'Icon', 'w9sme-addon' ), array(
    'exclude' => array(
        'align',
        'css',
        'el_class',
        'link',
        'animation_css',
        'animation_duration',
        'animation_delay',
        'tablet_css',
        'mobile_css',
    ),
    // we need only type, icon_fontawesome, icon_blabla..., NOT color and etc
), array(
    'element' => 'add_icon',
    'value'   => 'true',
) );

// populate integrated vc_icons params.
if ( is_array( $icons_params ) && !empty( $icons_params ) ) {
    foreach ( $icons_params as $key => $param ) {
        if ( is_array( $param ) && !empty( $param ) ) {
            if ( isset( $param['admin_label'] ) ) {
                // remove admin label
                unset( $icons_params[$key]['admin_label'] );
            }
        }
    }
}


vc_map( array(
    'name'        => __( 'Separator with Text', 'w9sme-addon' ),
    'base'        => 'vc_text_separator',
    'icon'        => 'w9 w9-ico-arrows-drag-vert',
    'category'    => __( 'Content', 'w9sme-addon' ),
    'description' => __( 'Horizontal separator line with heading', 'w9sme-addon' ),
    'post_type' => Vc_Grid_Item_Editor::postType(),
    'params'      => array_merge( array(
        array(
            'type'        => 'textfield',
            'heading'     => __( 'Title', 'w9sme-addon' ),
            'param_name'  => 'title',
            'holder'      => 'div',
            'value'       => __( 'Title', 'w9sme-addon' ),
            'description' => __( 'Add text to separator.', 'w9sme-addon' ),
        ),
        array(
            'type'       => 'checkbox',
            'heading'    => __( 'Add icon?', 'w9sme-addon' ),
            'param_name' => 'add_icon',
        ) ), $icons_params, array(
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Title position', 'w9sme-addon' ),
            'param_name'  => 'title_align',
            'value'       => array(
                __( 'Center', 'w9sme-addon' ) => 'separator_align_center',
                __( 'Left', 'w9sme-addon' )   => 'separator_align_left',
                __( 'Right', 'w9sme-addon' )  => 'separator_align_right',
            ),
            'description' => __( 'Select title location.', 'w9sme-addon' ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Separator alignment', 'w9sme-addon' ),
            'param_name'  => 'align',
            'value'       => array(
                __( 'Center', 'w9sme-addon' ) => 'align_center',
                __( 'Left', 'w9sme-addon' )   => 'align_left',
                __( 'Right', 'w9sme-addon' )  => 'align_right',
            ),
            'description' => __( 'Select separator alignment.', 'w9sme-addon' ),
        ),
        array(
            'type'               => 'dropdown',
            'heading'            => __( 'Color', 'w9sme-addon' ),
            'param_name'         => 'color',
            'value'              => W9sme_Map_Helpers::get_colors(),
            'std'                => 'p',
            'description'        => __( 'Select color of separator.', 'w9sme-addon' ),
            'param_holder_class' => 'vc_colored-dropdown',
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Custom Color', 'w9sme-addon' ),
            'param_name'  => 'accent_color',
            'description' => __( 'Custom separator color for your element.', 'w9sme-addon' ),
            'dependency'  => array(
                'element' => 'color',
                'value'   => array( 'custom' ),
            ),
        ),
        
        
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Style', 'w9sme-addon' ),
            'param_name'  => 'style',
            'value'       => getVcShared( 'separator styles' ),
            'description' => __( 'Separator display style.', 'w9sme-addon' ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Border width', 'w9sme-addon' ),
            'param_name'  => 'border_width',
            'value'       => getVcShared( 'separator border widths' ),
            'description' => __( 'Select border width (pixels).', 'w9sme-addon' ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Element width', 'w9sme-addon' ),
            'param_name'  => 'el_width',
            'value'       => getVcShared( 'separator widths' ),
            'description' => __( 'Separator element width in percents.', 'w9sme-addon' ),
        ),
	
	    // top line
	    array(
		    'type'        => 'switcher',
		    'heading'     => esc_html__( 'Text Top Line', 'w9sme-addon' ),
		    'param_name'  => 'separator_top_line_enable',
		    'description' => esc_html__( 'Enable top line for separator text', 'w9sme-addon' ),
		    'value'       => '',
	    ),
	
	    array(
		    'type'        => 'number',
		    'heading'     => esc_html__( 'Margin bottom', 'w9sme-addon' ),
		    'param_name'  => 'separator_top_line_margin_bottom',
		    'value'       => '15',
		    'description' => esc_html__( 'Enter the margin bottom for the top line (Unit: px).', 'w9sme-addon' ),
		    'group'       => __( 'Top Line', 'w9sme-addon' ),
		    'dependency'  => array(
			    'element' => 'separator_top_line_enable',
			    'value'   => '1',
		    ),
	    ),
	
	    array(
		    'type'        => 'dropdown',
		    'heading'     => esc_html__( 'Width', 'w9sme-addon' ),
		    'param_name'  => 'separator_top_line_width',
		    'value'       => array(
			    esc_html__( 'Default', 'w9sme-addon' )      => 'default',
			    esc_html__( 'Custom Width', 'w9sme-addon' ) => 'custom-width',
		    ),
		    'std'         => 'default',
		    'description' => esc_html__( 'Select separator text top line width. Default value is 46px.', 'w9sme-addon' ),
		    'group'       => __( 'Top Line', 'w9sme-addon' ),
		    'dependency'  => array(
			    'element' => 'separator_top_line_enable',
			    'value'   => '1',
		    ),
	    ),
	
	    array(
		    'type'        => 'textfield',
		    'heading'     => esc_html__( 'Custom width', 'w9sme-addon' ),
		    'param_name'  => 'separator_top_line_custom_width',
		    'value'       => '',
		    'description' => esc_html__( 'Enter custom width for the separator text top line (etc: 20px, 10em...).', 'w9sme-addon' ),
		    'dependency'  => array( 'element' => 'separator_top_line_width', 'value' => 'custom-width' ),
		    'group'       => __( 'Top Line', 'w9sme-addon' ),
	    ),
	
	    array(
		    'type'        => 'dropdown',
		    'heading'     => esc_html__( 'Height', 'w9sme-addon' ),
		    'param_name'  => 'separator_top_line_height',
		    'value'       => array(
			    esc_html__( 'Default', 'w9sme-addon' )       => 'default',
			    esc_html__( 'Custom Height', 'w9sme-addon' ) => 'custom-height',
		    ),
		    'std'         => 'default',
		    'description' => esc_html__( 'Select separator text top line height. Default value is 5px.', 'w9sme-addon' ),
		    'group'       => __( 'Top Line', 'w9sme-addon' ),
		    'dependency'  => array(
			    'element' => 'separator_top_line_enable',
			    'value'   => '1',
		    ),
	    ),
	
	    array(
		    'type'        => 'textfield',
		    'heading'     => esc_html__( 'Custom height', 'w9sme-addon' ),
		    'param_name'  => 'separator_top_line_custom_height',
		    'value'       => '',
		    'description' => esc_html__( 'Enter custom height for the separator text top line (etc: 20px, 10em...).', 'w9sme-addon' ),
		    'dependency'  => array( 'element' => 'separator_top_line_height', 'value' => 'custom-height' ),
		    'group'       => __( 'Top Line', 'w9sme-addon' ),
	    ),
	
	
	    array(
		    'type'               => 'dropdown',
		    'heading'            => esc_html__( 'Color', 'w9sme-addon' ),
		    'param_name'         => 'separator_top_line_color',
		    'param_holder_class' => 'vc_colored-dropdown',
		    'value'              => W9sme_Map_Helpers::get_colors(),
		    'description'        => esc_html__( 'Select Color Scheme.', 'w9sme-addon' ),
		    'group'              => __( 'Top Line', 'w9sme-addon' ),
		    'dependency'         => array(
			    'element' => 'separator_top_line_enable',
			    'value'   => '1',
		    ),
		    'std'                => 'p'
	    ),
	
	    array(
		    'type'       => 'colorpicker',
		    'heading'    => esc_html__( 'Custom color', 'w9sme-addon' ),
		    'param_name' => 'separator_top_line_custom_color',
		    'value'      => '',
		    'dependency' => array(
			    'element' => 'separator_top_line_color',
			    'value'   => 'custom'
		    ),
		    'group'      => __( 'Top Line', 'w9sme-addon' ),
	    ),
        
        
        array(
            'type'        => 'textfield',
            'heading'     => __( 'Extra class name', 'w9sme-addon' ),
            'param_name'  => 'el_class',
            'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'w9sme-addon' ),
        ),
        array(
            'type'       => 'hidden',
            'param_name' => 'layout',
            'value'      => 'separator_with_text',
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => __( 'CSS box', 'w9sme-addon' ),
            'param_name' => 'css',
            'group'      => __( 'Design Options', 'w9sme-addon' ),
        ),
    ) ),
    'js_view'     => 'VcTextSeparatorView',
) );
