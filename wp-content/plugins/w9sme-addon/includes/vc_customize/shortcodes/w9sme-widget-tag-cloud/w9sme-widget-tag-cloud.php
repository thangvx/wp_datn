<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-widget-tagcloud.php
 * @time    : 9/28/2016 9:51 AM
 * @author  : 9WPThemes Team
 */

if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Widget_Tag_Cloud extends WPBakeryShortCode {
    const SC_BASE = 'w9sme_shortcode_widget_tag_cloud';
}