<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-content-template-map.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


/*
 * Content Templpate Visual Composer
 * Outside Row
 */

//if ( is_admin() && ( ! defined( 'DOING_AJAX' ) && ( ! post_type_exists( W9sme_CPT_Content_Template::CPT_SLUG ) || w9sme_get_current_post_type() == W9sme_CPT_Content_Template::CPT_SLUG ) ) ) {
//	return;
//}

$content_template_list = w9sme_get_content_template_list( true, false );
$content_template_list = array_merge( array( __( '__ Select Content Template __', 'w9sme-addon' ) => '' ), $content_template_list );


/*-------------------------------------
	PREVENT RECURSIVE CONTENT TEMPLATE CALL
	BY NOT PRESENT THEM AS A VALID VALUE FOR SELECTING
---------------------------------------*/
$post_id = isset($_REQUEST['post_id']) ? $_REQUEST['post_id'] : 0;

if (!empty($post_id)) {
	$the_post = get_post($post_id);
	
	if ($the_post->post_type === W9sme_CPT_Content_Template::CPT_SLUG) {
		foreach ($content_template_list as $name => $key) {
			if ($the_post->post_name === $key) {
				unset($content_template_list[$name]);
				break;
			}
		}
	}
}


vc_map( array(
	'name'             => esc_html__( 'W9sme Content Template', 'w9sme-addon' ),
	'base'             => W9sme_SC_Content_Template::SC_BASE,
	'class'            => '',
	'icon'             => 'w9 w9-ico-arrows-squares',
	'category'         => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
	'description'      => __( 'Use content template in page', 'w9sme-addon' ),
//	"as_child"         => array( 'only' => 'vc_row' ),
	'php_class_name'   => 'W9sme_SC_Content_Template',
	'params'           => array(
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Content template', 'w9sme-addon' ),
			'param_name'  => 'template',
			'admin_label' => true,
			'value'       => $content_template_list,
			'description' => __( 'Select the content template will be displayed in this section.', 'w9sme-addon' ),
		),
//		array(
//			'type'        => 'switcher',
//			'heading'     => __( 'Outside row', 'w9sme-addon' ),
//			'param_name'  => 'outside_row',
//			'description' => __( 'Make this content template outside the row.', 'w9sme-addon' ),
//			'std'         => '1'
//		),
	),
//	'js_view'          => 'W9smeContentTemplateView',
//	'admin_enqueue_js' => array( W9sme_Addon::plugin_url() . 'assets/js/vc-custom.js' ),
) );