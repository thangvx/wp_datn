<?php

/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-content-template.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Content_Template extends WPBakeryShortCode{
    const SC_BASE = 'w9sme_shortcode_content_template';
    
}
