<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-heading-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

vc_map( W9sme_SC_Heading::map() );