<?php

/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-heading.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class W9sme_SC_Heading extends WPBakeryShortCode {
	const SC_BASE = 'w9sme_shortcode_heading';
	
	public static function map() {
		/*
		 * 1. Title
		 * - URL (Links)
		 * - Element tag
		 * - Styling Options
		 *
		 * 2. Enable Sub-Title
		 * - Content
		 * - Font Size
		 * - Line Height
		 *
		 *
		 * 3. Enable Separator
		 * - Separator Color
		 */
		
		return array(
			'name'           => esc_html__( 'W9sme Heading', 'w9sme-addon' ),
			'base'           => W9sme_SC_Heading::SC_BASE,
			'class'          => '',
			'icon'           => 'w9 w9-ico-software-font-underline',
			'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
			'description'    => __( 'Create awesome heading', 'w9sme-addon' ),
			'php_class_name' => 'W9sme_SC_Heading',
			'params'         => array(
				// TITLE
				array(
					'type'       => 'dropdown',
					'heading'    => esc_html__( 'Data source', 'w9sme-addon' ),
					'param_name' => 'heading_title_data_source',
					'value'      => array(
						esc_html__( 'Custom Content', 'w9sme-addon' )          => 'custom-content',
						esc_html__( 'From Current Page Title', 'w9sme-addon' ) => 'page-title',
						__( 'From Current "Raw" Page Title', 'w9sme-addon' ) => 'raw-page-title',
					),
					'std'        => 'custom-content'
				),
				array(
					'type'        => 'textarea_safe',
					'heading'     => esc_html__( 'Title', 'w9sme-addon' ),
					'param_name'  => 'heading_title',
					'value'       => '',
					'admin_label' => true,
					'std'         => 'This is heading title',
					'dependency'  => array( 'element' => 'heading_title_data_source', 'value' => 'custom-content' ),
				),
				
				array(
					'type'       => 'vc_link',
					'heading'    => esc_html__( 'Link (URL)', 'w9sme-addon' ),
					'param_name' => 'heading_link',
					'value'      => '',
					'dependency' => array( 'element' => 'heading_title_data_source', 'value' => 'custom-content' ),
				),
				
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Element tag', 'w9sme-addon' ),
					'param_name'  => 'heading_el_tag',
					'value'       => array(
						esc_html__( 'h1', 'w9sme-addon' )  => 'h1',
						esc_html__( 'h2', 'w9sme-addon' )  => 'h2',
						esc_html__( 'h3', 'w9sme-addon' )  => 'h3',
						esc_html__( 'h4', 'w9sme-addon' )  => 'h4',
						esc_html__( 'h5', 'w9sme-addon' )  => 'h5',
						esc_html__( 'h6', 'w9sme-addon' )  => 'h6',
						esc_html__( 'p', 'w9sme-addon' )   => 'p',
						esc_html__( 'div', 'w9sme-addon' ) => 'div',
					),
					'std'         => 'h2',
					'description' => esc_html__( 'Select heading element tag.', 'w9sme-addon' ),
					'admin_label' => true,
				),
				
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Title size', 'w9sme-addon' ),
					'param_name'  => 'heading_title_size',
					'value'       => array(
						esc_html__( 'Extra Small (18px)', 'w9sme-addon' ) => '18',
						esc_html__( 'Small (24px)', 'w9sme-addon' )       => '24',
						esc_html__( 'Medium (28px)', 'w9sme-addon' )      => '28',
						esc_html__( 'Large (36px)', 'w9sme-addon' )       => '36',
						esc_html__( 'Extra Large (48px)', 'w9sme-addon' ) => '48',
						esc_html__( 'Custom Size', 'w9sme-addon' )        => 'custom',
					),
					'std'         => '30',
					'description' => esc_html__( 'Select heading title size.', 'w9sme-addon' ),
					'admin_label' => true,
				),
				
				array(
					'type'        => 'number',
					'heading'     => esc_html__( 'Custom font size', 'w9sme-addon' ),
					'param_name'  => 'heading_title_custom_size',
					'value'       => '',
					'description' => esc_html__( 'Enter custom font-size for the heading title (Unit: px).', 'w9sme-addon' ),
					'dependency'  => array( 'element' => 'heading_title_size', 'value' => 'custom' ),
				),
				
				
				array(
					'type'             => 'textfield',
					'heading'          => esc_html__( 'Title line height', 'w9sme-addon' ),
					'param_name'       => 'heading_title_line_height',
					'value'            => '',
					'description'      => esc_html__( 'Leave blank if you want to use default line-height.', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'             => 'dropdown',
					'heading'          => esc_html__( 'Text align', 'w9sme-addon' ),
					'param_name'       => 'heading_text_align',
					'value'            => array(
						esc_html__( 'Inherit', 'w9sme-addon' ) => '',
						esc_html__( 'Left', 'w9sme-addon' )    => 'text-left',
						esc_html__( 'Center', 'w9sme-addon' )  => 'text-center',
						esc_html__( 'Right', 'w9sme-addon' )   => 'text-right',
						esc_html__( 'Justify', 'w9sme-addon' ) => 'text-justify'
					),
					'description'      => esc_html__( 'Select text align.', 'w9sme-addon' ),
					'std'              => '',
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'             => 'dropdown',
					'heading'          => esc_html__( 'Title letter spacing', 'w9sme-addon' ),
					'param_name'       => 'heading_title_ls',
					'value'            => array(
						esc_html__( 'Default CSS', 'w9sme-addon' ) => '',
						'0em'                                           => 'ls-0',
						'0.05em'                                        => 'ls-005',
						'0.1em'                                         => 'ls-01',
						'0.15em'                                        => 'ls-015',
						'0.2em'                                         => 'ls-02'
					),
					'description'      => esc_html__( 'Select title letter spacing.', 'w9sme-addon' ),
					'std'              => '',
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				
				array(
					'type'               => 'dropdown',
					'heading'            => esc_html__( 'Color', 'w9sme-addon' ),
					'param_name'         => 'heading_color',
					'param_holder_class' => 'vc_colored-dropdown',
					'value'              => W9sme_Map_Helpers::get_colors(),
					'description'        => esc_html__( 'Select Color Scheme.', 'w9sme-addon' ),
					'edit_field_class'   => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'             => 'colorpicker',
					'heading'          => esc_html__( 'Custom color', 'w9sme-addon' ),
					'param_name'       => 'heading_title_custom_color',
					'value'            => '',
					'dependency'       => array(
						'element' => 'heading_color',
						'value'   => 'custom'
					),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Select title font family', 'w9sme-addon' ),
					'param_name'  => 'heading_title_ff',
					'value'       => array(
						esc_html__( 'Primary Font', 'w9sme-addon' )   => 'p-font',
						esc_html__( 'Secondary Font', 'w9sme-addon' ) => 's-font',
						esc_html__( 'Third Font', 'w9sme-addon' )     => 't-font',
						esc_html__( 'Google Font', 'w9sme-addon' )    => 'google-font',
					),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
					'description' => esc_html__( 'Select title font family.', 'w9sme-addon' ),
					'admin_label' => true,
					'std'         => 's-font'
				),
				
				
				array(
					'type'             => 'dropdown',
					'heading'          => esc_html__( 'Title text transform', 'w9sme-addon' ),
					'param_name'       => 'heading_title_text_transform',
					'value'            => array(
						__( 'Default', 'w9sme-addon' )    => '',
						__( 'lowercase', 'w9sme-addon' )  => 'text-lowercase',
						__( 'UPPERCASE', 'w9sme-addon' )  => 'text-uppercase',
						__( 'Capitalize', 'w9sme-addon' ) => 'text-capitalize',
					),
					'description'      => esc_html__( 'Select title text transform.', 'w9sme-addon' ),
					'std'              => '',
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'             => 'dropdown',
					'heading'          => esc_html__( 'Title font style', 'w9sme-addon' ),
					'param_name'       => 'heading_text_fs',
					'value'            => array(
						__( 'Normal', 'w9sme-addon' ) => '',
						__( 'Italic', 'w9sme-addon' ) => 'fs-italic',
					),
					'description'      => esc_html__( 'Select font style.', 'w9sme-addon' ),
					'std'              => '',
					'dependency'       => array(
						'element'            => 'heading_title_ff',
						'value_not_equal_to' => 'google-font'
					),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'             => 'dropdown',
					'heading'          => esc_html__( 'Title font weight', 'w9sme-addon' ),
					'param_name'       => 'heading_text_fw',
					'value'            => array(
						__( 'Inherit', 'w9sme-addon' )         => 'fw-inherit',
						__( 'Light (300)', 'w9sme-addon' )     => 'fw-light',
						__( 'Regular (400)', 'w9sme-addon' )   => 'fw-regular',
						__( 'Medium (500)', 'w9sme-addon' )    => 'fw-medium',
						__( 'Semi Bold (600)', 'w9sme-addon' ) => 'fw-semibold',
						__( 'Bold (700)', 'w9sme-addon' )      => 'fw-bold',
					),
					'description'      => esc_html__( 'Select font weight. May be some value do not work because of the font does not support it.', 'w9sme-addon' ),
					'std'              => 'fw-inherit',
					'edit_field_class' => 'vc_col-sm-6 vc_column',
					'dependency'       => array(
						'element'            => 'heading_title_ff',
						'value_not_equal_to' => 'google-font'
					),
				),
				
				
				array(
					'type'       => 'google_fonts',
					'param_name' => 'heading_title_custom_ff',
					'value'      => 'font_family:Abril%20Fatface%3Aregular|font_style:400%20regular%3A400%3Anormal',
					'settings'   => array(
						'fields' => array(
							'font_family_description' => __( 'Select font family.', 'w9sme-addon' ),
							'font_style_description'  => __( 'Select font styling.', 'w9sme-addon' ),
						),
					),
					'dependency' => array(
						'element' => 'heading_title_ff',
						'value'   => 'google-font',
					),
				),
				
//				array(
//					'type'       => 'checkbox',
//					'heading'    => esc_html__( 'Remove space between title and other elements', 'w9sme-addon' ),
//					'param_name' => 'heading_title_no_space',
//					'value'      => '',
//				),
				
				// Sub Title
				array(
					'type'       => 'switcher',
					'heading'    => esc_html__( 'Sub-title', 'w9sme-addon' ),
					'param_name' => 'heading_subtitle_enable',
					'value'      => '',
				),
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Data source', 'w9sme-addon' ),
					'param_name'  => 'heading_subtitle_data_source',
					'value'       => array(
						esc_html__( 'Custom Content', 'w9sme-addon' )              => 'custom-content',
						esc_html__( 'From Current Page Sub Title', 'w9sme-addon' ) => 'page-subtitle',
					),
					'std'         => 'custom-content',
					'admin_label' => true,
					'group'       => __( 'Sub Title', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_subtitle_enable',
						'value'   => '1',
					),
				),
				array(
					'type'        => 'textarea_safe',
					'heading'     => esc_html__( 'Content', 'w9sme-addon' ),
					'param_name'  => 'heading_subtitle_content',
					'value'       => '',
					'description' => esc_html__( 'Enter subtitle content.', 'w9sme-addon' ),
					'group'       => __( 'Sub Title', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_subtitle_data_source',
						'value'   => 'custom-content',
					),
				),
				
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Size', 'w9sme-addon' ),
					'param_name'  => 'heading_subtitle_size',
					'value'       => array(
						esc_html__( 'Extra Small (12px)', 'w9sme-addon' ) => '12',
						esc_html__( 'Small (14px)', 'w9sme-addon' )       => '14',
						esc_html__( 'Medium (16px)', 'w9sme-addon' )      => '16',
						esc_html__( 'Large (18px)', 'w9sme-addon' )       => '18',
						esc_html__( 'Extra Large (20px)', 'w9sme-addon' ) => '20',
						esc_html__( 'Custom Size', 'w9sme-addon' )        => 'custom',
					),
					'std'         => 'fz-30',
					'description' => esc_html__( 'Select heading title size.', 'w9sme-addon' ),
					'group'       => __( 'Sub Title', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_subtitle_enable',
						'value'   => '1',
					),
				),
				
				array(
					'type'        => 'number',
					'heading'     => esc_html__( 'Custom font size', 'w9sme-addon' ),
					'param_name'  => 'heading_subtitle_custom_size',
					'value'       => '',
					'description' => esc_html__( 'Enter custom font-size for the heading subtitle (Unit: px).', 'w9sme-addon' ),
					'dependency'  => array( 'element' => 'heading_subtitle_size', 'value' => 'custom' ),
					'group'       => __( 'Sub Title', 'w9sme-addon' ),
				),
				
				array(
					'type'        => 'number',
					'heading'     => esc_html__( 'Margin top', 'w9sme-addon' ),
					'param_name'  => 'heading_subtitle_margin_top',
					'value'       => '',
					'description' => esc_html__( 'Enter the margin top for the heading subtitle (Unit: px).', 'w9sme-addon' ),
					'group'       => __( 'Sub Title', 'w9sme-addon' ),
					'dependency'       => array(
						'element' => 'heading_subtitle_enable',
						'value'   => '1',
					),
				),
				
				array(
					'type'             => 'textfield',
					'heading'          => esc_html__( 'Line height', 'w9sme-addon' ),
					'param_name'       => 'heading_subtitle_line_height',
					'value'            => '',
					'description'      => esc_html__( 'Leave blank if you want to use default line-height.', 'w9sme-addon' ),
					'group'            => __( 'Sub Title', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
					'dependency'       => array(
						'element' => 'heading_subtitle_enable',
						'value'   => '1',
					),
				),
				
				array(
					'type'             => 'textfield',
					'heading'          => esc_html__( 'Max width', 'w9sme-addon' ),
					'param_name'       => 'heading_subtitle_max_width',
					'description'      => esc_html__( 'Set max-width attribute for the heading subtitle (etc: 300px, 50%...).', 'w9sme-addon' ),
					'std'              => '',
					'edit_field_class' => 'vc_col-sm-6 vc_column',
					'group'            => __( 'Sub Title', 'w9sme-addon' ),
					'dependency'       => array(
						'element' => 'heading_subtitle_enable',
						'value'   => '1',
					),
				),
				
				array(
					'type'             => 'dropdown',
					'heading'          => esc_html__( 'Text transform', 'w9sme-addon' ),
					'param_name'       => 'heading_subtitle_text_transform',
					'value'            => array(
						__( 'Default', 'w9sme-addon' )    => '',
						__( 'lowercase', 'w9sme-addon' )  => 'text-lowercase',
						__( 'UPPERCASE', 'w9sme-addon' )  => 'text-uppercase',
						__( 'Capitalize', 'w9sme-addon' ) => 'text-capitalize',
					),
					'description'      => esc_html__( 'Select subtitle text transform.', 'w9sme-addon' ),
					'std'              => '',
					'group'            => __( 'Sub Title', 'w9sme-addon' ),
					'dependency'       => array(
						'element' => 'heading_subtitle_enable',
						'value'   => '1',
					),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'             => 'dropdown',
					'heading'          => esc_html__( 'Font style', 'w9sme-addon' ),
					'param_name'       => 'heading_subtitle_fs',
					'value'            => array(
						__( 'Inherit', 'w9sme-addon' ) => 'fs-inherit',
						__( 'Normal', 'w9sme-addon' )  => 'fs-normal',
						__( 'Italic', 'w9sme-addon' )  => 'fs-italic',
					),
					'description'      => esc_html__( 'Select font style.', 'w9sme-addon' ),
					'std'              => '',
					'edit_field_class' => 'vc_col-sm-6 vc_column',
					'group'            => __( 'Sub Title', 'w9sme-addon' ),
					'dependency'       => array(
						'element' => 'heading_subtitle_enable',
						'value'   => '1',
					),
				),
				
				array(
					'type'             => 'dropdown',
					'heading'          => esc_html__( 'Font weight', 'w9sme-addon' ),
					'param_name'       => 'heading_subtitle_fw',
					'value'            => array(
						__( 'Inherit', 'w9sme-addon' )         => 'fw-inherit',
						__( 'Light (300)', 'w9sme-addon' )     => 'fw-light',
						__( 'Regular (400)', 'w9sme-addon' )   => 'fw-regular',
						__( 'Medium (500)', 'w9sme-addon' )    => 'fw-medium',
						__( 'Semi Bold (600)', 'w9sme-addon' ) => 'fw-semibold',
						__( 'Bold (700)', 'w9sme-addon' )      => 'fw-bold',
					),
					'description'      => esc_html__( 'Select font weight. May be some value do not work because of the font does not support it.', 'w9sme-addon' ),
					'std'              => '',
					'edit_field_class' => 'vc_col-sm-6 vc_column',
					'group'            => __( 'Sub Title', 'w9sme-addon' ),
					'dependency'       => array(
						'element' => 'heading_subtitle_enable',
						'value'   => '1',
					),
				),
				
				// separator
				array(
					'type'        => 'switcher',
					'heading'     => esc_html__( 'Separator', 'w9sme-addon' ),
					'param_name'  => 'heading_separator_enable',
					'value'       => '',
					'admin_label' => true,
				),
				
				array(
					'type'        => 'number',
					'heading'     => esc_html__( 'Margin top', 'w9sme-addon' ),
					'param_name'  => 'heading_separator_margin_top',
					'value'       => '',
					'description' => esc_html__( 'Enter the margin top for the separator (Unit: px).', 'w9sme-addon' ),
					'group'       => __( 'Separator', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_separator_enable',
						'value'   => '1',
					),
				),
				
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Width', 'w9sme-addon' ),
					'param_name'  => 'heading_separator_width',
					'value'       => array(
						esc_html__( 'Default', 'w9sme-addon' )      => 'default',
						esc_html__( 'Custom Width', 'w9sme-addon' ) => 'custom-width',
					),
					'std'         => 'default',
					'description' => esc_html__( 'Select heading separator width. Default value is 105px.', 'w9sme-addon' ),
					'group'       => __( 'Separator', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_separator_enable',
						'value'   => '1',
					),
				),
				
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Custom width', 'w9sme-addon' ),
					'param_name'  => 'heading_separator_custom_width',
					'value'       => '',
					'description' => esc_html__( 'Enter custom width for the heading separator (etc: 20px, 10em...).', 'w9sme-addon' ),
					'dependency'  => array( 'element' => 'heading_separator_width', 'value' => 'custom-width' ),
					'group'       => __( 'Separator', 'w9sme-addon' ),
				),
				
				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Height', 'w9sme-addon' ),
					'param_name'  => 'heading_separator_height',
					'value'       => array(
						esc_html__( 'Default', 'w9sme-addon' )       => 'default',
						esc_html__( 'Custom Height', 'w9sme-addon' ) => 'custom-height',
					),
					'std'         => 'default',
					'description' => esc_html__( 'Select heading separator height. Default value is 2px.', 'w9sme-addon' ),
					'group'       => __( 'Separator', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_separator_enable',
						'value'   => '1',
					),
				),
				
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Custom height', 'w9sme-addon' ),
					'param_name'  => 'heading_separator_custom_height',
					'value'       => '',
					'description' => esc_html__( 'Enter custom height for the heading separator (etc: 20px, 10em...).', 'w9sme-addon' ),
					'dependency'  => array( 'element' => 'heading_separator_height', 'value' => 'custom-height' ),
					'group'       => __( 'Separator', 'w9sme-addon' ),
				),
				
				
				array(
					'type'               => 'dropdown',
					'heading'            => esc_html__( 'Color', 'w9sme-addon' ),
					'param_name'         => 'heading_separator_color',
					'param_holder_class' => 'vc_colored-dropdown',
					'value'              => W9sme_Map_Helpers::get_colors(),
					'description'        => esc_html__( 'Select Color Scheme.', 'w9sme-addon' ),
					'group'              => __( 'Separator', 'w9sme-addon' ),
					'dependency'         => array(
						'element' => 'heading_separator_enable',
						'value'   => '1',
					),
					'std'                => 'p'
				),
				
				array(
					'type'       => 'colorpicker',
					'heading'    => esc_html__( 'Custom color', 'w9sme-addon' ),
					'param_name' => 'heading_separator_custom_color',
					'value'      => '',
					'dependency' => array(
						'element' => 'heading_separator_color',
						'value'   => 'custom'
					),
					'group'      => __( 'Separator', 'w9sme-addon' ),
				),

				// top line
				array(
					'type'        => 'switcher',
					'heading'     => esc_html__( 'Top Line', 'w9sme-addon' ),
					'param_name'  => 'heading_top_line_enable',
					'value'       => '',
					'admin_label' => true,
				),

				array(
					'type'        => 'number',
					'heading'     => esc_html__( 'Margin bottom', 'w9sme-addon' ),
					'param_name'  => 'heading_top_line_margin_bottom',
					'value'       => '',
					'description' => esc_html__( 'Enter the margin bottom for the top line (Unit: px).', 'w9sme-addon' ),
					'group'       => __( 'Top Line', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_top_line_enable',
						'value'   => '1',
					),
				),

				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Width', 'w9sme-addon' ),
					'param_name'  => 'heading_top_line_width',
					'value'       => array(
						esc_html__( 'Default', 'w9sme-addon' )      => 'default',
						esc_html__( 'Custom Width', 'w9sme-addon' ) => 'custom-width',
					),
					'std'         => 'default',
					'description' => esc_html__( 'Select heading top line width. Default value is 46px.', 'w9sme-addon' ),
					'group'       => __( 'Top Line', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_top_line_enable',
						'value'   => '1',
					),
				),

				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Custom width', 'w9sme-addon' ),
					'param_name'  => 'heading_top_line_custom_width',
					'value'       => '',
					'description' => esc_html__( 'Enter custom width for the heading top line (etc: 20px, 10em...).', 'w9sme-addon' ),
					'dependency'  => array( 'element' => 'heading_top_line_width', 'value' => 'custom-width' ),
					'group'       => __( 'Top Line', 'w9sme-addon' ),
				),

				array(
					'type'        => 'dropdown',
					'heading'     => esc_html__( 'Height', 'w9sme-addon' ),
					'param_name'  => 'heading_top_line_height',
					'value'       => array(
						esc_html__( 'Default', 'w9sme-addon' )       => 'default',
						esc_html__( 'Custom Height', 'w9sme-addon' ) => 'custom-height',
					),
					'std'         => 'default',
					'description' => esc_html__( 'Select heading top line height. Default value is 5px.', 'w9sme-addon' ),
					'group'       => __( 'Top Line', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_top_line_enable',
						'value'   => '1',
					),
				),

				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Custom height', 'w9sme-addon' ),
					'param_name'  => 'heading_top_line_custom_height',
					'value'       => '',
					'description' => esc_html__( 'Enter custom height for the heading top line (etc: 20px, 10em...).', 'w9sme-addon' ),
					'dependency'  => array( 'element' => 'heading_top_line_height', 'value' => 'custom-height' ),
					'group'       => __( 'Top Line', 'w9sme-addon' ),
				),


				array(
					'type'               => 'dropdown',
					'heading'            => esc_html__( 'Color', 'w9sme-addon' ),
					'param_name'         => 'heading_top_line_color',
					'param_holder_class' => 'vc_colored-dropdown',
					'value'              => W9sme_Map_Helpers::get_colors(),
					'description'        => esc_html__( 'Select Color Scheme.', 'w9sme-addon' ),
					'group'              => __( 'Top Line', 'w9sme-addon' ),
					'dependency'         => array(
						'element' => 'heading_top_line_enable',
						'value'   => '1',
					),
					'std'                => 'p'
				),

				array(
					'type'       => 'colorpicker',
					'heading'    => esc_html__( 'Custom color', 'w9sme-addon' ),
					'param_name' => 'heading_top_line_custom_color',
					'value'      => '',
					'dependency' => array(
						'element' => 'heading_top_line_color',
						'value'   => 'custom'
					),
					'group'      => __( 'Top Line', 'w9sme-addon' ),
				),


				
				// responsive title
				array(
					'type'        => 'switcher',
					'heading'     => esc_html__( 'Responsive title', 'w9sme-addon' ),
					'param_name'  => 'heading_title_responsive_title_size',
					'description' => esc_html__( 'Enable or disable heading title font size responsive.', 'w9sme-addon' ),
					'value'       => '',
					'group'       => __( 'Responsive', 'w9sme-addon' )
				),
				
				array(
					'type'             => 'number',
					'heading'          => esc_html__( 'Title responsive compressor', 'w9sme-addon' ),
					'param_name'       => 'heading_title_scale_ratio',
					'value'            => '',
					'description'      => esc_html__( 'Enter responsive compressor (etc: 1.2, 1.5, 0.6, 0.7...). This is for responsive purpose. Default value is 1.', 'w9sme-addon' ),
					'dependency'       => array( 'element' => 'heading_title_responsive_title_size', 'value' => '1' ),
					'group'            => __( 'Responsive', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'             => 'number',
					'heading'          => esc_html__( 'Title minimum font size', 'w9sme-addon' ),
					'param_name'       => 'heading_title_minimum_size',
					'value'            => '',
					'description'      => esc_html__( 'Enter minimum font-size for the heading title (Unit: px). Default value is 20.', 'w9sme-addon' ),
					'dependency'       => array( 'element' => 'heading_title_responsive_title_size', 'value' => '1' ),
					'group'            => __( 'Responsive', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				// responsive subtitle
				array(
					'type'        => 'switcher',
					'heading'     => esc_html__( 'Responsive sub-title', 'w9sme-addon' ),
					'param_name'  => 'heading_subtitle_responsive_title_size',
					'description' => esc_html__( 'Enable or disable heading sub-title font size responsive.', 'w9sme-addon' ),
					'value'       => '',
					'group'       => __( 'Responsive', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'heading_subtitle_enable',
						'value'   => '1',
					),
				),
				
				array(
					'type'             => 'number',
					'heading'          => esc_html__( 'Sub-title responsive compressor', 'w9sme-addon' ),
					'param_name'       => 'heading_subtitle_scale_ratio',
					'value'            => '',
					'description'      => esc_html__( 'Enter responsive compressor (etc: 1.2, 1.5, 0.6, 0.7...). This is for responsive purpose. Default value is 1.', 'w9sme-addon' ),
					'dependency'       => array(
						'element' => 'heading_subtitle_responsive_title_size',
						'value'   => '1'
					),
					'group'            => __( 'Responsive', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'             => 'number',
					'heading'          => esc_html__( 'Sub-title minimum font size', 'w9sme-addon' ),
					'param_name'       => 'heading_subtitle_minimum_size',
					'value'            => '',
					'description'      => esc_html__( 'Enter minimum font-size for the heading title (Unit: px). Default value is 20.', 'w9sme-addon' ),
					'dependency'       => array(
						'element' => 'heading_subtitle_responsive_title_size',
						'value'   => '1'
					),
					'group'            => __( 'Responsive', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				// responsive text align
				array(
					'type'             => 'dropdown',
					'heading'          => __( 'Text align on tablet devices', 'w9-w9sme-options' ),
					'param_name'       => 'heading_text_align_on_tablet',
					'value'            => array(
						__( 'Inherit Form The Larger Size', 'w9sme-addon' ) => '',
						__( 'Left', 'w9sme-addon' )                         => 'text-left-on-tablet',
						__( 'Center', 'w9sme-addon' )                       => 'text-center-on-tablet',
						__( 'Right', 'w9sme-addon' )                        => 'text-right-on-tablet',
						__( 'Justify', 'w9sme-addon' )                      => 'text-justify-on-tablet',
					),
					'std'              => '',
					'description'      => esc_html__( 'Screen width from 480px to 991px.', 'w9sme-addon' ),
					'group'            => __( 'Responsive', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				array(
					'type'             => 'dropdown',
					'heading'          => __( 'Text align on mobile devices', 'w9-w9sme-options' ),
					'param_name'       => 'heading_text_align_on_mobile',
					'value'            => array(
						__( 'Inherit Form The Larger Size', 'w9sme-addon' ) => '',
						__( 'Left', 'w9sme-addon' )                         => 'text-left-on-mobile',
						__( 'Center', 'w9sme-addon' )                       => 'text-center-on-mobile',
						__( 'Right', 'w9sme-addon' )                        => 'text-right-on-mobile',
						__( 'Justify', 'w9sme-addon' )                      => 'text-justify-on-mobile',
					),
					'std'              => '',
					'description'      => esc_html__( 'Screen width smaller than 480px.', 'w9sme-addon' ),
					'group'            => __( 'Responsive', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column'
				),
				
				W9sme_Map_Helpers::extra_class(),
				W9sme_Map_Helpers::design_options(),
				W9sme_Map_Helpers::design_options_on_tablet(),
				W9sme_Map_Helpers::design_options_on_mobile(),
				W9sme_Map_Helpers::animation_css(),
				W9sme_Map_Helpers::animation_duration(),
				W9sme_Map_Helpers::animation_delay()
			)
		);
	}
}

if ( ! function_exists( 'w9sme_sc_heading' ) ) {
	function w9sme_sc_heading( array $attr = array() ) {
		$var = '';
		foreach ( $attr as $key => $value ) {
			$var .= sprintf( ' %s="%s"', $key, $value );
		}
		
		echo do_shortcode( "[w9sme_shortcode_heading $var]" );
	}
	
}