<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-blog-posts-map.php
 * @time    : 4/5/2017 7:33 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


$slider_params = vc_map_integrate_shortcode( W9sme_SC_Slider_Container::SC_BASE, '', __( 'Slider', 'w9sme-addon' ), array(
	'exclude' => array(
		'css',
		'el_class',
		'animation_css',
		'animation_duration',
		'animation_delay',
		'tablet_css',
		'mobile_css',
	),
	// we need only type, icon_fontawesome, icon_blabla..., NOT color and etc
), array(
	'element' => 'posts_layout_type',
	'value'   => 'layout-slider',
) );
$category      = array();
$categories    = get_categories();
if ( is_array( $categories ) ) {
	foreach ( $categories as $cat ) {
		$category[ $cat->slug ] = $cat->name;
	}
}
//var_dump($category);

vc_map(
	array(
		'name'           => esc_html__( 'W9sme Blog Posts', 'w9sme-addon' ),
		'base'           => W9sme_SC_Blog_Posts::SC_BASE,
		'icon'           => 'dashicons-before dashicons-admin-post',
		'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
		'php_class_name' => 'W9sme_SC_Blog_Posts',
		'params'         => array_merge(
			array(
				array(
					'type'       => 'multi-select',
					'heading'    => esc_html__( 'Categories', 'w9sme-addon' ),
					'param_name' => 'posts_category',
					'options'    => $category
				),
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Style', 'w9sme-addon' ),
					'param_name'  => 'posts_style',
					'value'       => array(
						__( 'Style 1', 'w9sme-addon' )               => 'style-1',
						__( 'Style 2', 'w9sme-addon' )               => 'style-2',
						__( 'Style 2 (Variability)', 'w9sme-addon' ) => 'style-2v',
						__( 'Style 3', 'w9sme-addon' )               => 'style-3',
						__( 'Style 4', 'w9sme-addon' )               => 'style-4',
					),
					'description' => __( 'Select the posts style', 'w9sme-addon' ),
					'admin_label' => true
				),
				
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Layout type', 'w9sme-addon' ),
					'param_name'  => 'posts_layout_type',
					'value'       => array(
						__( 'Grid', 'w9sme-addon' )   => 'layout-grid',
						__( 'Slider', 'w9sme-addon' ) => 'layout-slider',
					),
					'description' => __( 'Select the layout type for the posts.', 'w9sme-addon' ),
					'admin_label' => true
				),
				
				array(
					'type'       => 'checkbox',
					'heading'    => __( 'Equal height items ?', 'w9sme-addon' ),
					'param_name' => 'posts_equal_height_items',
					'value'      => array( esc_html__( 'Yes', 'w9sme-addon' ) => 'yes' ),
					'std'        => 'yes'
				),
				
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Sort order', 'w9sme-addon' ),
					'param_name'  => 'posts_sort_order',
					'value'       => array(
						__( 'Random', 'w9sme-addon' )  => 'random',
						__( 'Popular', 'w9sme-addon' ) => 'popular',
						__( 'Recent', 'w9sme-addon' )  => 'recent',
						__( 'Oldest', 'w9sme-addon' )  => 'oldest'
					),
					'std'         => 'recent',
					'description' => __( 'Select sorting order . ', 'w9sme-addon' ),
				),
				
				array(
					'type'        => 'number',
					'heading'     => __( 'Total items', 'w9sme-addon' ),
					'param_name'  => 'posts_total_items',
					'std'         => '3',
					'description' => __( 'Set max limit for items in grid or enter - 1 to display all', 'w9sme-addon' ),
					'admin_label' => true
				),
				
				array(
					'type'        => 'number',
					'heading'     => __( 'Column', 'w9sme-addon' ),
					'param_name'  => 'posts_column',
					'min'         => '1',
					'max'         => '4',
					'std'         => '3',
					'description' => __( 'Set the column numbers( min: 1, max: 4)', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'posts_layout_type',
						'value'   => array( 'layout-grid' ),
					),
				),
				
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Image size', 'w9sme-addon' ),
					'param_name'  => 'posts_image_size',
					'value'       => get_intermediate_image_sizes(),
					'std'         => 'w9sme_570',
					'description' => __( 'Select image size from list.', 'w9sme-addon' ),
					'group'       => __( 'Meta', 'w9sme-addon' ),
//					'dependency'  => array(
//						'element' => 'source',
//						'value'   => array( 'media_library', 'featured_image' ),
//					),
				),
				array(
					'type'        => 'dropdown',
					'heading'     => __( 'Image ratio', 'w9sme-addon' ),
					'description' => __( 'Image ratio base on image size width . ', 'w9sme-addon' ),
					'param_name'  => 'posts_image_ratio',
					'value'       => wp_parse_args( array( __( 'Original', 'w9sme-addon' ) => 'original' ), W9sme_Image::get_w9sme_ratio_list() ),
					'group'       => __( 'Meta', 'w9sme-addon' ),
					'std'         => '0.666666667'
				),
				
				array(
					'type'             => 'switcher',
					'heading'          => __( 'Show author ? ', 'w9sme-addon' ),
					'param_name'       => 'posts_show_author',
					'std'              => '1',
					'group'            => __( 'Meta', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
				),
				
				array(
					'type'             => 'switcher',
					'heading'          => __( 'Show date ? ', 'w9sme-addon' ),
					'param_name'       => 'posts_show_date',
					'std'              => '1',
					'group'            => __( 'Meta', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
				),
				
				array(
					'type'             => 'switcher',
					'heading'          => __( 'Show category ? ', 'w9sme-addon' ),
					'param_name'       => 'posts_show_category',
					'std'              => '1',
					'group'            => __( 'Meta', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
				),
				
				array(
					'type'             => 'switcher',
					'heading'          => __( 'Show excerpt ? ', 'w9sme-addon' ),
					'param_name'       => 'posts_show_excerpt',
					'std'              => '1',
					'group'            => __( 'Meta', 'w9sme-addon' ),
					'edit_field_class' => 'vc_col-sm-6 vc_column',
//					'description' => __( 'Show excerpt for posts . ', 'w9sme-addon' )
				),
				array(
					'type'        => 'number',
					'heading'     => __( 'Excerpt length (max words)', 'w9sme-addon' ),
					'param_name'  => 'posts_excerpt_length',
					'group'       => __( 'Meta', 'w9sme-addon' ),
					'std'         => '50',
					'description' => __( 'Limit the excerpt length (words) . Note: this value must be less than the value of the Excerpt length in Theme Options (Theme Options > Blog Archive).', 'w9sme-addon' ),
					'dependency'  => array(
						'element' => 'posts_show_excerpt',
						'value'   => array( '1' ),
					),
				),
			),
			$slider_params,
			array(
				W9sme_Map_Helpers::extra_class(),
				W9sme_Map_Helpers::design_options(),
				W9sme_Map_Helpers::animation_css(),
				W9sme_Map_Helpers::animation_duration(),
				W9sme_Map_Helpers::animation_delay()
			)
		),
	)
);