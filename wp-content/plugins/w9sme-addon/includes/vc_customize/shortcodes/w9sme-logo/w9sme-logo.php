<?php

/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-logo.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Logo extends WPBakeryShortCode {
    const SC_BASE = 'w9sme_shortcode_logo';
    
    public static function map() {
        return array(
            'name'           => __( 'W9sme Logo', 'w9sme-addon' ),
            'base'           => W9sme_SC_Logo::SC_BASE,
            'class'          => '',
            'icon'           => 'w9 w9-ico-software-transform-bezier',
            'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
            'description'    => __('Get site logo (upload in theme option)', 'w9sme-addon' ),
            'php_class_name' => 'W9sme_SC_Logo',
            'params'         => array(
                array(
                    'type'        => 'dropdown',
                    'heading'     => __('Choose logo source', 'w9sme-addon' ),
                    'param_name'  => 'logo_source',
                    'description' => 'Choose logo is defined in theme option or redefine <a href="' . admin_url( 'page=theme_options&tab=7' ) . '">here</a>.',
                    'value'       => array(
                        __( 'Logo', 'w9sme-addon' )          => 'logo',
                        __( 'Logo Option 1', 'w9sme-addon' ) => 'logo-option-1',
                        __( 'Logo Option 2', 'w9sme-addon' ) => 'logo-option-2',
                        __( 'Logo Option 3', 'w9sme-addon' ) => 'logo-option-3',
                    ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Logo width', 'w9sme-addon' ),
                    'description' => __( 'Define logo width in px , can fill number like 100, 200, 240 or auto.', 'w9sme-addon' ),
                    'param_name' => 'logo_width',
                    'std' => 'auto',
                    'edit_field_class' => 'vc_col-sm-6'
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __( 'Logo height', 'w9sme-addon' ),
                    'description' => __( 'Define logo width in px , can fill number like 100, 200, 240 or auto.', 'w9sme-addon' ),
                    'param_name' => 'logo_height',
                    'std' => 'auto',
                    'edit_field_class' => 'vc_col-sm-6'
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __( 'Logo align', 'w9sme-addon' ),
                    'description' => __( 'Define logo align left, right or center.', 'w9sme-addon' ),
                    'param_name' => 'logo_align',
                    'std' => '__',
                    'value' => array(
                        __( 'Inherit From Text Align', 'w9sme-addon' ) => '__',
                        __( 'Left', 'w9sme-addon' )  => 'text-left',
                        __( 'Right ', 'w9sme-addon' )  => 'text-right',
                        __( 'Center', 'w9sme-addon' )  => 'text-center',
                    ),
                    'edit_field_class' => 'vc_col-sm-6'
                ),
                W9sme_Map_Helpers::extra_class(),
                W9sme_Map_Helpers::design_options(),
                W9sme_Map_Helpers::animation_css(),
                W9sme_Map_Helpers::animation_duration(),
                W9sme_Map_Helpers::animation_delay(),
            )
        );
    }
}