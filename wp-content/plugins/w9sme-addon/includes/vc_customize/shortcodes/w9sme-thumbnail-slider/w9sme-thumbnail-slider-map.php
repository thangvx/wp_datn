<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-thumbnail-slider-map.php
 * @time    : 12/13/2016 4:47 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map( array(
	'name'           => __( 'W9sme Thumbnail Slider', 'w9sme-addon' ),
	'base'           => W9sme_SC_Thumbnail_Slider::SC_BASE,
	'php_class_name' => 'W9sme_SC_Thumbnail_Slider',
	'icon'           => 'w9 w9-ico-pictures',
	'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_PORTFOLIO_SC_CATEGORY ),
	'description'    => __( 'CPT Service and CPT Portfolio only!', 'w9sme-addon' ),
//	'post_type'      => W9sme_CPT_Portfolio::CPT_SLUG,
	'params'         => array(
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Image Source', 'w9sme-addon' ),
			'param_name'  => 'image_source',
			'value'       => array(
				__( 'Portfolio Gallery', 'w9sme-addon' ) => 'portfolio-gallery',
				__( 'Service Gallery', 'w9sme-addon' )   => 'service-gallery',
				__( 'Custom', 'w9sme-addon' )            => 'custom',
			),
			'std'         => 'custom',
			'admin_label' => true,
		),
		
		array(
			'type'        => 'attach_images',
			'heading'     => __( 'Images', 'w9sme-addon' ),
			'param_name'  => 'custom_images',
			'admin_label' => true,
			'value'       => '',
			'description' => __( 'Select images from media library.', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 'image_source',
				'value'   => array( 'custom' ),
			),
		),
		
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Slider 1 - Image size', 'w9sme-addon' ),
			'param_name'  => 's1_img_size',
			'value'       => wp_parse_args( array( __( 'Custom', 'w9sme-addon' ) => 'custom' ), get_intermediate_image_sizes() ),
			'std'         => 'w9sme_1170',
			'description' => __( 'Select image size from list.', 'w9sme-addon' ),
		),
		array(
			'type'        => 'textfield',
			'heading'     => __( 'Image size', 'w9sme-addon' ),
			'param_name'  => 's1_img_size_custom',
			'std'         => '1280x720',
			'description' => __( 'Enter image size in pixels. Example: 200x100 (Width x Height).', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 's1_img_size',
				'value'   => array( 'custom' ),
			),
		),
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Slider 1 - Image ratio', 'w9sme-addon' ),
			'description' => __( 'Image ratio base on image size width.', 'w9sme-addon' ),
			'param_name'  => 's1_image_ratio',
			'value'       => wp_parse_args( array( __( 'Original', 'w9sme-addon' ) => 'original' ), W9sme_Image::get_w9sme_ratio_list() ),
			'std'         => '0.5'
		),
		
		array(
			'type'        => 'switcher',
			'heading'     => __( 'Enable slider 2', 'w9sme-addon' ),
			'param_name'  => 's2_enable',
			'std'         => '1',
			'admin_label' => true
		),
		
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Slider 2 - Image size', 'w9sme-addon' ),
			'param_name'  => 's2_img_size',
			'value'       => wp_parse_args( array( __( 'Custom', 'w9sme-addon' ) => 'custom' ), get_intermediate_image_sizes() ),
			'std'         => 'w9sme_270',
			'description' => __( 'Select image size from list.', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 's2_enable',
				'value'   => array( '1' ),
			),
		),
		array(
			'type'        => 'textfield',
			'heading'     => __( 'Image size', 'w9sme-addon' ),
			'param_name'  => 's2_img_size_custom',
			'std'         => '320x180',
			'description' => __( 'Enter image size in pixels. Example: 200x100 (Width x Height).', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 's2_img_size',
				'value'   => array( 'custom' ),
			),
		),
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Slider 2 - Image ratio', 'w9sme-addon' ),
			'description' => __( 'Image ratio base on image size width.', 'w9sme-addon' ),
			'param_name'  => 's2_image_ratio',
			'value'       => wp_parse_args( array( __( 'Original', 'w9sme-addon' ) => 'original' ), W9sme_Image::get_w9sme_ratio_list() ),
			'std'         => '0.5625',
			'dependency'  => array(
				'element' => 's2_enable',
				'value'   => array( '1' ),
			),
		),
		W9sme_Map_Helpers::extra_class(),
		W9sme_Map_Helpers::design_options(),
		W9sme_Map_Helpers::animation_css(),
		W9sme_Map_Helpers::animation_duration(),
		W9sme_Map_Helpers::animation_delay()
	)
) );