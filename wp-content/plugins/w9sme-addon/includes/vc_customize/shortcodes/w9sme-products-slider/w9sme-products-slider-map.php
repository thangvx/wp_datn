<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-products-slider-map.php
 * @time    : 7/16/2017 2:34 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

//vc_map( array(
//	'name'           => esc_html__( 'W9sme Products Slider', 'w9sme-addon' ),
//	'base'           => W9sme_SC_Products_Slider::SC_BASE,
//	'class'          => '',
//	'icon'           => 'w9 w9-ico-shopper29',
//	'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
//	'php_class_name' => 'W9sme_SC_Products_Slider',
////	'description'    => __( 'Create team member with social profile', 'w9sme-addon' ),
//	'params'         => array(
//		W9sme_Map_Helpers::design_options(),
//		W9sme_Map_Helpers::animation_css(),
//		W9sme_Map_Helpers::animation_duration(),
//		W9sme_Map_Helpers::animation_delay(),
//		W9sme_Map_Helpers::extra_class()
//	)
//) );

// Important: Visit wc-vc-shortcodes-map.php for the map file.