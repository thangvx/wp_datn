<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-counter-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map( array(
	'name'           => __( 'W9sme Counter', 'w9sme-addon' ),
	'base'           => W9sme_SC_Counter::SC_BASE,
	'class'          => '',
	'icon'           => 'w9 w9-ico-basic-calculator',
	'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
	'description'    => __( 'Make couter group to count anything you want', 'w9sme-addon' ),
	'php_class_name' => 'W9sme_SC_Counter',
	'params'         => array(
		
		array(
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Style', 'w9sme-addon' ),
			'param_name'       => 'style',
			'value'            => array(
				__( 'Standard', 'w9sme-addon' )         => 'style1',
				__( 'No Icon', 'w9sme-addon' )     => 'style2',
			),
			'std'              => 'style1',
		),
		
		array(
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Icon position', 'w9sme-addon' ),
			'param_name'       => 'icon_position',
			'value'            => array(
				__( 'Left', 'w9sme-addon' )         => 'icon-pos-left',
				__( 'Right', 'w9sme-addon' )     => 'icon-pos-right',
				__( 'Top', 'w9sme-addon' )     => 'icon-pos-top',
			),
			'std'              => 'icon-pos-left',
			'dependency' => array(
				'element' => 'style',
				'value'   => 'style1'
			),
		),
		
		array(
			'type'       => 'param_group',
			'heading'    => __( 'Counters value', 'w9sme-addon' ),
			'param_name' => 'values',
			'value'      => urlencode( json_encode( array(
				array(
					'title' => __( 'HAPPY CLIENTS', 'w9sme-addon' ),
					'from'  => '0',
					'to'    => '2316',
					'type'  => 'fontawesome',
					'icon_fontawesome' => 'fa fa-user',
					'description' => ''
				),
				array(
					'title' => __( 'PROJECTS', 'w9sme-addon' ),
					'from'  => '0',
					'to'    => '485',
					'type'  => 'fontawesome',
					'icon_fontawesome' => 'fa fa-terminal',
					'description' => ''
				),
				array(
					'title' => __( 'THREAD DETECTED', 'w9sme-addon' ),
					'from'  => '0',
					'to'    => '716',
					'type'  => 'fontawesome',
					'icon_fontawesome' => 'fa fa-exclamation-circle',
					'description' => ''
				),
				array(
					'title' => __( 'PROVIDED SERVICES', 'w9sme-addon' ),
					'from'  => '0',
					'to'    => '1526',
					'type'  => 'w9sme',
					'icon_w9sme' => 'flor-ico flor-ico-icon-easel-alt',
					'description' => ''
				),
			) ) ),
			'params'     => array(
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Title', 'w9sme-addon' ),
					'param_name'  => 'title',
					'description' => __( 'Enter title for counter.', 'w9sme-addon' ),
					'admin_label' => true,
					'std'         => __( 'Counter title', 'w9sme-addon' ),
				),
				array(
					'type'        => 'number',
					'heading'     => __( 'From', 'w9sme-addon' ),
					'description' => __( 'Begin value of counter.', 'w9sme-addon' ),
					'param_name'  => 'from',
					'admin_label' => true,
				),
				array(
					'type'        => 'number',
					'heading'     => __( 'To', 'w9sme-addon' ),
					'description' => __( 'Finish value of counter.', 'w9sme-addon' ),
					'param_name'  => 'to',
					'admin_label' => true,
				),
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Prefix', 'w9sme-addon' ),
					'param_name'  => 'prefix',
					'description' => __( 'Enter title prefix for counter.', 'w9sme-addon' ),
				),
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Suffix', 'w9sme-addon' ),
					'param_name'  => 'suffix',
					'description' => __( 'Enter title suffix for counter.', 'w9sme-addon' ),
				),
				W9sme_Map_Helpers::get_icons_picker_type(),
				W9sme_Map_Helpers::get_icon_picker_9wpthemes( '', false ),
				W9sme_Map_Helpers::get_icon_picker_w9sme( '', false ),
				W9sme_Map_Helpers::get_icon_picker_fontawesome( '', false ),
				W9sme_Map_Helpers::get_icon_picker_openiconic( '', false ),
				W9sme_Map_Helpers::get_icon_picker_typicons( '', false ),
				W9sme_Map_Helpers::get_icon_picker_entypo( '', false ),
				W9sme_Map_Helpers::get_icon_picker_linecons( '', false ),
				W9sme_Map_Helpers::get_icon_picker_monosocial( '', false ),
				array(
					'type'        => 'textarea',
					'heading'     => __( 'Description', 'w9sme-addon' ),
					'param_name'  => 'description',
					'description' => __( 'Enter description for counter.', 'w9sme-addon' ),
				),
			
			),
		),
		//Number counter in row
		array_merge( W9sme_Map_Helpers::item_width('vc_col-sm-3'), array( 'group' => __( 'Responsive Options', 'w9sme-addon' ) ) ),
		W9sme_Map_Helpers::responsive_item_width(),
		//Counter Time
		array(
			'type'        => 'number',
			'heading'     => __( 'Count time', 'w9sme-addon' ),
			'description' => __( 'Time length to run counter.', 'w9sme-addon' ),
			'param_name'  => 'time',
			'admin_label' => true,
			'std'         => '2.5'
		),
		
		array(
			'type'             => 'dropdown',
			'heading'          => esc_html__( 'Content text align', 'w9sme-addon' ),
			'param_name'       => 'text_align',
			'value'            => array(
				__( 'Inherit', 'w9sme-addon' )   => 'text-__',
				__( 'Left', 'w9sme-addon' )   => 'text-left',
				__( 'Center', 'w9sme-addon' ) => 'text-center',
				__( 'Right', 'w9sme-addon' )  => 'text-right',
			),
			'description'      => esc_html__( 'Select the counter content text alignment.', 'w9sme-addon' ),
			'std'              => 'text-center',
			'admin_label' => true,
		),
		
		// icon
		
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Icon color', 'w9sme-addon' ),
			'param_holder_class' => 'vc_colored-dropdown',
			'param_name'         => 'icon_color',
			'value'              => W9sme_Map_Helpers::get_colors(),
			'description'        => __( 'Select color for the icon.', 'w9sme-addon' ),
			'std'                => 's',
			'dependency'         => array(
				'element' => 'style',
				'value'   => 'style1'
			),
		),
		
		
		array(
			'type'       => 'colorpicker',
			'heading'    => __( 'Custom icon color', 'w9sme-addon' ),
			'param_name' => 'custom_icon_color',
			'value'      => '',
			'dependency' => array(
				'element' => 'icon_color',
				'value'   => 'custom'
			),
		),
		
		// number
		
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Number color', 'w9sme-addon' ),
			'param_holder_class' => 'vc_colored-dropdown',
			'param_name'         => 'number_color',
			'value'              => W9sme_Map_Helpers::get_colors(),
			'description'        => __( 'Select color for the number.', 'w9sme-addon' ),
			'std'                => 'p',
		),
		
		
		array(
			'type'       => 'colorpicker',
			'heading'    => __( 'Custom number color', 'w9sme-addon' ),
			'param_name' => 'custom_number_color',
			'value'      => '',
			'dependency' => array(
				'element' => 'number_color',
				'value'   => 'custom'
			),
		),
		
		// title
		
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Title color', 'w9sme-addon' ),
			'param_holder_class' => 'vc_colored-dropdown',
			'param_name'         => 'title_color',
			'value'              => W9sme_Map_Helpers::get_colors(),
			'description'        => __( 'Select color for your title.', 'w9sme-addon' ),
			'std'                => '__',
			'admin_label' => true,
		),
		
		
		array(
			'type'       => 'colorpicker',
			'heading'    => __( 'Custom title color', 'w9sme-addon' ),
			'param_name' => 'custom_title_color',
			'value'      => '',
			'dependency' => array(
				'element' => 'title_color',
				'value'   => 'custom'
			),
		),
		
		W9sme_Map_Helpers::extra_class(),
		W9sme_Map_Helpers::design_options(),
		W9sme_Map_Helpers::animation_css(),
		W9sme_Map_Helpers::animation_duration(),
		W9sme_Map_Helpers::animation_delay()
	)
) );