<?php

/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-counter.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Counter extends WPBakeryShortCode {
    const SC_BASE = 'w9sme_shortcode_counter';
}
