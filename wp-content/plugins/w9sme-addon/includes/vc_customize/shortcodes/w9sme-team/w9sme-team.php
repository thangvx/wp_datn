<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-team.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Team extends WPBakeryShortCode {
    const SC_BASE = 'w9sme_shortcode_team';

    public static function get_team_social_list() {
        return array(
            'facebook'       => array(
                'label' => __( 'Facebook', 'w9sme-addon' ),
                'icon'  => 'fa fa-facebook'
            ),
            'twitter'        => array(
                'label' => __( 'Twitter', 'w9sme-addon' ),
                'icon'  => 'fa fa-twitter'
            ),
            'dribbble'       => array(
                'label' => __( 'Dribbble', 'w9sme-addon' ),
                'icon'  => 'fa fa-dribbble'
            ),
            'vimeo'          => array(
                'label' => __( 'Vimeo', 'w9sme-addon' ),
                'icon'  => 'fa fa-vimeo-square'
            ),
            'tumblr'         => array(
                'label' => __( 'Tumblr', 'w9sme-addon' ),
                'icon'  => 'fa fa-tumblr'
            ),
            'skype_username' => array(
                'label' => __( 'Skype', 'w9sme-addon' ),
                'icon'  => 'fa fa-skype'
            ),
            'linkedin'       => array(
                'label' => __( 'LinkedIn', 'w9sme-addon' ),
                'icon'  => 'fa fa-linkedin'
            ),
            'googleplus'     => array(
                'label' => __( 'Google+', 'w9sme-addon' ),
                'icon'  => 'w9 w9-ico-gplus'
            ),
            'flickr'         => array(
                'label' => __( 'Flickr', 'w9sme-addon' ),
                'icon'  => 'fa fa-flickr'
            ),
            'youtube'        => array(
                'label' => __( 'YouTube', 'w9sme-addon' ),
                'icon'  => 'fa fa-youtube'
            ),
            'pinterest'      => array(
                'label' => __( 'Pinterest', 'w9sme-addon' ),
                'icon'  => 'fa fa-pinterest'
            ),
            'foursquare'     => array(
                'label' => __( 'Foursquare', 'w9sme-addon' ),
                'icon'  => 'fa fa-foursquare'
            ),
            'instagram'      => array(
                'label' => __( 'Instagram', 'w9sme-addon' ),
                'icon'  => 'fa fa-instagram'
            ),
            'github'         => array(
                'label' => __( 'GitHub', 'w9sme-addon' ),
                'icon'  => 'fa fa-github'
            ),
            'xing'           => array(
                'label' => __( 'Xing', 'w9sme-addon' ),
                'icon'  => 'fa fa-xing'
            ),
            'behance'        => array(
                'label' => __( 'Behance', 'w9sme-addon' ),
                'icon'  => 'fa fa-behance'
            ),
            'deviantart'     => array(
                'label' => __( 'Deviantart', 'w9sme-addon' ),
                'icon'  => 'fa fa-deviantart'
            ),
            'soundcloud'     => array(
                'label' => __( 'SoundCloud', 'w9sme-addon' ),
                'icon'  => 'fa fa-soundcloud'
            ),
            'yelp'           => array(
                'label' => __( 'Yelp', 'w9sme-addon' ),
                'icon'  => 'fa fa-yelp'
            ),
            'rss'            => array(
                'label' => __( 'RSS Feed', 'w9sme-addon' ),
                'icon'  => 'fa fa-rss'
            )
        );
    }
}
