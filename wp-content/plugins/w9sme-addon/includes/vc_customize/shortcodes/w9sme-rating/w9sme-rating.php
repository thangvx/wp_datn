<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-rating.php
 * @time    : 7/8/2017 11:37 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class W9sme_SC_Rating extends WPBakeryShortCode {
	const SC_BASE = 'w9sme_shortcode_rating';
}
