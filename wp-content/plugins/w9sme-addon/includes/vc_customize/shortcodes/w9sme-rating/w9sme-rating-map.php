<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-rating-map.php
 * @time    : 7/8/2017 11:38 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map( array(
	'name'           => esc_html__( 'W9sme Rating', 'w9sme-addon' ),
	'base'           => W9sme_SC_Rating::SC_BASE,
	'class'          => '',
	'icon'           => 'fa fa-star',
	'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
	'php_class_name' => 'W9sme_SC_Rating',
//	'description'    => __( 'Create team member with social profile', 'w9sme-addon' ),
	'params'         => array(
		array(
			'type'        => 'slider',
			'heading'     => __( 'Rating', 'w9sme-addon' ),
			'param_name'  => 'rating',
			'min'         => '1',
			'max'         => '5',
			'step'        => '0.1',
			'std'         => '5',
			'description' => __( 'Choose rating' )
		),
		
		array(
			'type'        => 'number',
			'heading'     => esc_html__( 'Star size', 'w9sme-addon' ),
			'param_name'  => 'star_size',
			'value'       => '15',
			'description' => esc_html__( 'Enter icon size (Unit: px, Default: 15). ', 'w9sme-addon' ),
		),
		
		array(
			'type'       => 'colorpicker',
			'param_name' => 'star_color',
			'heading'    => __( 'Star color', 'w9sme-addon' ),
			'std'        => '#ddd'
		),
		
		array(
			'type'       => 'dropdown',
			'param_name' => 'star_rated_color',
			'heading'    => __( 'Star (rated) color', 'w9sme-addon' ),
			'value'      => array_merge( array(
				__( 'Default CSS', 'w9sme-addon' )  => '__',
				__( 'Custom Color', 'w9sme-addon' ) => 'custom',
			), W9sme_Map_Helpers::get_just_colors() ),
			'param_holder_class' => 'vc_colored-dropdown',
			'std'        => 'p'
		),
		
		array(
			'type'       => 'colorpicker',
			'param_name' => 'star_rated_color_cp',
			'dependency' => array(
				'element' => 'star_rated_color',
				'value'   => 'custom'
			),
			'std'        => ''
		),
		
		W9sme_Map_Helpers::design_options(),
		W9sme_Map_Helpers::animation_css(),
		W9sme_Map_Helpers::animation_duration(),
		W9sme_Map_Helpers::animation_delay(),
		W9sme_Map_Helpers::extra_class()
	)
) );


