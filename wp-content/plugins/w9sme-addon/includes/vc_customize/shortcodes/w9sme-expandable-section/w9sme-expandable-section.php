<?php

/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-expandable-section.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Expandable_Section extends WPBakeryShortCodesContainer {
    const SC_BASE = 'w9sme_shortcode_expandable_section';
}