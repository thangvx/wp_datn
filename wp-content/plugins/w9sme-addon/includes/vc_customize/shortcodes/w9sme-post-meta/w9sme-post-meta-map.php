<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-post-meta-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

vc_map( array(
    'name'           => __( 'W9sme Post Meta', 'w9sme-addon' ),
    'base'           => W9sme_SC_Post_Meta::SC_BASE,
    'php_class_name' => 'W9sme_SC_Post_Meta',
    'icon'           => 'w9 w9-ico-basic-info',
    'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
    'description'    => __( 'Get common meta info from single post, page and other single custom post type', 'w9sme-addon' ),
    'params'         => array(
        array(
            'type'        => 'param_group',
            'heading'     => __( 'Info fields', 'w9sme-addon' ),
            'param_name'  => 'info_list',
            'description' => __( 'Select info use in this shortcode.', 'w9sme-addon' ),
            'value'       => urlencode( json_encode( array(
                array(
                    'info'       => 'date',
                    'info_label' => __( 'Posted at:', 'w9sme-addon' ),
                ),
                array(
                    'info'       => 'categories',
                    'info_label' => __( 'Posted in:', 'w9sme-addon' ),
                )
            ) ) ),
            'params'      => array(
                array(
                    'type'        => 'dropdown',
                    'heading'     => __( 'Info', 'w9sme-addon' ),
                    'param_name'  => 'info',
                    'admin_label' => true,
                    'value'       => array(
                        __( 'Date', 'w9sme-addon' )       => 'date',
                        __( 'Author', 'w9sme-addon' )     => 'author',
                        __( 'Categories', 'w9sme-addon' ) => 'categories',
                        __( 'Tags', 'w9sme-addon' )       => 'tags',
                        __( 'Comments', 'w9sme-addon' )   => 'comments',
                    ),
                ),
                array(
                    'type'       => 'textfield',
                    'heading'    => __( 'Info label', 'w9sme-addon' ),
                    'param_name' => 'info_label',
                    'std'        => ''
                ),
                array(
                    'type'        => 'textfield',
                    'heading'     => __( 'Date format', 'w9sme-addon' ),
                    'param_name'  => 'date_format',
                    'description' => sprintf( __( 'Config the date format, leave blank to use default. Read the %s for more information', 'w9sme-addon' ), '<a href="https://codex.wordpress.org/Formatting_Date_and_Time" target="_blank">Wordpress Codex</a>' ),
                    'dependency'  => array(
                        'element' => 'info',
                        'value'   => array( 'date' )
                    )
                ),
                array(
                    'type'             => 'number',
                    'heading'          => __( 'Number of items to show', 'w9sme-addon' ),
                    'param_name'       => 'number_items',
                    'description'      => __( 'Config number of items to show, leave blank to show all.', 'w9sme-addon' ),
                    'dependency'       => array(
                        'element' => 'info',
                        'value'   => array( 'categories', 'tags' )
                    ),
                    'edit_field_class' => 'vc_col-sm-6 vc_column'
                ),

                array(
                    'type'             => 'textfield',
                    'param_name'       => 'item_separator',
                    'heading'          => __( 'Items separator' , 'w9sme-addon' ),
                    'std'              => ', ',
                    'description'      => __( 'Enter the separator between each items. Default is ", ".' , 'w9sme-addon' ),
                    'dependency'       => array(
                        'element' => 'info',
                        'value'   => array( 'categories', 'tags' )
                    ),
                    'edit_field_class' => 'vc_col-sm-6 vc_column'
                ),
            ),
        ),
        array(
            'type'             => 'textfield',
            'param_name'       => 'meta_separator',
            'heading'          => __( 'Info separator' , 'w9sme-addon' ),
            'std'              => '/',
            'description'      => __( 'Enter the separator between each info.' , 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'admin_label'      => true
        ),
        
        array(
            'type'             => 'dropdown',
            'param_name'       => 'meta_font_family',
            'heading'          => __( 'Font family' , 'w9sme-addon' ),
            'value'            => array(
                __( 'Inherit', 'w9sme-addon' )        => '',
                __( 'Primary Font', 'w9sme-addon' )   => 'p-font',
                __( 'Secondary Font', 'w9sme-addon' ) => 's-font',
                __( 'Third Font', 'w9sme-addon' )     => 't-font',
            ),
            'std'              => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'admin_label'      => true
        ),
        
        array(
            'type'             => 'dropdown',
            'param_name'       => 'meta_text_transform',
            'heading'          => __( 'Text transform' , 'w9sme-addon' ),
            'value'            => array(
                __( 'Initial', 'w9sme-addon' )    => 'text-initial',
                __( 'UPPERCASE', 'w9sme-addon' )  => 'text-uppercase',
                __( 'lowercase', 'w9sme-addon' )  => 'text-lowercase',
                __( 'Capitalize', 'w9sme-addon' ) => 'text-capitalize',
            ),
            'std'              => '',
            'edit_field_class' => 'vc_col-sm-6 vc_column'
        ),
        
        array(
            'type'             => 'dropdown',
            'param_name'       => 'meta_label_fw',
            'heading'          => __( 'Label font weight' , 'w9sme-addon' ),
            'value'            => array(
                __( 'Inherit', 'w9sme-addon' )         => 'fw-inherit',
                __( 'Light (300)', 'w9sme-addon' )     => 'fw-light',
                __( 'Regular (400)', 'w9sme-addon' )   => 'fw-regular',
                __( 'Medium (500)', 'w9sme-addon' )    => 'fw-medium',
                __( 'Semi Bold (600)', 'w9sme-addon' ) => 'fw-semibold',
                __( 'Bold (700)', 'w9sme-addon' )      => 'fw-bold',
            ),
            'std'              => 'fw-inherit',
            'edit_field_class' => 'vc_col-sm-6 vc_column'
        ),
        
        array(
            'type'             => 'dropdown',
            'param_name'       => 'meta_info_fw',
            'heading'          => __( 'Info font weight' , 'w9sme-addon' ),
            'value'            => array(
                __( 'Inherit', 'w9sme-addon' )         => 'fw-inherit',
                __( 'Light (300)', 'w9sme-addon' )     => 'fw-light',
                __( 'Regular (400)', 'w9sme-addon' )   => 'fw-regular',
                __( 'Medium (500)', 'w9sme-addon' )    => 'fw-medium',
                __( 'Semi Bold (600)', 'w9sme-addon' ) => 'fw-semibold',
                __( 'Bold (700)', 'w9sme-addon' )      => 'fw-bold',
            ),
            'std'              => 'fw-inherit',
            'edit_field_class' => 'vc_col-sm-6 vc_column'
        ),
        
        array(
            'type'             => 'dropdown',
            'heading'          => esc_html__( 'Font size', 'w9sme-addon' ),
            'param_name'       => 'meta_font_size',
            'value'            => array(
                esc_html__( 'Inherit', 'w9sme-addon' )     => '',
                '10px'                                         => '10',
                '11px'                                         => '11',
                '12px'                                         => '12',
                '13px'                                         => '13',
                '14px'                                         => '14',
                '15px'                                         => '15',
                '16px'                                         => '16',
                '17px'                                         => '17',
                '18px'                                         => '18',
                '19px'                                         => '19',
                '20px'                                         => '20',
                esc_html__( 'Custom Size', 'w9sme-addon' ) => 'custom',
            ),
            'std'              => 'fz-30',
            'description'      => esc_html__( 'Select font size.', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-6 vc_column',
            'admin_label'      => true
        ),
        
        array(
            'type'             => 'number',
            'heading'          => esc_html__( 'Custom font size', 'w9sme-addon' ),
            'param_name'       => 'meta_custom_size',
            'value'            => '',
            'description'      => esc_html__( 'Enter custom font-size (unit: px).', 'w9sme-addon' ),
            'dependency'       => array( 'element' => 'meta_font_size', 'value' => 'custom' ),
            'edit_field_class' => 'vc_col-sm-6 vc_column'
        ),
        
        array(
            'type'             => 'dropdown',
            'heading'          => __( 'Info items spacing', 'w9-w9sme' ),
            'description'      => esc_html__( 'Select spacing between each info. Not include the separator width.', 'w9sme-addon' ),
            'param_name'       => 'meta_spacing',
            'value'            => array(
                'None' => '0',
                '5px'  => '5',
                '10px' => '10',
                '15px' => '15',
                '20px' => '20',
                '25px' => '25',
                '30px' => '30',
            ),
            'std'              => '5',
            'edit_field_class' => 'vc_col-sm-6 vc_column'
        ),
        
        array(
            'type'        => 'switcher',
            'heading'     => esc_html__( 'Responsive font size', 'w9sme-addon' ),
            'param_name'  => 'responsive_size',
            'description' => esc_html__( 'Enable or disable font size responsive.', 'w9sme-addon' ),
            'value'       => '',
            'dependency'  => array( 'element' => 'meta_font_size', 'value_not_equal_to' => array( '' ) ),
            'group'       => __( 'Responsive', 'w9sme-addon' ),
        ),
        
        array(
            'type'             => 'number',
            'heading'          => esc_html__( 'Responsive compressor', 'w9sme-addon' ),
            'param_name'       => 'scale_ratio',
            'value'            => '',
            'description'      => esc_html__( 'Enter responsive compressor (etc: 1.2, 1.5, 0.6, 0.7...). This is for responsive purpose. Default value is 1.', 'w9sme-addon' ),
            'dependency'       => array( 'element' => 'responsive_size', 'value' => '1' ),
            'group'            => __( 'Responsive', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-6 vc_column'
        ),
        
        array(
            'type'             => 'number',
            'heading'          => esc_html__( 'Minimum font size', 'w9sme-addon' ),
            'param_name'       => 'minimum_size',
            'value'            => '',
            'description'      => esc_html__( 'Enter minimum font-size (unit px). Default value is 20..', 'w9sme-addon' ),
            'dependency'       => array( 'element' => 'responsive_size', 'value' => '1' ),
            'group'            => __( 'Responsive', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-6 vc_column'
        ),
        W9sme_Map_Helpers::extra_class(),
        W9sme_Map_Helpers::design_options(),
        W9sme_Map_Helpers::design_options_on_tablet(),
        W9sme_Map_Helpers::design_options_on_mobile(),
        W9sme_Map_Helpers::animation_css(),
        W9sme_Map_Helpers::animation_duration(),
        W9sme_Map_Helpers::animation_delay()
    )
) );