<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-current-post-content-map.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

//if ( ! defined( 'DOING_AJAX' ) ) {
//	return;
//}

//if ( ! post_type_exists( W9sme_CPT_Content_Template::CPT_SLUG )
//     || w9sme_get_current_post_type() !== W9sme_CPT_Content_Template::CPT_SLUG
//) {
//	return;
//}


vc_map( array(
	'name'           => esc_html__( 'W9sme Custom Post Content', 'w9sme-addon' ),
	'base'           => W9sme_SC_Current_Post_Content::SC_BASE,
	'class'          => '',
	'icon'           => 'w9 w9-ico-arrows-squares',
	'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
	'description'    => __( 'Support create layout by content template for service, portfolio, vacancy!', 'w9sme-addon' ),
	'post_type'      => array( W9sme_CPT_Content_Template::CPT_SLUG ),
	'php_class_name' => 'W9sme_SC_Current_Post_Content',
	'params'         => array(
//		array(
//			'type'        => 'dropdown',
//			'heading'     => __( 'Content template', 'w9sme-addon' ),
//			'param_name'  => 'template',
//			'admin_label' => true,
//			'value'       => $content_template_list,
//			'description' => __( 'Select the content template will be displayed in this section.', 'w9sme-addon' ),
//		),
		array(
			'type'        => 'hidden',
			'heading'     => __( 'Outside row', 'w9sme-addon' ),
			'param_name'  => 'outside_row',
			'description' => __( 'Make this content template outside the row.', 'w9sme-addon' ),
			'std'         => '1'
		),
	),
//	'js_view'        => 'W9smeContentTemplateView',
//	'admin_enqueue_js' => array( W9sme_Addon::plugin_url() . 'assets/js/vc-custom.js' ),
) );