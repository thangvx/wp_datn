<?php

/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-current-post-content.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}
class W9sme_SC_Current_Post_Content extends WPBakeryShortCode {
    const SC_BASE = 'w9sme_shortcode_current_post_content';
	
	public function check_recursive_call($post_content) {
		if (strpos($post_content, 'w9sme_shortcode_current_post_content') !== false) {
			return true;
		}
		
		$regex_content = '/\[w9sme_shortcode_content_template template="(.*?)"\](.*?)/';
		
		if ( preg_match_all( $regex_content, $post_content, $matches ) ) {
			if ( ! empty( $matches[1] ) && is_array( $matches[1] ) ) {
				$_matches = $matches[1];
				
				foreach ($_matches as $path) {
					$the_post = get_page_by_path( $path, OBJECT, W9sme_CPT_Content_Template::CPT_SLUG );
					
					if ($the_post instanceof WP_Post) {
						$is_recursive_call =  $this->check_recursive_call($the_post->post_content);
						
						if ($is_recursive_call) {
							return true;
						}
					}
				}
			}
		}
		
		return false;
	}
}
