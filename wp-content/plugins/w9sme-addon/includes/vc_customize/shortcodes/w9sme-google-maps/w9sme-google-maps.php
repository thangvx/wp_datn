<?php

/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-google-maps.php
 * @time    : 8/26/2016 12:39 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Google_Maps extends WPBakeryShortCode {
    const SC_BASE = 'w9sme_shortcode_google_maps';
}