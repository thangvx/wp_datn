<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-breadcrumb-map.php
 * @time    : 1/7/2017 11:58 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map(
	array(
		'name'           => esc_html__( 'W9sme Breadcrumb', 'w9sme-addon' ),
		'base'           => W9sme_SC_Breadcrumb::SC_BASE,
		'icon'           => 'w9 w9-ico-dot-3',
		'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
		'php_class_name' => 'W9sme_SC_Breadcrumb',
//		'description'    => __( 'W9sme widget download', 'w9sme-addon' ),
		'params'         => array(
//			array(
//				'type'       => 'textfield',
//				'param_name' => 'prepended_text',
//				'heading'    => __( 'Prepended text', 'w9sme-addon' ),
//				'std'        => __( 'You are here:', 'w9sme-addon' )
//			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'text_color',
				'heading'    => __( 'Text color', 'w9sme-addon' ),
				'value'      => array_merge( array(
					__( 'Default CSS', 'w9sme-addon' )  => '__',
					__( 'Custom Color', 'w9sme-addon' ) => 'custom',
				), W9sme_Map_Helpers::get_just_colors() ),
				'param_holder_class' => 'vc_colored-dropdown',
				'std'        => 'light'
			),
			
			array(
				'type'       => 'colorpicker',
				'param_name' => 'text_color_cp',
				'dependency' => array(
					'element' => 'text_color',
					'value'   => 'custom'
				),
				'std'        => ''
			),
			
			array(
				'type'       => 'colorpicker',
				'param_name' => 'background_color',
				'heading'    => __( 'Background color', 'w9sme-addon' ),
				'std'        => 'rgba(0, 0, 0, 0.4)'
			),
			
			array(
				'type'        => 'dropdown',
				'param_name'  => 'position',
				'admin_label' => true,
				'heading'     => __( 'Breadcrumbs position', 'w9sme-addon' ),
				'value'       => array(
					__( 'Left', 'w9sme-addon' )    => 'left',
					__( 'Right', 'w9sme-addon' )    => 'right'
				),
				'std'         => 'left'
			),
			W9sme_Map_Helpers::extra_class(),
			W9sme_Map_Helpers::design_options(),
			W9sme_Map_Helpers::animation_css(),
			W9sme_Map_Helpers::animation_duration(),
			W9sme_Map_Helpers::animation_delay()
		),
	)
);