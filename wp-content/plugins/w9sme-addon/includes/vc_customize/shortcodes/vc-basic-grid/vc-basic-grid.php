<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: vc-basic-grid.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

require_once W9sme_Addon::plugin_dir() . "includes/vc_customize/shortcodes-grid/__core/w9sme-basic-grid-custom.php";
class W9sme_SC_Basic_Grid extends W9sme_Basic_Grid {
    const SC_BASE = 'vc_basic_grid';
}