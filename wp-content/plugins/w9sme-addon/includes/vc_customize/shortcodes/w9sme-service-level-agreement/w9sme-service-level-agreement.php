<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-service-level-agreement.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

if ( !class_exists( 'W9sme_SC_SLA' ) ) {
    class W9sme_SC_SLA extends WPBakeryShortCode {
        const SC_BASE = 'w9sme_shortcode_sla';

        public function share_this($label) {
	        if (empty($label)) {
		        $label = __( 'SHARE:', 'w9sme-addon' );
	        }
	        
            $atts = array(
                'module_type'                 => 'share-this',
                'profiles'                    => 'social-twitter-url||social-facebook-url||social-googleplus-url',
//                'share_this_label'            => __( 'SHARE:', 'w9sme-addon' ),
                'share_this_label'            => $label,
                'icon_size'                   => '18',
                'colors'                      => 'icon-color-text',
                'colors_hover'                => 'icon-color-hover-p',
                'is_rounded_icon'             => '0',
                'rounded_size'                => '35',
                'background_colors'           => 'none',
                'background_hover_colors'     => 'none',
                'spacing_between_items'       => '15',
                'w9sme_extra_widget_classes' => '',
                'w9sme_remove_default_mb'    => '1',
            );

            $extra_class   = array( 'w9sme-widget-social-profiles' );
            $extra_class[] = $atts['w9sme_extra_widget_classes'];
            if ( !empty( $atts['w9sme_remove_default_mb'] ) ) {
                $extra_class[] = 'mb-0-i';
            }

            $args = array(
                'id'            => 'w9sme-widget-social-profiles',
                'name'          => __( 'W9sme Social Profiles', 'w9sme-addon' ),
                'before_widget' => '<section class="w9sme-widget %s ' . w9sme_clean_html_classes( $extra_class ) . '">',
                'after_widget'  => '</section>',
                'before_title'  => '<h3 class="w9sme-widget-title">',
                'after_title'   => '</h3>'
            );
            

            ob_start();
            the_widget( 'W9sme_Widget_Social_Profiles', $atts, $args );
            return ob_get_clean();
        }
    }
}
 