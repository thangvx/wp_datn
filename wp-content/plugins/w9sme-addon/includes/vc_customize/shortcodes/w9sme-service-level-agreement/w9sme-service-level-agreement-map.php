<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-service-level-agreement-map.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( is_admin() && ( ! defined( 'DOING_AJAX' ) && ( ! post_type_exists( W9sme_CPT_Service::CPT_SLUG ) ) ) ) {
	return;
}

$heading_title = vc_map_integrate_shortcode( W9sme_SC_Heading::SC_BASE, 'sla_title_', __( 'SLA Title', 'w9sme-addon' ), array(
	'exclude' => array(
		'heading_title_data_source',
		'heading_title',
		'heading_link',
		'css',
		'tablet_css',
		'mobile_css',
		'el_class',
		'animation_css',
		'animation_duration',
		'animation_delay',
		'css_inline',
	),
),
	array(
		'element' => 'sla_title_style',
		'value'   => 'custom'
	)
);


$heading_section_title = vc_map_integrate_shortcode( W9sme_SC_Heading::SC_BASE, 'sla_sec_title_', __( 'SLA Section Title', 'w9sme-addon' ), array(
	'exclude' => array(
		'heading_title_data_source',
		'heading_title',
		'heading_link',
		'css',
		'tablet_css',
		'mobile_css',
		'el_class',
		'animation_css',
		'animation_duration',
		'animation_delay',
		'css_inline',
	),
),
	array(
		'element' => 'sla_section_title_style',
		'value'   => 'custom'
	)
);


vc_map( array(
	'name'           => __( 'W9sme Service Level Agreement', 'w9sme-addon' ),
	'base'           => W9sme_SC_SLA::SC_BASE,
	'php_class_name' => 'W9sme_SC_SLA',
	'icon'           => 'w9 w9-ico-software-layout-8boxes',
	'category'       => array( __( 'Content', 'w9sme-addon' ) ),
	'description'    => __( 'Create service level agreement for service', 'w9sme-addon' ),
	'post_type'      => array(W9sme_CPT_Service::CPT_SLUG, W9sme_CPT_Content_Template::CPT_SLUG),
	'params'         => array_merge(
		array(
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Title Style', 'w9sme-addon' ),
				'param_name'  => 'sla_title_style',
				'description' => __( 'Select the sla title style', 'w9sme-addon' ),
				'admin_label' => true,
				'value'       => array(
					__( 'Default', 'w9sme-addon' )   => 'default',
					__( 'Separator', 'w9sme-addon' ) => 'separator',
					__( 'Custom', 'w9sme-addon' )    => 'custom',
				),
				'default'     => 'separator',
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Section Title Style', 'w9sme-addon' ),
				'param_name'  => 'sla_section_title_style',
				'admin_label' => true,
				'value'       => array(
					__( 'Default', 'w9sme-addon' )   => 'default',
					__( 'Top Line', 'w9sme-addon' )  => 'top-line',
					__( 'Separator', 'w9sme-addon' ) => 'separator',
					__( 'Custom', 'w9sme-addon' )    => 'custom',
				),
				'description' => __( 'Select the sla section title style', 'w9sme-addon' ),
				'default'     => 'top-line',
			),
		),
		$heading_title,
		$heading_section_title,
		array(
			W9sme_Map_Helpers::design_options(),
			W9sme_Map_Helpers::animation_css(),
			W9sme_Map_Helpers::animation_duration(),
			W9sme_Map_Helpers::animation_delay()
		) )
) );
