<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-vacancy-table.php
 * @time    : 9/28/17 12:08 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! class_exists( 'W9sme_SC_Vacancy_Table' ) ) {
	class W9sme_SC_Vacancy_Table extends WPBakeryShortCode {
		const SC_BASE = 'w9sme_shortcode_vacancy_table';
	}
}

