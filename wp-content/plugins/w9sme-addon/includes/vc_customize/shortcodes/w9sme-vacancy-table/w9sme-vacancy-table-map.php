<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-vacancy-table-map.php
 * @time    : 9/28/17 2:01 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$vacancy_cats      = w9sme_get_terms_by_tax( W9sme_CPT_Vacancy::TAX_SLUG, 'slug' );

vc_map( array(
	'name'           => esc_html__( 'W9sme Vacancy Table', 'w9sme-addon' ),
	'base'           => W9sme_SC_Vacancy_Table::SC_BASE,
	'class'          => '',
	'icon'           => 'w9 w9-ico-185082-man-people-streamline-user',
	'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
	'php_class_name' => 'W9sme_SC_Vacancy_Table',
	'description'    => __( 'Create vacancy table', 'w9sme-addon' ),
	'params'         => array(
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Data Source', 'osthemes-pure' ),
			'param_name'  => 'vacancy_data',
			'value'       => array(
				__( 'By categories', 'w9sme-addon' ) => 'category',
				__( 'All items', 'w9sme-addon' )     => 'all',
			),
			'admin_label' => true
		),
		array(
			'type'       => 'multi-select',
			'heading'    => __( 'Include (categories):', 'osthemes-pure' ),
			'param_name' => 'vacancy_category',
			'options'    => $vacancy_cats,
			'dependency' => array(
				'element' => 'vacancy_data',
				'value'   => array( 'category' ),
			),
		),
		
		array(
			'type'       => 'number',
			'heading'    => esc_html__( 'Item per page', 'w9sme-addon' ),
			'param_name' => 'vacancy_item_per_page',
			'value'      => '10',
			'admin_label' => true
		),
		
		array(
			'type'       => 'switcher',
			'heading'    => __( 'Enable search', 'w9sme-addon' ),
			'param_name' => 'vacancy_enable_search',
			'std'        => '1',
			'admin_label' => true
		),
		
		array(
			'type'       => 'switcher',
			'heading'    => __( 'Enable page length menu', 'w9sme-addon' ),
			'param_name' => 'vacancy_enable_length_menu',
			'std'        => '1',
			'admin_label' => true
		),
		
		array(
			'type'       => 'switcher',
			'heading'    => __( 'Enable result info', 'w9sme-addon' ),
			'param_name' => 'vacancy_enable_result_info',
			'std'        => '1',
			'admin_label' => true
		),
		
		W9sme_Map_Helpers::design_options(),
		W9sme_Map_Helpers::animation_css(),
		W9sme_Map_Helpers::animation_duration(),
		W9sme_Map_Helpers::animation_delay(),
		W9sme_Map_Helpers::extra_class()
	)
) );