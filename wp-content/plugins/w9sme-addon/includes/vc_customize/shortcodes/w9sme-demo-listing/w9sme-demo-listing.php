<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-demo-listing.php
 * @time    : 10/31/16 10:33 AM
 * @author  : 9WPThemes Team
 */

if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Demo_Listing extends WPBakeryShortCode{
    const SC_BASE = 'w9sme_shortcode_demo_listing';
}