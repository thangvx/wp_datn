<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-counter-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

vc_map( array(
    'name'           => __( 'W9sme Demo Listing', 'w9sme-addon' ),
    'base'           => W9sme_SC_Demo_Listing::SC_BASE,
    'class'          => '',
    'icon'           => 'w9 w9-ico-hotairballoon',
    'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
    'description'    => __( 'Make couter group to count anything you want', 'w9sme-addon' ),
    'php_class_name' => 'W9sme_SC_Counter',
    'params'         => array(
        
        
        W9sme_Map_Helpers::extra_class(),
        W9sme_Map_Helpers::design_options(),
        W9sme_Map_Helpers::animation_css(),
        W9sme_Map_Helpers::animation_duration(),
        W9sme_Map_Helpers::animation_delay()
    )
) );