<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-widget-menu.php
 * @time    : 9/26/2016 8:29 AM
 * @author  : 9WPThemes Team
 */

if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Widget_Menu extends WPBakeryShortCode {
    const SC_BASE = 'w9sme_shortcode_widget_menu';
}