<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: vc-masonry-media-grid-map.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

require_once W9sme_Addon::plugin_dir() . "includes/vc_customize/shortcodes-grid/__core/w9sme-grids-common.php";
$gridParams = W9sme_Grids_Common::getMasonryMediaCommonAtts();
vc_map( array(
    'name'           => __( 'Masonry Media Grids', 'w9sme-addon' ),
    'base'           => W9sme_SC_Masonry_Media_Grid::SC_BASE,
    'icon'           => 'w9 w9-ico-software-layout-header-4columns',
    'category'       => __( 'Content', 'w9sme-addon' ),
    'description'    => __( 'Media in grid', 'w9sme-addon' ),
    'php_class_name' => 'W9sme_SC_Masonry_Media_Grid',
    'params'         => $gridParams,
) );
