<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-div-wrapper-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

vc_map( array(
    'name'                      => __( 'W9sme Div Wrapper', 'w9sme-addon' ),
    'base'                      => W9sme_SC_Div_Wrapper::SC_BASE,
    'class'                     => '',
    'icon'                      => 'w9 w9-ico-basic-webpage-img-txt',
    'category'                  => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
    'controls'                  => array(),
    'as_parent'                 => array( 'only' => W9sme_SC_Div_Content::SC_BASE ),
    'show_settings_on_create'   => false,
    'content_element'         => true,
    'admin_enqueue_js'          => array( W9sme_Addon::plugin_url() . 'assets/js/vc-custom.js' ),
    'js_view'                   => 'W9smeBackendVcDivWrapperView',
    'description'               => __( 'Wrapper block of content, with some helper function', 'w9sme-addon' ),
    'default_content'           => '[w9sme_shortcode_div_content][/w9sme_shortcode_div_content]',
    'php_class_name'            => 'W9sme_SC_Div_Wrapper',
    'params'                    => array(
        
        array(
            'type'             => 'textfield',
            'heading'          => __( 'Min height', 'w9sme-addon' ),
            'param_name'       => 'min_height',
            'value'            => '',
            'description'      => __( 'Value with unit like px, %, em, cm... eg:(100px).', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-4 vc_column-with-padding',
        ),
        array(
            'type'             => 'textfield',
            'heading'          => __( 'Height', 'w9sme-addon' ),
            'param_name'       => 'height',
            'value'            => '',
            'description'      => __( 'Value with unit like px, %, em, cm... eg:(100px).', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-4',
        ),
        array(
            'type'             => 'textfield',
            'heading'          => __( 'Max height', 'w9sme-addon' ),
            'param_name'       => 'max_height',
            'value'            => '',
            'description'      => __( 'Value with unit like px, %, em, cm... eg:(100px).', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-4',
        ),
        array(
            'type'             => 'textfield',
            'heading'          => __( 'Min width', 'w9sme-addon' ),
            'param_name'       => 'min_width',
            'value'            => '',
            'description'      => __( 'Value with unit like px, %, em, cm... eg:(100px).', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-4',
        ),
        array(
            'type'             => 'textfield',
            'heading'          => __( 'Width', 'w9sme-addon' ),
            'param_name'       => 'width',
            'value'            => '',
            'description'      => __( 'Value with unit like px, %, em, cm... eg:(100px).', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-4',
        ),
        array(
            'type'             => 'textfield',
            'heading'          => __( 'Max width', 'w9sme-addon' ),
            'param_name'       => 'max_width',
            'value'            => '',
            'description'      => __( 'Value with unit like px, %, em, cm... eg:(100px).', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-4',
        ),
        array(
            'type'             => 'dropdown',
            'heading'          => __( 'Horizontal Align', 'w9sme-addon' ),
            'param_name'       => 'horizontal_align',
            'value'            => array(
                __( 'Default', 'w9sme-addon' ) => 'w9sme-div-wrapper-align-default',
                __( 'Left', 'w9sme-addon' ) => 'w9sme-div-wrapper-align-left',
                __( 'Right', 'w9sme-addon' ) => 'w9sme-div-wrapper-align-right',
                __( 'Center', 'w9sme-addon' ) => 'w9sme-div-wrapper-align-center',
            ),
//            'edit_field_class' => 'vc_col-sm-4',
        ),
        array(
            'type'             => 'dropdown',
            'heading'          => __( 'Fix ratio', 'w9sme-addon' ),
            'param_name'       => 'fixed_ratio',
            'value'            => array_merge(
                array(__( 'None', 'w9sme-addon' ) => 'none'),
                W9sme_Image::get_w9sme_ratio_list()
            ),
            'description'      => __( 'Pick suitable ratio for div wrapper. Div wrapper can keep ratio only when all div inside is in overlay mode', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-12',
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Overflow', 'w9sme-addon' ),
            'param_name'  => 'overflow',
            'value'       => array(
                __( 'Hidden', 'w9sme-addon' )  => 'hidden',
                __( 'Visible', 'w9sme-addon' ) => 'visible',
                __( 'Scroll', 'w9sme-addon' )  => 'scroll',
                __( 'Auto', 'w9sme-addon' )    => 'auto',
                __( 'Initial', 'w9sme-addon' ) => 'initial',
                __( 'Inherit', 'w9sme-addon' ) => 'inherit',
            ),
            'std'         => 'hidden',
            'description' => __( 'How div wrapper display content in its area.', 'w9sme-addon' ),
        ),
        array(
            'type'        => 'checkbox',
            'heading'     => __( 'Sticky enabled', 'w9sme-addon' ),
            'param_name'  => 'sticky_enabled',
            'admin_label' => true,
            'value'       => '',
            'description' => __( 'Enabled sticky for this div.', 'w9sme-addon' ),
        ),
        array(
            'type'             => 'number',
            'heading'          => __( 'Sticky start', 'w9sme-addon' ),
            'param_name'       => 'sticky_start',
            'value'            => '',
            'dependency'       => array(
                'element' => 'sticky_enabled',
                'value'   => 'true',
            ),
            'edit_field_class' => 'vc_col-sm-4',
            'suffix'           => __( 'px', 'w9sme-addon' ),
        ),

        array(
            'type'             => 'number',
            'heading'          => __( 'Sticky stop', 'w9sme-addon' ),
            'description'      => __( 'From bottom of page.', 'w9sme-addon' ),
            'param_name'       => 'sticky_stop',
            'value'            => '',
            'dependency'       => array(
                'element' => 'sticky_enabled',
                'value'   => 'true',
            ),
            'edit_field_class' => 'vc_col-sm-4',
            'suffix'           => __( 'px', 'w9sme-addon' ),
        ),

        array(
            'type'             => 'number',
            'heading'          => __( 'Sticky z-index', 'w9sme-addon' ),
            'param_name'       => 'sticky_z_index',
            'value'            => '',
            'std'              => '500',
            'dependency'       => array(
                'element' => 'sticky_enabled',
                'value'   => 'true',
            ),
            'edit_field_class' => 'vc_col-sm-4',
            'description'      => __( 'Object with higher z-index appear above ones lower.', 'w9sme-addon' ),
        ),

        W9sme_Map_Helpers::extra_class(),
        W9sme_Map_Helpers::design_options(),
        W9sme_Map_Helpers::design_options_on_tablet(),
        W9sme_Map_Helpers::design_options_on_mobile(),
        W9sme_Map_Helpers::animation_css(),
        W9sme_Map_Helpers::animation_duration(),
        W9sme_Map_Helpers::animation_delay()
    )
) );