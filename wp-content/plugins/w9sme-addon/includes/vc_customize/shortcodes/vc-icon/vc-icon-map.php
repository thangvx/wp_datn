<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: vc-icon-map.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

vc_map( array(
    'name'        => __( 'Icon', 'w9sme-addon' ),
    'base'        => 'vc_icon',
    'icon'        => 'w9 w9-ico-basic-pencil-ruler',
    'category'    => __( 'Content', 'w9sme-addon' ),
    'description' => __( 'Eye catching icons from libraries', 'w9sme-addon' ),
    'params'      => array(
        W9sme_Map_Helpers::get_icons_picker_type(),
        W9sme_Map_Helpers::get_icon_picker_9wpthemes(),
        W9sme_Map_Helpers::get_icon_picker_w9sme(),
        W9sme_Map_Helpers::get_icon_picker_fontawesome(),
        W9sme_Map_Helpers::get_icon_picker_openiconic(),
        W9sme_Map_Helpers::get_icon_picker_typicons(),
        W9sme_Map_Helpers::get_icon_picker_entypo(),
        W9sme_Map_Helpers::get_icon_picker_linecons(),
        W9sme_Map_Helpers::get_icon_picker_monosocial(),
        
        array(
            'type'               => 'dropdown',
            'heading'            => __( 'Icon color', 'w9sme-addon' ),
            'param_name'         => 'color',
            'value'              => array_merge( W9sme_Map_Helpers::get_colors(), array( __( 'Gradient', 'w9sme-addon' ) => 'gradient' ) ),
            'description'        => __( 'Select icon color.', 'w9sme-addon' ),
            'param_holder_class' => 'vc_colored-dropdown',
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Custom color', 'w9sme-addon' ),
            'param_name'  => 'custom_color',
            'description' => __( 'Select custom icon color.', 'w9sme-addon' ),
            'dependency'  => array(
                'element' => 'color',
                'value'   => 'custom',
            ),
        ),
        
        array(
            'type'               => 'dropdown',
            'heading'            => __( 'Gradient color 1', 'w9sme-addon' ),
            'param_name'         => 'gradient_color_1',
            'description'        => __( 'Select first color for gradient.', 'w9sme-addon' ),
            'param_holder_class' => 'vc_colored-dropdown',
            'value'              => W9sme_Map_Helpers::get_just_colors(),
            'std'                => 'p',
            'dependency'         => array(
                'element' => 'color',
                'value'   => array( 'gradient' ),
            ),
            'edit_field_class'   => 'vc_col-sm-6',
        ),
        
        array(
            'type'               => 'dropdown',
            'heading'            => __( 'Gradient color 2', 'w9sme-addon' ),
            'param_name'         => 'gradient_color_2',
            'description'        => __( 'Select second color for gradient.', 'w9sme-addon' ),
            'param_holder_class' => 'vc_colored-dropdown',
            'value'              => W9sme_Map_Helpers::get_just_colors(),
            'std'                => 's',
            'dependency'         => array(
                'element' => 'color',
                'value'   => array( 'gradient' ),
            ),
            'edit_field_class'   => 'vc_col-sm-6',
        ),
        
        array(
            'type'             => 'number',
            'heading'          => __( 'Gradient angle', 'w9sme-addon' ),
            'param_name'       => 'gradient_angle',
            'description'      => __( 'Enter the gradient angle, default value is 45.', 'w9sme-addon' ),
            'edit_field_class' => 'vc_col-sm-6',
            'dependency'       => array(
                'element' => 'color',
                'value'   => array( 'gradient' ),
            ),
            'std'              => '45'
        ),
        
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Background shape', 'w9sme-addon' ),
            'param_name'  => 'background_style',
            'value'       => array(
                __( 'None', 'w9sme-addon' )            => '',
                __( 'Circle', 'w9sme-addon' )          => 'rounded',
                __( 'Square', 'w9sme-addon' )          => 'boxed',
                __( 'Rounded', 'w9sme-addon' )         => 'rounded-less',
                __( 'Outline Circle', 'w9sme-addon' )  => 'rounded-outline',
                __( 'Outline Square', 'w9sme-addon' )  => 'boxed-outline',
                __( 'Outline Rounded', 'w9sme-addon' ) => 'rounded-less-outline',
            ),
            'description' => __( 'Select background shape and style for icon.', 'w9sme-addon' ),
        ),
        array(
            'type'               => 'dropdown',
            'heading'            => __( 'Background color', 'w9sme-addon' ),
            'param_name'         => 'background_color',
            'value'              => W9sme_Map_Helpers::get_colors(),
            'std'                => 'grey',
            'description'        => __( 'Select background color for icon.', 'w9sme-addon' ),
            'param_holder_class' => 'vc_colored-dropdown',
            'dependency'         => array(
                'element'   => 'background_style',
                'not_empty' => true,
            ),
        ),
        array(
            'type'        => 'colorpicker',
            'heading'     => __( 'Custom background color', 'w9sme-addon' ),
            'param_name'  => 'custom_background_color',
            'description' => __( 'Select custom icon background color.', 'w9sme-addon' ),
            'dependency'  => array(
                'element' => 'background_color',
                'value'   => 'custom',
            ),
        ),
        
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Size', 'w9sme-addon' ),
            'param_name'  => 'size',
            'value'       => array_merge( getVcShared( 'sizes' ), array( 'Extra Large' => 'xl', 'Custom Size' => 'custom' ) ),
            'std'         => 'md',
            'description' => __( 'Icon size.', 'w9sme-addon' ),
        ),
        array(
            'type'        => 'number',
            'heading'     => __( 'Custom icon size', 'w9sme-addon' ),
            'param_name'  => 'custom_size',
            'description' => __( 'Enter the custom size for the icon. The unit is px. Currently does not support other units. (etc: 20, 50).', 'w9sme-addon' ),
            'dependency'  => array(
                'element' => 'size',
                'value'   => 'custom'
            )
        ),
        
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Icon alignment', 'w9sme-addon' ),
            'param_name'  => 'align',
            'value'       => array(
                __( 'Left', 'w9sme-addon' )   => 'left',
                __( 'Right', 'w9sme-addon' )  => 'right',
                __( 'Center', 'w9sme-addon' ) => 'center',
            ),
            'description' => __( 'Select icon alignment.', 'w9sme-addon' ),
        ),
        
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'On click action', 'w9sme-addon' ),
            'param_name'  => 'onclick',
            'value'       => array(
                __( 'None', 'w9sme-addon' )             => '',
                __( 'Open custom link', 'w9sme-addon' ) => 'custom-link',
                __( 'Popup image', 'w9sme-addon' )      => 'popup-image',
                __( 'Popup search', 'w9sme-addon' )      => 'popup-search',
                __( 'Popup video', 'w9sme-addon' )      => 'popup-video',
            ),
            'description' => __( 'Select action for click action.', 'w9sme-addon' ),
            'std'         => '',
        ),
        
        array(
            'type'        => 'vc_link',
            'heading'     => __( 'URL (Link)', 'w9sme-addon' ),
            'param_name'  => 'link',
            'description' => __( 'Add link to icon.', 'w9sme-addon' ),
            'dependency'  => array(
                'element' => 'onclick',
                'value'   => 'custom-link'
            )
        ),
        
        array(
            'type'        => 'attach_image',
            'heading'     => __( 'Image', 'w9sme-addon' ),
            'param_name'  => 'image',
            'value'       => '',
            'description' => __( 'Select image from media library.', 'w9sme-addon' ),
            'dependency'  => array(
                'element' => 'onclick',
                'value'   => 'popup-image',
            ),
        ),
        
        array(
            'type'        => 'textfield',
            'heading'     => __( 'Video link', 'w9sme-addon' ),
            'param_name'  => 'video_link',
            'value'       => 'https://vimeo.com/51589652',
            'description' => sprintf( __( 'Enter link to video (Note: read more about available formats at WordPress <a href="%s" target="_blank">codex page</a>).', 'w9sme-addon' ), 'http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F' ),
            'dependency'  => array(
                'element' => 'onclick',
                'value'   => 'popup-video'
            ),
        ),
        W9sme_Map_Helpers::extra_class(),
        W9sme_Map_Helpers::design_options(),
        W9sme_Map_Helpers::design_options_on_tablet(),
        W9sme_Map_Helpers::design_options_on_mobile(),
        W9sme_Map_Helpers::animation_css(),
        W9sme_Map_Helpers::animation_duration(),
        W9sme_Map_Helpers::animation_delay(),
    ),
    'js_view'     => 'VcIconElementView_Backend',
) );