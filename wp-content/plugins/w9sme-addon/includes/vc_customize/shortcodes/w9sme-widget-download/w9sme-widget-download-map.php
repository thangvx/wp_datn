<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-widget-download-map.php
 * @time    : 12/9/2016 11:43 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map(
	array(
		'name'           => esc_html__( 'W9sme Widget Download', 'w9sme-addon' ),
		'base'           => W9sme_SC_Widget_Download::SC_BASE,
		'icon'           => 'w9 w9-ico-download-1',
		'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
		'php_class_name' => 'W9sme_SC_Widget_Download',
		'description'    => __( 'W9sme widget download', 'w9sme-addon' ),
		'params'         => array_merge( array(
			array(
				'type'        => 'textfield',
				'param_name'  => 'title',
				'heading'     => __( 'Widget title', 'w9sme-addon' ),
				'description' => __( 'What text use as a widget title. Leave blank if you don\' want to show the widget title.', 'w9sme-addon' ),
			),
			array(
				'type'       => 'getfile',
				'param_name' => 'file',
				'heading'    => esc_html__( 'Select File', 'w9sme-addon' ),
			),
			
			array(
				'type'        => 'textfield',
				'param_name'  => 'display_text',
				'admin_label' => true,
				'heading'     => esc_html__( 'Display Text', 'w9sme-addon' ),
				'std'         => __( 'DOWNLOAD', 'w9sme-addon' )
			),
			
			array(
				'type'        => 'dropdown',
				'param_name'  => 'file_type',
				'admin_label' => true,
				'heading'     => esc_html__( 'File Type', 'w9sme-addon' ),
				'value'       => array(
					esc_html__( 'DOC', 'w9sme-addon' )    => 'DOC',
					esc_html__( 'XLS', 'w9sme-addon' )    => 'XLS',
					esc_html__( 'PDF', 'w9sme-addon' )    => 'PDF',
					esc_html__( 'PPT', 'w9sme-addon' )    => 'PPT',
					esc_html__( 'Custom', 'w9sme-addon' ) => 'custom',
				),
				'std'         => 'DOC'
			),
		
		), W9sme_Map_Helpers::widget_common_params() )
	)
);
