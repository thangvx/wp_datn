<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-service-panel-map.php
 * @time    : 7/17/2017 6:08 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! post_type_exists( W9sme_CPT_Service::CPT_SLUG ) ) {
	return;
}

vc_map(
	array(
		'name'           => esc_html__( 'W9sme Service Reviews', 'w9sme-addon' ),
		'base'           => W9sme_SC_Service_Review::SC_BASE,
		'icon'           => 'fa fa-th-list',
		'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
		'php_class_name' => 'W9sme_SC_Service_Review',
		'description'    => __( 'Display service reviews', 'w9sme-addon' ),
		'post_type'      => array( W9sme_CPT_Service::CPT_SLUG, W9sme_CPT_Content_Template::CPT_SLUG ),
		'params'         => array(
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Style', 'w9sme-addon' ),
				'param_name'  => 'sr_style',
				'value'       => array(
					__( 'Style 1', 'w9sme-addon' ) => 'style-1',
					__( 'Style 2', 'w9sme-addon' ) => 'style-2',
				),
				'description' => __( 'Select the service panel style', 'w9sme-addon' ),
			),
			array(
				'type'        => 'multi-select',
				'heading'     => esc_html__( 'Service Item', 'w9sme-addon' ),
				'param_name'  => 'sr_name',
				'options'     => w9sme_get_service_list( false ),
				'admin_label' => true,
				'description' => esc_html__( 'Choose services', 'w9sme-addon' )
			),
			
			W9sme_Map_Helpers::extra_class(),
			W9sme_Map_Helpers::design_options(),
			W9sme_Map_Helpers::animation_css(),
			W9sme_Map_Helpers::animation_duration(),
			W9sme_Map_Helpers::animation_delay()
		),
	)
);