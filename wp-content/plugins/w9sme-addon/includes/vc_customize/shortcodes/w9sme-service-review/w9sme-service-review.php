<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-service-panel.php
 * @time    : 7/17/2017 6:08 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( !post_type_exists( W9sme_CPT_Service::CPT_SLUG ) ) {
	return;
}

class W9sme_SC_Service_Review extends WPBakeryShortCode {
	const SC_BASE = 'w9sme_shortcode_service_review';
	
	public function __construct($settings) {
		parent::__construct($settings);
	}
	
	
}
