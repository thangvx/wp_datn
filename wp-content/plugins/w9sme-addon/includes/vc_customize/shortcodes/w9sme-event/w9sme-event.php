<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-event.php
 * @time    : 4/25/2017 9:20 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( !post_type_exists( W9sme_CPT_Event::CPT_SLUG ) ) {
	return;
}

class W9sme_SC_Event extends WPBakeryShortCode {
	const SC_BASE = 'w9sme_shortcode_event';
	
	public function __construct($settings) {
		parent::__construct($settings);
	}
	
	
}