<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-icon-box-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map( array(
	'name'           => __( 'Icon Box', 'w9sme-addon' ),
	'base'           => W9sme_SC_Icon_Box::SC_BASE,
	'icon'           => 'w9 w9-ico-basic-flag2',
	'category'       => W9SME_SC_CATEGORY,
	'description'    => __( 'Adds icon box with font icons', 'w9sme-addon' ),
	'php_class_name' => 'W9sme_SC_Icon_Box',
	'params'         => array(
		W9sme_Map_Helpers::get_icons_picker_type(),
		W9sme_Map_Helpers::get_icon_picker_9wpthemes(),
		W9sme_Map_Helpers::get_icon_picker_w9sme(),
		W9sme_Map_Helpers::get_icon_picker_fontawesome(),
		W9sme_Map_Helpers::get_icon_picker_openiconic(),
		W9sme_Map_Helpers::get_icon_picker_typicons(),
		W9sme_Map_Helpers::get_icon_picker_entypo(),
		W9sme_Map_Helpers::get_icon_picker_linecons(),
		W9sme_Map_Helpers::get_icon_picker_monosocial(),
		
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Icon style', 'w9sme-addon' ),
			'param_name'  => 'ib_i_style',
			'admin_label' => true,
			'value'       => array(
				__( 'Normal', 'w9sme-addon' )  => 'normal',
				__( 'Rounded', 'w9sme-addon' ) => 'rounded',
			),
			'description' => __( 'Select icon style.', 'w9sme-addon' ),
//		    'dependency'  => array(
//			    'element' => 'ib_i_align',
//			    'value' => array( 'left' ,'top-center', 'right')
//		    ),
			'std'         => 'rounded'
		),
		
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Icon alignment', 'w9sme-addon' ),
			'param_name'  => 'ib_i_style_rounded_align',
//			'admin_label' => true,
			'value'       => array(
				__( 'Left', 'w9sme-addon' )       => 'left',
				__( 'Top-Center', 'w9sme-addon' ) => 'top-center',
				__( 'Right', 'w9sme-addon' )      => 'right',
			),
			'description' => __( 'Select icon alignment.', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 'ib_i_style',
				'value'   => 'rounded'
			),
			'std'         => 'left'
		),
		
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Icon alignment', 'w9sme-addon' ),
			'param_name'  => 'ib_i_style_normal_align',
//			'admin_label' => true,
			'value'       => array(
				__( 'Top-Left', 'w9sme-addon' )   => 'top-left-inline',
				__( 'Top-Right', 'w9sme-addon' )  => 'top-right-inline',
				__( 'Top-Center', 'w9sme-addon' ) => 'top-center',
				__( 'Left', 'w9sme-addon' )       => 'left',
				__( 'Right', 'w9sme-addon' )      => 'right',
				__( 'Bottom', 'w9sme-addon' )     => 'bottom'
			),
			'description' => __( 'Select icon alignment.', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 'ib_i_style',
				'value'   => 'normal'
			),
			'admin_label' => true,
		),
		
		array(
			'type'       => 'slider',
			'heading'    => __( 'Icon scale', 'w9sme-addon' ),
			'param_name' => 'ib_i_size',
			'unit'       => '%',
			'min'        => '50',
			'max'        => '200',
			'step'       => '5',
			'std'        => '100%',
			'dependency' => array(
				'element'            => 'ib_i_style_normal_align',
				'value_not_equal_to' => array( 'bottom' )
			),
		),
		
		array(
			'type'        => 'textfield',
			'heading'     => __( 'Title', 'w9sme-addon' ),
			'param_name'  => 'ib_title',
			'value'       => '',
			'description' => __( 'Enter the title for this element.', 'w9sme-addon' ),
		),
		array(
			'type'        => 'vc_link',
			'heading'     => __( 'Link (URL)', 'w9sme-addon' ),
			'param_name'  => 'ib_link',
			'value'       => '',
			'description' => __( 'Note: If \'Icon Alignment\' is \'bottom\', this will be a text under the icon.', 'w9sme-addon' ),
		),
//		array(
//			'type'        => 'textfield',
//			'heading'     => __( 'Sub title', 'w9sme-addon' ),
//			'param_name'  => 'ib_sub_title',
//			'value'       => '',
//			'description' => __( 'Provide the sub title for this element.', 'w9sme-addon' ),
//		),
		array(
			'type'        => 'textarea',
			'heading'     => __( 'Description', 'w9sme-addon' ),
			'param_name'  => 'ib_description',
			'value'       => '',
			'description' => __( 'Provide the description for this element.', 'w9sme-addon' )
		),
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Icon color', 'w9sme-addon' ),
			'param_name'         => 'ib_i_color',
			'group'              => __( 'Style', 'w9sme-addon' ),
			'description'        => __( 'Select icon color. You can add more colors by setting "Define Most Used Colors" in theme options.', 'w9sme-addon' ),
//			'admin_label'        => true,
			'param_holder_class' => 'vc_colored-dropdown',
			'value'              => array(
				                        __( 'Primary Color', 'w9sme-addon' )   => 'p',
				                        __( 'Secondary Color', 'w9sme-addon' ) => 's',
				                        __( 'Gradient', 'w9sme-addon' )        => 'gradient',
				                        __( 'Text Color', 'w9sme-addon' )      => 'text',
				                        __( 'Meta Text Color', 'w9sme-addon' ) => 'meta-text',
				                        __( 'Border Color', 'w9sme-addon' )    => 'border',
				                        __( 'Light #FFF', 'w9sme-addon' )      => 'light',
				                        __( 'Dark #000', 'w9sme-addon' )       => 'dark',
				                        __( 'Gray #222', 'w9sme-addon' )       => 'gray2',
				                        __( 'Gray #444', 'w9sme-addon' )       => 'gray4',
				                        __( 'Gray #666', 'w9sme-addon' )       => 'gray6',
				                        __( 'Gray #888', 'w9sme-addon' )       => 'gray8',
			                        ) + w9sme_get_most_used_colors( 'name_key' ),
			'std'                => 'light'
		),
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Gradient color 1', 'w9sme-addon' ),
			'param_name'         => 'ib_i_gradient_color_1',
			'description'        => __( 'Select first color for gradient.', 'w9sme-addon' ),
			'param_holder_class' => 'vc_colored-dropdown',
			'value'              => W9sme_Map_Helpers::get_just_colors(),
			'std'                => 'p',
			'dependency'         => array(
				'element' => 'ib_i_color',
				'value'   => array( 'gradient' ),
			),
			'edit_field_class'   => 'vc_col-sm-6',
			'group'              => __( 'Style', 'w9sme-addon' ),
		),
		
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Gradient color 2', 'w9sme-addon' ),
			'param_name'         => 'ib_i_gradient_color_2',
			'description'        => __( 'Select second color for gradient.', 'w9sme-addon' ),
			'param_holder_class' => 'vc_colored-dropdown',
			'value'              => W9sme_Map_Helpers::get_just_colors(),
			'std'                => 's',
			'dependency'         => array(
				'element' => 'ib_i_color',
				'value'   => array( 'gradient' ),
			),
			'edit_field_class'   => 'vc_col-sm-6',
			'group'              => __( 'Style', 'w9sme-addon' )
		),
		
		array(
			'type'             => 'number',
			'heading'          => __( 'Gradient angle', 'w9sme-addon' ),
			'param_name'       => 'ib_i_gradient_angle',
			'description'      => __( 'Enter the gradient angle.', 'w9sme-addon' ),
			'edit_field_class' => 'vc_col-sm-6',
			'dependency'       => array(
				'element' => 'ib_i_color',
				'value'   => array( 'gradient' ),
			),
			'std'              => '45',
			'group'            => __( 'Style', 'w9sme-addon' )
		),
		
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Icon background color', 'w9sme-addon' ),
			'param_name'         => 'ib_i_bgc',
//			'admin_label'        => true,
			'group'              => __( 'Style', 'w9sme-addon' ),
			'value'              => W9sme_Map_Helpers::get_just_colors(),
			'param_holder_class' => 'vc_colored-dropdown',
			'description'        => __( 'Select icon background color. You can add more colors by setting "Define Most Used Colors" in theme options', 'w9sme-addon' ),
			'dependency'         => array(
				'element' => 'ib_i_style',
				'value'   => array( 'rounded' )
			),
			'std'                => 'p'
		),
		
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Icon color when hover', 'w9sme-addon' ),
			'param_name'         => 'ib_i_hover_color',
//			'admin_label'        => true,
			'group'              => __( 'Style', 'w9sme-addon' ),
			'value'              => W9sme_Map_Helpers::get_just_colors(),
			'param_holder_class' => 'vc_colored-dropdown',
			'description'        => __( 'Select icon color when hover. You can add more colors by setting "Define Most Used Colors" in theme options', 'w9sme-addon' ),
			'dependency'         => array(
				'element' => 'ib_i_style',
				'value'   => array( 'rounded' )
			),
			'std'                => 'light'
		),
		
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Icon background color when hover', 'w9sme-addon' ),
			'param_name'         => 'ib_i_hover_bgc',
//			'admin_label'        => true,
			'group'              => __( 'Style', 'w9sme-addon' ),
			'value'              => W9sme_Map_Helpers::get_just_colors(),
			'param_holder_class' => 'vc_colored-dropdown',
			'description'        => __( 'Select icon background color when hover. You can add more colors by setting "Define Most Used Colors" in theme options', 'w9sme-addon' ),
			'dependency'         => array(
				'element' => 'ib_i_style',
				'value'   => array( 'rounded' )
			),
			'std'                => 'p'
		),
		
		array(
			'type'               => 'dropdown',
			'heading'            => __( 'Text color', 'w9sme-addon' ),
			'param_name'         => 'ib_tx_color',
//			'admin_label'        => true,
			'group'              => __( 'Style', 'w9sme-addon' ),
			'value'              => array_merge( array(
					'Inherit' => '__'
				)
				, W9sme_Map_Helpers::get_just_colors() ),
			'param_holder_class' => 'vc_colored-dropdown',
			'description'        => __( 'Select text color.', 'w9sme-addon' ),
			'std'                => '__'
		),
//        array(
//            'type'        => 'dropdown',
//            'heading'     => __( 'Box shadow effect', 'w9sme-addon' ),
//            'param_name'  => 'ib_boxshadow_effect',
//            'admin_label' => true,
//            'group'       => __( 'Style', 'w9sme-addon' ),
//            'value'       => array(
//                __( 'None', 'w9sme-addon' )       => '',
//                __( 'Permanent', 'w9sme-addon' )  => 'ib-eff-boxshadow',
//                __( 'Hover Only', 'w9sme-addon' ) => 'ib-eff-hover-boxshadow',
//            ),
//            'description' => __( 'Select box shadow effect.', 'w9sme-addon' ),
//            'std'         => ''
//        ),

		// top line
		array(
			'type'        => 'switcher',
			'heading'     => esc_html__( 'Top Line', 'w9sme-addon' ),
			'param_name'  => 'ib_top_line_enable',
			'value'       => '',
			'admin_label' => true,
		),

		array(
			'type'        => 'number',
			'heading'     => esc_html__( 'Margin bottom', 'w9sme-addon' ),
			'param_name'  => 'ib_top_line_margin_bottom',
			'value'       => '',
			'description' => esc_html__( 'Enter the margin bottom for the top line (Unit: px).', 'w9sme-addon' ),
			'group'       => __( 'Top Line', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 'ib_top_line_enable',
				'value'   => '1',
			),
		),

		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Width', 'w9sme-addon' ),
			'param_name'  => 'ib_top_line_width',
			'value'       => array(
				esc_html__( 'Default', 'w9sme-addon' )      => 'default',
				esc_html__( 'Custom Width', 'w9sme-addon' ) => 'custom-width',
			),
			'std'         => 'default',
			'description' => esc_html__( 'Select icon box title top line width. Default value is 46px.', 'w9sme-addon' ),
			'group'       => __( 'Top Line', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 'ib_top_line_enable',
				'value'   => '1',
			),
		),

		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Custom width', 'w9sme-addon' ),
			'param_name'  => 'ib_top_line_custom_width',
			'value'       => '',
			'description' => esc_html__( 'Enter custom width for the icon box title top line (etc: 20px, 10em...).', 'w9sme-addon' ),
			'dependency'  => array( 'element' => 'ib_top_line_width', 'value' => 'custom-width' ),
			'group'       => __( 'Top Line', 'w9sme-addon' ),
		),

		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Height', 'w9sme-addon' ),
			'param_name'  => 'ib_top_line_height',
			'value'       => array(
				esc_html__( 'Default', 'w9sme-addon' )       => 'default',
				esc_html__( 'Custom Height', 'w9sme-addon' ) => 'custom-height',
			),
			'std'         => 'default',
			'description' => esc_html__( 'Select icon box title top line height. Default value is 5px.', 'w9sme-addon' ),
			'group'       => __( 'Top Line', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 'ib_top_line_enable',
				'value'   => '1',
			),
		),

		array(
			'type'        => 'textfield',
			'heading'     => esc_html__( 'Custom height', 'w9sme-addon' ),
			'param_name'  => 'ib_top_line_custom_height',
			'value'       => '',
			'description' => esc_html__( 'Enter custom height for the icon box title top line (etc: 20px, 10em...).', 'w9sme-addon' ),
			'dependency'  => array( 'element' => 'ib_top_line_height', 'value' => 'custom-height' ),
			'group'       => __( 'Top Line', 'w9sme-addon' ),
		),


		array(
			'type'               => 'dropdown',
			'heading'            => esc_html__( 'Color', 'w9sme-addon' ),
			'param_name'         => 'ib_top_line_color',
			'param_holder_class' => 'vc_colored-dropdown',
			'value'              => W9sme_Map_Helpers::get_colors(),
			'description'        => esc_html__( 'Select Color Scheme.', 'w9sme-addon' ),
			'group'              => __( 'Top Line', 'w9sme-addon' ),
			'dependency'         => array(
				'element' => 'ib_top_line_enable',
				'value'   => '1',
			),
			'std'                => 'p'
		),

		array(
			'type'       => 'colorpicker',
			'heading'    => esc_html__( 'Custom color', 'w9sme-addon' ),
			'param_name' => 'ib_top_line_custom_color',
			'value'      => '',
			'dependency' => array(
				'element' => 'ib_top_line_color',
				'value'   => 'custom'
			),
			'group'      => __( 'Top Line', 'w9sme-addon' ),
		),

		W9sme_Map_Helpers::extra_class(),
		W9sme_Map_Helpers::design_options(),
		W9sme_Map_Helpers::design_options_on_tablet(),
		W9sme_Map_Helpers::design_options_on_mobile(),
		W9sme_Map_Helpers::animation_css(),
		W9sme_Map_Helpers::animation_duration(),
		W9sme_Map_Helpers::animation_delay()
	)
) );