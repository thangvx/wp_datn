<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: vc-column-text.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

vc_map( array(
    'name'          => __( 'Text Block', 'w9sme-addon' ),
    'icon'          => 'w9 w9-ico-basic-webpage-txt',
    'base'          => 'vc_column_text',
    'wrapper_class' => 'clearfix',
    'category'      => __( 'Content', 'w9sme-addon' ),
    'description'   => __( 'A block of text with WYSIWYG editor', 'w9sme-addon' ),
    'params'        => array(
        array(
            'type'       => 'textarea_html',
            'holder'     => 'div',
            'heading'    => __( 'Text', 'w9sme-addon' ),
            'param_name' => 'content',
            'value'      => __( '<p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>', 'w9sme-addon' ),
        ),
        array(
            'type'       => 'switcher',
            'param_name' => 'custom_list_style',
            'heading'    => __( 'Custom list style', 'w9sme-addon' ),
            'std'        => '0'
        ),
        array(
            'type'       => 'dropdown',
            'param_name' => 'unorder_list_style_type',
            'heading'    => __( 'Un-order list style type', 'w9sme-addon' ),
            'value'      => array(
                __( 'Default (Disc)', 'w9sme-addon' )         => 'disc',
                __( 'Circle', 'w9sme-addon' )                 => 'circle',
                __( 'Square', 'w9sme-addon' )                 => 'square',
                __( 'Dash', 'w9sme-addon' )                   => 'dash',
                __( 'Number Order In Circle', 'w9sme-addon' ) => 'number-order-in-circle'
            ),
            'dependency' => array(
                'element' => 'custom_list_style',
                'value'   => array( '1' )
            )
        ),
        
        array(
            'type'               => 'dropdown',
            'heading'            => esc_html__( 'List style color', 'w9sme-addon' ),
            'param_holder_class' => 'vc_colored-dropdown',
            'param_name'         => 'list_style_color',
            'value'              => array_merge( array( __( 'Default CSS', 'w9sme-addon' ) => '' ), W9sme_Map_Helpers::get_just_colors() ),
            'description'        => esc_html__( 'Select color for text display in the row. Please notice that this option will not effect with the inner element that has its own color scheme.', 'w9sme-addon' ),
            'std'                => '',
            'dependency'         => array(
                'element' => 'custom_list_style',
                'value'   => array( '1' )
            )
        ),
        W9sme_Map_Helpers::extra_class(),
        /*-------------------------------------
            DESIGN OPTIONS
        ---------------------------------------*/
        W9sme_Map_Helpers::design_options(),
        W9sme_Map_Helpers::design_options_on_tablet(),
        W9sme_Map_Helpers::design_options_on_mobile(),
        /*-------------------------------------
            ANIMATIONS
        ---------------------------------------*/
        W9sme_Map_Helpers::animation_css(),
        W9sme_Map_Helpers::animation_duration(),
        W9sme_Map_Helpers::animation_delay(),
    ),
) );
