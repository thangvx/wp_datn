<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-service-list.php
 * @time    : 4/24/2017 8:28 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( !post_type_exists( W9sme_CPT_Service::CPT_SLUG ) ) {
	return;
}

class W9sme_SC_User_Feedback extends WPBakeryShortCode {
	const SC_BASE = 'w9sme_shortcode_user_feedback';
	
	public function __construct($settings) {
		parent::__construct($settings);
	}
}