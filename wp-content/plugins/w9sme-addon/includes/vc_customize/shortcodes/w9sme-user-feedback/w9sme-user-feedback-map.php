<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-service-list-map.php
 * @time    : 4/24/2017 8:30 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map( array(
	'name'           => __( 'W9sme User Feedback', 'w9sme-addon' ),
	'base'           => W9sme_SC_User_Feedback::SC_BASE,
	'icon'           => 'fa fa-list-alt',
	'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
	'php_class_name' => 'W9sme_SC_User_Feedback',
	'params'         => array(
		array(
			'type'       => 'param_group',
			'heading'    => esc_html__( 'Review Types', 'w9sme-addon' ),
			'param_name' => 'uf_manual_data',
			'value'      => array(),
			'params'     => array(
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Type', 'w9sme-addon' ),
					'param_name'  => 'uf_type',
					'value'       => '',
					'description' => esc_html__( 'Enter custom review type.', 'w9sme-addon' ),
					'admin_label' => true
				)
			),
		),
//		array(
//			'type'        => 'number',
//			'heading'     => __( 'Total items', 'w9sme-addon' ),
//			'param_name'  => 'sl_total_items',
//			'std'         => '10',
//			'description' => __( 'Set max limit for items or enter - 1 to display all', 'w9sme-addon' ),
//			'dependency'  => array( 'element' => 'sl_data', 'value' => 'cpt' )
//		),
//		array(
//			'type'               => 'dropdown',
//			'heading'            => __( 'Text color', 'w9sme-addon' ),
//			'param_name'         => 'sl_tx_color',
//			'param_holder_class' => 'vc_colored-dropdown',
//			'value'              => W9sme_Map_Helpers::get_colors(),
//			'description'        => __( 'Select Color Scheme.', 'w9sme-addon' ),
////			'edit_field_class'   => 'vc_col-sm-6 vc_column'
//		),
//		array(
//			'type'             => 'colorpicker',
//			'heading'          => __( 'Custom text color', 'w9sme-addon' ),
//			'param_name'       => 'sl_tx_custom_color',
//			'value'            => '',
//			'dependency'       => array(
//				'element' => 'sl_tx_color',
//				'value'   => 'custom'
//			),
////			'edit_field_class' => 'vc_col-sm-6 vc_column'
//		),
//		array(
//			'type'               => 'dropdown',
//			'heading'            => __( 'Line color', 'w9sme-addon' ),
//			'param_name'         => 'sl_line_color',
//			'param_holder_class' => 'vc_colored-dropdown',
//			'value'              => W9sme_Map_Helpers::get_colors(),
//			'description'        => __( 'Select Color Scheme.', 'w9sme-addon' ),
//			'edit_field_class'   => 'vc_col-sm-6 vc_column'
//		),
//		array(
//			'type'             => 'colorpicker',
//			'heading'          => __( 'Custom line color', 'w9sme-addon' ),
//			'param_name'       => 'sl_line_custom_color',
//			'value'            => '',
//			'dependency'       => array(
//				'element' => 'sl_line_color',
//				'value'   => 'custom'
//			),
//			'edit_field_class' => 'vc_col-sm-6 vc_column'
//		),
		W9sme_Map_Helpers::extra_class(),
		W9sme_Map_Helpers::design_options(),
		W9sme_Map_Helpers::animation_css(),
		W9sme_Map_Helpers::animation_duration(),
		W9sme_Map_Helpers::animation_delay()
	)
) );