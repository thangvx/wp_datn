<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-portfolio-page-title.php
 * @time    : 8/29/16 4:54 PM
 * @author  : 9WPThemes Team
 */

if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

if ( !class_exists( 'W9sme_SC_Portfolio_Info' ) ) {
    class W9sme_SC_Portfolio_Page_Title extends WPBakeryShortCode {
        const SC_BASE = 'w9sme_shortcode_portfolio_page_title';
    }
}
