<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-widget-shop-account-map.php
 * @time    : 10/7/2016 3:44 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map(
	array(
		'name'           => esc_html__( 'W9sme Widget Shop Account', 'w9sme-addon' ),
		'base'           => W9sme_SC_Widget_Shop_Account::SC_BASE,
		'icon'           => 'w9 w9-ico-profile-male',
		'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
		'php_class_name' => 'W9sme_SC_Widget_Shop_Account',
		'description'    => __( 'Widget for WooCommerce "my account"', 'w9sme-addon' ),
		'params'         => array_merge( array(
			array(
				'type'        => 'textfield',
				'param_name'  => 'title',
				'heading'     => __( 'Widget title', 'w9sme-addon' ),
				'description' => __( 'What text use as a widget title. Leave blank if you don\' want to show the widget title.', 'w9sme-addon' ),
			),
			
			array(
				'type'       => 'dropdown',
				'param_name' => 'module_type',
				'heading'    => __( 'Module type', 'w9sme-addon' ),
				'value'      => array(
					__( 'Link', 'w9sme-addon' )  => 'link',
					__( 'Popup', 'w9sme-addon' ) => 'popup',
				),
				'std'        => 'link'
			),
			
			array(
				'type'       => 'dropdown',
				'param_name' => 'link_to',
				'heading'    => __( 'Link to', 'w9sme-addon' ),
				'value'      => array(
					__( 'Link to "My account" page', 'w9sme-addon' ) => 'my-account',
					__( 'Custom link', 'w9sme-addon' )               => 'custom',
				),
				'dependency' => array(
					'element' => 'module_type',
					'value'   => 'link'
				),
				'std'        => 'my-account'
			),
			
			array(
				'type'       => 'textfield',
				'param_name' => 'custom_link',
				'heading'    => __( 'Custom link', 'w9sme-addon' ),
				'dependency' => array(
					'element' => 'link_to',
					'value'   => 'custom'
				),
				'std'        => ''
			),
		), W9sme_Map_Helpers::widget_common_params() )
	)
);