<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-portfolio-about-map.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

if ( is_admin()  && (!defined( 'DOING_AJAX' ) && ( !post_type_exists( W9sme_CPT_Portfolio::CPT_SLUG ) || w9sme_get_current_post_type() !== W9sme_CPT_Portfolio::CPT_SLUG ))) {
    return;
}

vc_map( array(
    'name'           => __( 'W9sme Portfolio About', 'w9sme-addon' ),
    'base'           => W9sme_SC_Portfolio_About::SC_BASE,
    'php_class_name' => 'W9sme_SC_Portfolio_About',
    'icon'           => 'w9 w9-ico-software-layout-8boxes',
    'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_PORTFOLIO_SC_CATEGORY ),
    'description'    => __('Get meta info about from single portfolio', 'w9sme-addon' ),
    'post_type'      => W9sme_CPT_Portfolio::CPT_SLUG,
    'params'         => array(
        W9sme_Map_Helpers::extra_class(),
        W9sme_Map_Helpers::design_options(),
        W9sme_Map_Helpers::animation_css(),
        W9sme_Map_Helpers::animation_duration(),
        W9sme_Map_Helpers::animation_delay()
    )
) );
