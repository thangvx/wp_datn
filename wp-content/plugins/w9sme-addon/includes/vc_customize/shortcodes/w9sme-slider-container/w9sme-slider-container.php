<?php

/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-slider-container.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

class W9sme_SC_Slider_Container extends WPBakeryShortCodesContainer {
    const SC_BASE = 'w9sme_shortcode_slider_container';
}


if ( !function_exists( 'w9sme_sc_slider_container' ) ) {
    function w9sme_sc_slider_container( $content, array $attr = array() ) {
        $var = '';
        foreach ( $attr as $key => $value ) {
            $var .= sprintf( ' %s="%s"', $key, $value );
        }
        
        echo do_shortcode( "[w9sme_shortcode_slider_container $var]" . $content . "[/w9sme_shortcode_slider_container]" );
    }
}