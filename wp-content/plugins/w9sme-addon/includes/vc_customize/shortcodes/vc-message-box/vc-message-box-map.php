<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: vc-message-box-map.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$pixel_icons   = vc_pixel_icons();
$custom_colors = array(
    __( 'W9sme Informational', 'w9sme-addon' )  => 'w9sme-info',
    __( 'W9sme Warning', 'w9sme-addon' )        => 'w9sme-warning',
    __( 'W9sme Success', 'w9sme-addon' )        => 'w9sme-success',
    __( 'W9sme Error', 'w9sme-addon' )          => 'w9sme-danger',
    __( 'W9sme Notice', 'w9sme-addon' )         => 'w9sme-notice',
    __( 'Informational', 'w9sme-addon' )         => 'info',
    __( 'Warning', 'w9sme-addon' )               => 'warning',
    __( 'Success', 'w9sme-addon' )               => 'success',
    __( 'Error', 'w9sme-addon' )                 => 'danger',
    __( 'Informational Classic', 'w9sme-addon' ) => 'alert-info',
    __( 'Warning Classic', 'w9sme-addon' )       => 'alert-warning',
    __( 'Success Classic', 'w9sme-addon' )       => 'alert-success',
    __( 'Error Classic', 'w9sme-addon' )         => 'alert-danger',
);

vc_map( array(
    'name'        => __( 'Message Box', 'w9sme-addon' ),
    'base'        => 'vc_message',
    'icon'        => 'w9 w9-ico-basic-postcard-multiple',
    'category'    => __( 'Content', 'w9sme-addon' ),
    'description' => __( 'Notification box', 'w9sme-addon' ),
    'params'      => array(
        array(
            'type'               => 'params_preset',
            'heading'            => __( 'Message box presets', 'w9sme-addon' ),
            'param_name'         => 'color',
            // due to backward compatibility, really it is message_box_type
            'value'              => '',
            'options'            => array(
                array(
                    'label'  => __( 'Custom', 'w9sme-addon' ),
                    'value'  => '',
                    'params' => array(),
                ),
                array(
                    'label'  => __( 'Informational', 'w9sme-addon' ),
                    'value'  => 'info',
                    'params' => array(
                        'message_box_color' => 'info',
                        'type'              => 'fontawesome',
                        'icon_fontawesome'  => 'fa fa-info-circle',
                    ),
                ),
                array(
                    'label'  => __( 'Warning', 'w9sme-addon' ),
                    'value'  => 'warning',
                    'params' => array(
                        'message_box_color' => 'warning',
                        'type'              => 'fontawesome',
                        'icon_fontawesome'  => 'fa fa-exclamation-triangle',
                    ),
                ),
                array(
                    'label'  => __( 'Success', 'w9sme-addon' ),
                    'value'  => 'success',
                    'params' => array(
                        'message_box_color' => 'success',
                        'type'              => 'fontawesome',
                        'icon_fontawesome'  => 'fa fa-check',
                    ),
                ),
                array(
                    'label'  => __( 'Error', 'w9sme-addon' ),
                    'value'  => 'danger',
                    'params' => array(
                        'message_box_color' => 'danger',
                        'type'              => 'fontawesome',
                        'icon_fontawesome'  => 'fa fa-times',
                    ),
                ),
                array(
                    'label'  => __( 'Informational Classic', 'w9sme-addon' ),
                    'value'  => 'alert-info',
                    // due to backward compatibility
                    'params' => array(
                        'message_box_color' => 'alert-info',
                        'type'              => 'pixelicons',
                        'icon_pixelicons'   => 'vc_pixel_icon vc_pixel_icon-info',
                    ),
                ),
                array(
                    'label'  => __( 'Warning Classic', 'w9sme-addon' ),
                    'value'  => 'alert-warning',
                    // due to backward compatibility
                    'params' => array(
                        'message_box_color' => 'alert-warning',
                        'type'              => 'pixelicons',
                        'icon_pixelicons'   => 'vc_pixel_icon vc_pixel_icon-alert',
                    ),
                ),
                array(
                    'label'  => __( 'Success Classic', 'w9sme-addon' ),
                    'value'  => 'alert-success',
                    // due to backward compatibility
                    'params' => array(
                        'message_box_color' => 'alert-success',
                        'type'              => 'pixelicons',
                        'icon_pixelicons'   => 'vc_pixel_icon vc_pixel_icon-tick',
                    ),
                ),
                array(
                    'label'  => __( 'Error Classic', 'w9sme-addon' ),
                    'value'  => 'alert-danger',
                    // due to backward compatibility
                    'params' => array(
                        'message_box_color' => 'alert-danger',
                        'type'              => 'pixelicons',
                        'icon_pixelicons'   => 'vc_pixel_icon vc_pixel_icon-explanation',
                    ),
                ),
            ),
            'description'        => __( 'Select predefined message box design or choose "Custom" for custom styling.', 'w9sme-addon' ),
            'param_holder_class' => 'vc_message-type vc_colored-dropdown',
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Size', 'w9sme-addon' ),
            'param_name'  => 'message_box_size',
            // due to backward compatibility message_box_shape
            'std'         => 'mini',
            'value'       => array(
                __( 'Mini', 'w9sme-addon' )  => 'mini',
                __( 'Large', 'w9sme-addon' ) => 'large',
            ),
            'description' => __( 'Select message box size.', 'w9sme-addon' ),
        ),
        
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Style', 'w9sme-addon' ),
            'param_name'  => 'message_box_style',
            'value'       => getVcShared( 'message_box_styles' ),
            'description' => __( 'Select message box design style.', 'w9sme-addon' ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Shape', 'w9sme-addon' ),
            'param_name'  => 'style',
            // due to backward compatibility message_box_shape
            'std'         => 'rounded',
            'value'       => array(
                __( 'Square', 'w9sme-addon' )  => 'square',
                __( 'Rounded', 'w9sme-addon' ) => 'rounded',
                __( 'Round', 'w9sme-addon' )   => 'round',
            ),
            'description' => __( 'Select message box shape.', 'w9sme-addon' ),
            'admin_label' => true
        ),
        array(
            'type'               => 'dropdown',
            'heading'            => __( 'Color', 'w9sme-addon' ),
            'param_name'         => 'message_box_color',
            'value'              => $custom_colors + getVcShared( 'colors' ),
            'description'        => __( 'Select message box color.', 'w9sme-addon' ),
            'param_holder_class' => 'vc_message-type vc_colored-dropdown',
            'admin_label'        => true
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Icon library', 'w9sme-addon' ),
            'value'       => array(
                __( '9WPThemes', 'w9sme-addon' )    => '9wpthemes',
                __( 'Font Awesome', 'w9sme-addon' ) => 'fontawesome',
                __( 'Open Iconic', 'w9sme-addon' )  => 'openiconic',
                __( 'Typicons', 'w9sme-addon' )     => 'typicons',
                __( 'Entypo', 'w9sme-addon' )       => 'entypo',
                __( 'Linecons', 'w9sme-addon' )     => 'linecons',
                __( 'Pixel', 'w9sme-addon' )        => 'pixelicons',
                __( 'Mono Social', 'w9sme-addon' )  => 'monosocial',
            ),
            'param_name'  => 'type',
            'description' => __( 'Select icon library.', 'w9sme-addon' ),
        ),
        W9sme_Map_Helpers::get_icon_picker_9wpthemes(),
        W9sme_Map_Helpers::get_icon_picker_fontawesome(),
        W9sme_Map_Helpers::get_icon_picker_openiconic(),
        W9sme_Map_Helpers::get_icon_picker_typicons(),
        W9sme_Map_Helpers::get_icon_picker_entypo(),
        W9sme_Map_Helpers::get_icon_picker_linecons(),
        W9sme_Map_Helpers::get_icon_picker_monosocial(),
        array(
            'type'        => 'iconpicker',
            'heading'     => __( 'Icon', 'w9sme-addon' ),
            'param_name'  => 'icon_pixelicons',
            'settings'    => array(
                'emptyIcon' => false,
                // default true, display an "EMPTY" icon?
                'type'      => 'pixelicons',
                'source'    => $pixel_icons,
            ),
            'dependency'  => array(
                'element' => 'type',
                'value'   => 'pixelicons',
            ),
            'description' => __( 'Select icon from library.', 'w9sme-addon' ),
        ),
        array(
            'type'       => 'textarea_html',
            'holder'     => 'div',
            'class'      => 'messagebox_text',
            'heading'    => __( 'Message text', 'w9sme-addon' ),
            'param_name' => 'content',
            'value'      => __( '<p>I am message box. Click edit button to change this text.</p>', 'w9sme-addon' ),
        ),
        array(
            'type'       => 'dropdown',
            'heading'    => __( 'Message font family', 'w9sme-addon' ),
            'param_name' => 'messagebox_ff',
            'value'      => array(
                __( 'Inherit', 'w9sme-addon' )        => '',
                __( 'Primary Font', 'w9sme-addon' )   => 'p-font',
                __( 'Secondary Font', 'w9sme-addon' ) => 's-font',
                __( 'Third Font', 'w9sme-addon' )     => 't-font',
            ),
        ),
        array(
            'type'       => 'dropdown',
            'heading'    => __( 'Message font size', 'w9sme-addon' ),
            'param_name' => 'messagebox_fz',
            'value'      => array(
                __( 'Inherit', 'w9sme-addon' ) => '',
                '10px'                             => 'fz-10',
                '11px'                             => 'fz-11',
                '12px'                             => 'fz-12',
                '13px'                             => 'fz-13',
                '14px'                             => 'fz-14',
                '15px'                             => 'fz-15',
                '16px'                             => 'fz-16',
                '17px'                             => 'fz-17',
                '18px'                             => 'fz-18',
                '19px'                             => 'fz-19',
                '20px'                             => 'fz-20',
                '21px'                             => 'fz-21',
                '22px'                             => 'fz-22',
                '23px'                             => 'fz-23',
                '24px'                             => 'fz-24',
                '25px'                             => 'fz-25',
                '26px'                             => 'fz-26',
                '27px'                             => 'fz-27',
                '28px'                             => 'fz-28',
                '29px'                             => 'fz-29',
                '30px'                             => 'fz-30',
            ),
        ),
        array(
            'type'        => 'switcher',
            'param_name'  => 'message_box_dismissible',
            'heading'     => __( 'Message box dismissible?', 'w9sme-addon' ),
            'std'         => '1',
            'admin_label' => true
        ),
        array(
            'type'        => 'textfield',
            'heading'     => __( 'Extra class name', 'w9sme-addon' ),
            'param_name'  => 'el_class',
            'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'w9sme-addon' ),
        ),
        array(
            'type'       => 'css_editor',
            'heading'    => __( 'CSS box', 'w9sme-addon' ),
            'param_name' => 'css',
            'group'      => __( 'Design Options', 'w9sme-addon' ),
        ),
        W9sme_Map_Helpers::animation_css(),
        W9sme_Map_Helpers::animation_duration(),
        W9sme_Map_Helpers::animation_delay()
    )
) );