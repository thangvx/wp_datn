<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-blog-archive-map.php
 * @time    : 4/9/2017 3:42 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map(
	array(
		'name'           => esc_html__( 'W9sme Blog Archive', 'w9sme-addon' ),
		'base'           => W9sme_SC_Blog_Archive::SC_BASE,
		'icon'           => 'w9 w9-ico-dot-3',
		'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
		'php_class_name' => 'W9sme_SC_Blog_Archive',
		'params'         => array(
			array(
				'type'        => 'dropdown',
				'param_name'  => 'ba_display_type',
				'heading'     => esc_html__( 'Display type', 'w9sme-addon' ),
				'description' => esc_html__( 'Select archive display type.', 'w9sme-addon' ),
				'value'       => array(
					esc_html__( 'Wide', 'w9sme-addon' )    => 'wide',
					esc_html__( 'Grid', 'w9sme-addon' )    => 'grid',
					esc_html__( 'Masonry', 'w9sme-addon' ) => 'masonry',
				),
				'std'         => 'wide',
				'admin_label' => true,
			),
			array(
				'type'        => 'dropdown',
				'param_name'  => 'ba_display_style',
				'heading'     => esc_html__( 'Display style', 'w9sme-addon' ),
				'description' => esc_html__( 'Select archive display style.', 'w9sme-addon' ),
				'value'       => array(
					esc_html__( 'Classic', 'w9sme-addon' )  => 'style-1',
					esc_html__( 'Modern 1', 'w9sme-addon' ) => 'style-2',
					esc_html__( 'Modern 2', 'w9sme-addon' ) => 'style-3'
				),
				'std'         => 'style-1',
				'admin_label' => true,
			),
			
			array(
				'type'        => 'dropdown',
				'param_name'  => 'ba_display_columns',
				'heading'     => esc_html__( 'Display columns', 'w9sme-addon' ),
				'description' => esc_html__( 'Choose the number of columns to display on archive pages.', 'w9sme-addon' ),
				'value'       => array(
					'2' => '2',
					'3' => '3',
					'4' => '4'
				),
				'std'         => '3',
				'dependency'  => array(
					'element' => 'ba_display_type',
					'value'   => array( 'masonry', 'grid' ),
				),
				'admin_label' => true,
			),
			
			
			array(
				'type'        => 'switcher',
				'param_name'  => 'ba_item_header',
				'heading'     => esc_html__( 'Enable blog item header', 'w9sme-addon' ),
				'description' => esc_html__( 'Enable blog item header.', 'w9sme-addon' ),
				'std'         => 1
			),
			// meta
			//============================
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Blog item image size', 'w9sme-addon' ),
				'param_name'  => 'ba_item_image_size',
				'value'       => get_intermediate_image_sizes(),
				'group'       => __( 'Meta', 'w9sme-addon' ),
				'description' => __( 'Select image size from list.', 'w9sme-addon' ),
				'std'         => 'w9sme_1170',
			),
			
			array(
				'type'       => 'dropdown',
				'param_name' => 'ba_item_image_ratio',
				'heading'    => esc_html__( 'Blog item image ratio', 'w9sme-addon' ),
//			'description' => esc_html__( 'Action with the feature image of the blog item.', 'w9sme-addon' ),
				'value'      => wp_parse_args( array( __( 'Original', 'w9sme-addon' ) => 'original' ), W9sme_Image::get_w9sme_ratio_list() ),
				'group'      => __( 'Meta', 'w9sme-addon' ),
				'std'        => 'original',
			),
			
			array(
				'type'        => 'dropdown',
				'param_name'  => 'ba_item_image_action',
				'heading'     => esc_html__( 'Blog item feature image action', 'w9sme-addon' ),
				'description' => esc_html__( 'Action with the feature image of the blog item.', 'w9sme-addon' ),
				'value'       => array(
					esc_html__( 'None', 'w9sme-addon' )         => 'none',
					esc_html__( 'Link to post', 'w9sme-addon' ) => 'link',
				),
				'group'       => __( 'Meta', 'w9sme-addon' ),
				'std'         => 'link',
			),

			array(
				'type'             => 'switcher',
				'heading'          => __( 'Show date ? ', 'w9sme-addon' ),
				'param_name'       => 'ba_show_date',
				'std'              => '1',
				'group'            => __( 'Meta', 'w9sme-addon' ),
				'edit_field_class' => 'vc_col-sm-6 vc_column',
			),
			
			array(
				'type'             => 'switcher',
				'heading'          => __( 'Show author ? ', 'w9sme-addon' ),
				'param_name'       => 'ba_show_author',
				'std'              => '1',
				'group'            => __( 'Meta', 'w9sme-addon' ),
				'edit_field_class' => 'vc_col-sm-6 vc_column',
			),
			
			array(
				'type'             => 'switcher',
				'heading'          => __( 'Show category ? ', 'w9sme-addon' ),
				'param_name'       => 'ba_show_category',
				'std'              => '1',
				'group'            => __( 'Meta', 'w9sme-addon' ),
				'edit_field_class' => 'vc_col-sm-6 vc_column',
			),
			
			array(
				'type'             => 'switcher',
				'heading'          => __( 'Show tags ? ', 'w9sme-addon' ),
				'param_name'       => 'ba_show_tags',
				'std'              => '0',
				'group'            => __( 'Meta', 'w9sme-addon' ),
				'edit_field_class' => 'vc_col-sm-6 vc_column',
			),
			
			array(
				'type'             => 'switcher',
				'heading'          => __( 'Show comments ? ', 'w9sme-addon' ),
				'param_name'       => 'ba_show_comments',
				'std'              => '1',
				'group'            => __( 'Meta', 'w9sme-addon' ),
				'edit_field_class' => 'vc_col-sm-6 vc_column',
			),
			
			array(
				'type'             => 'switcher',
				'heading'          => __( 'Show social-share ? ', 'w9sme-addon' ),
				'param_name'       => 'ba_show_social_share',
				'std'              => '1',
				'group'            => __( 'Meta', 'w9sme-addon' ),
				'edit_field_class' => 'vc_col-sm-6 vc_column',
			),
			
			//============================
			
			//paging
			array(
				'type'        => 'number',
				'heading'     => esc_html__( 'Number Of Items To Show', 'w9-9wpthemes-addon' ),
				'description' => esc_html__( 'Default value is 6. If you want to show all post, then enter value -1.', 'w9-9wpthemes-addon' ),
				'param_name'  => 'ba_item_amount',
				'admin_label' => true,
				'value'       => '6'
			),
			
			array(
				'type'        => 'dropdown',
				'param_name'  => 'ba_paging_type',
				'heading'     => esc_html__( 'Paging type', 'w9sme-addon' ),
				'description' => esc_html__( 'Select archive paging type.', 'w9sme-addon' ),
				'value'       => array(
					esc_html__( 'No Pagination (Show all)', 'w9-9wpthemes-addon' ) => 'no-paging',
					esc_html__( 'Default', 'w9sme-addon' )                         => 'default',
					esc_html__( 'Load More', 'w9sme-addon' )                       => 'load-more',
					esc_html__( 'Infinite Scroll', 'w9sme-addon' )                 => 'infinite-scroll'
				),
				'std'         => 'default',
				'dependency'  => array(
					'element' => 'ba_item_amount',
					'value'   => array( '-1' )
				),
			),
			
			array(
				'type'       => 'number',
				'heading'    => esc_html__( 'Items Per Page', 'w9-9wpthemes-addon' ),
				'param_name' => 'ba_item_per_page',
				'value'      => '6',
				'dependency' => array(
					'element' => 'ba_paging_type',
					'value'   => array( 'default', 'load-more', 'infinite-scroll' )
				),
			),
			
			array(
				'type'        => 'dropdown',
				'param_name'  => 'ba_paging_style',
				'heading'     => esc_html__( 'Paging style', 'w9sme-addon' ),
				'description' => esc_html__( 'Select archive paging style.', 'w9sme-addon' ),
				'value'       => array(
					esc_html__( 'Classic', 'w9sme-addon' ) => 'style-1',
					esc_html__( 'Modern', 'w9sme-addon' )  => 'style-2',
				),
				'std'         => 'style-2',
				'dependency'  => array(
					'element' => 'ba_paging_type',
					'value'   => array( 'default' ),
				)
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => esc_html__( 'Order By', 'w9-9wpthemes-addon' ),
				'param_name'  => 'ba_order_by',
				'admin_label' => true,
				'value'       => array(
					esc_html__( 'Random', 'w9-9wpthemes-addon' )  => 'random',
					esc_html__( 'Popular', 'w9-9wpthemes-addon' ) => 'popular',
					esc_html__( 'Recent', 'w9-9wpthemes-addon' )  => 'recent',
					esc_html__( 'Oldest', 'w9-9wpthemes-addon' )  => 'oldest'
				),
				'std'         => 'recent',
				'description' => esc_html__( 'Select Orderby.', 'w9-9wpthemes-addon' )
			),
			
			array(
				'type'        => 'dropdown',
				'param_name'  => 'ba_paging_align',
				'heading'     => esc_html__( 'Paging align', 'w9sme-addon' ),
				'description' => esc_html__( 'Select archive paging align.', 'w9sme-addon' ),
				'value'       => array(
					esc_html__( 'left', 'w9sme-addon' )   => 'left',
					esc_html__( 'Center', 'w9sme-addon' ) => 'center',
					esc_html__( 'Right', 'w9sme-addon' )  => 'right',
				),
				'std'         => 'center',
				'dependency'  => array(
					'element' => 'ba_paging_type',
					'value'   => array( 'default' ),
				)
			),
			W9sme_Map_Helpers::extra_class(),
			W9sme_Map_Helpers::design_options(),
		)
	)
);