<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-portfolio-map.php
 * @time    : 4/20/2017 3:02 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! post_type_exists( W9sme_CPT_Portfolio::CPT_SLUG ) ) {
	return;
}

$slider_params = vc_map_integrate_shortcode( W9sme_SC_Slider_Container::SC_BASE, '', __( 'Slider', 'w9sme-addon' ), array(
	'exclude' => array(
		'css',
		'el_class',
		'animation_css',
		'animation_duration',
		'animation_delay',
		'tablet_css',
		'mobile_css',
	),
	// we need only type, icon_fontawesome, icon_blabla..., NOT color and etc
), array(
	'element' => 'portfolios_layout_type',
	'value'   => 'layout-slider',
) );

$portfolio_cats      = w9sme_get_terms_by_tax( W9sme_CPT_Portfolio::TAX_SLUG, 'slug' );
$portfolio_cpt_slug  = W9sme_CPT_Portfolio::CPT_SLUG;
$portfolio_get_posts = get_posts( array(
	'posts_per_page' => - 1,
	'post_status'    => 'any',
	'post_type'      => $portfolio_cpt_slug,
	'post_parent'    => null
) );
$portfolio_names     = array();
if ( is_array( $portfolio_get_posts ) ) {
	foreach ( $portfolio_get_posts as $item ) {
		$portfolio_names[ $item->ID ] = $item->post_title;
	}
}

vc_map(
	array(
		'name'           => esc_html__( 'W9sme Portfolios', 'w9sme-addon' ),
		'base'           => W9sme_SC_Portfolio::SC_BASE,
		'icon'           => 'w9 w9-ico-cd',
		'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
		'php_class_name' => 'W9sme_SC_Portfolio',
		'params'         => array(
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Data Source', 'osthemes-pure' ),
				'param_name'  => 'portfolios_data',
				'value'       => array(
					__( 'Categories', 'w9sme-addon' ) => 'category',
					__( 'All', 'w9sme-addon' )        => 'all',
				),
				'admin_label' => true
			),
			array(
				'type'       => 'multi-select',
				'heading'    => __( 'Include (categories):', 'osthemes-pure' ),
				'param_name' => 'portfolios_category',
				'options'    => $portfolio_cats,
				'dependency' => array(
					'element' => 'portfolios_data',
					'value'   => array( 'category' ),
				),
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Style', 'w9sme-addon' ),
				'param_name'  => 'portfolios_style',
				'value'       => array(
					__( 'Simple', 'w9sme-addon' )  => 'simple',
					__( 'Overlay', 'w9sme-addon' ) => 'overlay',
				),
				'description' => __( 'Select the portfolios style', 'w9sme-addon' ),
				'admin_label' => true
			),
			
			array(
				'type'        => 'colorpicker',
				'heading'     => __( 'Overlay color', 'w9-sprig-addon' ),
				'param_name'  => 'portfolio_overlay_color',
				'description' => __( 'Select portfolio overlay color.', 'w9-sprig-addon' ),
				'dependency'  => array(
					'element' => 'portfolios_style',
					'value'   => array( 'overlay' ),
				),
				'std'         => '#ffffff'
			),
			
			array(
				'type'        => 'colorpicker',
				'heading'     => __( 'Overlay background color', 'w9-sprig-addon' ),
				'param_name'  => 'portfolio_overlay_background_color',
				'description' => __( 'Select portfolio overlay background color.', 'w9-sprig-addon' ),
				'dependency'  => array(
					'element' => 'portfolios_style',
					'value'   => array( 'overlay' ),
				),
				'std'         => 'rgba(0, 0, 0, 0.8)'
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Overlay appear effect', 'w9sme-addon' ),
				'param_name'  => 'portfolio_overlay_appear_effect',
				'value'       => array(
					esc_html__( 'None', 'w9sme' )           => 'none',
					esc_html__( 'Hover Dir', 'w9sme' )      => '__hoverdir',
					esc_html__( 'Fade In', 'w9sme' )        => '__animation-fade-in',
					esc_html__( 'Fade In Left', 'w9sme' )   => '__animation-fade-in-left',
					esc_html__( 'Fade In Right', 'w9sme' )  => '__animation-fade-in-right',
					esc_html__( 'Fade In Top', 'w9sme' )    => '__animation-fade-in-top',
					esc_html__( 'Fade In Bottom', 'w9sme' ) => '__animation-fade-in-bottom',
					esc_html__( 'Zoom In', 'w9sme' )        => '__animation-zoom-in',
					esc_html__( 'Flip In X', 'w9sme' )      => '__animation-flip-in-x',
					esc_html__( 'Flip In Y', 'w9sme' )      => '__animation-flip-in-y',
				),
				'std'         => '__hoverdir',
				'description' => __( 'Choose overlay appear effect', 'w9sme-addon' ),
				'dependency'  => array(
					'element' => 'portfolios_style',
					'value'   => array( 'overlay' ),
				),
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Display type', 'w9sme-addon' ),
				'param_name'  => 'portfolios_display_type',
				'value'       => array(
					__( 'Grid', 'w9sme-addon' )    => 'grid',
					__( 'Masonry', 'w9sme-addon' ) => 'masonry',
				),
				'description' => __( 'Select the display type for the portfolios.', 'w9sme-addon' ),
				'admin_label' => true
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Sort order', 'w9sme-addon' ),
				'param_name'  => 'portfolios_sort_order',
				'value'       => array(
					__( 'Random', 'w9sme-addon' )  => 'random',
					__( 'Popular', 'w9sme-addon' ) => 'popular',
					__( 'Recent', 'w9sme-addon' )  => 'recent',
					__( 'Oldest', 'w9sme-addon' )  => 'oldest'
				),
				'std'         => 'recent',
				'description' => __( 'Select sorting order . ', 'w9sme-addon' ),
				'dependency'  => array(
					'element' => 'portfolios_data',
					'value'   => array( 'category' ),
				),
			),
			
			array(
				'type'        => 'number',
				'heading'     => __( 'Total items', 'w9sme-addon' ),
				'param_name'  => 'portfolios_total_items',
				'std'         => '-1',
				'description' => __( 'Set max limit for items or enter - 1 to display all', 'w9sme-addon' ),
				'admin_label' => true
			),
			
			array(
				'type'        => 'dropdown',
				'param_name'  => 'portfolios_paging_type',
				'heading'     => esc_html__( 'Paging type', 'w9sme-addon' ),
				'description' => esc_html__( 'Select archive paging type.', 'w9sme-addon' ),
				'value'       => array(
					esc_html__( 'No Pagination (Show all)', 'w9-9wpthemes-addon' ) => 'no-paging',
					esc_html__( 'Default', 'w9sme-addon' )                         => 'default',
					esc_html__( 'Load More', 'w9sme-addon' )                       => 'load-more',
					esc_html__( 'Infinite Scroll', 'w9sme-addon' )                 => 'infinite-scroll'
				),
				'std'         => 'default',
				'dependency'  => array(
					'element' => 'portfolios_total_items',
					'value'   => array( '-1' )
				),
			),
			
			array(
				'type'       => 'number',
				'heading'    => esc_html__( 'Items Per Page', 'w9-9wpthemes-addon' ),
				'param_name' => 'portfolios_item_per_page',
				'value'      => '6',
				'dependency' => array(
					'element' => 'portfolios_paging_type',
					'value'   => array( 'default', 'load-more', 'infinite-scroll' )
				),
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Columns', 'w9sme-addon' ),
				'param_name'  => 'portfolios_column',
				'std'         => '3',
				'value'       => array(
					__( '1', 'w9sme-addon' ) => '1',
					__( '2', 'w9sme-addon' ) => '2',
					__( '3', 'w9sme-addon' ) => '3',
					__( '4', 'w9sme-addon' ) => '4',
					__( '6', 'w9sme-addon' ) => '6',
				),
				'description' => __( 'Set the column numbers.', 'w9sme-addon' ),
				'admin_label' => true
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Columns spacing', 'w9sme-addon' ),
				'param_name'  => 'portfolios_archive_gutter',
				'std'         => '30',
				'value'       => array(
					'0px'  => '0',
					'1px'  => '1',
					'2px'  => '2',
					'3px'  => '3',
					'4px'  => '4',
					'5px'  => '5',
					'10px' => '10',
					'15px' => '15',
					'20px' => '20',
					'25px' => '25',
					'30px' => '30',
					'35px' => '35',
					'40px' => '40',
					'45px' => '45',
					'50px' => '50',
					'55px' => '55',
					'60px' => '60',
				),
				'description' => __( 'Set the column numbers.', 'w9sme-addon' ),
				'admin_label' => true
			),
			
			// Meta
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Image size', 'w9sme-addon' ),
				'param_name'  => 'portfolios_image_size',
				'value'       => get_intermediate_image_sizes(),
				'std'         => 'w9sme_570',
				'description' => __( 'Select image size from list.', 'w9sme-addon' ),
				'group'       => __( 'Meta', 'w9sme-addon' ),
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Image ratio', 'w9sme-addon' ),
				'description' => __( 'Image ratio base on image size width . ', 'w9sme-addon' ),
				'param_name'  => 'portfolios_image_ratio',
				'value'       => wp_parse_args( array( __( 'Original', 'w9sme-addon' ) => 'original' ), W9sme_Image::get_w9sme_ratio_list() ),
				'group'       => __( 'Meta', 'w9sme-addon' ),
				'std'         => '0.666666667'
			),
			
			array(
				'type'        => 'switcher',
				'heading'     => esc_html__( 'Enable filter', 'w9sme-addon' ),
				'param_name'  => 'portfolios_enable_filter',
				'description' => __( 'Enable portfolios filter', 'w9sme-addon' ),
				'group'       => __( 'Meta', 'w9sme-addon' ),
				'value'       => '0',
			),
			
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Filter Alignment', 'w9sme-addon' ),
				'param_name'  => 'portfolios_filter_alignment',
				'std'         => 'left',
				'value'       => array(
					__( 'Left', 'w9sme-addon' )   => 'text-left',
					__( 'Center', 'w9sme-addon' ) => 'text-center',
					__( 'Right', 'w9sme-addon' )  => 'text-right',
				),
				'description' => __( 'Set alignment for the filter.', 'w9sme-addon' ),
				'group'       => __( 'Meta', 'w9sme-addon' ),
				'dependency'  => array(
					'element' => 'portfolios_enable_filter',
					'value'   => array( '1' )
				)
			),
			
			W9sme_Map_Helpers::extra_class(),
			W9sme_Map_Helpers::design_options(),
			W9sme_Map_Helpers::animation_css(),
			W9sme_Map_Helpers::animation_duration(),
			W9sme_Map_Helpers::animation_delay()
		)
	)
);