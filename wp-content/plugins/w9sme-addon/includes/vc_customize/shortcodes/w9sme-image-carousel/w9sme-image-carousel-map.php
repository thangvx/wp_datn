<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-image-carousel-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$slider_setting_map = vc_map_integrate_shortcode( W9sme_SC_Slider_Container::SC_BASE , '', __( 'Slider', 'w9_sping_addon' ),null);

vc_map(
    array(
        'name' => __( 'W9sme Images Carousel', 'w9sme-addon' ),
        'base' => W9sme_SC_Image_Carousel::SC_BASE,
        'php_class_name' => 'W9sme_SC_Image_Carousel',
        'icon' => 'w9 w9-ico-basic-picture-multiple',
        'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
        'description' => __( 'Animated carousel with images', 'w9sme-addon' ),
        'params' => array_merge(array(
            array(
                'type' => 'attach_images',
                'heading' => __( 'Images', 'w9sme-addon' ),
                'param_name' => 'images',
                'admin_label' => true,
                'value' => '',
                'description' => __( 'Select images from media library.', 'w9sme-addon' ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'Image size', 'w9sme-addon' ),
                'param_name' => 'img_size',
                'value' => wp_parse_args( array( __( 'Custom', 'w9sme-addon' ) => 'custom' ), get_intermediate_image_sizes() ),
                'std' => 'w9sme_1170',
                'description' => __( 'Select image size from list.', 'w9sme-addon' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Image size', 'w9sme-addon' ),
                'param_name' => 'img_size_custom',
                'std' => '1280x720',
                'description' => __( 'Enter image size in pixels. Example: 200x100 (Width x Height).', 'w9sme-addon' ),
                'dependency' => array(
                    'element' => 'img_size',
                    'value' => array( 'custom'),
                ),
            ),
            array(
                'type'        => 'dropdown',
                'heading'     => __( 'Image ratio', 'w9sme-addon' ),
                'description' => __( 'Image ratio base on image size width.', 'w9sme-addon' ),
                'param_name'  => 'image_ratio',
                'value'       => wp_parse_args( array( __( 'Original', 'w9sme-addon' ) => 'original' ), W9sme_Image::get_w9sme_ratio_list() ),
                'std'         => 'original'
            ),
            array(
                'type' => 'dropdown',
                'heading' => __( 'On click action', 'w9sme-addon' ),
                'param_name' => 'onclick',
                'value' => array(
                    __( 'None', 'w9sme-addon' ) => 'link_no',
                    __( 'Open Pretty Photo', 'w9sme-addon' ) => 'magnific',
                ),
                'description' => __( 'Select action for click event.', 'w9sme-addon' ),
            ),
        ), $slider_setting_map)
    )
);