<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-company-history.php
 * @time    : 9/28/17 3:44 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class W9sme_SC_Company_History extends WPBakeryShortCode {
	const SC_BASE = 'w9sme_shortcode_company_history';
}