<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme-company-history.php
 * @time    : 9/28/17 3:44 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
	die( '-1' );
}

vc_map( array(
	'name'           => __( 'W9sme Company History', 'w9sme-addon' ),
	'base'           => W9sme_SC_Company_History::SC_BASE,
	'class'          => '',
	'icon'           => 'w9 w9-ico-basic-todo-txt',
	'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
	'description'    => __( 'Build simple company history events', 'w9sme-addon' ),
	'php_class_name' => 'W9sme_SC_Company_History',
	'params'         => array(
		array(
			'type'       => 'param_group',
			'heading'    => __( 'Events', 'w9sme-addon' ),
			'param_name' => 'ch_values',
			'value'      => urlencode( json_encode( array(
				array(
					'year'  => '2017',
					'title' => 'Event 1'
				)
			) ) ),
			'params'     => array(
				array(
					'type'        => 'textfield',
					'heading'     => esc_html__( 'Year', 'w9sme-addon' ),
					'param_name'  => 'year',
					'admin_label' => true
				),
				
				array(
					'type'        => 'textfield',
					'heading'     => __( 'Event', 'w9sme-addon' ),
					'param_name'  => 'event',
					'description' => __( 'Enter the event.', 'w9sme-addon' ),
					'admin_label' => true,
					'std'         => __( 'Event 1', 'w9sme-addon' ),
				),
				
				array(
					'type'        => 'textarea',
					'heading'     => __( 'Description', 'w9sme-addon' ),
					'param_name'  => 'description',
					'description' => __( 'Enter description for the event.', 'w9sme-addon' ),
					'std'         => '',
				),
				
				array(
					'type'       => 'vc_link',
					'heading'    => esc_html__( 'Link (URL)', 'w9sme-addon' ),
					'param_name' => 'event_link',
					'value'      => '',
				),
			)
		),
		
		array(
			'type'        => 'dropdown',
			'heading'     => __( 'Layout style', 'w9sme-addon' ),
			'param_name'  => 'ch_layout_style',
			'value'       => array(
				__( 'Style 1', 'w9sme-addon' ) => 'style-1',
				__( 'Style 2', 'w9sme-addon' ) => 'style-2',
				__( 'Style 3', 'w9sme-addon' ) => 'style-3',
			),
			'admin_label' => true,
			'std'         => 'style-1',
			'description' => __( 'Select style for the company history.', 'w9sme-addon' )
		),
		
//		array(
//			'type'        => 'dropdown',
//			'heading'     => __( 'Event spacing', 'w9sme-addon' ),
//			'param_name'  => 'ch_event_spacing',
//			'value'       => array(
//				'None' => '0',
//				'5px'  => '5',
//				'10px' => '10',
//				'15px' => '15',
//				'20px' => '20',
//				'25px' => '25',
//				'30px' => '30',
//				'35px' => '35',
//				'40px' => '40',
//			),
//			'std'         => '0',
//			'admin_label' => true,
//			'description' => esc_html__( 'Adjust the spacing between each list items.', 'w9sme-addon' ),
//		),
		
		array(
			'type'        => 'dropdown',
			'heading'     => esc_html__( 'Alignment', 'w9sme-addon' ),
			'param_name'  => 'ch_align',
			'value'       => array(
				esc_html__( 'Left', 'w9sme-addon' )  => 'align-left',
				esc_html__( 'Right', 'w9sme-addon' ) => 'align-right',
			),
			'admin_label' => true,
			'description' => esc_html__( 'Select the icon alignment.', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 'ch_layout_style',
				'value'   => array(
					'style-1',
					'style-2'
				),
			),
		),
		
		array(
			'type'        => 'number',
			'heading'     => esc_html__( 'Year Wrapper Width', 'w9sme-addon' ),
			'param_name'  => 'ch_year_wrapper_width',
			'max'         => '300',
			'description' => esc_html__( 'If the value is greater than 300, then 300 will be used.', 'w9sme-addon' ),
			'dependency'  => array(
				'element' => 'ch_layout_style',
				'value'   => array(
					'style-1',
					'style-2'
				),
			),
		),
		
		W9sme_Map_Helpers::extra_class(),
		W9sme_Map_Helpers::design_options(),
		W9sme_Map_Helpers::animation_css(),
		W9sme_Map_Helpers::animation_duration(),
		W9sme_Map_Helpers::animation_delay()
	)
) );