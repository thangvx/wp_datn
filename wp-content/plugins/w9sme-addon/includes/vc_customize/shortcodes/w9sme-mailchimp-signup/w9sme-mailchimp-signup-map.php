<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-mailchimp-signup-map.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

vc_map( array(
    'name'           => esc_html__( 'W9sme Mailchimp Signup Form', 'w9sme-addon' ),
    'base'           => W9sme_Mailchimp_Signup::SC_BASE,
    'class'          => '',
    'icon'           => 'w9 w9-ico-basic-mail-open-text',
    'category'       => array( __( 'Content', 'w9sme-addon' ), W9SME_SC_CATEGORY ),
    'description'    => __('Show the mailchimp signup form', 'w9sme-addon' ),
    'php_class_name' => 'W9sme_Mailchimp_Signup',
    'params'         => array(
//        array(
//            'type'        => 'textfield',
//            'heading'     => esc_html__( 'Title', 'w9sme-addon' ),
//            'param_name'  => 'title',
//            'admin_label' => true,
//            'value'       => '',
//        ),
	    W9sme_Map_Helpers::extra_class(),
        W9sme_Map_Helpers::design_options(),
        W9sme_Map_Helpers::animation_css(),
        W9sme_Map_Helpers::animation_duration(),
        W9sme_Map_Helpers::animation_delay()
    )
) );