<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-gitem-image-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/**
 * @var $single_image_param
 */

$shortcodes[W9sme_SC_Gitem_Image::SC_BASE] = array(
    'name'           => __( 'W9sme Post Image', 'w9sme-addon' ),
    'base'           => W9sme_SC_Gitem_Image::SC_BASE,
    'category'       => __( 'Elements', 'w9sme-addon' ), //Use same name with JS composer
    'description'    => __( 'Single Image From Meta Or Custom', 'w9sme-addon' ),
    'class'          => 'vc-show-detail',
    'icon'           => 'fa fa-image',
    'php_class_name' => 'W9sme_SC_Gitem_Image',
    'post_type'      => Vc_Grid_Item_Editor::postType(),
    'params'         => array(
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Image source', 'w9sme-addon' ),
            'param_name'  => 'source',
            'value'       => array(
                __( 'Featured Image', 'w9sme-addon' ) => 'featured_image',
                __( 'Meta Key', 'w9sme-addon' )       => 'meta_key',
                __( 'Media library', 'w9sme-addon' )  => 'media_library',
            ),
            'std'         => 'media_library',
            'description' => __( 'Select image source.', 'w9sme-addon' ),
        ),
        array(
            'type'       => 'textfield',
            'heading'    => __( 'Meta key', 'w9sme-addon' ),
            'param_name' => 'meta_key',
            'dependency' => array(
                'element' => 'source',
                'value'   => array( 'meta_key' ),
            ),
        ),
        array(
            'type'        => 'attach_image',
            'heading'     => __( 'Image', 'w9sme-addon' ),
            'param_name'  => 'image',
            'value'       => '',
            'description' => __( 'Select image from media library.', 'w9sme-addon' ),
            'dependency'  => array(
                'element' => 'source',
                'value'   => 'media_library',
            ),
            'admin_label' => true
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Image size', 'w9sme-addon' ),
            'param_name'  => 'img_size',
            'value'       => wp_parse_args( array( __( 'Custom', 'w9sme-addon' ) => 'custom' ), get_intermediate_image_sizes() ),
            'std' => 'w9sme_1170',
            'description' => __( 'Select image size from list.', 'w9sme-addon' ),
            'dependency'  => array(
                'element' => 'source',
                'value'   => array( 'media_library', 'featured_image' ),
            ),
        ),
        array(
            'type'        => 'textfield',
            'heading'     => __( 'Image size', 'w9sme-addon' ),
            'param_name'  => 'img_size_custom',
            'std'         => '1280x720',
            'description' => __( 'Enter image size in pixels. Example: 200x100 (Width x Height).', 'w9sme-addon' ),
            'dependency'  => array(
                'element' => 'img_size',
                'value'   => array( 'custom' ),
            ),
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Image ratio', 'w9sme-addon' ),
            'description' => __( 'Image ratio base on image size width', 'w9sme-addon' ),
            'param_name'  => 'image_ratio',
            'value'       => wp_parse_args( array( __( 'Original', 'w9sme-addon' ) => 'original' ), W9sme_Image::get_w9sme_ratio_list() ),
            'std'         => 'original'
        ),
        array(
            'type'       => 'dropdown',
            'heading'    => __( 'Image action', 'w9sme-addon' ),
            'param_name' => 'action',
            'value'      => array(
                __( 'None', 'w9sme-addon' )      => 'none',
                __( 'Post link', 'w9sme-addon' ) => 'post_link',
                __( 'Meta URL', 'w9sme-addon' ) => 'meta_url',
                __( 'Light box', 'w9sme-addon' ) => 'light_box',
                __( 'Post link & Light box', 'w9sme-addon' ) => 'post_link_and_light_box',
            )
        ),
        array(
            'type'       => 'checkbox',
            'param_name' => 'link_target',
            'heading'    => __( 'Open link in new tab?', 'w9sme-addon' ),
            'value'      => array(
                __( 'Yes, please!', 'w9sme-addon' ) => 'yes',
            ),
            'std'        => '',
            'dependency' => array(
                'element' => 'action',
                'value' => array('meta_url', 'post_link', 'post_link_and_light_box')
            )
        ),
        array(
            'type'       => 'textfield',
            'heading'    => __( 'Meta key', 'w9sme-addon' ),
            'param_name' => 'meta_url',
            'dependency' => array(
                'element' => 'action',
                'value'   => array( 'meta_url' ),
            ),
        ),
	    array(
		    'type'        => 'checkbox',
		    'heading'     => __( 'Add hover effect?', 'w9sme-addon' ),
		    'param_name'  => 'add_hover_effect',
		    'description' => __( 'Add image hover effect.', 'w9sme-addon' ),
		    'value'       => array( __( 'Yes', 'w9sme-addon' ) => 'yes' ),
		    'std'         => 'yes',
		    'dependency' => array(
			    'element' => 'action',
			    'value_not_equal_to'   => array('none', 'post_link_and_light_box')
		    ),
	    ),
        W9sme_Map_Helpers::extra_class(),
        W9sme_Map_Helpers::design_options(),
    ),
);