<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: vc-gitem-map.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/**
 * @var $shortcodes array
 */

$shortcodes['vc_gitem'] = array(
    'name'                    => __( 'Grid Item', 'w9sme-addon' ),
    'base'                    => 'vc_gitem',
    'is_container'            => true,
    'icon'                    => 'icon-wpb-gitem',
    'content_element'         => false,
    'show_settings_on_create' => false,
    'category'                => __( 'Content', 'w9sme-addon' ),
    'description'             => __( 'Main grid item', 'w9sme-addon' ),
    'admin_enqueue_js'        => array( W9sme_Addon::plugin_url() . 'assets/js/vc-custom.js' ),
    'params'                  => array(
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Normal block width', 'w9sme-addon' ),
            'description' => __( 'Define width or normal block if have "Additional Block" left or right it.', 'w9sme-addon' ),
            'param_name'  => 'normal_block_width',
            'value'       => array(
                '1/12'  => 'w9sme-normal-block-1-12',
                '2/12'  => 'w9sme-normal-block-2-12',
                '3/12'  => 'w9sme-normal-block-3-12',
                '4/12'  => 'w9sme-normal-block-4-12',
                '5/12'  => 'w9sme-normal-block-5-12',
                '6/12'  => 'w9sme-normal-block-6-12',
                '7/12'  => 'w9sme-normal-block-7-12',
                '8/12'  => 'w9sme-normal-block-8-12',
                '9/12'  => 'w9sme-normal-block-9-12',
                '10/12'  => 'w9sme-normal-block-10-12',
                '11/12'  => 'w9sme-normal-block-10-12',
                '5%'  => 'w9sme-normal-block-5',
                '10%' => 'w9sme-normal-block-10',
                '15%' => 'w9sme-normal-block-15',
                '20%' => 'w9sme-normal-block-20',
                '25%' => 'w9sme-normal-block-25',
                '30%' => 'w9sme-normal-block-30',
                '35%' => 'w9sme-normal-block-35',
                '40%' => 'w9sme-normal-block-40',
                '45%' => 'w9sme-normal-block-45',
                '50%' => 'w9sme-normal-block-50',
                '55%' => 'w9sme-normal-block-55',
                '60%' => 'w9sme-normal-block-60',
                '65%' => 'w9sme-normal-block-65',
                '70%' => 'w9sme-normal-block-70',
                '75%' => 'w9sme-normal-block-75',
                '80%' => 'w9sme-normal-block-80',
                '85%' => 'w9sme-normal-block-85',
                '90%' => 'w9sme-normal-block-90',
                '95%' => 'w9sme-normal-block-95',
            ),
            'std'=> 'w9sme-normal-block-50'
        ),
        
        array(
            'type'        => 'multi-select',
            'heading'     => __( 'Horizontal become vertical in screen width', 'w9sme-addon' ),
            'description' => __( 'Avalable when have "Additional Block" left or right "Normal Block" only.', 'w9sme-addon' ),
            'param_name'  => 'become_normal_in_screen',
            'options'     => array(
                'w9sme-vertical-xxs'  => __( 'From 0px to 479px', 'w9sme-addon' ),
                'w9sme-vertical-xs'  => __( 'From 480px to 767px', 'w9sme-addon' ),
                'w9sme-vertical-sm'  => __( 'From 768px to 991px', 'w9sme-addon' ),
                'w9sme-vertical-md'  => __( 'From 992px to 1199px', 'w9sme-addon' ),
                'w9sme-vertical-lg'  => __( 'From 1200px to 1600px', 'w9sme-addon' ),
                'w9sme-vertical-xlg' => __( 'From 1600px', 'w9sme-addon' ),
            )
        ),
        
        array(
            'type'       => 'css_editor',
            'heading'    => __( 'CSS box', 'w9sme-addon' ),
            'param_name' => 'css',
        ),
        array(
            'type'        => 'textfield',
            'heading'     => __( 'Extra class name', 'w9sme-addon' ),
            'param_name'  => 'el_class',
            'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'w9sme-addon' ),
        ),
    ),
    'js_view'                 => 'VcGitemView',
    'post_type'               => Vc_Grid_Item_Editor::postType(),
);