<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-gitem-heading-map.php
 * @time    : 9/9/16 12:17 PM
 * @author  : 9WPThemes Team
 */

if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

require_once W9sme_Addon::plugin_dir() . 'includes/vc_customize/shortcodes/w9sme-heading/w9sme-heading.php';
global $vc_gitem_add_link_param;
$heading_link = $vc_gitem_add_link_param;
$heading_link['value'][__( 'Meta URL Key', 'w9sme-addon' )] = 'meta-key';
$heading_params = W9sme_SC_Heading::map()['params'];
foreach ($heading_params as $index=>$param){
    if(isset($param['param_name']) && $param['param_name'] == 'heading_title_data_source'){
        $heading_params[$index]['value'] = array(
            esc_html__( 'Custom Content', 'w9sme-addon' )          => 'custom-content',
            esc_html__( 'Post Title', 'w9sme-addon' ) => 'post-title',
        );
    }
    if(isset($param['param_name']) && $param['param_name'] == 'heading_link'){
        $heading_params[$index]['dependency'] = array(
            'element' => 'gitem_heading_link',
            'value' => 'custom',
        );
    }
}

$shortcodes[W9sme_SC_Gitem_Heading::SC_BASE]  = (array(
    'name'           => __( 'W9sme Heading', 'w9sme-addon' ),
    'base'           => W9sme_SC_Gitem_Heading::SC_BASE,
    'category'       => __( 'Elements', 'w9sme-addon' ), //Use same name with JS composer
    'description'    => __( 'Create awesome heading', 'w9sme-addon' ),
    'post_type'      => Vc_Grid_Item_Editor::postType(),
    'class'          => 'vc-show-detail',
    'icon'           => 'w9 w9-ico-software-font-underline ',
    'php_class_name' => 'W9sme_SC_Gitem_Heading',
    'params'         => array_merge(
        array(
            array(
                'type' => 'dropdown',
                'heading' => __( 'Heading URL', 'w9sme-addon' ),
                'param_name' => 'gitem_heading_link',
                'value' => array(
                    __( 'None', 'w9sme-addon' ) => 'none',
                    __( 'Post Link', 'w9sme-addon') => 'post_link',
                    __( 'Meta Key', 'w9sme-addon' ) => 'meta_key',
                    __( 'Custom' , 'w9sme-addon') => 'custom',
                )
            ),
            array(
                'type' => 'textfield',
                'heading' => __( 'Meta key', 'w9sme-addon' ),
                'param_name' => 'gitem_heading_link_meta',
                'dependency' => array(
                    'element' => 'gitem_heading_link',
                    'value' => array( 'meta_key' ),
                ),
                'description' => __( 'Fill meta key contain url.', 'w9sme-addon' ),
            ),
            array(
                'type'       => 'checkbox',
                'param_name' => 'link_target',
                'heading'    => __( 'Open link in new tab?', 'w9sme-addon' ),
                'value'      => array(
                    __( 'Yes, please!', 'w9sme-addon' ) => 'yes',
                ),
                'std'        => '',
                'dependency' => array(
                    'element' => 'gitem_heading_link',
                    'value' => array('meta_key', 'post_link')
                )
            )
        ),
        $heading_params
    ),
));