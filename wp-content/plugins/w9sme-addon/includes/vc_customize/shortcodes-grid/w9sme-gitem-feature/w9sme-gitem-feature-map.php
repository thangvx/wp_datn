<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-gitem-feature-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$shortcodes[W9sme_SC_Gitem_Feature::SC_BASE] = array(
    'name'           => __( 'W9sme Post Feature', 'w9sme-addon' ),
    'base'           => W9sme_SC_Gitem_Feature::SC_BASE,
    'category'       => __( 'Elements', 'w9sme-addon' ), //Use same name with JS composer
    'description'    => __( 'Define simple feature in format label : content', 'w9sme-addon' ),
    'post_type'      => Vc_Grid_Item_Editor::postType(),
    'class'          => 'vc-show-detail',
    'icon'           => 'fa fa-rocket',
    'php_class_name' => 'W9sme_SC_Gitem_Feature',
    'params'         => array(
        array(
            'type'        => 'textfield',
            'heading'     => __( 'Label', 'w9sme-addon' ),
            'param_name'  => 'label',
            'admin_label' => true,
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Post feature', 'w9sme-addon' ),
            'description' => __( 'Select post feature, some feature not available in all post type.', 'w9sme-addon' ),
            'param_name'  => 'post_feature',
            'value'       => array(
                __( 'Date', 'w9sme-addon' )           => 'date',
                __( 'Categories', 'w9sme-addon' )     => 'categories',
                __( 'Authors', 'w9sme-addon' )        => 'author',
                __( 'Number Comment', 'w9sme-addon' ) => 'number-comment',
                __( 'Tags', 'w9sme-addon' )           => 'tags',
                __( 'Social Follow', 'w9sme-addon' )  => 'social-follow',
                __( 'Terms', 'w9sme-addon' )          => 'terms',
            ),
            'admin_label' => true,
        ),
        array(
            'type'       => 'textfield',
            'heading'    => __( 'Taxonomy', 'w9sme-addon' ),
            'param_name' => 'taxonomy',
            'dependency' => array(
                'element' => 'post_feature',
                'value'   => 'terms'
            )
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Separator', 'w9sme-addon' ),
            'description' => __( 'Separator between categories, term, tags.', 'w9sme-addon' ),
            'param_name'  => 't_separator',
            'value'       => array(
                __( 'Comma', 'w9sme-addon' ) => 'comma',
                __( 'Slash', 'w9sme-addon' ) => 'slash',
                __( 'Minus', 'w9sme-addon' ) => 'minus'
            ),
            'dependency'  => array(
                'element' => 'post_feature',
                'value'   => 'categories'
            )
        ),
        array(
            'type'        => 'textfield',
            'heading'     => __( 'Empty string', 'w9sme-addon' ),
            'description' => __( 'This stirng print when feature not available.', 'w9sme-addon' ),
            'param_name'  => 'empty_string',
        ),
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Feature display', 'w9sme-addon' ),
            'description' => __( 'Control feature display with another feature, object in post grid.', 'w9sme-addon' ),
            'param_name'  => 'feature_display',
            'value'       => array(
                __( 'Block', 'w9sme-addon' )  => 'block',
                __( 'Inline', 'w9sme-addon' ) => 'inline',
            )
        ),
        W9sme_Map_Helpers::extra_class(),
        W9sme_Map_Helpers::design_options(),
    ),
);