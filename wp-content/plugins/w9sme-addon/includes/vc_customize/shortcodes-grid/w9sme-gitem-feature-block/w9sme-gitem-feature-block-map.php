<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-gitem-feature-block-map.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$shortcodes[W9sme_SC_Gitem_Feature_Block::SC_BASE] = ( array(
    'name'           => __( 'W9sme Post Feature Block', 'w9sme-addon' ),
    'base'           => W9sme_SC_Gitem_Feature_Block::SC_BASE,
    'category'       => __( 'Elements', 'w9sme-addon' ), //Use same name with JS composer
    'description'    => __( 'Get post feature in block style', 'w9sme-addon' ),
    'post_type'      => Vc_Grid_Item_Editor::postType(),
    'class'          => 'vc-show-detail',
    'icon'           => 'fa fa-object-group',
    'php_class_name' => 'W9sme_SC_Gitem_Feature_Block',
    'params'         => array(
        array(
            'type'        => 'dropdown',
            'heading'     => __( 'Output type', 'w9sme-addon' ),
            'param_name'  => 'output_type',
            'admin_label' => true,
            'value'       => array(
                __( 'Image', 'w9sme-addon' )               => 'image',
                __( 'Image Carousel', 'w9sme-addon' )      => 'gallery',
                __( 'Video', 'w9sme-addon' )               => 'video',
                __( 'Audio', 'w9sme-addon' )               => 'audio',
                __( 'Auto by post format', 'w9sme-addon' ) => 'embed',
            ),
            'std'         => 'text'
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Image source', 'w9sme-addon'),
            'param_name' => 'output_type',
            'admin_label' => true,
            'value' => array(
                __( 'Post Thumbnail', 'w9sme-addon' ) => 'post_thumbnail',
                __( 'Single Image From Post Type', 'w9sme-addon' ) => 'post_thumbnail',
            )
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Video source', 'w9sme-addon'),
            'param_name' => 'output_type',
            'admin_label' => true,
            'value' => array(
                __( 'Post format video', 'w9sme-addon' ) => '',
            )
        ),
        W9sme_Map_Helpers::extra_class(),
        W9sme_Map_Helpers::design_options(),
    ),
) );