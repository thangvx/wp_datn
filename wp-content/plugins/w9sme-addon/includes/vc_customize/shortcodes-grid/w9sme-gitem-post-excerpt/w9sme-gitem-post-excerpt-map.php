<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-gitem-post-excerpt-map.php
 * @time    : 9/10/16 4:46 PM
 * @author  : 9WPThemes Team
 */

if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$shortcodes[W9sme_SC_Gitem_Post_Excerpt::SC_BASE] = array(
    'name'           => __( 'W9sme Post Excerpt', 'w9sme-addon' ),
    'base'           => W9sme_SC_Gitem_Post_Excerpt::SC_BASE,
    'category'       => __( 'Elements', 'w9sme-addon' ), //Use same name with JS composer
    'description'    => __( 'Single Image From Meta Or Custom', 'w9sme-addon' ),
    'class'          => 'vc-show-detail',
    'icon'           => 'fa fa-credit-card',
    'php_class_name' => 'W9sme_SC_Gitem_Post_Excerpt',
    'post_type'      => Vc_Grid_Item_Editor::postType(),
    'params'         => array(
        array(
            'type' => 'number',
            'heading' => __( 'Post Excerpt Max Character', 'w9sme-addon' ),
            'description' => __( 'Post excerpt max character.', 'w9sme-addon' ),
            'param_name' => 'post_excerpt_max_length',
            'std' => '100'
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Text Before Read More Link', 'w9sme-addon' ),
            'description' => __( 'Text before read more link', 'w9sme-addon' ),
            'param_name' => 'text_before_readmore_link',
            'std' => '... '
        ),
        array(
            'type' => 'textfield',
            'heading' => __( 'Read More Label', 'w9sme-addon' ),
            'description' => __( 'Read more text label.', 'w9sme-addon' ),
            'param_name' => 'readmore_text',
            'std' => 'Read more'
        ),
        W9sme_Map_Helpers::design_options(),
        W9sme_Map_Helpers::extra_class(),
    ),
);