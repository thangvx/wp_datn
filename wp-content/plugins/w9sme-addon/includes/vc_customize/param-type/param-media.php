<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: param-media.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

vc_add_shortcode_param( 'media', 'w9sme_param_media' );

function w9sme_param_media( $settings, $value ) {
    $param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
    $type       = isset( $settings['type'] ) ? $settings['type'] : '';
    $class      = array( $param_name, $type . '_field' );
    ob_start();

    $uni_id = uniqid( 'w9sme-image-selector-' );
    ?>
    <p class="w9sme-file-selector w9sme-image-selector <?php echo esc_attr( $uni_id ); ?>" style="margin: 0;">
        <input type="text"
               id="<?php echo esc_attr( $param_name ); ?>"
               name="<?php echo esc_attr( $param_name ); ?>"
               class="wpb_vc_param_value wpb-textinput <?php w9sme_the_clean_html_classes( $class ); ?>"
               value="<?php echo esc_attr( $value ); ?>" readonly="readonly">
        <button style="margin-top: 10px" title="<?php echo esc_html__( 'Click to browse media', 'w9sme-addon' ) ?>"
                class="browse-files browse-images button-primary selector-btn" type="button"><?php echo esc_html__( 'Browse', 'w9sme-addon' ) ?></button>
        <button style="margin-top: 10px" title="<?php echo esc_html__( 'Click to remove media', 'w9sme-addon' ) ?>"
                class="browse-files browse-images button-secondary remove-btn" type="button"><?php echo esc_html__( 'Remove', 'w9sme-addon' ) ?></button>
        <br />
        <?php if ( !empty( $value ) ): ?>
            <img class="widget-image-selected" src="<?php echo esc_attr( $value ); ?>" alt="" />
        <?php endif; ?>
    </p>
    <script>
        (function ($) {
            'use strict';
            $(document).ready(function () {
                if (typeof w9sme_admin !== 'undefined') {
                    w9sme_admin.file_selector($(<?php echo '\'.' . $uni_id . '\'' ?>));
                }
            });
        })(jQuery);
    </script>
    <?php
    return ob_get_clean();
}

vc_add_shortcode_param( 'getfile', 'w9sme_file_selector' );

function w9sme_file_selector( $settings, $value ) {
    $param_name = isset( $settings['param_name'] ) ? $settings['param_name'] : '';
    $type       = isset( $settings['type'] ) ? $settings['type'] : '';
    $class      = array( $param_name, $type . '_field' );
    ob_start();
    
    $uni_id = uniqid( 'w9sme-file-selector-' );
    ?>
    <p class="w9sme-file-selector <?php echo esc_attr( $uni_id ); ?>" style="margin: 0;">
        <input type="text"
               id="<?php echo esc_attr( $param_name ); ?>"
               name="<?php echo esc_attr( $param_name ); ?>"
               class="wpb_vc_param_value wpb-textinput <?php w9sme_the_clean_html_classes( $class ); ?>"
               value="<?php echo esc_attr( $value ); ?>" readonly="readonly">
        <button style="margin-top: 10px" title="<?php echo esc_html__( 'Click to browse media', 'w9sme-addon' ) ?>"
                class="browse-files button-primary selector-btn" type="button"><?php echo esc_html__( 'Browse', 'w9sme-addon' ) ?></button>
        <button style="margin-top: 10px" title="<?php echo esc_html__( 'Click to remove media', 'w9sme-addon' ) ?>"
                class="browse-files button-secondary remove-btn" type="button"><?php echo esc_html__( 'Remove', 'w9sme-addon' ) ?></button>
    </p>
    <script>
        (function ($) {
            'use strict';
            $(document).ready(function () {
                if (typeof w9sme_admin !== 'undefined') {
                    w9sme_admin.file_selector($(<?php echo '\'.' . $uni_id . '\'' ?>));
                }
            });
        })(jQuery);
    </script>
    <?php
    return ob_get_clean();
}