<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme-slider.php
 * @time    : 9/15/16 10:34 AM
 * @author  : 9WPThemes Team
 */

if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$presets_settings['w9sme_shortcode_slider_container'] = array(
    array(
        'name'     => __( 'Common Section Title', 'w9sme-addon' ),
//            'default'  => true,
        'settings' => array(),
    ),
);