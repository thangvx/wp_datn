<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme_shortcode_portfolio.php
 * @time    : 10/6/17 5:41 PM
 * @author  : 9WPThemes Team
 *
 * @var $this W9sme_SC_Portfolio
 * @var $atts
 */

$portfolios_data                    = '';
$portfolios_category                = '';
$portfolios_style                   = '';
$portfolios_display_type            = '';
$portfolios_sort_order              = '';
$portfolios_total_items             = '';
$portfolios_paging_type             = '';
$portfolios_item_per_page           = '';
$portfolios_column                  = '';
$portfolios_archive_gutter          = '';
$portfolios_image_size              = '';
$portfolios_image_ratio             = '';
$el_class                           = '';
$css                                = '';
$animation_css                      = '';
$animation_duration                 = '';
$animation_delay                    = '';
$portfolios_filter_alignment        = '';
$portfolios_enable_filter           = '';
$portfolio_overlay_color            = '';
$portfolio_overlay_background_color = '';
$portfolio_overlay_appear_effect    = '';

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );
extract( $atts );

$class_sc_wrapper = array(
	$el_class,
	W9sme_Map_Helpers::get_class_animation( $animation_css ),
	vc_shortcode_custom_css_class( $css )
);

if ( ! empty( $portfolios_archive_gutter ) ) {
	$class_sc_wrapper[] = 'w9sme-gutter-' . $portfolios_archive_gutter;
} else {
	$class_sc_wrapper[] = 'w9sme-gutter-30';
}

$portfolio_loop_class   = array();
$portfolio_loop_class[] = 'paging-' . $portfolios_paging_type;
$portfolio_loop_class[] = 'blog-type-' . $portfolios_display_type;

$archive_article_wrapper_class = array();
if ( in_array( $portfolios_display_type, array( 'masonry', 'grid' ) ) ) {
	$portfolio_loop_class[] = 'blog-columns-' . $portfolios_column;
	$portfolio_loop_class[] = 'row';
	switch ( $portfolios_column ) {
		case 1:
			$archive_article_wrapper_class[] = 'col-xs-12';
			break;
		case 2:
			$archive_article_wrapper_class[] = 'col-md-6 col-sm-12 col-xs-12';
			break;
		case 3:
			$archive_article_wrapper_class[] = 'col-md-4 col-sm-6 col-xs-12';
			break;
		case 4:
			$archive_article_wrapper_class[] = 'col-md-3 col-sm-6 col-xs-12';
			break;
		case 6:
			$archive_article_wrapper_class[] = 'col-xlg-2 col-md-4 col-sm-6 col-xs-12';
			break;
	}
}

//
// Portfolio post class
//
$portfolio_post_class = array( 'w9sme-portfolio-classic-default' );

//
// Item Design
//
$portfolio_template = 'content-simple';
switch ( $portfolios_style ) {
	case 'simple' :
		$portfolio_template     = 'content-simple';
		$portfolio_loop_class[] = 'portfolio-items-simple';
		$portfolio_post_class   = array( 'w9sme-portfolio-simple-default' );
		break;
	case 'overlay':
		$portfolio_template     = 'content-overlay';
		$portfolio_loop_class[] = 'portfolio-items-overlay';
		$portfolio_post_class   = array( 'w9sme-portfolio-overlay-default w9sme-overlay-container' );
		if ( $portfolio_overlay_appear_effect !== 'none' ) {
			$portfolio_post_class[] = $portfolio_overlay_appear_effect;
		}
		break;
	default:
		$portfolio_template     = 'content-simple';
		$portfolio_loop_class[] = 'portfolio-items-overlay';
}


//------------------------------------------
//	QUERY VARS
//------------------------------------------
$portfolios_total_items = intval( $portfolios_total_items );
if ( ! $portfolios_total_items ) {
	$portfolios_total_items = 6;
}
$portfolios_item_per_page = intval( $portfolios_item_per_page );
if ( ! $portfolios_item_per_page ) {
	$portfolios_item_per_page = 6;
}

$query_vars = array();

$query_vars['posts_per_page']      = $portfolios_total_items > - 1 ? $portfolios_total_items : $portfolios_item_per_page;
$query_vars['post_status']         = 'publish';
$query_vars['ignore_sticky_posts'] = true;
$query_vars['post_type']           = W9sme_CPT_Portfolio::CPT_SLUG;
$portfolios_categories             = array();
if ( $portfolios_data == 'category' && ! empty( $portfolios_category ) ) {
	$portfolios_categories     = explode( '||', $portfolios_category );
	$query_vars['tax_query'][] = array(
		'taxonomy' => W9sme_CPT_Portfolio::TAX_SLUG,
		'terms'    => explode( '||', $portfolios_category ),
		'field'    => 'slug',
		'operator' => 'IN'
	);
}


$query_vars['order'] = 'DESC';
switch ( $portfolios_sort_order ) {
	case 'random':
		$query_vars['orderby'] = 'rand';
		break;
	case 'popular':
		$query_vars['orderby'] = 'comment_count';
		break;
	case 'oldest':
		$query_vars['orderby'] = 'post_date';
		$query_vars['order']   = 'ASC';
		break;
	default:
		$query_vars['orderby'] = 'post_date';
		break;
}

if ( $portfolios_paging_type == 'no-paging' && $portfolios_total_items == - 1 ) {
	$query_vars['nopaging'] = true;
}

if ( is_front_page() ) {
	$paged = get_query_var( 'page' ) ? intval( get_query_var( 'page' ) ) : 1;
} else {
	$paged = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
}

$query_vars['paged'] = $paged;

/*-------------------------------------
	IMAGE SIZE & RATIO
---------------------------------------*/
$portfolio_thumbnail_ratio = $portfolios_image_ratio;
$portfolio_thumbnail_size  = $portfolios_image_size;

/*-------------------------------------
	PORTFOLIO OVERLAY
---------------------------------------*/
$portfolio_archive_custom_style = '';
$portfolio_archive_custom_style .= '.portfolio-items-overlay, .portfolio-items-overlay a, .portfolio-items-overlay a:hover{color: ' . $portfolio_overlay_color . ';}';
$portfolio_archive_custom_style .= '.w9sme-portfolio-overlay-default .entry-content .__content-inner{background-color: ' . $portfolio_overlay_background_color . ';}';
?>

<div class="w9sme portfolio-wrapper <?php w9sme_the_clean_html_classes( $class_sc_wrapper ); ?>" <?php W9sme_Map_Helpers::get_inline_style( array(), $animation_duration, $animation_delay ) ?>>
	<?php
	query_posts( $query_vars );
	if ( have_posts() ) :
		$filter_id = '';
		if ( $portfolios_enable_filter ) :
			$u_id = uniqid( 'filter-' );
			$filter_id = sprintf( 'data-filter-id=#%s', $u_id );
			$term = get_terms( array(
				'taxonomy'   => W9sme_CPT_Portfolio::TAX_SLUG,
				'hide_empty' => true,
			) );
			
			$filter_list = array();
			$li_class = '';
			if ( ! empty( $term ) ) : ?>
				<ul id="<?php echo esc_attr( $u_id ) ?>" class="w9sme-filter-list filter-style-simple list-unstyled <?php w9sme_the_clean_html_classes( $portfolios_filter_alignment ); ?>">
					<?php if ( empty( $portfolios_categories ) || count( $portfolios_categories ) > 1 ): ?>
						<li class="active">
							<a href="javascript:void(0);" class="filter-link" data-filter="*"><?php echo __( 'All', 'w9sme-addon' ) ?></a>
						</li>
						<?php
					else:
						$li_class = 'active';
					endif;
					
					foreach ( $term as $cat ) :
						if ( ! empty( $portfolios_categories ) && ! in_array( $cat->slug, $portfolios_categories ) ) {
							continue;
						}
						
						echo sprintf( '<li class="hide %s"><a href="javascript:void(0);" class="filter-link" data-filter=".portfolio-filter-cat-%s">%s</a></li>', $li_class, $cat->slug, $cat->name );
						$li_class = ''; // just active the first li
					endforeach;
					?>
				</ul>
				<?php
			endif;
		endif;
		
		?>
		<div <?php echo esc_attr( $filter_id ) ?> class="posts-loop portfolio-loop <?php w9sme_the_clean_html_classes( $portfolio_loop_class ); ?>">
			<?php
			echo sprintf( '<style>%s</style>', $portfolio_archive_custom_style );
			
			while ( have_posts() ) : the_post();
				$data_filter_info = array();
				$this_terms       = get_the_terms( get_the_ID(), W9sme_CPT_Portfolio::TAX_SLUG );
				if ( is_array( $this_terms ) ) {
					foreach ( $this_terms as $term ) {
						$data_filter_info[] = 'portfolio-filter-cat-' . $term->slug;
					}
				}
				
				$wrapper_class = array_merge( $data_filter_info, $archive_article_wrapper_class );
				?>
				<div class="loop-item portfolio-item-wrapper <?php w9sme_the_clean_html_classes( $wrapper_class ); ?>">
					<?php require( W9sme_Addon::plugin_dir() . 'includes/custom-post-type/portfolio/templates/parts/' . $portfolio_template . '.php' ) ?>
				</div>
			<?php endwhile; ?>
		</div>
		<?php
	else:
		require( W9sme_Addon::plugin_dir() . 'includes/custom-post-type/portfolio/templates/parts/content-none.php' );
	endif;
	
	W9sme_Blog::the_post_navigation( $portfolios_paging_type );
	
	wp_reset_query();
	?>
</div>
