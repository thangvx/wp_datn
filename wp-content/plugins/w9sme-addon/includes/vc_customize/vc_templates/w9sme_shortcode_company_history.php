<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme_shortcode_company_history.php
 * @time    : 9/28/17 4:00 PM
 * @author  : 9WPThemes Team
 *
 * @var $this W9sme_SC_Company_History
 * @var $atts
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$ch_values             = '';
$ch_layout_style       = '';
//$ch_event_spacing      = '';
$ch_align              = '';
$ch_year_wrapper_width = '';
$el_class              = '';
$css                   = '';
$animation_css         = '';
$animation_duration    = '';
$animation_delay       = '';


$atts = vc_map_get_attributes( $this::SC_BASE, $atts );
extract( $atts );

$sc_classes = array(
	'ch-style-' . $ch_layout_style,
//	'ch-item-spacing-' . $ch_event_spacing,
	'ch-' . $ch_align,
	$el_class,
	W9sme_Map_Helpers::get_class_animation( $animation_css ),
	vc_shortcode_custom_css_class( $css ),
);


/*-------------------------------------
	VALUES
---------------------------------------*/
if ( ! empty( $ch_values ) ) {
	$ch_values = (array) vc_param_group_parse_atts( $ch_values );
} else {
	$ch_values = array();
}

/*-------------------------------------
	ch_year_wrapper_width
---------------------------------------*/
$ch_event_wrapper_style = array();

$ch_year_wrapper_width = intval($ch_year_wrapper_width);
if (empty($ch_year_wrapper_width)) {
	$ch_year_wrapper_width = 0;
}

if ($ch_year_wrapper_width > 300) {
	$ch_year_wrapper_width = 300;
}

if ($ch_year_wrapper_width) {
	$ch_event_wrapper_style[] = "width: " . $ch_year_wrapper_width . 'px';
}

?>

<div class="w9sme-ch-wrapper <?php w9sme_the_clean_html_classes( $sc_classes ); ?>" <?php echo W9sme_Map_Helpers::get_inline_style( array(), $animation_duration, $animation_delay ); ?>>
	<?php
	foreach ( $ch_values as $item ):
		
		$item_year        = isset( $item['year'] ) ? $item['year'] : '';
		$item_event       = isset( $item['event'] ) ? $item['event'] : '';
		$item_description = isset( $item['description'] ) ? $item['description'] : '';
		
		if ( isset( $item['event_link'] ) ) {
			$link     = vc_build_link( $item['event_link'] );
			$a_href   = ! empty( $link['url'] ) ? $link['url'] : '';
			$a_title  = ! empty( $link['title'] ) ? $link['title'] : '';
			$a_target = ! empty( $link['target'] ) ? $link['target'] : '_self';
			$a_rel    = ! empty( $link['rel'] ) ? $link['rel'] : '';
			
			if ( $a_href !== '' ) {
				$item_event = W9sme_Wrap::link( $item_event, $a_href, array(
					'title'  => $a_title,
					'target' => $a_target,
					'rel'    => $a_rel
				) );
			}
		}
		
		
		include( W9sme_Addon::plugin_dir( __FILE__ ) . '/parts/w9sme-company-history/' . $ch_layout_style . '.php' );
	endforeach;
	?>

</div>
