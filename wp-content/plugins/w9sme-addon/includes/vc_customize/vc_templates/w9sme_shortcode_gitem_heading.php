<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme_shortcode_gitem_heading.php
 * @time    : 9/9/16 12:20 PM
 * @author  : 9WPThemes Team
 */

if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/**
 * @var $this W9sme_SC_Gitem_Heading
 */

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );

?>

{{w9sme_gitem_heading:<?php echo http_build_query( $atts ) ?>}}