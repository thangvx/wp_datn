<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme_shortcode_service_review.php
 * @time    : 11/15/17 12:58 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$sr_style = '';
$sr_name = '';
$el_class = '';
$css = '';
$animation_css = '';
$animation_duration = '';
$animation_delay = '';

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );
extract( $atts );


$services = explode('||', $sr_name);

$args = array(
	'post_type' => W9sme_CPT_Review::CPT_SLUG,
	'post_status'         => 'publish',
	'ignore_sticky_posts' => 1,
	'orderby'             => 'date',
	'order'               => 'ASC',
	'posts_per_page'      => -1,
	'meta_query' => array(
		array(
			'key' => 'meta-review-service-name',
			'value' => $services,
			'compare' => 'IN',
		)
	)
);
$query = new WP_Query($args);


while ( $query->have_posts() ):
	$query->the_post();
	
	$post_id = get_the_ID();
	$name = get_post_meta( $post_id, 'meta-review-reviewer-name', true );
	$email = get_post_meta( $post_id, 'meta-review-reviewer-email', true );
	$content = get_post_meta( $post_id, 'meta-review-content', true );
	$avatar_rating = get_post_meta( $post_id, 'meta-review-rating', true );
	
	echo '<br/>';
	echo 'Rating:' . $avatar_rating;
	echo '<br/>';
	echo 'Rating:' . $name;
	echo '<br/>';
	echo 'Content:' . $content;
	echo '<br/>';
	echo '=====================';
endwhile;





wp_reset_postdata();

