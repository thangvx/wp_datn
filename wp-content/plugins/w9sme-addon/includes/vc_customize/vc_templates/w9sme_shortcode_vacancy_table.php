<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme_shortcode_service.php
 * @time    : 4/20/2017 3:03 PM
 * @author  : 9WPThemes Team
 *
 * @var $this W9sme_SC_Vacancy_Table
 * @var $atts
 */


if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! post_type_exists( W9sme_CPT_Vacancy::CPT_SLUG ) ) {
	return;
}

wp_enqueue_style( W9sme_Enqueue::STYLE_PREFIX . 'datatables' );
wp_enqueue_script( W9sme_Enqueue::SCRIPT_PREFIX . 'datatables' );

$vacancy_data               = '';
$vacancy_category           = '';
$vacancy_item_per_page      = '';
$vacancy_enable_search      = '';
$vacancy_enable_length_menu = '';
$vacancy_enable_result_info = '';
$css                        = '';
$animation_css              = '';
$animation_duration         = '';
$animation_delay            = '';
$el_class                   = '';

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );
extract( $atts );

$class_vacancy = array(
	'w9sme-vacancy-table',
	$el_class,
	vc_shortcode_custom_css_class( $css ),
	W9sme_Map_Helpers::get_class_animation( $animation_css )
);

$unique_id = uniqid( 'w9sme-vacancy-table-' );

/*-------------------------------------
	ITEM PER PAGE
---------------------------------------*/
$item_per_page = intval( $vacancy_item_per_page );
if ( empty( $item_per_page ) ) {
	$item_per_page = 10;
}

/*-------------------------------------
	LENGTH MENU OPTIONS
---------------------------------------*/
$length_menu_option   = [ 10, 25, 50, 100 ];
$length_menu_option[] = $item_per_page;

$length_menu_option = array_unique( $length_menu_option );
sort( $length_menu_option, SORT_NUMERIC );

$dom_position = "lfrtip";
if ( empty( $vacancy_enable_search ) ) {
	$dom_position = str_replace( 'f', '', $dom_position );
}

if ( empty( $vacancy_enable_length_menu ) ) {
	$dom_position = str_replace( 'l', '', $dom_position );
}

if ( empty( $vacancy_enable_result_info ) ) {
	$dom_position = str_replace( 'i', '', $dom_position );
}

?>

<div class="vacancy-table-wrapper <?php w9sme_the_clean_html_classes( $class_vacancy ); ?>" id="<?php echo $unique_id; ?>"
	<?php echo W9sme_Map_Helpers::get_inline_style( array(), $animation_duration, $animation_delay ); ?>>
	<table class="vacancy-table" id="vacancy-table">
		<thead>
		<th class="all text-center" width="30">#</th>
		<th class="all"><?php _e( "Job Title", "w9sme" ) ?></th>
		<th class="min-tablet-l"><?php _e( "Location", "w9sme" ) ?></th>
		<th class="min-tablet-l"><?php _e( "Departments", "w9sme" ) ?></th>
		<th class="min-tablet-l"><?php _e( "Date", "w9sme" ) ?></th>
		</thead>
	</table>

	<script>
		(function ($) {
			$(document).on('ready', function () {
				$('#<?php echo $unique_id; ?> > .vacancy-table').DataTable({
					"processing" : true,
					"serverSide" : true,
					"responsive" : true,
					"autoWidth"  : false,
					"language"   : {
						"processing": '<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> ' + "<?php _e( "Loading Data...", "w9sme" ) ?>",
						"lengthMenu": "<?php _e( "Rows: _MENU_", "w9sme" ); ?>",
						"paginate"  : {
							"first"   : '<i class="fa fa-step-backward" aria-hidden="true"></i>',
							"last"    : '<i class="fa fa-step-forward" aria-hidden="true"></i>',
							"next"    : '<i class="fa fa-caret-right" aria-hidden="true"></i>',
							"previous": '<i class="fa fa-caret-left" aria-hidden="true"></i>'
						},
						"info"      : "<?php _e( "Results _START_ - _END_ of _TOTAL_", "w9sme" ); ?>",
						"infoEmpty" : "<?php _e( "Results 0 - 0 of 0", "w9sme" ); ?>",
						"search"    : "<?php _e( "Search:", "w9sme" ); ?>"
					},
					"ajax"       : {
						"url" : w9sme_main_vars.ajax_url,
						"type": "GET",
						"data": {
							"action"          : 'w9sme_load_vacancy_data',
							"target_term_slug": '<?php echo $vacancy_category; ?>'
						}
					},
					"columns"    : [
						{
							"data"              : "no",
							"orderable"         : false,
							"searchable"        : false,
							"responsivePriority": 1
						},
						{
							"data"              : "title",
							"orderable"         : true,
							"searchable"        : true,
							"responsivePriority": 2
						},
						{
							"data"              : "location",
							"orderable"         : true,
							"searchable"        : false,
							"responsivePriority": 3
						},
						{
							"data"              : "cat",
							"orderable"         : false,
							"searchable"        : false,
							"responsivePriority": 4
						},
						{
							"data"              : "date",
							"orderable"         : true,
							"searchable"        : false,
							"responsivePriority": 6
						}
					],
					"order"      : [[1, 'desc']],
					"orderMulti" : false,
					"searchDelay": 500,
					"pageLength" : '<?php echo $item_per_page; ?>',
					"dom"        : '<?php echo $dom_position; ?>',
					"lengthMenu" : <?php echo '[' . join( ', ', $length_menu_option ) . ']' ?>
				});
			});
		})(jQuery);
	</script>
</div>

