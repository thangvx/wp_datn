<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: style-1.php
 * @time    : 9/28/17 4:41 PM
 * @author  : 9WPThemes Team
 *
 * @var $item_year
 * @var $item_event
 * @var $item_description
 * @var $ch_align
 * @var $ch_event_wrapper_style
 */

?>
<div class="ch-event-wrapper">
	<div class="ch-top clearfix">
		<?php if ( $ch_align == "align-left" ): ?>
			<div class="__year fz-30" <?php echo W9sme_Map_Helpers::get_inline_style( $ch_event_wrapper_style)?>>
				<?php echo $item_year; ?>
			</div>
			<div class="__event text-uppercase fw-bold fz-16"><?php echo $item_event; ?></div>
		<?php else: ?>
			<div class="ch-top-inner">
				<div class="__event text-uppercase fw-bold fz-16"><?php echo $item_event; ?></div>
				<div class="__year fz-30" <?php echo W9sme_Map_Helpers::get_inline_style( $ch_event_wrapper_style)?>>
					<?php echo $item_year; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if ( $item_description ): ?>
		<div class="ch-bottom"><?php echo $item_description; ?></div>
	<?php endif; ?>
</div>
