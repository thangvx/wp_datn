<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: style-1.php
 * @time    : 9/28/17 4:41 PM
 * @author  : 9WPThemes Team
 *
 * @var $item_year
 * @var $item_event
 * @var $item_description
 * @var $ch_align
 */

?>
<div class="ch-event-wrapper">
	<div class="__year fz-30"><?php echo $item_year; ?></div>
	<div class="__separator"></div>
	<div class="__event text-uppercase fw-bold fz-16"><?php echo $item_event; ?></div>
	<div class="__description"><?php echo $item_description; ?></div>
</div>
