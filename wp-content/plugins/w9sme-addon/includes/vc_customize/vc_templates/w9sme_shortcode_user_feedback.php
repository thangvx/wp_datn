<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme_shortcode_user_feedback.php
 * @time    : 11/15/17 9:32 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! class_exists( 'W9sme_CPT_Review' ) || ! post_type_exists( W9sme_CPT_Review::CPT_SLUG ) ) {
	echo 'Review post type is not enabled, please enable the Review post type before using this shortcode.';
}

$uf_manual_data     = '';
$el_class           = '';
$css                = '';
$animation_css      = '';
$animation_duration = '';
$animation_delay    = '';

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );
extract( $atts );

if ( ! empty( $uf_manual_data ) ) {
	$uf_manual_data = (array) vc_param_group_parse_atts( $uf_manual_data );
} else {
	$uf_manual_data = array();
}

$uf_cats = array();

foreach ( $uf_manual_data as $data ) {
	if ( empty( $data['uf_type'] ) ) {
		continue;
	}
	
	$uf_cats[] = $data['uf_type'];
}

if ( count( $uf_cats ) == 0 ) {
	return;
}

$default_cat = $uf_cats[0];

$cat_from_get = isset( $_GET['review-cat'] ) ? $_GET['review-cat'] : '';

if ( ! empty( $cat_from_get ) && in_array( $cat_from_get, $uf_cats ) ) {
	$default_cat = $cat_from_get;
}


$unique_id = esc_attr( uniqid( 'user-feedback-form-' ) );
?>

<div class="user-feedback-wrapper" id="<?php echo $unique_id; ?>">
	<div class="w9sme-form w9sme-contact-form-1 w9sme-service-feedback-form">
		<div class="submit-response">

		</div>
		<form action="#" class="user-feedback-form">
			<div class="__contact-form-inner clearfix">
				<div class="__input mb-20">
					<?php _e( 'Category', 'w9sme-addon' ) ?>:
					<select name="feedback-type" class="feedback-type" required>
						<?php foreach ( $uf_cats as $cat ): ?>
							<option value="<?php echo $cat ?>"
								<?php if ( $default_cat == $cat ) {
									echo 'selected';
								} ?>
							>
								<?php
								echo $cat;
								?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="__input mb-20">
					<input type="hidden" name="rating" class="rating">
					<?php _e( 'Rating', 'w9sme-addon' ) ?>:
					<div class="rating-wrapper">
						<span class="rating" data-star="1"><span class="star" style="width: 0"></span></span>
						<span class="rating" data-star="2"><span class="star" style="width: 0"></span></span>
						<span class="rating" data-star="3"><span class="star" style="width: 0"></span></span>
						<span class="rating" data-star="4"><span class="star" style="width: 0"></span></span>
						<span class="rating" data-star="5"><span class="star" style="width: 0"></span></span>
					</div>
				</div>
				<div class="__input __message mb-20">
					<?php _e( 'Content', 'w9sme-addon' ) ?>:
					<textarea name="message" required placeholder="<?php _e( 'Your message', 'w9sme-addon' ) ?>" cols="40" rows="8" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
				</div>
				<div class="__input mb-20">
					<?php _e( 'Name', 'w9sme-addon' ) ?>:
					<input type="text" name="name" placeholder="<?php _e( 'Your name', 'w9sme-addon' ) ?>" required value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
				</div>
				<div class="__input __email">
					<?php _e( 'Job Title', 'w9sme-addon' ) ?>:
					<input type="text" name="job-title" placeholder="<?php _e( 'Who do you do', 'w9sme-addon' ) ?>" required value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false">
				</div>
				<div class="__input __name">
					<?php _e( 'Email', 'w9sme-addon' ) ?>:
					<input type="email" name="email" placeholder="<?php _e( 'Your email', 'w9sme-addon' ) ?>" required value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">
				</div>
				<div class="__btn pt-20 pb-20">
					<button type="submit" class="w9sme-btn fw-semibold p-font btn-style-solid btn-shape-square btn-size-md icon-effect-none hover-effect-default light-color p-bgc s-hover-button-bgc ready" title="SUBMIT" style="border: none; min-width: 170px">
						<span class="fz-14 ls-02"><?php _e( 'SUBMIT', 'w9sme-addon' ) ?></span>
						<div class="ctf7-loader"><i class="fa fa-cog fa-spin"></i></div>
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
	(function ($) {
		$(document).ready(function () {
			var formId = '<?php echo $unique_id ?>';
			var $ratingWrapper = $('#' + formId + ' .rating-wrapper');
			$ratingWrapper.find('.rating').each(function () {
				var $this = $(this);
				$this.on('click', function () {
					$ratingWrapper.find('.star').css('width', '0');
					$this.prevAll().find('.star').css('width', '100%');
					$this.find('.star').css('width', '100%');

					$('#' + formId + ' .rating').val($this.attr('data-star'));
				});

			});

			/*-------------------------------------
				ON SUBMIT
			---------------------------------------*/
			var $form = $('#' + formId + ' .user-feedback-form');
			var $response = $('#' + formId + ' .submit-response');
			var $_loader = $form.find('.ctf7-loader');
			var errorMsg = '<?php _e( 'There was en error occurred when submitting data', 'w9sme-addon' ); ?>';

			function appendMessage(status, message) {
				$response.empty();

				if (message) {
					$response.append(
						'<p class="alert alert-' + status.toLowerCase() + '">' + message + '</p>'
					)
				}
			}

			$form.on('submit', function () {
				var $this = $(this);

				var data = {};
				data['action'] = 'submit_user_feed_back';

				var formData = $this.serializeArray();
				for (var i in formData) {
					data[formData[i]['name']] = formData[i]['value'];
				}

				appendMessage('', '');
				$_loader.fadeIn(200);

				$.ajax({
					type   : 'post',
					url    : w9sme_main_vars.ajax_url,
					data   : data,
					success: function (response) {
						try {
							response = JSON.parse(response);
							appendMessage(response.status, response.message);

							$form.find('input, textarea').val('');
							$ratingWrapper.find('.star').css('width', '0');

						} catch (e) {
							appendMessage('danger', errorMsg);
						}
					},
					error  : function () {
						console.error('Could not load data');
						appendMessage('danger', errorMsg);
					}
				}).done(function () {
					$_loader.fadeOut(200);
					$('#' + formId + ' .submit-response').animate({
						scrollTop: 0
					}, 200);
				});

				return false;
			});
		});
	})(jQuery);
</script>

