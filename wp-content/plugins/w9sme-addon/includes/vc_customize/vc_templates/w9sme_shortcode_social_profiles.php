<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme_shortcode_social_profiles.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 *
 * @var $this W9sme_SC_Social_Profiles
 * @var $atts
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Widget param
$title = $module_type = $profiles = $share_this_label = $alignment = $icon_size = $icon_color = $colors = $colors_cp = $colors_hover = $colors_hover_cp = $is_rounded_icon = $rounded_size = $background_colors = $background_colors_cp = $background_hover_colors = $background_hover_colors_cp = $spacing_between_items = $w9sme_extra_widget_classes = $w9sme_remove_default_mb = '';

// Widget wrapper param
$css = $animation_css = $animation_duration = $animation_delay = $el_class = '';

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );
extract( $atts );

$widget_atts = array(
	'title'                      => $title,
	"module_type"                => $module_type,
	"profiles"                   => $profiles,
	"share_this_label"           => $share_this_label,
	"alignment"                  => $alignment,
	"icon_size"                  => $icon_size,
	"icon_color"                 => $icon_color,
	"colors"                     => $colors,
	"colors_cp"                  => $colors_cp,
	"colors_hover"               => $colors_hover,
	"colors_hover_cp"            => $colors_hover_cp,
	"is_rounded_icon"            => $is_rounded_icon,
	"rounded_size"               => $rounded_size,
	"background_colors"          => $background_colors,
	"background_colors_cp"       => $background_colors_cp,
	"background_hover_colors"    => $background_hover_colors,
	"background_hover_colors_cp" => $background_hover_colors_cp,
	"spacing_between_items"      => $spacing_between_items,
);

$widget_wrapper_class = array(
	$el_class,
	vc_shortcode_custom_css_class( $css ),
	W9sme_Map_Helpers::get_class_animation( $animation_css )
);

$widget_class   = array();
$widget_class[] = $w9sme_extra_widget_classes;
if ( ! empty( $w9sme_remove_default_mb ) ) {
	$widget_class[] = 'mb-0-i';
}

$args = array(
	'before_widget' => '<div class="w9sme-widget %s ' . w9sme_clean_html_classes( $widget_class ) . '">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3 class="w9sme-widget-title">',
	'after_title'   => '</h3>'
);
?>
<div class="w9sme-sc-widget w9sme-widget-wrapper <?php w9sme_the_clean_html_classes( $widget_wrapper_class ); ?>" <?php echo W9sme_Map_Helpers::get_inline_style( array(), $animation_duration, $animation_delay ); ?>>
	<?php the_widget( 'W9sme_Widget_Social_Profiles', $widget_atts, $args ); ?>
</div>

