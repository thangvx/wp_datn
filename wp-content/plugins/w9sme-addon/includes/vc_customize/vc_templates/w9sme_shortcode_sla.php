<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme_shortcode_sla.php
 * @time    : 11/10/17 9:46 PM
 * @author  : 9WPThemes Team
 */

$sla_title_style         = '';
$sla_section_title_style = '';
$css                     = '';
$animation_css           = '';
$animation_duration      = '';
$animation_delay         = '';


$atts = vc_map_get_attributes( W9sme_SC_SLA::SC_BASE, $atts );
extract( $atts );

$post_type = w9sme_get_current_post_type();

if ( $post_type === W9sme_CPT_Content_Template::CPT_SLUG ) {
	?>
	<div class="gray4-bgc light-color text-center pt-20 pb-20 fw-semibold text-uppercase">
		<?php _e('Service Level Agreement goes here!', 'w9sme-addon') ?>
	</div>
	<?php
}

if ( $post_type != W9sme_CPT_Service::CPT_SLUG ) {
	return;
}

$sla = w9sme_get_meta_option( 'service-sla' );

if ( empty( $sla ) || ! is_array( $sla ) ) {
	return;
}

/*-------------------------------------
	TITLE
---------------------------------------*/
$sla_title     = w9sme_get_meta_option( 'service-sla-title' );
$heading_title = array();

if ( ! empty( $sla_title ) ) {
	if ( $sla_title_style == 'custom' ) {
		$heading_title = vc_map_integrate_parse_atts(
			$this::SC_BASE,
			W9sme_SC_Heading::SC_BASE,
			$atts,
			'sla_title_'
		);
	} else {
		$heading_title = array(
			'heading_title_size'                  => "24",
			'heading_title_text_transform'        => "text-uppercase",
			'heading_text_fw'                     => "fw-semibold",
			'heading_subtitle_enable'             => "0",
			'heading_separator_enable'            => "0",
			'heading_title_responsive_title_size' => "0",
			'heading_text_align'                  => "text-center",
			'heading_title_ff'                    => 'p-font'
		);
		
		if ( $sla_title_style == 'separator' ) {
			$heading_title['heading_separator_enable']        = '1';
			$heading_title['heading_separator_height']        = 'custom-height';
			$heading_title['heading_separator_custom_height'] = '5px';
		}
	}
	
	$heading_title['heading_title'] = $sla_title;
}


/*-------------------------------------
	SECTION TITLE
---------------------------------------*/
$heading_section_title = array();
if ( $sla_section_title_style === 'custom' ) {
	$heading_section_title = vc_map_integrate_parse_atts(
		$this::SC_BASE,
		W9sme_SC_Heading::SC_BASE,
		$atts,
		'sla_sec_title_'
	);
} else {
	$heading_section_title = array(
		'heading_title_size'                  => "18",
		'heading_title_text_transform'        => "text-uppercase",
		'heading_text_fw'                     => "fw-semibold",
		'heading_subtitle_enable'             => "0",
		'heading_separator_enable'            => "0",
		'heading_title_responsive_title_size' => "0",
		'heading_title_ff'                    => 'p-font'
	);
	
	if ( $sla_section_title_style === 'separator' ) {
		$heading_section_title['heading_separator_enable']        = '1';
		$heading_section_title['heading_separator_height']        = 'custom-height';
		$heading_section_title['heading_separator_custom_height'] = '5px';
	}
	
	if ( $sla_section_title_style === 'top-line' ) {
		$heading_section_title['heading_title_ff']        = 'p-font';
		$heading_section_title['heading_top_line_enable'] = '1';
	}
}


if ( $sla_title ) {
	w9sme_sc_heading( $heading_title );
}

foreach ( $sla as $item ): ?>
	<?php if ( ! empty( $item['label'] ) ) :
		$heading_section_title['heading_title'] = $item['label'];
		?>
		<h4><?php w9sme_sc_heading( $heading_section_title ); ?></h4>
		<?php
	endif; ?>
	
	<?php if ( ! empty( $item['content'] ) ): ?>
		<div class="sla-content mb-40">
			<?php echo $item['content']; ?>
		</div>
	<?php endif;
endforeach; ?>

