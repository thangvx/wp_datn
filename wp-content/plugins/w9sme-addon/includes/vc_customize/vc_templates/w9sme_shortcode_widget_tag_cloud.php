<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme_shortcode_widget_tagcloud.php
 * @time    : 9/28/2016 9:54 AM
 * @author  : 9WPThemes Team
 *
 * @var $this W9sme_SC_Widget_Tag_Cloud
 * @var $atts
 */


if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

// Widget param
$title = $taxonomy = $tag_color = $text_color = $text_color_cp = $text_hover_color = $text_hover_color_cp = $background_color = $background_color_cp = $background_hover_color = $background_hover_color_cp = $border_color = $border_color_cp = $border_hover_color = $border_hover_color_cp = $w9sme_extra_widget_classes = $w9sme_remove_default_mb = '';

// Widget wrapper param
$css = $animation_css = $animation_duration = $animation_delay = $el_class = '';

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );
extract( $atts );

$widget_atts = array(
    'title'                     => $title,
    'taxonomy'                  => $taxonomy,
    'tag_color'                 => $tag_color,
    'text_color'                => $text_color,
    'text_color_cp'             => $text_color_cp,
    'text_hover_color'          => $text_hover_color,
    'text_hover_color_cp'       => $text_hover_color_cp,
    'background_color'          => $background_color,
    'background_color_cp'       => $background_color_cp,
    'background_hover_color'    => $background_hover_color,
    'background_hover_color_cp' => $background_hover_color_cp,
    'border_color'              => $border_color,
    'border_color_cp'           => $border_color_cp,
    'border_hover_color'        => $border_hover_color,
    'border_hover_color_cp'     => $border_hover_color_cp
);

$widget_wrapper_class = array(
    $el_class,
    vc_shortcode_custom_css_class( $css ),
    W9sme_Map_Helpers::get_class_animation( $animation_css )
);

$widget_class   = array();
$widget_class[] = $w9sme_extra_widget_classes;
if ( !empty( $w9sme_remove_default_mb ) ) {
    $widget_class[] = 'mb-0-i';
}

$args = array(
    'before_widget' => '<div class="w9sme-widget %s ' . w9sme_clean_html_classes( $widget_class ) . '">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="w9sme-widget-title">',
    'after_title'   => '</h3>'
);
?>
<div class="w9sme-sc-widget w9sme-widget-wrapper <?php w9sme_the_clean_html_classes( $widget_wrapper_class ); ?>" <?php echo W9sme_Map_Helpers::get_inline_style( array(), $animation_duration, $animation_delay ); ?>>
    <?php the_widget( 'W9sme_Widget_Tag_Cloud', $widget_atts, $args ); ?>
</div>