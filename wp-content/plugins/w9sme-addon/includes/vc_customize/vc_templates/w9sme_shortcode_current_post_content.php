<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme_shortcode_current_post_content.php
 * @time    : 11/1/17 10:34 PM
 * @author  : 9WPThemes Team
 *
 * @var $this W9sme_SC_Current_Post_Content
 * @var $atts
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! is_singular() ) {
	return;
}

$post_type = w9sme_get_current_post_type();

if ( $post_type == W9sme_CPT_Content_Template::CPT_SLUG ) {
	?>
	<div class="gray4-bgc light-color text-center pt-20 pb-20 fw-semibold text-uppercase">
		<?php _e( 'Current post content goes here', 'w9sme-addon' ) ?>
	</div>
	<?php
}

if ( $post_type != W9sme_CPT_Service::CPT_SLUG
     && $post_type != W9sme_CPT_Portfolio::CPT_SLUG
     && $post_type != W9sme_CPT_Vacancy::CPT_SLUG ) {
	return;
}

$target_layout = '';

switch ( $post_type ) {
	case W9sme_CPT_Service::CPT_SLUG:
		$target_layout = w9sme_get_option( 'service-single-content-layout' );
		break;
	case W9sme_CPT_Portfolio::CPT_SLUG:
		$target_layout = w9sme_get_option( 'portfolio-single-content-layout' );
		break;
	case W9sme_CPT_Vacancy::CPT_SLUG:
		$target_layout = w9sme_get_option( 'vacancy-single-content-layout' );
		break;
}

if ( ! empty( $target_layout ) ) {
	global $post;
	
	if ( $this->check_recursive_call( $post->post_content ) ) {
		?>
		<div class="gray4-bgc light-color text-center pt-20 pb-20 fw-semibold text-uppercase">
			<?php echo __( 'Recursive content call detected! Please check again the post content!' ); ?>
		</div>
		<?php
		return;
	};
	
	the_content();
}
