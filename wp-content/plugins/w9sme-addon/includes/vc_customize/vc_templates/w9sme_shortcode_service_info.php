<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme_shortcode_service_info.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 *
 * @var $this W9sme_SC_Service_Info
 * @var $atts
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/**
 * var $this W9sme_SC_Service_Info
 */
$info_list = $add_button = $info_style = $info_df_booking_url = '';
$el_class  = $css = $animation_css = $animation_duration = $animation_delay = '';

$atts = vc_map_get_attributes( W9sme_SC_Service_Info::SC_BASE, $atts );
extract( $atts );

$sc_classes = array(
	$el_class,
	'info-' . $info_style,
	W9sme_Map_Helpers::get_class_animation( $animation_css ),
	vc_shortcode_custom_css_class( $css ),
);

$inline_css       = array();
$info_list        = (array) vc_param_group_parse_atts( $info_list );
$info_string_list = array();
foreach ( $info_list as $info ) {
	$label_format = '<span class="__info-label">%s</span> ';
	$label        = ! empty( $info['info_label'] ) ? sprintf( $label_format, $info['info_label'] ) : '';
	
	if ( $info['info'] === 'categories' ) {
		$info_string_list[] = get_the_term_list( get_the_ID(), W9sme_CPT_Service::TAX_SLUG, $label, ", " );
	} elseif ( $info['info'] === 'date' ) {
		$info_string_list[] = $label . get_the_date();
	} elseif ( $info['info'] === 'price' ) {
		$price = w9sme_get_meta_option( 'service-price' );
		if (!empty($price)) {
			$info_string_list[] = $label . $price;
		}
	} elseif ( $info['info'] === 'time' ) {
		$time = w9sme_get_meta_option( 'service-time' );
		if ($time) {
			$info_string_list[] = $label . w9sme_get_meta_option( 'service-time' );
		}
	} elseif ( $info['info'] === 'single-additional-info' ) {
		if (isset($info['additional_label_name'])) {
			$info_label = $info['additional_label_name'];
			
			if ( ! empty( $info_label ) ) {
				$service_addition_info = w9sme_get_meta_option( 'service-addition-info' );
				if ( is_array( $service_addition_info ) && ! empty( $service_addition_info ) ) {
					foreach ( $service_addition_info as $additional_info ) {
						if ( $additional_info['label'] == $info_label ) {
							$content            = $label . wp_kses_post( $additional_info['content'] );
							$info_string_list[] = $content;
							
							break;
						}
					}
				}
			}
		}
	} elseif ( $info['info'] === 'addition-info' ) {
		$service_addition_info = w9sme_get_meta_option( 'service-addition-info' );
		if ( is_array( $service_addition_info ) && ! empty( $service_addition_info ) ) {
			foreach ( $service_addition_info as $additional_info ) {
				$content            = sprintf( $label_format, $additional_info['label'] ) . wp_kses_post( $additional_info['content'] );
				$info_string_list[] = $content;
			}
		}
	} elseif ( $info['info'] === 'share-this' ) {
		$info_string_list[] = $this->share_this($label);
	}
}


if ( $add_button === 'yes' ) {
	$button_atts = vc_map_integrate_parse_atts( $this::SC_BASE, W9sme_SC_Button::SC_BASE, $atts );
	
	if ( ! empty( $info_df_booking_url ) ) {
		$booking_page_url = '#';
		$booking_page_id  = w9sme_get_option( 'service-booking-page' );
		if ( ! empty( $booking_page_id ) ) {
			$booking_page_url = urlencode( get_page_link( $booking_page_id ) );
		}
		
		$booking_page_url = W9sme_CPT_Service::append_service_name( $booking_page_url );
		
		$target                  = urlencode( '_self' );
		$button_atts['btn_link'] = sprintf( 'url:%s||target:%s|', $booking_page_url, $target );
	}
	
	if ( ! empty( $info_df_booking_text ) ) {
		$btn_text                = w9sme_get_option( 'service-single-booking-btn-tx' );
		$button_atts['btn_text'] = $btn_text;
	}
	
	$info_string_list[] = Vc_Shortcodes_Manager::getInstance()->getElementClass( W9sme_SC_Button::SC_BASE )->output( $button_atts );
}

//Print result
?>
<div class="w9sme-service-info-wrapper <?php w9sme_the_clean_html_classes( $sc_classes ) ?>" <?php echo W9sme_Map_Helpers::get_inline_style( $inline_css, $animation_duration, $animation_delay ) ?>>
	<ul class="w9sme-service-info-list list-unstyled">
		<?php
		foreach ( $info_string_list as $info_string ) {
			echo sprintf( '<li>%s</li>', $info_string );
		}
		?>
	</ul>
</div>