<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme_shortcode_gitem_image.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 *
 * @var $atts
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/**
 * @var $this W9sme_SC_Gitem_Image
 */

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );

?>

{{w9sme_gitem_image:<?php echo http_build_query( $atts ) ?>}}