<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: w9sme_shortcode_widget_download.php
 * @time    : 12/9/2016 3:48 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Widget param
$title = $file = $display_text = $file_type = $w9sme_extra_widget_classes = $w9sme_remove_default_mb = '';

// Widget wrapper param
$css = $animation_css = $animation_duration = $animation_delay = $el_class = '';

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );
extract( $atts );

$widget_atts = array(
	'title'        => $title,
	'file'         => $file,
	'display_text' => $display_text,
	'file_type'    => $file_type
);

$widget_wrapper_class = array(
	$el_class,
	vc_shortcode_custom_css_class( $css ),
	W9sme_Map_Helpers::get_class_animation( $animation_css )
);

$widget_class   = array();
$widget_class[] = $w9sme_extra_widget_classes;
if ( ! empty( $w9sme_remove_default_mb ) ) {
	$widget_class[] = 'mb-0-i';
}

$args = array(
	'before_widget' => '<div class="w9sme-widget %s ' . w9sme_clean_html_classes( $widget_class ) . '">',
	'after_widget'  => '</div>',
	'before_title'  => '<h3 class="w9sme-widget-title">',
	'after_title'   => '</h3>'
);
?>
<div class="w9sme-sc-widget w9sme-widget-wrapper <?php w9sme_the_clean_html_classes( $widget_wrapper_class ); ?>" <?php echo W9sme_Map_Helpers::get_inline_style( array(), $animation_duration, $animation_delay ); ?>>
	<?php the_widget( 'W9sme_Widget_Download', $widget_atts, $args ); ?>
</div>