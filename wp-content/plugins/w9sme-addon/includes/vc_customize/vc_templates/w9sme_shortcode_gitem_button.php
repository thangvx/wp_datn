<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: w9sme_shortcode_gitem_button.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 *
 * @var $atts
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/**
 * @var $this W9sme_SC_Gitem_Button
 */

$atts = vc_map_get_attributes( $this::SC_BASE, $atts );

?>
{{w9sme_gitem_button:<?php echo http_build_query( $atts ) ?>}}
 