<?php
/**
 * Plugin Name: W9 W9sme Add-on
 * Plugin URI: http://w9sme.9wpthemes.com
 * Description: Just an add-on of W9sme Premium Wordpress Theme.
 * Version: 1.0.0
 * Author: 9WPThemes Team
 * Author URI: http://9wpthemes.com
 * Requires at least: 4.4
 *
 * Text Domain: w9-w9sme-addon
 * Domain Path: /languages/
 *
 * @package 9WPThemes
 * @author  9WPThemes Team
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$current_theme = wp_get_theme( get_template() );
/*-------------------------------------
    YOU MUST ACTIVE W9SME THEME TO USE THIS ADDON
---------------------------------------*/
if ( ! $current_theme->exists() || $current_theme->get_stylesheet() !== 'w9sme' ) {
	add_action( 'admin_notices', function () {
		?>
		<div class="updated">
			<p>
				<strong><?php _e( 'Please install and activate W9sme theme to use the W9sme Add-on plugin.', 'w9sme-addon' ); ?></strong>
			</p>
			<?php
			$screen = get_current_screen();
			if ( $screen->base !== 'themes' ): ?>
				<p>
					<a href="<?php echo esc_url( admin_url( 'themes.php' ) ); ?>"><?php echo esc_html__( 'Activate theme', 'w9sme-addon' ); ?></a>
				</p>
			<?php endif; ?>
		</div>
		<?php
	} );
	
	return;
}


if ( ! class_exists( 'W9sme_Addon' ) ) :
	
	final class W9sme_Addon {
		const VERSION = '1.0.1';
		
		/**
		 * W9sme_Addon constructor.
		 */
		public function __construct() {
			/*-------------------------------------
				DEFINE CONSTANTS
			---------------------------------------*/
			$this->define_constants();
			
			/*-------------------------------------
				WE DO CUSTOMIZE THE VC
			---------------------------------------*/
			require_once( self::plugin_dir() . 'includes/vc_customize/class-w9sme-vc-customize.php' );
			new W9sme_VC_Customize();
			
			/*-------------------------------------
				REGISTER NEW CUSTOM POST TYPE
			---------------------------------------*/
			add_action( 'w9sme_before_load_theme_options', array( $this, 'register_custom_post_type' ) );
			
			/*-------------------------------------
				INIT
			---------------------------------------*/
			add_action( 'init', array( $this, 'init' ), 50 );
			
			/*-------------------------------------
				LOAD WIDGETS
			---------------------------------------*/
			add_action( 'widgets_init', array( $this, 'register_w9sme_widgets' ) );
			
			/*-------------------------------------
				PLUGINS UPDATE CHECKER
			---------------------------------------*/
//			self::plugin_update_checker();
		}
		
		
		/**
		 * Init the addon
		 */
		function init() {
			/*-------------------------------------
				CUSTOM MIME TYPES
			---------------------------------------*/
			add_filter( 'upload_mimes', array( $this, 'mime_types' ) );
			
			/*-------------------------------------
				LOAD TEXT DOMAIN
			---------------------------------------*/
			$this->load_text_domain();
		}
		
		/**
		 * define some constants
		 */
		function define_constants() {
			define( 'W9SME_STYLE_PREFIX', 'w9sme-style-' );
			define( 'W9SME_SCRIPT_PREFIX', 'w9sme-script-' );
			define( 'W9SME_SC_PREFIX', 'w9sme_shortcode_' );
			define( 'W9SME_SC_CATEGORY', 'W9sme Elements' );
			define( 'W9SME_PORTFOLIO_SC_CATEGORY', 'W9sme Portfolio' );
		}
		
		/**
		 * Load libs
		 */
		function load_libraries() {
			/*-------------------------------------
				LOAD WPALCHEMY METABOX
			---------------------------------------*/
			if ( ! class_exists( 'WPAlchemy_MetaBox' ) ) {
				require_once self::plugin_dir() . 'includes/library/wpalchemy/MetaBox.php';
				require_once self::plugin_dir() . 'includes/library/wpalchemy/MediaAccess.php';
			}
		}
		
		/**
		 * Load text domain
		 */
		function load_text_domain() {
			load_plugin_textdomain( 'w9sme-addon', false, self::plugin_dir() . 'languages/' );
		}
		
		/**
		 * Custom mime types
		 *
		 * @param $mimes
		 *
		 * @return mixed
		 */
		function mime_types( $mimes ) {
			$mimes['eot']  = 'application/vnd.ms-fontobject';
			$mimes['woff'] = 'application/x-font-woff';
			$mimes['ttf']  = 'application/x-font-truetype';
			$mimes['svg']  = 'image/svg+xml';
			
			return apply_filters( 'w9sme-addon/mime-types', $mimes );
		}
		
		
		/**
		 * Register Widget
		 *
		 * Name the widget class and its file follow this rule: W9sme_Widget_{your_widget_name},
		 * class-w9sme-widget-{your_widget_name}.php then place it in the widgets folder.
		 *
		 * Your widget class need to extends the W9sme_Widget_Base class.
		 *
		 * Add your widget class name to $widgets if you want to register it.
		 */
		function register_w9sme_widgets() {
			$widgets = array(
				'W9sme_Widget_Posts',
				'W9sme_Widget_FB_Page',
				'W9sme_Widget_Social_Profiles',
				'W9sme_Widget_Logo',
				'W9sme_Widget_Menu',
//				'W9sme_Widget_Tag_Cloud',
				'W9sme_Widget_Download',
				'W9sme_Widget_Simple_Calendar',
//				'W9sme_Widget_Post_Author',
//                'W9sme_Widget_Image_Info',
//                'W9sme_Widget_MailChimp',
				'W9sme_Widget_Twitter',
//				'W9sme_Widget_Shop_Account'
			);
			
			require_once( self::plugin_dir() . 'includes/widgets/class-w9sme-widget-base.php' );
			foreach ( $widgets as $widget ) {
				$widget_sanitized = strtolower( str_replace( '_', '-', $widget ) );
				if ( file_exists( self::plugin_dir() . 'includes/widgets/class-' . $widget_sanitized . '.php' ) ) {
					require_once( self::plugin_dir() . 'includes/widgets/class-' . $widget_sanitized . '.php' );
					register_widget( $widget );
				}
			}
		}
		
		/**
		 * Register new cpt
		 */
		function register_custom_post_type() {
			/*-------------------------------------
				   LOAD LIBRARY
			   ---------------------------------------*/
//            $this->load_libraries();
			
			/*-------------------------------------
				JUST REGISTER NEW CUSTOM POST TYPE
			---------------------------------------*/
			// CPT BASE ABSTRACT
			require_once( self::plugin_dir() . 'includes/custom-post-type/class-w9sme-cpt-base.php' );
			
			// CPT CONTENT TEMPLATE
			require_once( self::plugin_dir() . 'includes/custom-post-type/class-w9sme-cpt-content-template.php' );
			
			// CPT PORTFOLIO
            require_once( self::plugin_dir() . 'includes/custom-post-type/class-w9sme-cpt-portfolio.php' );
			
			// CPT SERVICE
			require_once( self::plugin_dir() . 'includes/custom-post-type/class-w9sme-cpt-service.php' );
			
			// CPT EVENT
//			require_once( self::plugin_dir() . 'includes/custom-post-type/class-w9sme-cpt-event.php' );
			
			// CPT REVIEW
			require_once( self::plugin_dir() . 'includes/custom-post-type/class-w9sme-cpt-review.php' );
			
			// CPT THEME DEMO
//            require_once( self::plugin_dir() . 'includes/custom-post-type/class-w9sme-cpt-theme-demo.php' );
			
			// CPT VACANCY
			require_once( self::plugin_dir() . 'includes/custom-post-type/class-w9sme-cpt-vacancy.php' );
			
			// flush_rewrite_rules
			flush_rewrite_rules();
		}
		
		/**
		 * Plugin update checker
		 */
		static function plugin_update_checker() {
			require self::plugin_dir() . 'includes/library/plugin-update-checker/plugin-update-checker.php';
			PucFactory::buildUpdateChecker(
				'http://9wpthemes.com/wp-content/uploads/wp-updates/?action=get_metadata&slug=w9-w9sme-addon',
				__FILE__
			);
		}
		
		static function require_w9sme_color( $once = true ) {
			if ( $once ) {
				require_once W9sme_Addon::plugin_dir() . 'includes/library/phpColors/Color.php';
			} else {
				require W9sme_Addon::plugin_dir() . 'includes/library/phpColors/Color.php';
			}
		}
		
		static function plugin_dir( $file = __FILE__ ) {
			return trailingslashit( plugin_dir_path( $file ) );
		}
		
		static function plugin_url( $file = __FILE__ ) {
			return trailingslashit( plugin_dir_url( $file ) );
		}
		
		static function is_vc_active() {
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			
			return is_plugin_active( 'js_composer/js_composer.php' );
		}
		
		static function is_woocommerce_active() {
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			
			return is_plugin_active( 'woocommerce/woocommerce.php' );
		}
	}
	
	new W9sme_Addon();
endif;


