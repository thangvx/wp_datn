<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package w9sme
 */

?>
					<?php
					/**
					 * @hook: Hook something here!
					 */
					do_action('w9sme_page_content_inner_end');
					?>
	
				</div><!-- #content -->
				<?php
				/**
				 * @hook: Hook something here!
				 */
				do_action('w9sme_page_content_after');
				?>
	
			</div><!-- #page -->
			<?php
			/**
			 * @hook: Hook something here!
			 *
			 */
		do_action('w9sme_page_after');
			?>

		</div><!-- #page wrapper -->
		<?php
		/**
		 * @hook: Hook something here!
		 *
		 */
		do_action('w9sme_page_wrapper_after');
		?>
		<?php wp_footer(); ?>
	</body>
</html>
<!-- Look so cool, right? -->