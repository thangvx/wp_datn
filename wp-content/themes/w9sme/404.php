<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link    https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package w9sme
 */

get_header();

w9sme_get_template_part( '404' );

get_footer();