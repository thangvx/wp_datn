<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: single.php
 * @time    : 9/5/16 3:36 PM
 * @author  : 9WPThemes Team
 */

/**
 * The template for displaying all single posts.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package w9sme
 */

get_header();

w9sme_get_template_part( 'single' );

get_footer();

  