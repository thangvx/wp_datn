=== W9sme Premium Wordpress Theme ===
Designed & Developed by 9WPThemes
Theme URI: http://w9sme.9wpthemes.com
Author: 9WPThemes Team
Author URI: http://9wpthemes.com
Contributors: 9WPThemes
Tags: two-columns, three-columns, left-sidebar, right-sidebar, custom-background, custom-header, custom-menu, editor-style, post-formats, rtl-language-support, sticky-post, theme-options, translation-ready, accessibility-ready


== Description ==

* W9sme Premium WordPress Theme

== Changelog ==
= Version 1.0.0

* Initial Release!


	

