<?php
/**
 * _9WPThemes class
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package 9WPThemes
 */

/**
 * Is in dev mode or not?
 * @return bool
 */
function w9sme_is_dev_mode() {
    return ( defined( 'W9SME_DEV_MODE' ) && W9SME_DEV_MODE ) || ( isset($_GET['w9sme_dev_mode']) );
}

/**
 * Get theme directory
 * @return string
 */
function w9sme_theme_dir() {
    return trailingslashit( get_template_directory() );
}

/**
 * Get theme url
 * @return string
 */
function w9sme_theme_url() {
    return trailingslashit( get_template_directory_uri() );
}


// Init the theme object
$GLOBALS['w9sme'] = W9sme();

/**
 * Class W9sme_By_9WPThemes
 */
class W9sme_By_9WPThemes {
    //singleton implementation
    private static $instance;
    
    /**
     * @var W9sme_Templates
     */
    public $templates;
    /**
     * @var W9sme_Options
     */
    public $options;
    /**
     * @var W9sme_Metaboxes
     */
    public $metaboxes;
    
    /**
     * @var W9sme_Breadcrumb
     */
    public $breadcrumb;
    
    
    /** get single-ton instance
     * @return W9sme_By_9WPThemes
     */
    public static function get_instance() {
        if ( self::$instance == null ) {
            self::$instance = new self;
        }
        
        return self::$instance;
    }
    
    /**
     * W9sme_By_9WPThemes constructor.
     */
    private function __construct() {
        $this->define_constants();
        
        $this->check_php_version();
        
        spl_autoload_register( array( $this, 'autoload_classes' ) );
        
        $this->helpers();
        
        $this->load_current_preset();
        
        $this->load_theme_options_data();
        
        $this->theme_setup();
        
        $this->after_theme_setup();
        
        $this->create_common_objects();
        
        $this->add_9wpthemes_admin_panel();
        
//        $this->theme_update_checker();
        
    }
    
    private function define_constants() {
        define( 'W9SME_THEME_OPTIONS_DEFAULT_NAME', 'w9sme_options' );
        define( 'W9SME_THEME_OPTIONS_VAR', 'w9sme_options' );
    
        define( 'W9SME_PRESET_LIST_KEY', 'w9sme_preset_list' );
        define( 'W9SME_GLOBAL_PRESET_KEY', 'w9sme_global_preset' );
        define( 'W9SME_THEME_VERSION', wp_get_theme()->get( 'Version' ) );
        define( 'W9SME_DEFAULT_DATE_FORMAT', "Y-m-d" );
    }
    
    /**
     * Autoloader
     *
     * Automatically load files when their classes are required.
     */
    private function autoload_classes( $class_name ) {
        if ( class_exists( $class_name ) ) {
            return;
        }
        // sanitize the class name
        $class_name = strtolower( str_replace( '_', '-', $class_name ) );
        
        $class_path = w9sme_theme_dir() . 'includes/class/class-' . $class_name . '.php';
        
        if ( file_exists( $class_path ) ) {
            require_once( $class_path );
        }
    }
    
    
    /**
     * Load helper functions
     */
    private function helpers() {
        /*-------------------------------------
        	Custom functions that act independently of the theme templates.
        ---------------------------------------*/
        require_once( w9sme_theme_dir() . 'includes/common/extras.php' );
        
        /*-------------------------------------
        	Import image resize functions
        ---------------------------------------*/
        require_once( w9sme_theme_dir() . 'includes/library/resize/w9sme-resize.php' );
        
        /*-------------------------------------
        	PRESET MANAGE HELPER
        ---------------------------------------*/
        require_once( w9sme_theme_dir() . 'includes/common/helpers-preset.php' );
        
    }
    
    /**
     * Add 9WPThemes Wordpress admin panel
     */
    private function add_9wpthemes_admin_panel() {
        require_once( w9sme_theme_dir() . 'includes/admin-panel/class-w9sme-admin-panel.php' );
        new W9sme_Admin_Panel();
    }
    
    private function check_php_version() {
        $php_version = phpversion();
        if ( version_compare( $php_version, '5.4' ) == - 1 ) {
            echo '<h3>' . wp_get_theme()->get( 'Name' ) . ' Wordpress Theme developed by 9WPThemes Team is no longer support PHP version lower than 5.4. </h3>';
            echo '<p>- You site is currently using php version <strong>' . $php_version . '</strong>.</p>';
            echo '<p>- Please contact your hosting provider or server administrator to upgrade php version.</p>';
            echo '<p>- Read more about <a href="https://wordpress.org/about/requirements/">Wordpress Requirement</a>.</p>';
            wp_die();
        }
    }
    
    /**
     * Setup the theme
     */
    private function theme_setup() {
        /*-------------------------------------
        	Required Plugins for this theme
        ---------------------------------------*/
        require_once( w9sme_theme_dir() . 'includes/common/theme-required-plugins.php' );
        
        /*-------------------------------------
        	Theme Setup
        ---------------------------------------*/
        require_once( w9sme_theme_dir() . 'includes/common/theme-setup.php' );
    }
    
    private function after_theme_setup() {
        /*-------------------------------------
        	Create extra widget options
        ---------------------------------------*/
        new W9sme_Extra_Widget_Options();
        
        /*-------------------------------------
        	Create user profile field
        ---------------------------------------*/
        new W9sme_User_Social_Profile();
        
        /*-------------------------------------
        	Register sidebars
        ---------------------------------------*/
        require_once( w9sme_theme_dir() . 'includes/common/theme-sidebars.php' );
        
        /*-------------------------------------
        	Include tax meta
        ---------------------------------------*/
        require_once( w9sme_theme_dir() . 'includes/common/theme-tax-meta.php' );
        
        
        new W9sme_Common();
        
        new W9sme_Wrap();
        
        new W9sme_Blog();
        
        new W9sme_AJAX();
        
        new W9sme_Menu();
        
        new W9sme_Enqueue();
        
        new W9sme_Image();
        
        if ( w9sme_is_woocommerce_active() ) {
            new W9sme_Woocommerce();
        }
    }
    
    private function load_current_preset() {
        $preset_list = w9sme_get_preset_list();
        
        // clean-up
        if ( empty( $preset_list ) || !is_array( $preset_list ) ) {
            $preset_list = array(
                W9SME_THEME_OPTIONS_DEFAULT_NAME => esc_html__( 'Default Preset', 'w9sme' ),
            );
            set_theme_mod( W9SME_PRESET_LIST_KEY, $preset_list );
        } else {
            // if the preset list does not contain the default => create one
            if ( !array_key_exists( W9SME_THEME_OPTIONS_DEFAULT_NAME, $preset_list ) ) {
                set_theme_mod( W9SME_PRESET_LIST_KEY, array_merge( array(
                    W9SME_THEME_OPTIONS_DEFAULT_NAME => esc_html__( 'Default Preset', 'w9sme' ),
                ), $preset_list ) );
            }
        }
        
        if ( is_admin() && !empty( $_REQUEST['opt_name'] ) ) {
            $w9sme_current_preset = $_REQUEST['opt_name'];
            if ( !array_key_exists( $w9sme_current_preset, $preset_list ) ) {
                $w9sme_current_preset = W9SME_THEME_OPTIONS_DEFAULT_NAME;
            }
            
            // change after, using js
            if ( isset( $_REQUEST['make_global'] ) ) {
//                set_theme_mod( W9SME_GLOBAL_PRESET_KEY, $w9sme_current_preset );
                w9sme_set_global_preset( $w9sme_current_preset );
            }
        } else {
            $w9sme_current_preset = w9sme_get_global_preset();
            
            if ( $w9sme_current_preset === false || !array_key_exists( $w9sme_current_preset, $preset_list ) ) {
                $w9sme_current_preset = W9SME_THEME_OPTIONS_DEFAULT_NAME;
                w9sme_set_global_preset( $w9sme_current_preset );
            }
        }
        
        // define theme options name
        global $w9sme_global_preset_name;
        $w9sme_global_preset_name = $w9sme_current_preset;
    }
    
    private function load_theme_options_data() {
        /*-------------------------------------
            Helper functions
        ---------------------------------------*/
        require_once( w9sme_theme_dir() . 'includes/common/helpers-options.php' );
        
        /*-------------------------------------
            BEFORE LOAD THEME OPTIONS
        ---------------------------------------*/
        do_action( 'w9sme_before_load_theme_options' );
        
        /*-------------------------------------
        	Load metabox options
        ---------------------------------------*/
        if ( !class_exists( 'ReduxFramework_extension_metaboxes' ) ) {
            require_once( w9sme_theme_dir() . 'includes/library/w9sme-option-extensions/extensions-init.php' );
        }
        
        // switch_theme_options_by_meta
        $this->switch_theme_options_by_meta();
        
        $this->metaboxes = new W9sme_Metaboxes();
        
        /*-------------------------------------
        	Load theme options
        ---------------------------------------*/
        if ( !class_exists( 'ReduxFramework' ) ) {
            require_once( w9sme_theme_dir() . 'includes/library/w9sme-option-framework/class-w9sme-option-framework.php' );
            W9smeOptionFramework::instance();
        }
        $this->options = new W9sme_Options();
    }
    
    /**
     * switch_theme_options_by_meta
     */
    function switch_theme_options_by_meta() {
        // load preset from metabox or from $_GET
        if ( !is_admin() ) {
            $use_preset = isset( $_GET['use-preset'] ) ? sanitize_title( $_GET['use-preset'] ) : '';
            if ( !empty( $use_preset ) && w9sme_is_preset_exist( $use_preset ) ) {
                w9sme_set_current_preset( $use_preset );
            } else {
                if ( class_exists( 'ReduxFramework_extension_metaboxes' ) ) {
                    $post_id = ReduxFramework_extension_metaboxes::get_current_post_id();
                    
                    if ( !empty( $post_id ) ) {
                        $meta_preset = get_post_meta( $post_id, 'meta-using-preset', true );
                        if ( !empty( $meta_preset ) ) {
                            if ( w9sme_is_preset_exist( $meta_preset ) ) {
                                w9sme_set_current_preset( $meta_preset );
                            } else {
                                delete_post_meta( $post_id, 'meta-using-preset' );
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * create_common_objects
     */
    private function create_common_objects() {
        $this->templates  = new W9sme_Templates();
        $this->breadcrumb = new W9sme_Breadcrumb();
    }
    
    
    /**
     * theme_update_checker
     */
    private function theme_update_checker() {
        require w9sme_theme_dir() . 'includes/library/theme-updates/theme-update-checker.php';
        $theme_slug = get_template();
        new ThemeUpdateChecker(
            $theme_slug, //Theme slug. Usually the same as the name of its directory.
            'http://9wpthemes.com/wp-content/uploads/wp-updates/?action=get_metadata&slug=' . $theme_slug  //Metadata URL.
        );
    }
}

/**
 * @return W9sme_By_9WPThemes
 */
function W9sme() {
    return W9sme_By_9WPThemes::get_instance();
}

//
// USE THESE FUNCTION IN ACTION, FILTER OR IN THEME TEMPLATE
//
/**
 * @return W9sme_Options
 */
function W9sme_Options() {
    return W9sme()->options;
}


/**
 * @return W9sme_SCSS
 */
function W9sme_SCSS() {
    return W9sme_Options()->compiler;
}


/**
 * @return W9sme_Metaboxes
 */
function W9sme_Metaboxes() {
    return W9sme()->metaboxes;
}

/**
 * @return W9sme_Variables
 */
function W9sme_Variables() {
    return W9sme_Options()->variables;
}

/**
 * @return W9sme_Breadcrumb
 */
function W9sme_Breadcrumb() {
    return W9sme()->breadcrumb;
}

/**
 * @return W9sme_Templates
 */
function W9sme_Templates() {
    return W9sme()->templates;
}

/**
 * @param       $slug
 * @param null  $name
 * @param array $args
 */
function w9sme_get_template_part( $slug, $name = null, $args = array() ) {
    W9sme_Templates()->get_template_part( $slug, $name, $args );
}


// load dev mode config, remove this when deploy
if ( file_exists( w9sme_theme_dir() . 'includes/common/dev_mode_config.php' ) ) {
    require_once( w9sme_theme_dir() . 'includes/common/dev_mode_config.php' );
}