<?php
/**
 * The template for displaying search results pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package w9sme
 */

get_header();

w9sme_get_template_part('search');

get_footer();