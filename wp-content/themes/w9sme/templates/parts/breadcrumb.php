<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: breadcrumb.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

//$breadcrumb_wrapper_class = array();
//
//$template_prefix = w9sme_get_template_prefix();
//
//$breadcrumb_float_mode = w9sme_get_meta_or_option( 'page-title-breadcrumbs-float-mode', '', '1', $template_prefix );
//
//if ( $breadcrumb_float_mode == '0' ) {
//    $breadcrumb_wrapper_class[] = 'text-' . w9sme_get_meta_or_option( 'page-title-breadcrumbs-align', '', 'center', $template_prefix );
//} else {
//    $breadcrumb_wrapper_class[] = 'float-mode';
//}

?>
<div class="w9sme-breadcrumb-wrapper no-mb float-mode">
    <?php W9sme_Breadcrumb()->print_breadcrumb_html( '', 'no-mb' ); ?>
</div>
