<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: title-default.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$template_prefix = w9sme_get_template_prefix();
// Layout
$site_title_layout = w9sme_get_meta_or_option( 'page-title-layout', '', 'container', $template_prefix );

//
// Classes
//
$site_title_inner_class              = array();
$site_title_breadcrumb_wrapper_class = array();

// text options
$site_title_inner_class[] = w9sme_get_meta_or_option( 'page-title-text-align', '', 'container', $template_prefix );

//$site_title_breadcrumb_wrapper_class[] = '';

$title_text_class   = array();
$title_text_class[] = w9sme_get_meta_or_option( 'title-font-style', '', '', $template_prefix );
$title_text_class[] = w9sme_get_meta_or_option( 'title-text-transform', '', 'text-uppercase', $template_prefix );

$page_title_top_line = w9sme_get_meta_or_option( 'page-title-top-line', '', '', $template_prefix );

if ( $page_title_top_line == '1' ) {
	$title_text_class[] = 'top-line';
}

$subtitle_text_class   = array();
$subtitle_text_class[] = w9sme_get_meta_or_option( 'subtitle-font-style', '', 'fs-italic', $template_prefix );
$subtitle_text_class[] = w9sme_get_meta_or_option( 'subtitle-text-transform', '', 'text-transform-__', $template_prefix );

//
// Title image options
//
$title_bg_url = w9sme_get_meta_or_option( 'page-title-background', 'url', '', $template_prefix );

//
// Parallax effect options
//
$parallax_effect   = w9sme_get_meta_or_option( 'page-title-parallax-effect', '', 'no-effect', $template_prefix );
$parallax_position = w9sme_get_meta_or_option( 'page-title-parallax-position', '', 'center', $template_prefix );

//
// Page title
//
$page_subtitle = w9sme_get_page_subtitle();

//
// Breadcrumb
//
$page_title_breadcrumb                 = w9sme_get_meta_or_option( 'page-title-breadcrumbs', '', false, $template_prefix );
$breadcrumb_position                   = w9sme_get_meta_or_option( 'breadcrumbs-position', '', 'right', $template_prefix );
$site_title_breadcrumb_wrapper_class[] = $breadcrumb_position;
// prepended text
//$get_prepended_text = w9sme_get_meta_or_option( 'breadcrumbs-prepended-text', '', '', $template_prefix );
$get_prepended_text = '';

//
// Calculate columns
//
$site_title_inner_col   = '';
$breadcrumb_wrapper_col = '';
if ( ! $page_title_breadcrumb ) {
	$site_title_inner_col   = '';
	$breadcrumb_wrapper_col = '';
}

$site_title_inner_class[]              = $site_title_inner_col;
$site_title_breadcrumb_wrapper_class[] = $breadcrumb_wrapper_col;

// CTA Button
$page_title_cta               = w9sme_get_meta_or_option( 'page-title-cta', '', false, $template_prefix );
$page_title_cta_btn_text      = '';
$page_title_cta_btn_shortcode = '';
if ( ! empty( $page_title_cta ) ) {
	$page_title_cta_btn_text = w9sme_get_meta_or_option( 'page-title-cta-btn-text', '', false, $template_prefix );
	if ( $page_title_cta == 'simple' ) {
		$page_title_cta_btn_shortcode = "[w9sme_shortcode_button btn_text=\"{{cta-btn-text}}\" btn_ff=\"p-font\" btn_style=\"btn-style-border-2\" btn_icon_align=\"align-icon-right\" type=\"9wpthemes\" icon_9wpthemes=\"w9 w9-ico-right-open\" btn_icon_size=\"150%\" btn_size=\"btn-size-sm\" btn_icon_color=\"__\" btn_bgc=\"transparent\" btn_border_color=\"light\" btn_text_hover_color=\"text\" btn_bgc_hover_color=\"light\" btn_border_hover_color=\"light\" btn_add_icon=\"1\"]";
	} else {
		$page_title_cta_btn_shortcode = w9sme_get_meta_or_option( 'page-title-cta-btn-shortcode', '', '', $template_prefix );
	}
	
	$page_title_cta_btn_shortcode = str_replace( '{{cta-btn-text}}', $page_title_cta_btn_text, $page_title_cta_btn_shortcode );
}
?>
<section class="site-title page-title <?php w9sme_the_clean_html_classes( $template_prefix ) ?>">
	<?php if ( $parallax_effect !== 'no-effect' ): ?>
		<div class="bg-wrapper"
			 data-stellar-background-ratio="<?php echo esc_attr( $parallax_effect ); ?>"
			 style="background-image: url('<?php echo esc_url( $title_bg_url ); ?>'); background-position: center <?php echo esc_attr( $parallax_position ); ?>;">
		</div>
	<?php else: ?>
		<div class="bg-wrapper no-parallax"
			 style="background-image: url('<?php echo esc_url( $title_bg_url ); ?>'); background-position: center <?php echo esc_attr( $parallax_position ); ?>;"></div>
	<?php endif; ?>
	<div class="site-title-layout <?php echo esc_attr( $site_title_layout ); ?>">
		<div class="row">
			<div class="col-xs-12">
				<div class="site-title-inner-wrapper">
					<div class="site-title-inner <?php w9sme_the_clean_html_classes( $site_title_inner_class ); ?>">
						<div class="__inner-wrapper clearfix">
							<div class="__inner-left">
								<h1 class="title no-mb <?php w9sme_the_clean_html_classes( $title_text_class ); ?>"><?php echo w9sme_get_page_title(); ?></h1>
								<?php if ( $page_subtitle ): ?>
									<p class="sub-title no-mb <?php w9sme_the_clean_html_classes( $subtitle_text_class ); ?>"><?php echo esc_html( $page_subtitle ); ?></p>
								<?php endif; ?>
							</div>
							
							<?php if ( ! empty( $page_title_cta ) ): ?>
								<div class="__inner-right cta-button-wrapper">
									<?php echo do_shortcode( ( $page_title_cta_btn_shortcode ) ); ?>
								</div>
							<?php endif; ?>
						</div>
					</div>
					<?php if ( $page_title_breadcrumb ) : ?>
						<div class="site-title-breadcrumb-wrapper clearfix <?php w9sme_the_clean_html_classes( $site_title_breadcrumb_wrapper_class ); ?>">
							<div class="w9sme-breadcrumb-wrapper float-mode">
								<?php W9sme_Breadcrumb()->print_breadcrumb_html( $get_prepended_text, 'no-mb' ); ?>
								<div class="__shape">
									<div class="__rect"></div>
									<div class="__slope">
										<svg height="100%" width="100%" viewBox="0 0 100 100"
											 preserveAspectRatio="none">
											<?php if ( $breadcrumb_position === 'left' ) { ?>
												<polygon points="0,0 0,100 100,100" />
											<?php } else { ?>
												<polygon points="0,100 100,100 100,0" />
											<?php } ?>
										</svg>
									</div>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>