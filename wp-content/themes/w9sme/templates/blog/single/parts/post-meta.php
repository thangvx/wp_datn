<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: post-meta.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$blog_single_post_meta = w9sme_get_option( 'blog-single-post-meta' );
$enable_date           = $enable_author = $enable_comments = $enable_categories = 0;
if ( is_array( $blog_single_post_meta ) && ! empty( $blog_single_post_meta ) ) {
	$enable_date       = ! empty( $blog_single_post_meta['date'] ) ? 1 : 0;
	$enable_author     = ! empty( $blog_single_post_meta['author'] ) ? 1 : 0;
//	$enable_comments   = ! empty( $blog_single_post_meta['comments'] ) ? 1 : 0;
	$enable_categories = ! empty( $blog_single_post_meta['categories'] ) ? 1 : 0;
//	$enable_tags       = ! empty( $blog_single_post_meta['tags'] ) ? 1 : 0;
}

if ( $enable_date || $enable_author || $enable_categories ):
	?>
	<div class="entry-meta-wrapper __group-meta-1">
		<ul class="w9sme-entry-meta list-unstyled no-mb">
			<?php if ( ! empty( $enable_author ) ): ?>
				<li class="entry-meta-author">
					<span class="fw-semibold"><?php esc_html_e( 'By', 'w9sme' ); ?></span> <?php printf( '<a class="author vcard" href="%1$s">%2$s </a>', esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), esc_html( get_the_author() ) ); ?>
				</li>
			<?php endif; ?>
			
			<?php if ( ! empty( $enable_date ) ): ?>
				<li class="entry-meta-date">
					<a href="<?php echo get_day_link( get_the_time( 'Y' ), get_the_time( 'm' ), get_the_time( 'd' ) ); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php echo get_the_date( get_option( 'date_format' ) ); ?> </a>
				</li>
			<?php endif; ?>
			
			<?php if ( ! empty( $enable_categories ) && has_category() ): ?>
				<li class="entry-meta-category">
					<span class="fw-semibold"><?php esc_html_e( 'In ', 'w9sme' ); ?></span> <?php echo get_the_category_list( ', ' ); ?>
				</li>
			<?php endif; ?>
			
			<?php if ( ! empty( $enable_comments ) && ( comments_open() || get_comments_number() ) ) :
				?>
				<li class="entry-meta-comment">
					<?php comments_popup_link(
						sprintf( '<span>%s</span>', esc_html__( 'No Comment', 'w9sme' ) ),
						sprintf( '<span>%s</span>', esc_html__( 'One Comment', 'w9sme' ) ),
						sprintf( '%s <span>%s</span>', get_comments_number(), esc_html__( 'Comments', 'w9sme' ) ) );
					?>
				</li>
			<?php endif;
			?>
			
			<?php edit_post_link( esc_html__( 'Edit', 'w9sme' ), '<li class="entry-edit-link">', '</li>' ); ?>
		</ul>
	</div>
<?php endif; ?>