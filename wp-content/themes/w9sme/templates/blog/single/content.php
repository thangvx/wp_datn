<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: content.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$image_size = W9sme_Templates()->get_template_args( 'image-size', 'blog/single/content', '' );

if ( w9sme_get_meta_or_option( 'blog-single-resize-image' ) == '0' ) {
	$image_size = false;
}

$blog_single_post_meta = W9sme_Templates()->get_template_args( 'post-meta', 'blog/single/content', '' );
$enable_date           = $enable_comments = $enable_categories = $enable_tags = $enable_social_share = '0';
if ( is_array( $blog_single_post_meta ) && ! empty( $blog_single_post_meta ) ) {
	$enable_date         = ! empty( $blog_single_post_meta['date'] ) ? 1 : 0;
	$enable_author       = ! empty( $blog_single_post_meta['author'] ) ? 1 : 0;
	$enable_comments     = ! empty( $blog_single_post_meta['comments'] ) ? 1 : 0;
	$enable_categories   = ! empty( $blog_single_post_meta['categories'] ) ? 1 : 0;
	$enable_tags         = ! empty( $blog_single_post_meta['tags'] ) ? 1 : 0;
	$enable_social_share = ! empty( $blog_single_post_meta['social-share'] ) ? 1 : 0;
}

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="entry-content-wrapper">
		<h3 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
		</h3>
		<?php
		w9sme_get_template_part( 'blog/single/parts/post', 'meta' );
		
		$entry_header = W9sme_Blog::post_feature_format_auto();
		if ( $entry_header ) {
			?>
			<div class="entry-thumbnail-wrapper">
				<?php echo sprintf( '%s', $entry_header ); ?>
			</div>
			<?php
		}
		?>

		<div class="entry-content">
			<?php
			the_content();
			echo W9sme_Blog::get_link_pages();
			?>
		</div>
		<!--Post tags-->
		<?php if ( ! ( empty( $enable_tags ) && empty( $enable_social_share ) ) ) { ?>
			<div class="__group-meta-2 clearfix">
				<?php w9sme_get_template_part( 'blog/single/parts/post', 'tags' );
				if ( ! empty( $enable_social_share ) ) {
					w9sme_get_template_part( 'blog/parts/post', 'social-share' );
				}
				?>
			</div>
		<?php } ?>
	</div><!-- .entry-content -->
	
	<?php
	
	/**
	 * Include Post Author Info
	 */
	w9sme_get_template_part( 'blog/single/parts/post', 'author-info' );
	
	/**
	 * Include Post Navigation Links
	 */
	w9sme_get_template_part( 'blog/single/parts/post', 'nav' );
	
	/**
	 * Include Comments Template
	 */
	comments_template();
	
	?>

</article><!-- #post-## -->

