<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: main-nav.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$template_prefix = w9sme_get_template_prefix();

$nav_width       = w9sme_get_meta_or_option( 'nav-width', '', null, $template_prefix );
$nav_boxed       = w9sme_get_meta_or_option( 'nav-boxed-enabled', '', null, $template_prefix );
$nav_content     = w9sme_get_meta_or_option( 'nav-content', '', null, $template_prefix );
$nav_logo_select = w9sme_get_meta_or_option( 'nav-logo-select', '', null, $template_prefix );
if ( empty( $nav_logo_select ) ) {
	$nav_logo_select = 'logo';
}
$nav_sticky_logo_select = w9sme_get_meta_or_option( 'nav-sticky-logo-select', '', null, $template_prefix );
if ( empty( $nav_sticky_logo_select ) ) {
	$nav_sticky_logo_select = 'logo';
}

$nav_logo_height                  = w9sme_get_option( 'nav-logo-height', '', null, $template_prefix ); //Style
$nav_item_height                  = w9sme_get_option( 'nav-item-height', '', null, $template_prefix ); //Style
$nav_item_padding                 = w9sme_get_option( 'nav-item-padding', '', null, $template_prefix ); //Style
$nav_item_color                   = w9sme_get_option( 'nav-item-color', '', null, $template_prefix ); //Style
$nav_link_hover_background        = w9sme_get_option( 'nav-link-hover-background', '', null, $template_prefix ); //Style
$nav_background                   = w9sme_get_option( 'nav-background', '', null, $template_prefix ); //Style
$nav_background_overlay           = w9sme_get_option( 'nav-background-overlay', '', null, $template_prefix ); //Style
$nav_menu_item_hover_effect       = w9sme_get_meta_or_option( 'nav-menu-item-hover-effect', '', null, $template_prefix );
$nav_item_separator               = w9sme_get_option( 'nav-item-separator', '', null, $template_prefix );
$nav_border                       = w9sme_get_option( 'nav-border', '', null, $template_prefix ); //Style
$nav_occupy_spacing               = w9sme_get_meta_or_option( 'nav-occupy-spacing', '', null, $template_prefix );
$nav_sticky                       = w9sme_get_meta_or_option( 'nav-sticky', '', null, $template_prefix );
$nav_headroom                     = w9sme_get_meta_or_option( 'nav-headroom', '', null, $template_prefix );
$nav_sticky_logo_height           = w9sme_get_option( 'nav-sticky-logo-height', '', null, $template_prefix ); //Style
$nav_sticky_item_height           = w9sme_get_option( 'nav-sticky-item-height', '', null, $template_prefix ); //Style
$nav_sticky_item_color            = w9sme_get_option( 'nav-sticky-item-color', '', null, $template_prefix ); //Style
$nav_sticky_link_hover_background = w9sme_get_option( 'nav-sticky-link-hover-background', '', null, $template_prefix ); //Style
$nav_sticky_background            = w9sme_get_option( 'nav-sticky-background', '', null, $template_prefix ); //Style
$nav_sticky_background_overlay    = w9sme_get_option( 'nav-sticky-background-overlay', '', null, $template_prefix ); //Style
$nav_sticky_border                = w9sme_get_option( 'nav-sticky-border', '', null, $template_prefix ); //Style

$header_container    = w9sme_get_option( 'nav-width', '', null, $template_prefix );
$wrapper_nav_classes = array();
$main_nav_classes    = array();

// Add prefix class
$template_prefix       = w9sme_get_template_prefix();
$wrapper_nav_classes[] = $template_prefix;
//Main nav
if ( $nav_occupy_spacing != 'on' ) {
	$main_nav_classes[]    = 'w9sme-menu-overlay';
	$wrapper_nav_classes[] = 'w9sme-menu-overlay-wrapper';
}

if ( $nav_boxed === 'on' ) {
	$main_nav_classes[] = 'w9sme-menu-boxed-' . $nav_width;
}
//Sticky config
if ( $nav_sticky === 'on' ) {
	$main_nav_classes[] = 'w9sme-sticky';
	$mobile_nav_class[] = 'w9sme-sticky';
	
	if ( $nav_headroom === 'on' ) {
		$main_nav_classes[] = 'w9sme-headroom';
		$mobile_nav_class[] = 'w9sme-headroom';
	}
}

//Nav item separator
if ( $nav_item_separator === 'on' ) {
	$main_nav_classes[] = 'w9sme-nav-enable-separator';
}

//Header module
$nav_modules = $nav_module_desktop = $nav_module_desktop_sticky = $nav_module_mobile = array();
//$set_content_customize_for_singular = w9sme_get_meta_option( 'header-content-customize', '', '', 0 );
if ( w9sme_get_meta_option( 'nav-module-desktop-custom', '', '', 0 ) ) {
	$nav_module_desktop = w9sme_get_meta_option( 'nav-module-desktop' );
} else {
	$nav_module_desktop = w9sme_get_option( 'nav-module-desktop', '', null, $template_prefix );
}

if ( w9sme_get_meta_option( 'nav-module-desktop-sticky-custom', '', '', 0 ) ) {
	$nav_module_desktop_sticky = w9sme_get_meta_option( 'nav-module-desktop-sticky' );
} else {
	$nav_module_desktop_sticky = w9sme_get_option( 'nav-module-desktop-sticky', '', null, $template_prefix );
}

if ( w9sme_get_meta_option( 'nav-module-mobile-custom', '', '', 0 ) ) {
	$nav_module_mobile = w9sme_get_meta_option( 'nav-module-mobile' );
} else {
	$nav_module_mobile = w9sme_get_option( 'nav-module-mobile', '', null, $template_prefix );
}

//Merge Module and get module order
if ( isset( $nav_module_desktop['enabled'] ) && is_array( $nav_module_desktop['enabled'] ) ) {
	unset( $nav_module_desktop['enabled']['placebo'] );
	foreach ( array_keys( $nav_module_desktop['enabled'] ) as $order => $module ) {
		$nav_modules[ $module ]['desktop_order'] = $order;
	}
}
if ( isset( $nav_module_desktop_sticky['enabled'] ) && is_array( $nav_module_desktop_sticky['enabled'] ) ) {
	unset( $nav_module_desktop_sticky['enabled']['placebo'] );
	foreach ( array_keys( $nav_module_desktop_sticky['enabled'] ) as $order => $module ) {
		$nav_modules[ $module ]['desktop_sticky_order'] = $order;
	}
}
if ( isset( $nav_module_mobile['enabled'] ) && is_array( $nav_module_mobile['enabled'] ) ) {
	unset( $nav_module_mobile['enabled']['placebo'] );
	foreach ( array_keys( $nav_module_mobile['enabled'] ) as $order => $module ) {
		$nav_modules[ $module ]['mobile_order'] = $order;
	}
}

//Main menu
$menu_content_class               = array( 'w9sme-main-menu-content nav' );
$menu_content_class[]             = $nav_menu_item_hover_effect;
$menu_content_class[]             = w9sme_get_meta_or_option( 'menu-sub-appear-effect', null, 'w9sme-effect-fade' );
$menu_content_class[]             = w9sme_get_meta_or_option( 'menu-sub-item-hover-effect', null, 'w9sme-menu-sub-item-hover-none' );
$menu_content_class[]             = w9sme_get_meta_or_option( 'menu-mega-separator-enable', null, 1 ) ? 'w9sme-enable-mega-menu-separator' : '';
$nav_module_desktop_biggest       = w9sme_get_meta_or_option( 'nav-module-desktop-biggest', '', null, $template_prefix );
$nav_module_desktop_biggest_align = w9sme_get_meta_or_option( 'nav-module-desktop-biggest-align', '', null, $template_prefix );
$nav_module_mobile_biggest        = w9sme_get_meta_or_option( 'nav-module-mobile-biggest', '', null, $template_prefix );
$nav_module_mobile_biggest_align  = w9sme_get_meta_or_option( 'nav-module-mobile-biggest-align', '', null, $template_prefix );
?>
<header class="main-header w9sme-main-header <?php w9sme_the_clean_html_classes( $wrapper_nav_classes ); ?>">
	<nav id="w9sme-main-nav" class="w9sme-main-nav <?php w9sme_the_clean_html_classes( $main_nav_classes ); ?>">
		<div class="w9sme-nav-body">
			<div class="<?php w9sme_the_clean_html_classes( $nav_width ); ?>">
				<div class="w9sme-nav-body-content">
					<?php foreach ( $nav_modules as $key => $nav_module ):
						$nav_item_class = array( 'w9sme-nav-item' );
						$nav_item_class[] = 'w9sme-nav-' . $key . '-wrapper';
						
						if ( $key === $nav_module_desktop_biggest ) {
							$nav_item_class[] = 'biggest-nav-item-desktop';
							$nav_item_class[] = $nav_module_desktop_biggest_align;
						}
						if ( $key === $nav_module_mobile_biggest ) {
							$nav_item_class[] = 'biggest-nav-item-mobile';
							$nav_item_class[] = $nav_module_mobile_biggest_align;
						}
						
						$nav_item_class[] = isset( $nav_module['desktop_order'] ) ? 'nav-item-desktop-' . $nav_module['desktop_order'] : 'nav-item-desktop-none';
						$nav_item_class[] = isset( $nav_module['desktop_sticky_order'] ) ? 'nav-item-desktop-sticky-' . $nav_module['desktop_sticky_order'] : 'nav-item-desktop-sticky-none';
						$nav_item_class[] = isset( $nav_module['mobile_order'] ) ? 'nav-item-mobile-' . $nav_module['mobile_order'] : 'nav-item-mobile-none';
						
						?>
						<div class="<?php w9sme_the_clean_html_classes( $nav_item_class ); ?> ">
							<?php
							
							if ( $key === 'logo' ) {
								?>
								<div class="w9sme-nav-logo">
									<div class="__nav-logo">
										<?php echo W9sme_Image::logo( $nav_logo_select ); ?>
									</div>
									<div class="__sticky-nav-logo">
										<?php echo W9sme_Image::logo( $nav_sticky_logo_select ); ?>
									</div>
								</div>
								<?php
							} elseif ( $key === 'main-menu' ) {
								if ( has_nav_menu( 'primary' ) ):
									$menu_param = array(
										'menu'            => $nav_content,
										'theme_location'  => 'primary',
										'menu_id'         => 'primary-nav',
										'menu_class'      => w9sme_clean_html_classes( $menu_content_class ),
										'container_id'    => 'w9sme-main-menu',
										'container_class' => 'w9sme-main-menu',
										'walker'          => new W9sme_Walker_Nav_Menu,
									);
									wp_nav_menu( $menu_param );
								endif;
							} elseif ( $key === 'cart' && function_exists( 'wc_get_cart_url' ) ) {
								// dark, light, transparent
								$mini_cart_theme = w9sme_get_option( 'shop-mini-cart-theme', '', 'theme-dark' );
								if ( empty( $mini_cart_theme ) ) {
									$mini_cart_theme = 'theme-dark';
								}
								
								$show_minicart = ! is_cart() && ! is_checkout();
								
								$scroll_bar_theme = ( $mini_cart_theme === 'theme-dark' ) ? 'light' : 'dark';
								?>
								<div class="w9sme-mini-cart woocommerce dropdown">
									<a class="__cart-toggle" href="<?php echo wc_get_cart_url(); ?>">
										<i class="w9sme-block-icon <?php echo w9sme_get_option( 'shop-cart-icon' ); ?>"></i>
										<span class="__number-product"></span>
									</a>
									<div class="__cart-wrapper <?php w9sme_the_clean_html_classes( $mini_cart_theme ); ?>" data-scrollbar-theme="<?php echo esc_attr( $scroll_bar_theme ); ?>" data-allow-minicart="<?php echo esc_attr( $show_minicart ); ?>">
										<div class="widget_shopping_cart_content"></div>
									</div>
								</div>
								<?php
								
							} elseif ( $key === 'search' ) {
								?>
								<div class="w9sme-search-button-caller">
									<a href="javascript:void(0);" onclick="w9sme.page.popup('w9sme-search', 'popup')">
										<i class="w9sme-block-icon <?php echo w9sme_get_option( 'nav-search-icon' ) ?>"></i>
									</a>
								</div>
								<?php
							} elseif ( $key === 'popup' ) {
								$popup_type    = w9sme_get_option( 'nav-module-popup-type' );
								$popup_content = w9sme_get_option( 'nav-module-popup-' . $popup_type );
								$popup_id      = w9sme_add_popup_item( array(
									'type'    => $popup_type,
									'content' => $popup_content
								) );
								?>
								<div class="w9sme-popup-caller">
									<a href="javascript:void(0);" onclick="w9sme.page.popup('<?php echo esc_attr( $popup_type ) ?>', '<?php echo esc_attr( $popup_content ) ?>')">
										<i class="w9sme-block-icon <?php echo w9sme_get_option( 'nav-toggle-popup-icon' ); ?>"></i>
									</a>
								</div>
								<?php
							} elseif ( $key === 'toggle-leftzone' ) {
								?>
								<div class="w9sme-subzone-caller w9sme-leftzone-caller">
									<a href="javascript:void(0);" onclick="w9sme.page.page_leftzone()">
										<i class="w9sme-block-icon <?php echo w9sme_get_option( 'nav-toogle-leftzone-icon' ); ?>"></i>
									</a>
								</div>
								<?php
							} elseif ( $key === 'toggle-rightzone' ) {
								?>
								<div class="w9sme-subzone-caller w9sme-rightzone-caller">
									<a href="javascript:void(0);" onclick="w9sme.page.page_rightzone()">
										<i class="w9sme-block-icon <?php echo w9sme_get_option( 'nav-toogle-rightzone-icon' ); ?>"></i>
									</a>
								</div>
								<?php
							} elseif ( $key === 'custom-content' ) {
								$custom_content = w9sme_get_meta_or_option( 'nav-custom-content' );
								if ( ! empty( $custom_content ) ) {
									?>
									<div class="w9sme-nav-custom-content">
										<?php
										echo do_shortcode( ( $custom_content ) );
										?>
									</div>
									<?php
								}
							}
							?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</nav>
</header>
