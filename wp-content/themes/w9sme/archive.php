<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package w9sme
 */

get_header();

w9sme_get_template_part('archive');

get_footer();