<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: dev_mode_config.php
 * @time    : 5/13/2016
 * @author  : 9WPThemes Team
 */

if ( !w9sme_is_dev_mode() ) {
	return;
}


// Config here!

/*-------------------------------------
    Auto compile style.css
---------------------------------------*/
w9sme_SCSS()->auto_compile_changed( array( 'bs', 'custom-style', 'custom-style.scss', 'admin-modules' ), false );

/*-------------------------------------
	Compile single scss file
---------------------------------------*/

$compile_scss_files = array(
	array(
		'input'      => w9sme_theme_dir() . 'assets/scss/admin-modules/style-admin.scss',
		'output'     => w9sme_theme_dir() . 'assets/css/style-admin.css',
		'output_min' => w9sme_theme_dir() . 'assets/css/style-admin.min.css',
	),
	array(
		'input'      => w9sme_theme_dir() . 'assets/scss/custom-3rd/redux/custom-redux-admin.scss',
		'output'     => w9sme_theme_dir() . 'assets/css/custom-redux-admin.css',
		'output_min' => w9sme_theme_dir() . 'assets/css/custom-redux-admin.min.css',
	),
	array(
		'input'      => w9sme_theme_dir() . 'assets/scss/custom-3rd/redux/custom-redux-fields.scss',
		'output'     => w9sme_theme_dir() . 'assets/css/custom-redux-fields.css',
		'output_min' => w9sme_theme_dir() . 'assets/css/custom-redux-fields.min.css',
	),
	array(
		'input'      => w9sme_theme_dir() . 'assets/scss/custom-3rd/redux/custom-redux-jquery-ui.scss',
		'output'     => w9sme_theme_dir() . 'assets/css/custom-redux-jquery-ui.css',
		'output_min' => w9sme_theme_dir() . 'assets/css/custom-redux-jquery-ui.min.css',
	),
	array(
		'input'      => WP_PLUGIN_DIR . '/w9sme-addon/assets/scss/custom-vc.scss',
		'output'     => WP_PLUGIN_DIR . '/w9sme-addon/assets/css/custom-vc.css',
		'output_min' => WP_PLUGIN_DIR . '/w9sme-addon/assets/css/custom-vc.min.css',
	),
	array(
		'input'      => WP_PLUGIN_DIR . '/w9sme-addon/assets/scss/vc-extends.scss',
		'output'     => WP_PLUGIN_DIR . '/w9sme-addon/assets/css/vc-extends.css',
		'output_min' => WP_PLUGIN_DIR . '/w9sme-addon/assets/css/vc-extends.min.css',
	),
	array(
		'input'      => w9sme_theme_dir() . 'includes/admin-panel/assets/scss/style-admin-panel.scss',
		'output'     => w9sme_theme_dir() . 'includes/admin-panel/assets/css/style-admin-panel.css',
		'output_min' => w9sme_theme_dir() . 'includes/admin-panel/assets/css/style-admin-panel.min.css',
	),
	array(
		'input'      => w9sme_theme_dir() . 'assets/scss/modules/panel-selector.scss',
		'output'     => w9sme_theme_dir() . 'assets/css/panel-selector.css',
		'output_min' => w9sme_theme_dir() . 'assets/css/panel-selector.min.css',
	),
	array(
		'input'      => w9sme_theme_dir() . 'includes/switcher/assets/scss/style-switcher.scss',
		'output'     => w9sme_theme_dir() . 'includes/switcher/assets/css/style-switcher.css',
		'output_min' => w9sme_theme_dir() . 'includes/switcher/assets/css/style-switcher.min.css',
	),
);

if ( isset( $_GET['compile_scss'] ) ) {
	w9sme_SCSS()->set_variables( w9sme_Variables()->get_variables() );
	w9sme_SCSS()->compile_single_scss_file( $compile_scss_files, true );
}

/*-------------------------------------
    minify single js
---------------------------------------*/

$minify_js_files = array(
	array(
		'input'  => w9sme_theme_dir() . 'assets/js/main.js',
		'output' => w9sme_theme_dir() . 'assets/js/main.min.js',
	),
	array(
		'input'  => w9sme_theme_dir() . 'assets/js/main-admin.js',
		'output' => w9sme_theme_dir() . 'assets/js/main-admin.min.js',
	),
	array(
		'input'  => w9sme_theme_dir() . 'includes/admin-panel/assets/js/main-admin-panel.js',
		'output' => w9sme_theme_dir() . 'includes/admin-panel/assets/js/main-admin-panel.min.js',
	),
	array(
		'input'  => w9sme_theme_dir() . 'includes/switcher/assets/js/main-switcher.js',
		'output' => w9sme_theme_dir() . 'includes/switcher/assets/js/main-switcher.min.js',
	)
);


if ( isset( $_GET['minify_js'] ) ) {
	w9sme_minify_single_js_file( $minify_js_files );
}

function w9sme_minify_single_js_file( $files_config, $debug = false ) {
	if ( !is_array( $files_config ) ) {
		return;
	}

	$time     = microtime();
	$minified = false;
	foreach ( $files_config as $config ) {
		$input = isset( $config['input'] ) ? $config['input'] : '';
		if ( !file_exists( $input ) ) {
			continue;
		}

		$output = isset( $config['output'] ) ? $config['output'] : '';

		if ( !empty( $output ) ) {
			w9sme_jshrink_minify_js( $input, $output );
			$minified = true;
		}
	}
	if ( $debug ) {
		!$minified || var_dump( "MINIFY SPECIFIC JS FILE TIME: " . ( microtime() - $time ) );
	}
}


/*-------------------------------------
	Concat files
---------------------------------------*/
$concat_js_files = array(
	'input'      => array(
		w9sme_theme_dir() . 'assets/vendor/modernizr/modernizr.min.js',
		w9sme_theme_dir() . 'assets/vendor/bootstrap/bootstrap.min.js',
		w9sme_theme_dir() . 'assets/vendor/jquery_easing/jquery.easing.min.js',
		w9sme_theme_dir() . 'assets/vendor/magnific-popup/jquery.magnific-popup.min.js',
		w9sme_theme_dir() . 'assets/vendor/owl-carousel2/owl.carousel.min.js',
		w9sme_theme_dir() . 'assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js',
		w9sme_theme_dir() . 'assets/vendor/smooth-scroll/SmoothScroll.min.js',
		w9sme_theme_dir() . 'assets/vendor/stellar/jquery.stellar.min.js',
		w9sme_theme_dir() . 'assets/vendor/isotope/isotope.pkgd.min.js',
		w9sme_theme_dir() . 'assets/vendor/imagesloaded/imagesloaded.pkgd.min.js',
		w9sme_theme_dir() . 'assets/vendor/retina/retina.min.js',
		w9sme_theme_dir() . 'assets/vendor/headroom/headroom.min.js',
		w9sme_theme_dir() . 'assets/vendor/headroom/jQuery.headroom.js',
	),
	'output'     => w9sme_theme_dir() . 'assets/js/vendor.js',
	'output_min' => w9sme_theme_dir() . 'assets/js/vendor.min.js',
);

$concat_css_files = array(
	'input'      => array(
		w9sme_theme_dir() . 'assets/css/wordpress.required.css',
		w9sme_theme_dir() . 'assets/css/spinkit.css',
		w9sme_theme_dir() . 'assets/vendor/owl-carousel2/assets/owl.carousel.css',
		w9sme_theme_dir() . 'assets/vendor/magnific-popup/magnific-popup.css',
		w9sme_theme_dir() . 'assets/vendor/nprogress/nprogress.css',
		w9sme_theme_dir() . 'assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.css'
	),
	'output'     => w9sme_theme_dir() . 'assets/css/vendor.css',
	'output_min' => w9sme_theme_dir() . 'assets/css/vendor.min.css',
);

if ( isset( $_GET['concat_js_files'] ) ) {
	w9sme_concat_file( $concat_js_files, 'js' );
}

if ( isset( $_GET['concat_css_files'] ) ) {
	w9sme_concat_file( $concat_css_files, 'css' );
}

function w9sme_concat_file( $config, $file_ext = 'js', $debug = false ) {
	if ( empty( $config ) || !is_array( $config ) ) {
		return;
	}
	// input
	$input = isset( $config['input'] ) ? $config['input'] : '';
	if ( empty( $input ) ) {
		return;
	}

	$time    = microtime();
	$content = array();
	foreach ( (array) $input as $item ) {
		if ( file_exists( $item ) ) {
			$content[] = w9sme_get_file_contents( $item );
		}
	}

	$content = implode( '', $content );

	//
	// Output
	//
	$output = isset( $config['output'] ) ? $config['output'] : '';
	if ( !empty( $output ) ) {
		w9sme_put_file_content( $output, $content );
	}

	// Output min
	$output_min = isset( $config['output_min'] ) ? $config['output_min'] : '';
	if ( !empty( $output ) ) {
		switch ( $file_ext ) {
			case 'js':
				require_once( w9sme_theme_dir() . 'includes/library/jshrink/Minifier.php' );

				$content = JShrink\Minifier::minify( $content, array( 'flaggedComments' => false ) );
//                $content     = w9sme_minify_js( $content );
				break;
			case 'css':
				$content = w9sme_minify_css( $content );
				break;
			case 'html':
				$content = w9sme_minify_html( $content );
				break;
		}
		w9sme_put_file_content( $output_min, $content );
	}

	if ( $debug ) {
		var_dump( "CONCAT TIME: " . ( microtime() - $time ) );
	}
}