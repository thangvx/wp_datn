<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: helpers-preset.php
 * @time    : 9/5/16 3:36 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}


/**
 * @return string
 */
function w9sme_get_preset_list() {
    return get_theme_mod( W9SME_PRESET_LIST_KEY, array() );
}

/**
 * Is preset exist
 *
 * @param $preset_name
 *
 * @return bool
 */
function w9sme_is_preset_exist( $preset_name ) {
    return !empty( $preset_name ) && array_key_exists( $preset_name, w9sme_get_preset_list() );
}


/**
 * w9sme_is_valid_preset
 *
 * @param $preset_name
 * @param $preset_title
 *
 * @return bool
 */
function w9sme_is_valid_preset( $preset_name, $preset_title ) {
    if ( !empty( $preset_name ) && !empty( $preset_title ) ) {
        $preset_list = w9sme_get_preset_list();
        
        return !in_array( $preset_name, array_keys( $preset_list ) ) && !in_array( $preset_title, array_values( $preset_list ) );
    }
    
    return false;
}

/**
 * w9sme_create_new_preset
 *
 * @param $preset_name
 * @param $preset_title
 */
function w9sme_create_new_preset( $preset_name, $preset_title ) {
    if ( w9sme_is_valid_preset( $preset_name, $preset_title ) ) {
        set_theme_mod( W9SME_PRESET_LIST_KEY, array_merge( w9sme_get_preset_list(), array(
            $preset_name => $preset_title,
        ) ) );
    }
}

/**
 * w9sme_remove_preset
 *
 * @param $preset_name
 *
 * @return bool
 */
function w9sme_remove_preset( $preset_name ) {
    if ( w9sme_is_preset_exist( $preset_name ) ) {
        $preset_list = w9sme_get_preset_list();
        if ( $preset_name === w9sme_get_global_preset() ) {
            w9sme_set_global_preset( W9SME_THEME_OPTIONS_DEFAULT_NAME );
        }
        
        unset( $preset_list[$preset_name] );
        
        set_theme_mod( W9SME_PRESET_LIST_KEY, $preset_list );
        
        return true;
    }
    
    return false;
}


function w9sme_change_preset_title( $preset_name, $preset_title ) {
    if ( w9sme_is_preset_exist( $preset_name ) ) {
        $preset_list = w9sme_get_preset_list();
        
        $preset_list[$preset_name] = $preset_title;
        
        set_theme_mod( W9SME_PRESET_LIST_KEY, $preset_list );
        
        return true;
    }
    
    return false;
}

/**
 * get global preset
 *
 * @return string
 */
function w9sme_get_global_preset() {
    return get_theme_mod( W9SME_GLOBAL_PRESET_KEY );
}


/**
 * set global preset
 *
 * @param $preset_name
 *
 * @return bool
 */
function w9sme_set_global_preset( $preset_name ) {
    if ( !empty( $preset_name ) && array_key_exists( $preset_name, w9sme_get_preset_list() ) ) {
        set_theme_mod( W9SME_GLOBAL_PRESET_KEY, $preset_name );
        
        return true;
    }
    
    return false;
}


/**
 * get current preset
 * @return string
 */
function w9sme_get_current_preset() {
    global $w9sme_global_preset_name;
    
    if ( empty( $w9sme_global_preset_name ) ) {
        $_main                     = w9sme_get_global_preset();
        $w9sme_global_preset_name = !empty( $_main ) ? $_main : W9SME_THEME_OPTIONS_DEFAULT_NAME;
    }
    
    return $w9sme_global_preset_name;
}

/**
 * Set current preset
 *
 * @param $preset_name
 */
function w9sme_set_current_preset( $preset_name ) {
    global $w9sme_global_preset_name;
    if ( !empty( $preset_name ) && array_key_exists( $preset_name, w9sme_get_preset_list() ) ) {
        $w9sme_global_preset_name = $preset_name;
    }
}

/**
 * get_current_blog_css_folder
 *
 * @param string $current_preset
 *
 * @return string
 */
function w9sme_get_current_blog_css_folder( $current_preset = '' ) {
    $blog_id = get_current_blog_id();
    if ( empty( $blog_id ) ) {
        return w9sme_theme_dir();
    } else {
        if ( empty( $current_preset ) ) {
            $current_preset = w9sme_get_current_preset();
        }
        
        if ( $blog_id == 1 && $current_preset === W9SME_THEME_OPTIONS_DEFAULT_NAME ) {
            return w9sme_theme_dir();
        } else {
            $upload_dir = wp_upload_dir();
            if ( $upload_dir['error'] === false ) {
                $preset_css_folder = trailingslashit( $upload_dir['basedir'] ) . 'w9sme-preset-css/blog-' . $blog_id;
                
                if ( !file_exists( $preset_css_folder ) && !wp_mkdir_p( $preset_css_folder ) ) {
                    return w9sme_theme_dir();
                }
                
                return trailingslashit( $preset_css_folder );
            }
            
            return w9sme_theme_url();
        }
    }
}

/**
 * get_current_blog_css_folder_path
 *
 * @param string $current_preset
 *
 * @return string
 */
function w9sme_get_current_blog_css_folder_path( $current_preset = '' ) {
    $blog_id = get_current_blog_id();
    if ( empty( $blog_id ) ) {
        return w9sme_theme_url();
    } else {
        if ( empty( $current_preset ) ) {
            $current_preset = w9sme_get_current_preset();
        }
        
        if ( $blog_id == 1 && $current_preset === W9SME_THEME_OPTIONS_DEFAULT_NAME ) {
            return w9sme_theme_url();
        } else {
            $upload_dir = wp_upload_dir();
            
            if ( $upload_dir['error'] === false ) {
                $preset_css_folder = trailingslashit( $upload_dir['baseurl'] ) . 'w9sme-preset-css/blog-' . $blog_id;
                
                return trailingslashit( $preset_css_folder );
            } else {
                return w9sme_theme_url();
            }
        }
    }
}


/**
 * w9sme_clean_preset_css_files
 *
 * @param $preset_name
 */
function w9sme_clean_preset_css_files( $preset_name ) {
    if ( $preset_name === W9SME_THEME_OPTIONS_DEFAULT_NAME ) {
        return;
    }
    
    $current_css_folder = w9sme_get_current_blog_css_folder( $preset_name );
    $css_files = w9sme_recursive_scandir( $current_css_folder );
    
    $preset_list = w9sme_get_preset_list();
    
    if ( !empty( $preset_list ) && is_array( $preset_list ) ) {
        foreach ( $preset_list as $preset_name => $preset_title ) {
            foreach ( $css_files as $id => $file ) {
                if ( strpos( $file, $preset_name ) !== false ) {
                    unset( $css_files[$id] );
                }
            }
        }
    }
    
    // unlink the files
    foreach ( $css_files as $id => $file ) {
        @unlink( $file );
    }
}

//w9sme_set_global_preset( '__preset_1' );
//$current = w9sme_get_preset_list();
////////
//set_theme_mod( W9SME_PRESET_LIST_KEY, array_merge( $current, array(
//    '__preset_1'     => 'Preset 1',
//    '__preset_2'     => 'Preset 2',
//    '__main_options' => 'Preset 3',
//) ) );
//remove_theme_mod( W9SME_PRESET_LIST_KEY );

//set_theme_mod( '_options_preset_2', array() );





