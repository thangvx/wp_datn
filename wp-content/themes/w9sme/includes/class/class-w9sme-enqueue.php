<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: class-w9sme-enqueue.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

class W9sme_Enqueue extends W9sme_Base {
	/*
	 * Just the prefix
	 */
	const STYLE_PREFIX = 'w9sme-style-';
	const SCRIPT_PREFIX = 'w9sme-script-';
	
	
	/**
	 * W9sme_Enqueue constructor.
	 */
	public function __construct() {
		parent::__construct();
	}
	
	protected function add_actions() {
		//admin
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_admin_styles' ), 100 );
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_admin_scripts' ), 100 );
		//front-end
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_front_end_styles' ), 100 );
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_front_end_scripts' ), 100 );
		
		// custom redux output
//        add_action( 'wp_head', array( __CLASS__, 'custom_redux_output' ), 300 );
		
		
		//user custom css
		add_action( 'wp_head', array( __CLASS__, 'output_extra_style_css' ), 200 );
		add_action( 'wp_head', array( __CLASS__, 'output_custom_singular_style' ), 300 );
		add_action( 'wp_head', array( __CLASS__, 'output_user_custom_css' ), 999 );
		add_action( 'wp_footer', array( __CLASS__, 'output_user_custom_js' ), 999 );
		
		//custom redux style
		add_action( 'redux-enqueue-' . w9sme_get_current_preset(), array( __CLASS__, 'enqueue_redux_custom' ) );
		add_action( 'redux/metaboxes/' . w9sme_get_current_preset() . '/enqueue', array(
			__CLASS__,
			'enqueue_redux_custom'
		) );
		
		add_action( 'redux-enqueue-' . w9sme_get_current_preset(), array( __CLASS__, 'enqueue_redux_custom_script' ) );
//        add_action( 'redux/page/' . w9sme_get_current_preset() . '/enqueue', array() );
	}
	
	protected function add_filters() {
		add_filter( 'the_content', array( __CLASS__, 'enqueue_contact_form_resources' ), 9999, 1 );
	}
	
	
	static function enqueue_front_end_styles() {
		global $post;
		/*-------------------------------------
			FONT AWESOME
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'font-awesome',
			w9sme_theme_url() . 'assets/vendor/font-awesome-4.6.1/css/font-awesome' . w9sme_resource_suffix() . '.css',
			array(),
			'4.6.1' );
		
		/*-------------------------------------
			9WPTHEME ICONS
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . '9wpthemes-icons',
			w9sme_theme_url() . 'assets/vendor/_9wpthemes-icons/styles' . w9sme_resource_suffix() . '.css',
			array(),
			W9SME_THEME_VERSION );
		
		/*-------------------------------------
			W9SME ICONS
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'w9sme-icons',
			w9sme_theme_url() . 'assets/vendor/w9sme-icons/styles' . w9sme_resource_suffix() . '.css',
			array(),
			W9SME_THEME_VERSION );
		
		
		/*-------------------------------------
			PLUGINS VENDOR INCLUDE INE ONE FILE
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'vendor',
			w9sme_theme_url() . 'assets/css/vendor' . w9sme_resource_suffix() . '.css',
			array(),
			W9SME_THEME_VERSION );
		
		/*-------------------------------------
			SLICK CAROUSEL FOR WOOCOMMERCE MODULE
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'slick-carousel',
			w9sme_theme_url() . 'assets/vendor/slick-carousel/slick' . w9sme_resource_suffix() . '.css',
			array(), W9SME_THEME_VERSION );
		
		
		/*-------------------------------------
			PANEL SELECTOR
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'panel-selector',
			w9sme_theme_url() . 'assets/css/panel-selector' . w9sme_resource_suffix() . '.css',
			array(),
			W9SME_THEME_VERSION );
		
		/*-------------------------------------
			FULLPAGE
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'fullpage',
			w9sme_theme_url() . 'assets/vendor/fullPage_js/jquery.fullpage' . w9sme_resource_suffix() . '.css',
			array(),
			'2.8.2' );
		
		/*-------------------------------------
			DATATABLES
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'datatables',
			w9sme_theme_url() . 'assets/vendor/datatables/datatables.min.css',
			array(),
			'1.10.12' );
		wp_register_style(
			self::STYLE_PREFIX . 'datatables-bootstrap',
			w9sme_theme_url() . 'assets/vendor/datatables/plugins/bootstrap/datatables.bootstrap.css',
			array(),
			'1.10.12' );
		
		/*-------------------------------------
			BOOTSTRAP DATEPICKER
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'datepicker',
			w9sme_theme_url() . 'assets/vendor/bootstrap-datepicker-1.7.1/css/bootstrap-datepicker3.standalone' . w9sme_resource_suffix() . '.css',
			array(),
			'1.7.1' );
		
		/*-------------------------------------
			SELECT2 4.0.3
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'select2',
			w9sme_theme_url() . 'assets/vendor/select2/css/select2' . w9sme_resource_suffix() . '.css',
			array(),
			'4.0.3' );
		
		/*-------------------------------------
			RTL CSS
		---------------------------------------*/
		if ( w9sme_is_rtl() || is_rtl() ) {
			wp_register_style(
				self::STYLE_PREFIX . 'rtl',
				w9sme_theme_url() . 'rtl' . w9sme_resource_suffix() . '.css',
				array(),
				W9SME_THEME_VERSION );
		}
		
		/*-------------------------------------
			THIS THEME STYLESHEET
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'main',
			w9sme_get_current_blog_css_file_path(),
			array(),
			W9SME_THEME_VERSION );
		
		/*-------------------------------------
			THIS THEME STYLESHEET
		---------------------------------------*/
		
		
		// Enqueue all of them
		wp_enqueue_style( self::STYLE_PREFIX . 'font-awesome' );
		wp_enqueue_style( self::STYLE_PREFIX . '9wpthemes-icons' );
		wp_enqueue_style( self::STYLE_PREFIX . 'w9sme-icons' );
		wp_enqueue_style( self::STYLE_PREFIX . 'vendor' );

//		if ( in_array( 'woocommerce', get_body_class() ) || in_array( 'woocommerce-page', get_body_class() ) ) {
//			wp_enqueue_style( self::STYLE_PREFIX . 'slick-carousel' );
//		}
		
		if ( w9sme_get_option( 'panel-selector' ) == 1 ) {
			wp_enqueue_style( self::STYLE_PREFIX . 'panel-selector' );
		}
		
		if ( w9sme_get_meta_option( 'slipscreen', '', '', 0 ) ) {
			wp_enqueue_style( self::STYLE_PREFIX . 'fullpage' );
		}
		
		wp_enqueue_style( self::STYLE_PREFIX . 'main' );
		
		if ( w9sme_is_rtl() || is_rtl() ) {
			wp_enqueue_style( self::STYLE_PREFIX . 'rtl' );
		}
	}
	
	static function enqueue_front_end_scripts() {
		
		wp_register_script(
			self::SCRIPT_PREFIX . 'vendor',
			w9sme_theme_url() . 'assets/js/vendor' . w9sme_resource_suffix() . '.js',
			array( 'jquery' ), W9SME_THEME_VERSION, true );
		
		/*-------------------------------------
			SLICK CAROUSEL FOR WOOCOMMERCE MODULE
		---------------------------------------*/
		wp_register_script(
			self::SCRIPT_PREFIX . 'slick-carousel',
			w9sme_theme_url() . 'assets/vendor/slick-carousel/slick' . w9sme_resource_suffix() . '.js',
			array( 'jquery' ), W9SME_THEME_VERSION, true );
		
		wp_register_script(
			self::SCRIPT_PREFIX . 'smooth-scroll',
			w9sme_theme_url() . 'assets/vendor/smooth-scroll/SmoothScroll' . w9sme_resource_suffix() . '.js',
			array( 'jquery' ), W9SME_THEME_VERSION, true );
		
		wp_register_script(
			self::SCRIPT_PREFIX . 'panel-selector',
			w9sme_theme_url() . 'assets/js/panel-selector.js',
			array( 'jquery' ), W9SME_THEME_VERSION, true );
		
		wp_register_script(
			self::SCRIPT_PREFIX . 'jquery.mouse-wheel',
			w9sme_theme_url() . 'assets/vendor/mouse-wheel/jquery.mousewheel' . w9sme_resource_suffix() . '.js',
			array( 'jquery' ), W9SME_THEME_VERSION, true );
		
		wp_register_script(
			self::SCRIPT_PREFIX . 'jquery.fullpage',
			w9sme_theme_url() . 'assets/vendor/fullPage_js/jquery.fullpage' . w9sme_resource_suffix() . '.js',
			array( 'jquery' ), W9SME_THEME_VERSION, true );
		
		wp_register_script(
			self::SCRIPT_PREFIX . 'jquery.fullpage-helper',
			w9sme_theme_url() . 'assets/vendor/fullPage_js/scrolloverflow' . w9sme_resource_suffix() . '.js',
			array( 'jquery' ), W9SME_THEME_VERSION, true );
		
		wp_register_script(
			self::SCRIPT_PREFIX . 'main',
			w9sme_theme_url() . 'assets/js/main' . w9sme_resource_suffix() . '.js',
			array( 'jquery' ), W9SME_THEME_VERSION, true );

//      enqueue them all
//		wp_  enqueue  _script( 'jquery' );
		
		/*-------------------------------------
			Vendor
		---------------------------------------*/
		wp_enqueue_script( self::SCRIPT_PREFIX . 'vendor' );
		
		/*-------------------------------------
			SLICK
		---------------------------------------*/
//		if ( in_array( 'woocommerce', get_body_class() ) || in_array( 'woocommerce-page', get_body_class() ) ) {
//			wp_enqueue_script( self::SCRIPT_PREFIX . 'slick-carousel' );
//		}
		
		/*-------------------------------------
			SMOOTH SCROLL
		---------------------------------------*/
		if ( w9sme_get_option( 'smooth-scroll' ) == '1' ) {
			wp_enqueue_script( self::SCRIPT_PREFIX . 'smooth-scroll' );
		}
		
		/*-------------------------------------
			DATATABLES
		---------------------------------------*/
		wp_register_script(
			self::SCRIPT_PREFIX . 'datatables',
			w9sme_theme_url() . 'assets/vendor/datatables/datatables.min.js',
			array(),
			'1.10.12' );
		
		wp_register_script(
			self::SCRIPT_PREFIX . 'datatables-bootstrap',
			w9sme_theme_url() . 'assets/vendor/datatables/plugins/bootstrap/datatables.bootstrap.js',
			array(),
			'1.10.12' );
		
		/*-------------------------------------
			BOOTSTRAP DATEPICKER
		---------------------------------------*/
		wp_register_script(
			self::SCRIPT_PREFIX . 'datepicker',
			w9sme_theme_url() . 'assets/vendor/bootstrap-datepicker-1.7.1/js/bootstrap-datepicker' . w9sme_resource_suffix() . '.js',
			array(),
			'1.7.1' );
		
		/*-------------------------------------
			SELECT2 4.0.3
		---------------------------------------*/
		wp_register_script(
			self::SCRIPT_PREFIX . 'select2',
			w9sme_theme_url() . 'assets/vendor/select2/js/select2.full.min.js',
			array(),
			'4.0.3',
			true );
		
		/*-------------------------------------
			COMMENT REPLY
		---------------------------------------*/
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

//		if ( w9sme_get_option( 'panel-selector' ) == 1 ) {
//			wp_enqueue_script( self::SCRIPT_PREFIX . 'panel-selector' );
//		}
		
		if ( w9sme_get_meta_option( 'slipscreen', '', '', 0 ) ) {
			wp_enqueue_script( self::SCRIPT_PREFIX . 'jquery.fullpage-helper' );
			wp_enqueue_script( self::SCRIPT_PREFIX . 'jquery.fullpage' );
		}
		
		wp_enqueue_script( self::SCRIPT_PREFIX . 'main' );
		wp_localize_script( self::SCRIPT_PREFIX . 'main', 'w9sme_main_vars',
			array(
				'ajax_url'                          => admin_url( 'admin-ajax.php' ),
				'w9sme_theme_url'                   => w9sme_theme_url(),
				'w9sme_sticky_show_up'              => w9sme_get_option( 'nav-sticky-show-up', 'top', '' ),
				'w9sme_scroll_set_submenu_position' => w9sme_get_option( 'scroll-set-submenu-position', '', 1 ),
				'w9sme_datepicker_date_format'      => w9sme_get_option( 'service-booking-date-format', '', 'yyyy/M/dd' ),
				'w9sme_service_select_placeholder'  => w9sme_get_option( 'service-booking-service-picker-placeholder', '', '--' ),
				'w9sme_use_service_data_in_booking' => w9sme_get_option( 'service-booking-using-service-data', '', '0' ),
				'w9sme_auto_select_service'         => w9sme_get_option( 'service-booking-auto-select-service', '', '0' ),
				'w9sme_vacancy_select_placeholder'  => w9sme_get_option( 'vacancy-picker-placeholder', '', '--' ),
				'w9sme_use_vacancy_data'            => w9sme_get_option( 'vacancy-using-vacancy-data', '', '0' ),
				'w9sme_auto_select_vacancy'         => w9sme_get_option( 'vacancy-auto-select', '', '0' ),
				'error_message'                     => esc_html__( '<span style="color: red;">There was an unexpected error occurred!</span>', 'w9sme' )
			)
		);
	}
	
	static function enqueue_admin_styles() {
		wp_register_style( self::STYLE_PREFIX . 'select2', w9sme_theme_url() . 'assets/vendor/select2/css/select2' . w9sme_resource_suffix() . '.css', array(), W9SME_THEME_VERSION );
		wp_register_style( self::STYLE_PREFIX . 'style-admin', w9sme_theme_url() . 'assets/css/style-admin' . w9sme_resource_suffix() . '.css', array(), W9SME_THEME_VERSION );
		/*-------------------------------------
		   FONT AWESOME
	   ---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'font-awesome',
			w9sme_theme_url() . 'assets/vendor/font-awesome-4.6.1/css/font-awesome' . w9sme_resource_suffix() . '.css',
			array(), '4.6.1' );
		
		/*-------------------------------------
			9WPTHEME ICONS
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . '9wpthemes-icons',
			w9sme_theme_url() . 'assets/vendor/_9wpthemes-icons/styles' . w9sme_resource_suffix() . '.css',
			array(),
			W9SME_THEME_VERSION );
		/*-------------------------------------*/
		
		/*-------------------------------------
			W9SME ICONS
		---------------------------------------*/
		wp_register_style(
			self::STYLE_PREFIX . 'w9sme-icons',
			w9sme_theme_url() . 'assets/vendor/w9sme-icons/styles' . w9sme_resource_suffix() . '.css',
			array(),
			W9SME_THEME_VERSION );
		/*-------------------------------------*/
		
		wp_enqueue_style( self::STYLE_PREFIX . 'font-awesome' );
		wp_enqueue_style( self::STYLE_PREFIX . '9wpthemes-icons' );
		wp_enqueue_style( self::STYLE_PREFIX . 'w9sme-icons' );
		// check for redux select2-css
		if ( ! wp_style_is( 'select2-css' ) ) {
			wp_enqueue_style( self::STYLE_PREFIX . 'select2' );
		}
		wp_enqueue_style( self::STYLE_PREFIX . 'style-admin' );
	}
	
	/**
	 * Redux Custom
	 */
	static function enqueue_redux_custom() {
		/*-------------------------------------
		  CUSTOM REDUX STYLE
		---------------------------------------*/
		wp_dequeue_style( 'redux-admin-css' );
		wp_register_style(
			self::STYLE_PREFIX . 'redux-admin-css',
			w9sme_theme_url() . 'assets/css/custom-redux-admin' . '.min' . '.css',
			array() );
		wp_enqueue_style( self::STYLE_PREFIX . 'redux-admin-css' );
		
		wp_dequeue_style( 'redux-fields-css' );
		wp_register_style(
			self::STYLE_PREFIX . 'redux-fields-css',
			w9sme_theme_url() . 'assets/css/custom-redux-fields' . '.min' . '.css',
			array() );
		wp_enqueue_style( self::STYLE_PREFIX . 'redux-fields-css' );
		
		wp_dequeue_style( 'jquery-ui-css' );
		wp_register_style(
			self::STYLE_PREFIX . 'jquery-ui-css',
			w9sme_theme_url() . 'assets/css/custom-redux-jquery-ui' . '.min' . '.css',
			array() );
		wp_enqueue_style( self::STYLE_PREFIX . 'jquery-ui-css' );
	}
	
	/**
	 * Enqueue redux custom script
	 */
	static function enqueue_redux_custom_script() {
		wp_register_script( self::SCRIPT_PREFIX . 'redux-extend',
			w9sme_theme_url() . 'assets/js/main-redux-extend.js',
			array(),
			W9SME_THEME_VERSION, true );
		
		
		wp_enqueue_script( self::SCRIPT_PREFIX . 'redux-extend' );
		wp_localize_script( self::SCRIPT_PREFIX . 'redux-extend', 'w9sme_redux_extend_vars',
			array(
				'validation_error_message'      => '<span class="message" style="color: red;">' . esc_html__( 'Invalid preset name, please try an other one!', 'w9sme' ) . '</span>',
				'error_message'                 => '<span class="message" style="color: red;">' . esc_html__( 'There was an unexpected error occurred!', 'w9sme' ) . '</span>',
				'confirm_remove_preset_message' => esc_html__( 'Are you sure to remove this preset?', 'w9sme' )
			)
		);
	}
	
	/**
	 * Enqueue admin script
	 */
	static function enqueue_admin_scripts() {
		wp_register_script( self::SCRIPT_PREFIX . 'select2', w9sme_theme_url() . 'assets/vendor/select2/js/select2.full.min.js', array(), W9SME_THEME_VERSION, true );
		wp_register_script( self::SCRIPT_PREFIX . 'main-admin', w9sme_theme_url() . 'assets/js/main-admin' . w9sme_resource_suffix() . '.js', array(), W9SME_THEME_VERSION, true );
		
		
		if ( function_exists( 'wp_enqueue_media' ) ) {
			wp_enqueue_media();
		} else {
			wp_enqueue_script( 'media-upload' );
		}
		
		//Use for icon picker and ...
		wp_enqueue_script( 'jquery-ui-dialog' );
		// check for redux select2
		if ( ! wp_script_is( 'select2-js' ) ) {
			wp_enqueue_script( self::SCRIPT_PREFIX . 'select2' );
		}
		wp_enqueue_script( self::SCRIPT_PREFIX . 'main-admin' );
		wp_localize_script( self::SCRIPT_PREFIX . 'main-admin', 'w9sme_main_admin_vars',
			array(
				'ajax_url'        => admin_url( 'admin-ajax.php' ),
				'w9sme_theme_url' => w9sme_theme_url(),
			)
		);
	}
	
	static function output_extra_style_css() {
		$css = <<<CSS
		.w9sme-main-nav .w9sme-nav-logo .__sticky-nav-logo {
			display: none
		}

		.is-sticky .w9sme-main-nav .w9sme-nav-logo .__nav-logo {
			display: none
		}

		.is-sticky .w9sme-main-nav .w9sme-nav-logo .__sticky-nav-logo {
			display: block
		}
CSS;
		echo sprintf( '<style id="%extra-css">%s</style>', self::STYLE_PREFIX, w9sme_minify_css( $css ) );
	}
	
	static function output_user_custom_css() {
		$user_custom_css = w9sme_get_option( 'custom-css' );
		if ( $user_custom_css ) {
			try {
				$variables = W9sme_Variables()->get_variables();
				W9sme_SCSS()->set_variables( $variables );
				$formatter = w9sme_resource_suffix() ? 'compressed' : 'expanded';
				W9sme_SCSS()->compiler->setLineNumberStyle( null );
				W9sme_SCSS()->compiler->setFormatter( 'Leafo\ScssPhp\Formatter\\' . ( $formatter ? ucfirst( $formatter ) : 'Nested' ) );
				$css = W9sme_SCSS()->compiler->compile( $user_custom_css );
				
				echo sprintf( '<style id="%suser-custom-css">%s</style>', self::STYLE_PREFIX, w9sme_resource_suffix() ? w9sme_minify_css( $css ) : $css );
			} catch ( Exception $e ) {
				echo sprintf( '<style id="%suser-custom-css">/* %s */ %s</style>', self::STYLE_PREFIX, $e->getMessage(), w9sme_resource_suffix() ? w9sme_minify_css( $user_custom_css ) : $user_custom_css );
			}
		}
	}
	
	static function output_user_custom_js() {
		$user_custom_js = w9sme_get_option( 'custom-js' );
		if ( $user_custom_js ) {
			echo sprintf( '<script id="%suser-custom-js">%s</script>', self::SCRIPT_PREFIX, w9sme_resource_suffix() ? w9sme_minify_js( $user_custom_js ) : $user_custom_js );
		}
	}
	
	static function output_custom_singular_style() {
		$variables = W9sme_Variables()->get_variables();
		
		$custom_style_file = w9sme_theme_dir() . 'assets/scss/custom-style.scss';
		W9sme_SCSS()->set_variables( $variables );
		$formatter = w9sme_resource_suffix() ? 'compressed' : 'expanded';
		$css       = W9sme_SCSS()->compile( $custom_style_file, false, $formatter );
		
		echo sprintf( '<style id="%scustom-style">%s</style>', self::STYLE_PREFIX, $css );
	}
	
	static function enqueue_contact_form_resources( $the_content ) {
		if ( strpos( $the_content, 'w9sme-reservation-form' ) !== false ) {
			wp_enqueue_style( self::STYLE_PREFIX . 'datepicker' );
			wp_enqueue_script( self::SCRIPT_PREFIX . 'datepicker' );
			
			wp_enqueue_style( self::STYLE_PREFIX . 'select2' );
			wp_enqueue_script( self::SCRIPT_PREFIX . 'select2' );
		}
		
		return $the_content;
	}
}