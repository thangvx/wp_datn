<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-content-page.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/*
 * General Section
*/

$sections[] = array(
    'title'  => esc_html__( 'Other Options', 'w9sme' ),
    'desc'   => esc_html__( 'Other options for page.', 'w9sme' ),
    'icon'   => 'el el-cogs',
    'fields' => array(
        array(
            'id'             => 'meta-page-margin',
            'type'           => 'spacing',
            'mode'           => 'margin',
            'units'          => 'px',
            'units_extended' => 'false',
            'title'          => esc_html__( 'Page margin', 'w9sme' ),
            'subtitle'       => esc_html__( 'Set page margin.', 'w9sme' ),
            'desc'           => '',
            'left'           => false,
            'right'          => false,
            'default'        => array(),
            'output'         => array( '.site-main-page' ),
        ),
        
        array(
            'id'       => 'meta-page-comment',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Enable comments?', 'w9sme' ),
            'subtitle' => esc_html__( 'If on, the comment form will be avaliable for this page.', 'w9sme' ),
            'options'  => array(
                '1' => 'On',
                '-1'  => 'Default',
                '0' => 'Off',
            ),
            'default'  => '',
        ),

//        array(
//            'id'       => 'meta-slipscreen',
//            'type'     => 'switch',
//            'title'    => esc_html__( 'Slipscreen', 'w9sme' ),
//            'subtitle' => esc_html__( 'Enable or disable slipscreen mode.', 'w9sme' ),
//            'desc'     => '',
//            'default'  => 0
//        ),

//        array(
//            'id'       => 'meta-page-fullscreen',
//            'type'     => 'switch',
//            'title'    => esc_html__( 'Page full screen', 'w9sme' ),
//            'subtitle' => esc_html__( 'Enable or disable "Page Full Screen" mode. This option does not work with "Slipscreen" mode and "Page Left Title" template.', 'w9sme' ),
//            'desc'     => '',
//            'default'  => 0
//        )
    ),
);