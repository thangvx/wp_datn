<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: metabox-header-content.php
 * @time    : 5/16/2017 7:31 AM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$sections[] = array(
	'title'  => esc_html__( 'Header Content', 'w9sme' ),
	'desc'   => esc_html__( 'Modify the content of the header.', 'w9sme' ),
	'icon'   => 'el el-puzzle',
	'fields' => array(
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Header Content Settings', 'w9sme' ),
		),
		
		array(
			'id'       => 'meta-nav-module-desktop-custom',
			'type'     => 'switch',
			'title'    => esc_html__( 'Custom nav module in desktop?', 'w9sme' ),
			'default'  => 0,
		),
		
		array(
			'id'      => 'meta-nav-module-desktop',
			'type'    => 'sorter',
			'title'   => esc_html__( 'Nav module in desktop', 'w9sme' ),
			'desc'    => esc_html__( 'Select nav module appear in normal desktop nav, you also use this for sorter position of module in nav.', 'w9sme' ),
			'options' => array(
				'enabled'  => array(
					'logo'      => esc_html__( 'Logo', 'w9sme' ),
					'main-menu' => esc_html__( 'Main Menu', 'w9sme' ),
					'search'    => esc_html__( 'Search', 'w9sme' ),
					'cart'      => esc_html__( 'Cart', 'w9sme' ),
				),
				'disabled' => array(
					'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
					'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
					'popup'            => esc_html__( 'Popup', 'w9sme' ),
					'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
				)
			),
			'required' => array( 'meta-nav-module-desktop-custom', '=', array( 1 ) ),
		),
		
		array(
			'id'       => 'meta-nav-module-desktop-sticky-custom',
			'type'     => 'switch',
			'title'    => esc_html__( 'Custom sticky nav module in desktop?', 'w9sme' ),
			'default'  => 0,
		),
		
		array(
			'id'       => 'meta-nav-module-desktop-sticky',
			'type'     => 'sorter',
			'title'    => esc_html__( 'Nav module in desktop sticky nav', 'w9sme' ),
			'subtitle' => esc_html__( 'Select nav module appear in sticky nav in desktop screen, you also use this for sorter position of module in nav.', 'w9sme' ),
			'options'  => array(
				'enabled'  => array(
					'logo'      => esc_html__( 'Logo', 'w9sme' ),
					'main-menu' => esc_html__( 'Main Menu', 'w9sme' ),
					'search'    => esc_html__( 'Search', 'w9sme' ),
					'cart'      => esc_html__( 'Cart', 'w9sme' ),
				),
				'disabled' => array(
					'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
					'popup'            => esc_html__( 'Popup', 'w9sme' ),
					'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
					'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
				)
			),
			'required' => array( 'meta-nav-module-desktop-sticky-custom', '=', array( 1 ) ),
		),
		
		array(
			'id'       => 'meta-nav-module-mobile-custom',
			'type'     => 'switch',
			'title'    => esc_html__( 'Custom nav module in mobile?', 'w9sme' ),
			'default'  => 0,
		),
		
		array(
			'id'      => 'meta-nav-module-mobile',
			'type'    => 'sorter',
			'title'   => esc_html__( 'Nav module in mobile', 'w9sme' ),
			'desc'    => esc_html__( 'Select nav module appear in sticky nav in desktop screen, you also use this for sorter position of module in nav.', 'w9sme' ),
			'options' => array(
				'enabled'  => array(
					'logo'            => esc_html__( 'Logo', 'w9sme' ),
					'toggle-leftzone' => esc_html__( 'Toggle Left Zone', 'w9sme' ),
				),
				'disabled' => array(
					'search'           => esc_html__( 'Search', 'w9sme' ),
					'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
					'cart'             => esc_html__( 'Cart', 'w9sme' ),
					'popup'            => esc_html__( 'Popup', 'w9sme' ),
					'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
				)
			),
			'required' => array( 'meta-nav-module-mobile-custom', '=', array( 1 ) ),
		),
		
		array(
			'id'      => 'meta-nav-module-desktop-biggest',
			'type'    => 'select',
			'title'   => esc_html__( 'Biggest module in desktop screen', 'w9sme' ),
			'desc'     => esc_html__( 'Leave empty mean use this setting from "Theme Options".', 'w9sme' ),
			'options' => array(
				'-1'                        => esc_html__( 'Default', 'w9sme' ),
				'logo'             => esc_html__( 'Logo', 'w9sme' ),
				'main-menu'        => esc_html__( 'Main Menu', 'w9sme' ),
				'cart'             => esc_html__( 'Cart', 'w9sme' ),
				'search'           => esc_html__( 'Search', 'w9sme' ),
				'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
				'popup'            => esc_html__( 'Popup', 'w9sme' ),
				'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
				'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
			),
			'default' => ''
		),
		
		array(
			'id'      => 'meta-nav-module-desktop-biggest-align',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Biggest module in desktop screen align', 'w9sme' ),
			'options' => array(
				'-1'    => esc_html__( 'Default', 'w9sme' ),
				'nav-item-desktop-left'   => esc_html__( 'Left', 'w9sme' ),
				'nav-item-desktop-center' => esc_html__( 'Center', 'w9sme' ),
				'nav-item-desktop-right'  => esc_html__( 'Right', 'w9sme' ),
			),
			'default' => ''
		),
		
		array(
			'id'      => 'meta-nav-module-mobile-biggest',
			'type'    => 'select',
			'title'   => esc_html__( 'Biggest module in mobile screen', 'w9sme' ),
			'desc'     => esc_html__( 'Leave empty mean use this setting from "Theme Options".', 'w9sme' ),
			'options' => array(
				'-1'                        => esc_html__( 'Default', 'w9sme' ),
				'logo'             => esc_html__( 'Logo', 'w9sme' ),
				'cart'             => esc_html__( 'Cart', 'w9sme' ),
				'search'           => esc_html__( 'Search', 'w9sme' ),
				'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
				'popup'            => esc_html__( 'Popup', 'w9sme' ),
				'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
				'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
			),
			'default' => '',
		),
		
		array(
			'id'      => 'meta-nav-module-mobile-biggest-align',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Biggest module in mobile screen align', 'w9sme' ),
			'options' => array(
				'-1'    => esc_html__( 'Default', 'w9sme' ),
				'nav-item-mobile-left'   => esc_html__( 'Left', 'w9sme' ),
				'nav-item-mobile-center' => esc_html__( 'Center', 'w9sme' ),
				'nav-item-mobile-right'  => esc_html__( 'Right', 'w9sme' ),
			),
			'default' => ''
		),
		
		array(
			'id'       => 'meta-nav-custom-content',
			'type'     => 'textarea',
			'title'    => esc_html__( 'Custom content', 'w9sme' ),
			'subtitle' => esc_html__( 'Define your custom Content to nav module "Custom Content".', 'w9sme' ),
			'desc'     => esc_html__( 'Content can be raw HTML, shortcode string or mix of them.', 'w9sme' ),
			'default'  => ''
		)

	
	) // #fields
);