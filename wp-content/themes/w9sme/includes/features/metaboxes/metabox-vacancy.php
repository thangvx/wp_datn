<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-vacancy.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$content_template_list = w9sme_get_content_template_list();

$content_template_list = array_merge( array( '-1' => esc_html__( '__Default__', 'w9sme' ) ), $content_template_list );

$sections[] = array(
	'title'  => esc_html__( 'Vacancy', 'w9sme' ),
	'desc'   => esc_html__( 'Change the vacanvy section configuration.', 'w9sme' ),
	'icon'   => 'dashicons-before dashicons-share-alt',
	'fields' => array(
		array(
			'id'      => 'meta-vacancy-single-content-layout',
			'type'    => 'select',
			'title'   => esc_html__( 'Content Layout', 'w9sme' ),
			'desc'    => esc_html__( 'Select content template which made up layout for single vacancy.', 'w9sme' ),
			'options' => $content_template_list,
			'default' => '-1',
		),
		array(
			'id'          => 'meta-vacancy-date',
			'type'        => 'date',
			'title'       => esc_html__( 'Date', 'w9sme' ),
			'subtitle'    => esc_html__( 'Set end date of the vacancy', 'w9sme' ),
			'placeholder' => "yy-mm-dd"
		),
		array(
			'id'       => 'meta-vacancy-location',
			'type'     => 'text',
			'title'    => esc_html__( 'Location', 'w9sme' ),
			'subtitle' => esc_html__( 'Set vacancy location.', 'w9sme' )
		),
		
		array(
			'id'       => 'meta-vacancy-addition-info',
			'type'     => 'multi_label_text',
			'title'    => esc_html__( 'Addition info', 'w9sme' ),
			'validate' => 'text',
			'subtitle' => esc_html__( 'Addition info for this label, can be print in shortcode vacancy setting.', 'w9sme' ),
		),
	)
);
