<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-product-general.php
 * @time    : 9/17/16 10:29 AM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

if (!w9sme_is_woocommerce_active()) {
	return;
}

$sections[] = array(
    'title'  => esc_html__( 'General Settings', 'w9sme' ),
    'desc'   => esc_html__( 'Change the general layout settings. Leave all as blank if you want to use the default options from Theme Options.', 'w9sme' ),
    'icon'   => 'el-icon-lines',
    'fields' => array(
     
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__('General Layout', 'w9sme' ),
        ),
        array(
            'id'       => 'meta-single-layout',
            'type'     => 'select',
            'title'    => esc_html__( 'Layout', 'w9sme' ),
            'subtitle' => esc_html__( 'Select page layout.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
                'container'       => esc_html__( 'Container', 'w9sme' ),
                'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
                'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
            ),
            'default'  => ''
        ),
	
	    array(
		    'id'       => 'meta-single-widget-title-style',
		    'type'     => 'button_set',
		    'title'    => esc_html__( 'Widget title style', 'w9sme' ),
		    'subtitle' => esc_html__( 'Select widget title style.', 'w9sme' ),
		    'desc'     => '',
		    'options'  => array(
			    '-1'        => esc_html__( 'Default', 'w9sme' ),
			    'style-1' => esc_html__( 'Style 1', 'w9sme' ),
			    'style-2' => esc_html__( 'Style 2', 'w9sme' ),
		    ),
		    'default'  => ''
	    ),
        
        array(
            'id'       => 'meta-single-sidebar',
            'type'     => 'image_select',
            'title'    => esc_html__( 'Sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                '-1'      => array( 'title' => 'Default', 'img' => w9sme_theme_url() . 'assets/images/sidebar-default.jpg' ),
                'none'  => array( 'title' => 'None', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
                'left'  => array( 'title' => 'Left', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
                'right' => array( 'title' => 'Right', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
                'both'  => array( 'title' => 'Both', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
            ),
            'default'  => ''
        ),
        array(
            'id'       => 'meta-single-sidebar-width',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
                '-1'      => esc_html__( 'Default', 'w9sme' ),
                'large' => esc_html__( 'Large (1/3)', 'w9sme' )
            ),
            'default'  => '',
            'required' => array( 'meta-single-sidebar', '=', array( 'left', 'both', 'right' ) ),
        ),
        array(
            'id'       => 'meta-single-sidebar-left',
            'type'     => 'select',
            'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => '',
            'required' => array( 'meta-single-sidebar', '=', array( 'left', 'both' ) ),
        ),
        array(
            'id'       => 'meta-single-sidebar-right',
            'type'     => 'select',
            'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => '',
            'required' => array( 'meta-single-sidebar', '=', array( 'right', 'both' ) ),
        ),
    ),
);