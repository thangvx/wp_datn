<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-portfolio.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$content_template_list = w9sme_get_content_template_list();

$content_template_list = array_merge( array( '-1' => esc_html__( '__Default__', 'w9sme' ) ), $content_template_list );

$sections[] = array(
    'title'  => esc_html__( 'Portfolio', 'w9sme' ),
    'desc'   => esc_html__( 'Change the portfolio section configuration.', 'w9sme' ),
    'icon'   => 'dashicons-before dashicons-awards',
    'fields' => array(
	    array(
		    'id'      => 'meta-portfolio-single-content-layout',
		    'type'    => 'select',
		    'title'   => esc_html__( 'Content Layout', 'w9sme' ),
		    'desc'    => esc_html__( 'Select content template which made up layout for single portfolio.', 'w9sme' ),
		    'options' => $content_template_list,
		    'default' => '-1',
	    ),
        // Header general
//        array(
//            'id'       => 'meta-portfolio-about-title',
//            'type'     => 'text',
//            'title'    => esc_html__( 'Portfolio about title', 'w9sme' ),
//            'subtitle' => esc_html__( 'If on, this layout part will be displayed.', 'w9sme' ),
//            'default'  => esc_html__( 'About the project', 'w9sme' ),
//        ),
//        array(
//            'id' => 'meta-portfolio-about-content',
//            'type' => 'textarea',
//            'title' => esc_html__( 'Portfolio about content', 'w9sme' ),
//            'subtitle' => esc_html__( 'Allow some html tag inside: <a>, <br/>, <strong>, <em>.', 'w9sme' ),
//            'allowed_html' => array(
//                'a' => array(
//                    'href' => array(),
//                    'title' => array()
//                ),
//                'br' => array(),
//                'em' => array(),
//                'strong' => array()
//            )
//        ),
        array(
            'id' =>'meta-portfolio-client-name',
            'type' => 'text',
            'title' => esc_html__( 'Client name', 'w9sme' ),
        ),
        array(
            'id' =>'meta-portfolio-client-url',
            'type' => 'text',
            'title' => esc_html__( 'Client URL', 'w9sme' ),
        ),
//        array(
//            'id' =>'meta-portfolio-project-url',
//            'type' => 'text',
//            'title' => esc_html__( 'Project URL', 'w9sme' ),
//        ),
//        array(
//            'id' =>'meta-portfolio-video',
//            'type' => 'text',
//            'subtitle' => esc_html__( 'Enter link to video (Note: read more about available formats at WordPress <a href="http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F">codex page</a>).', 'w9sme' ),
//            'title' => esc_html__( 'Video URL', 'w9sme' ),
//        ),
        array(
            'id'=>'meta-portfolio-addition-info',
            'type' => 'multi_label_text',
            'title' => esc_html__('Addition info', 'w9sme'),
            'validate' => 'text',
            'subtitle' => esc_html__('Addition info for this label, can be print in shortcode portfolio setting.', 'w9sme'),
        ),
//        array(
//            'id'          => 'meta-portfolio-gallery',
//            'type'        => 'slides',
//            'title'       => esc_html__( 'Gallery slider', 'w9sme' ),
//            'subtitle'    => esc_html__( 'Upload images or add from media library.', 'w9sme' ),
//            'placeholder' => array(
//                'title' => esc_html__( 'Title', 'w9sme' ),
//            ),
//            'show'        => array(
//                'title'       => true,
//                'description' => false,
//                'url'         => false,
//            )
//        ),
	    array(
		    'id'          => 'meta-portfolio-gallery',
		    'type'        => 'gallery',
		    'title'       => esc_html__( 'Gallery slider', 'w9sme' ),
		    'subtitle'    => esc_html__( 'Upload images or add from media library.', 'w9sme' ),
	    ),
    
    ) // #fields
);
