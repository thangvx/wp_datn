<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-test.php
 * @time    : 8/26/16 12:31 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$sections[] = array(
    'title'  => esc_html__( 'Test', 'w9sme' ),
    'desc'   => esc_html__( 'Change the footer section configuration.', 'w9sme' ),
    'icon'   => 'el-icon-home',
    'fields' => array(
        // Header general
        array(
            'id'       => 'meta-test-text',
            'type'     => 'text',
            'title'    => esc_html__( 'Test meta text', 'w9sme' ),
            'default'  => 'Test meta text',
        ),
        
        array(
            'id'       => 'meta-test-href',
            'type'     => 'text',
            'title'    => esc_html__( 'Test meta text linkr', 'w9sme' ),
            'default'  => esc_url( home_url( '/' ) ),
        ),

        array(
            'id'       => 'meta-test-image',
            'type'     => 'media',
            'title'    => esc_html__( 'Test image', 'w9sme' ),
            'default'  => '',
        ),
    
        array(
            'id'          => 'meta-test-gallery',
            'type'        => 'slides',
            'title'       => esc_html__( 'Gallery slider', 'w9sme' ),
            'subtitle'    => esc_html__( 'Upload images or add from media library.', 'w9sme' ),
            'placeholder' => array(
                'title' => esc_html__( 'Title', 'w9sme' ),
            ),
            'show'        => array(
                'title'       => true,
                'description' => false,
                'url'         => false,
            )
        ),
    
        array(
            'id'       => 'meta-test-embed-video-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Test meta embed Video URL', 'w9sme' ),
            'default'  => '',
        ),
    
        array(
            'id'       => 'meta-test-embed-video-html',
            'type'     => 'textarea',
            'title'    => esc_html__( 'Test meta embed Video Html', 'w9sme' ),
            'default'  => '',
        ),
    
        array(
            'id'       => 'meta-test-embed-audio-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Test meta embed Audio Url', 'w9sme' ),
            'default'  => '',
        ),
    
        array(
            'id'       => 'meta-test-embed-audio-html',
            'type'     => 'textarea',
            'title'    => esc_html__( 'Test meta embed Audio HTML', 'w9sme' ),
            'default'  => '',
        ),

    ) // #fields
);