<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-header.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$sections[] = array(
	'title'  => esc_html__( 'Header', 'w9sme' ),
	'desc'   => esc_html__( 'Change the footer section configuration.', 'w9sme' ),
	'icon'   => 'el-icon-home',
	'fields' => array(
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Header Settings', 'w9sme' ),
		),
		
		array(
			'id'       => 'meta-header-enable',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Enable header?', 'w9sme' ),
			'subtitle' => esc_html__( 'Show or hide header.', 'w9sme' ),
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'-1'    => esc_html__( 'Default', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'default'  => '',
		),
		
		array(
			'id'       => 'meta-nav-content',
			'type'     => 'select',
			'title'    => esc_html__( 'Select menu for header', 'w9sme' ),
			'subtitle' => esc_html__( 'Select menu appear on header.', 'w9sme' ),
			'desc'     => esc_html__( 'Leave empty mean use this setting from "Theme Options".', 'w9sme' ),
			// Must provide key => value pairs for select options
			'options'  => w9sme_get_menu_list(),
			'default'  => '',
		),
		
		array(
			'id'      => 'meta-nav-width',
			'type'    => 'select',
			'title'   => esc_html__( 'Header width', 'w9sme' ),
			'desc'    => esc_html__( 'Leave empty mean use this setting from "Theme Option".', 'w9sme' ),
			'output'  => array( '.w9sme-nav-body .w9sme-nav-logo-wrapper' ),
			'options' => array(
				'-1'                        => esc_html__( 'Default', 'w9sme' ),
				'container'               => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'           => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid'         => esc_html__( 'Container Fluid', 'w9sme' ),
				'container-nav-fullwidth' => esc_html__( 'Full Width', 'w9sme' ),
			),
			'default' => '',
		),
		
		array(
			'id'       => 'meta-nav-boxed-enabled',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Enable boxed mode', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable boxed mode', 'w9sme' ),
			'options'  => array(
				'-1'    => esc_html__( 'Default', 'w9sme' ),
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' )
			),
			'default'  => '',
			'required' => array(
				array( 'nav-width', '!=', 'container-fluid' ),
				array( 'nav-width', '!=', 'container-nav-fullwidth' ),
			)
		),
		
		array(
			'id'       => 'meta-nav-occupy-spacing',
			'type'     => 'button_set',
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'-1'    => esc_html__( 'Default', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'title'    => esc_html__( 'Nav occupy space?', 'w9sme' ),
			'subtitle' => esc_html__( 'Nav occupy page space or not.', 'w9sme' ),
			'default'  => '',
		),
		
		array(
			'id'             => 'meta-nav-overlay-offset-top',
			'type'           => 'spacing',
			'output'         => array( '.w9sme-main-header' ),
			'mode'           => 'absolute',
			'top'            => true,
			'left'           => false,
			'right'          => false,
			'bottom'         => false,
			'units'          => array( 'px' ),
			'units_extended' => true,
			'title'          => esc_html__( 'Main nav offset top', 'w9sme' ),
			'subtitle'       => esc_html__( 'Main nav offset top in px unit.', 'w9sme' ),
			'default'        => array(),
			'required'       => array(
				'meta-nav-occupy-spacing',
				'=',
				'off'
			)
		),
		
		array(
			'id'       => 'meta-nav-sticky',
			'type'     => 'button_set',
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'-1'    => esc_html__( 'Default', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'title'    => esc_html__( 'Enable sticky menu', 'w9sme' ),
			'subtitle' => esc_html__( 'Nav will be fixed on top of window when scroll down position bigger than window height.', 'w9sme' ),
			'default'  => '',
		),
		array(
			'id'       => 'meta-nav-headroom',
			'type'     => 'button_set',
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'-1'    => esc_html__( 'Default', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'title'    => esc_html__( 'Enable sticky headroom', 'w9sme' ),
			'subtitle' => esc_html__( 'Hide sticky menu when scroll down, show it when scroll up.', 'w9sme' ),
			'default'  => '',
			'required' => array(
				'meta-nav-sticky',
				'!=',
				'off'
			)
		),
		
		array(
			'id'             => 'meta-nav-item-height',
			'type'           => 'dimensions',
			'compiler'       => true,
			'width'          => false,
			'units'          => 'px',
			'output'         => array( '.w9sme-main-nav .w9sme-main-menu-content>li>a' ),
			'units-extended' => true,
			'title'          => esc_html__( 'Menu item height', 'w9sme' ),
			'subtitle'       => esc_html__( 'Choose the nav items height.', 'w9sme' ),
		),
		
		array(
			'id'             => 'meta-nav-sticky-item-height',
			'type'           => 'dimensions',
			'compiler'       => true,
			'width'          => false,
			'units'          => 'px',
			'output'         => array( '.is-sticky .w9sme-main-nav .w9sme-main-menu-content>li>a' ),
			'units-extended' => true,
			'title'          => esc_html__( 'Sticky menu item height', 'w9sme' ),
			'subtitle'       => esc_html__( 'Choose the nav items height.', 'w9sme' ),
		),
		
		array(
			'id'       => 'meta-nav-logo-select',
			'type'     => 'select',
			'compiler' => false,
			'title'    => esc_html__( 'Select logo', 'w9sme' ),
			'subtitle' => esc_html__( 'Select logo from logo list in "Logo & Favicon" tab of "Theme Options".', 'w9sme' ),
			'options'  => array(
				'-1'              => esc_html__( 'Default', 'w9sme' ),
				'logo'          => esc_html__( 'Basic logo', 'w9sme' ),
				'logo-option-1' => esc_html__( 'Logo option 1', 'w9sme' ),
				'logo-option-2' => esc_html__( 'Logo option 2', 'w9sme' ),
				'logo-option-3' => esc_html__( 'Logo option 3', 'w9sme' ),
			),
			'default'  => '',
		),
		
		array(
			'id'       => 'meta-nav-sticky-logo-select',
			'type'     => 'select',
			'compiler' => false,
			'title'    => esc_html__( 'Select logo for sticky nav', 'w9sme' ),
			'subtitle' => esc_html__( 'Select logo from logo list in "Logo & Favicon" tab.', 'w9sme' ),
			'options'  => array(
				'-1'              => esc_html__( 'Default', 'w9sme' ),
				'logo'          => esc_html__( 'Basic logo', 'w9sme' ),
				'logo-option-1' => esc_html__( 'Logo option 1', 'w9sme' ),
				'logo-option-2' => esc_html__( 'Logo option 2', 'w9sme' ),
				'logo-option-3' => esc_html__( 'Logo option 3', 'w9sme' ),
			),
			'default'  => '',
		),
		
		//Header Style
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Static Style', 'w9sme' ),
		),
		
		array(
			'id'       => 'meta-nav-item-color',
			'output'   => array( 'color' => '.w9sme-nav-body' ),
			'type'     => 'color',
			'title'    => esc_html__( 'Nav item color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav color.', 'w9sme' ),
			'default'  => '',
		),
		
		array(
			'id'       => 'meta-nav-item-link-color',
			'output'   => array( '.w9sme-nav-body .w9sme-main-menu-content>li>a, .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a' ),
			'type'     => 'link_color',
			'title'    => esc_html__( 'Nav item link color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav link color.', 'w9sme' ),
			'desc'     => esc_html__( 'Hover, active color available for link in nav only.', 'w9sme' ),
			'default'  => array()
		),
		
		array(
			'id'       => 'meta-nav-link-hover-background',
			'type'     => 'color_rgba',
			'output'   => array( 'background-color' => '.w9sme-nav-body .w9sme-main-menu-content>li>a:hover, .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a:hover' ),
			'title'    => esc_html__( 'Main nav link hover background', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose link background color when hover.', 'w9sme' ),
			'default'  => '',
			'required' => array(
				'meta-nav-menu-item-hover-effect',
				'=',
				array(
					'w9sme-menu-item-hover-none',
					'w9sme-menu-item-hover-underline',
					'w9sme-menu-item-hover-bracket'
				)
			),
		),
		
		array(
			'id'       => 'meta-nav-menu-item-hover-effect',
			'type'     => 'select',
			'title'    => esc_html__( 'Nav menu item hover effect', 'w9sme' ),
			'subtitle' => esc_html__( 'Select menu item (link in menu only) hover effect.', 'w9sme' ),
			'options'  => array(
				'-1'                                      => esc_html__( 'Default', 'w9sme' ),
				'w9sme-menu-item-hover-none'              => esc_html__( 'None', 'w9sme' ),
				'w9sme-menu-item-hover-underline'         => esc_html__( 'Underline', 'w9sme' ),
				'w9sme-menu-item-hover-bracket'           => esc_html__( 'Brackets', 'w9sme' ),
				'w9sme-menu-item-hover-magic-line-top'    => esc_html__( 'Magic Line Top', 'w9sme' ),
				'w9sme-menu-item-hover-magic-line-bottom' => esc_html__( 'Magic Line Bottom', 'w9sme' ),
			),
			'default'  => '-1',
		),
		
		array(
			'id'      => 'meta-nav-background',
			'type'    => 'background',
			'output'  => array( '.w9sme-nav-body' ),
			'title'   => esc_html__( 'Main nav background', 'w9sme' ),
			'default' => array(),
		),
		
		array(
			'id'      => 'meta-nav-background-overlay',
			'type'    => 'color_rgba',
			'title'   => esc_html__( 'Main nav background overlay', 'w9sme' ),
			'output'  => array( 'background-color' => '.w9sme-nav-body:before' ),
			// See Notes below about these lines.
			//'output'    => array('background-color' => '.site-header'),
			'default' => array(),
		),
		
		array(
			'id'      => 'meta-nav-border',
			'type'    => 'border',
			'title'   => esc_html__( 'Nav border', 'w9sme' ),
			'output'  => array( '.w9sme-nav-body' ),
			'all'     => false,
			'color'   => false,
			'default' => array(),
		),
		
		array(
			'id'       => 'meta-nav-border-color',
			'title'    => esc_html__( 'Nav border color', 'w9sme' ),
			'subtitle' => esc_html__( 'Specify the nav border color.', 'w9sme' ),
			'output'   => array(
				'border-color' => '.w9sme-nav-body'
			),
			'type'     => 'color_rgba',
			'default'  => array(),
		),
		
		//Nav Sticky
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Sticky Style', 'w9sme' ),
		),
		
		array(
			'id'       => 'meta-nav-sticky-item-color',
			'output'   => array( 'color' => '.is-sticky .w9sme-nav-body' ),
			'type'     => 'color',
			'title'    => esc_html__( 'Sticky nav item color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav color.', 'w9sme' ),
			'default'  => '',
		),
		
		array(
			'id'       => 'meta-nav-sticky-item-link-color',
			'type'     => 'link_color',
			'title'    => esc_html__( 'Sticky nav item link color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav link color.', 'w9sme' ),
			'output'   => array( '.is-sticky .w9sme-nav-body .w9sme-main-menu-content>li>a,.is-sticky .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a' ),
			'desc'     => esc_html__( 'Hover, active color available for link in nav only.', 'w9sme' ),
			'default'  => array(),
		),
		
		array(
			'id'       => 'meta-nav-sticky-link-hover-background',
			'type'     => 'color_rgba',
			'output'   => array( 'background-color' => '.is-sticky .w9sme-nav-body .w9sme-main-menu-content>li>a:hover, .is-sticky .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a:hover' ),
			'title'    => esc_html__( 'Sticky nav link hover background', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose link background color when hover.', 'w9sme' ),
			'default'  => '',
//		    'validate' => 'color',
		),
		
		array(
			'id'      => 'meta-nav-sticky-background',
			'type'    => 'background',
			'output'  => array( '.is-sticky .w9sme-nav-body' ),
			'title'   => esc_html__( 'Sticky nav background', 'w9sme' ),
			'default' => array(),
		),
		
		array(
			'id'      => 'meta-nav-sticky-background-overlay',
			'type'    => 'color_rgba',
			'title'   => esc_html__( 'Sticky nav background overlay', 'w9sme' ),
			'output'  => array( 'background-color' => '.is-sticky .w9sme-nav-body:before' ),
			'default' => array(),
		),
		
		array(
			'id'      => 'meta-nav-sticky-border',
			'type'    => 'border',
			'title'   => esc_html__( 'Sticky nav border', 'w9sme' ),
			'output'  => array( '.is-sticky .w9sme-nav-body' ),
			'all'     => false,
			'color'   => false,
			'default' => array()
		),
		
		array(
			'id'       => 'meta-nav-sticky-border-color',
			'title'    => esc_html__( 'Sticky nav border color', 'w9sme' ),
			'subtitle' => esc_html__( 'Specify the sticky nav border color', 'w9sme' ),
			'output'   => array(
				'border-color' => '.is-sticky .w9sme-nav-body'
			),
			'type'     => 'color_rgba',
			'default'  => array(),
		),
	
	) // #fields
);