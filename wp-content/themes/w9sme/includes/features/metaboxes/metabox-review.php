<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-review.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/*
 * Quote content
 * Rating
 * Reviewer name
 * Reviewer job
 * Reviewer avatar
 */

$sections[] = array(
	//'title' => esc_html__(' Settings', 'w9sme'),
	'icon'   => 'el-icon-screen',
	'fields' => array(
		array(
			'id'      => 'meta-review-type',
			'type'    => 'text',
			'title'   => esc_html__( 'Review type', 'w9sme' ),
		),
		
		array(
			'id'       => 'meta-review-content',
			'type'     => 'editor',
			'title'    => esc_html__( 'Review content', 'w9sme' ),
			'subtitle' => esc_html__( 'Enter review content.', 'w9sme' ),
			'args'     => array(
				'teeny'         => true,
				'textarea_rows' => 10,
				'media_buttons' => false,
			)
		),
		array(
			'id'         => 'meta-review-rating',
			'type'       => 'slider',
			'title'      => esc_html__( 'Rating', 'w9sme' ),
			'default'    => 2.5,
			'min'        => 0,
			'max'        => 5,
			'step'       => 0.01,
			'resolution' => 0.01
		),
		
		array(
			'id'       => 'meta-review-reviewer-name',
			'type'     => 'text',
			'title'    => esc_html__( 'Reviewer name', 'w9sme' ),
			'subtitle' => esc_html__( 'Enter reviewer name of the review.', 'w9sme' ),
			'desc'     => '',
			'default'  => '',
		),
		array(
			'id'       => 'meta-review-reviewer-email',
			'type'     => 'text',
			'title'    => esc_html__( 'Reviewer Email', 'w9sme' ),
			'subtitle' => esc_html__( 'Enter reviewer email of the review.', 'w9sme' ),
			'desc'     => '',
			'default'  => '',
		),
		
		array(
			'id'       => 'meta-review-reviewer-job',
			'type'     => 'text',
			'title'    => esc_html__( 'Reviewer job', 'w9sme' ),
			'subtitle' => esc_html__( 'Enter reviewer job of the review.', 'w9sme' ),
			'desc'     => '',
			'default'  => '',
		),
		
		array(
			'id'       => 'meta-review-reviewer-avatar',
			'type'     => 'media',
			'title'    => esc_html__( 'Reviewer avatar', 'w9sme' ),
			'subtitle' => esc_html__( 'Pick an avatar of the reviewer.', 'w9sme' ),
			'desc'     => '',
			'default'  => ''
		),
	)
);