<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-service.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}


$sections[] = array(
	'title'  => esc_html__( 'Service Level Agreement', 'w9sme' ),
	'desc'   => esc_html__( 'Change the service level agreement (SLA) configuration.', 'w9sme' ),
	'icon'   => 'dashicons-before dashicons-share-alt',
	'fields' => array(
		array(
			'id'       => 'meta-service-sla-title',
			'type'     => 'text',
			'title'    => esc_html__( 'Title', 'w9sme' ),
		),
		
		array(
			'id'       => 'meta-service-sla',
			'type'     => 'multi_label_textarea',
			'title'    => esc_html__( 'Sections', 'w9sme' ),
		),
	)
);
