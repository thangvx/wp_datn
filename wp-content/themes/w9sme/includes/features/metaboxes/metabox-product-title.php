<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-product-title.php
 * @time    : 9/17/16 10:29 AM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

if (!w9sme_is_woocommerce_active()) {
	return;
}

/*
 * Title Wrapper Section
*/

$content_template_list = w9sme_get_content_template_list();

$sections[] = array(
    'title'  => esc_html__( 'Page Title', 'w9sme' ),
    'desc'   => esc_html__( 'Change the page title section configuration.', 'w9sme' ),
    'icon'   => 'el el-minus',
    'fields' => array(
        
        array(
            'id'       => 'meta-page-title-custom',
            'type'     => 'text',
            'title'    => esc_html__( 'Custom title', 'w9sme' ),
            'subtitle' => '',
            'desc'     => '',
            'default'  => '',
//            'required' => array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
        ),
    
        array(
            'id'       => 'meta-page-title-subtitle',
            'type'     => 'text',
            'title'    => esc_html__( 'Sub title', 'w9sme' ),
            'subtitle' => '',
            'desc'     => '',
            'default'  => '',
//            'required' => array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
        ),
        
        array(
            'id'       => 'meta-page-title-enable',
            'type'     => 'select',
            'title'    => esc_html__( 'Page title template', 'w9sme' ),
            'subtitle' => esc_html__( 'If on, this layout part will be displayed.', 'w9sme' ),
            'options'  => array(
                '-1'       => esc_html__( 'Default', 'w9sme' ),
                'simple' => esc_html__( 'Simple', 'w9sme' ),
                'custom' => esc_html__( 'Content Template', 'w9sme' ),
                'off'    => esc_html__( 'Off', 'w9sme' )
            ),
            'default'  => '',
        ),

        array(
            'id'       => 'meta-page-title-content-template',
            'type'     => 'select',
            'title'    => esc_html__( 'Page title content template', 'w9sme' ),
            'options'  => $content_template_list,
            'desc'     => esc_html__( 'Select content template for page title.', 'w9sme' ),
            'default'  => '',
            'required' => array( 'meta-page-title-enable', '=', array( 'custom', '' ) )
        ),

        array(
            'id'       => 'meta-page-title-background',
            'type'     => 'media',
            'url'      => true,
            'title'    => esc_html__( 'Title background', 'w9sme' ),
            'subtitle' => esc_html__( 'Upload any media using the WordPress native uploader.', 'w9sme' ),
            'required' => array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
        ),

        array(
            'id'       => 'meta-page-title-parallax-effect',
            'type'     => 'select',
            'title'    => esc_html__( 'Parallax effect', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose parallax effect for the background image.', 'w9sme' ),
            'options'  => array(
                '-1'          => esc_html__( 'Default', 'w9sme' ),
                'no-effect' => esc_html__( 'No effect', 'w9sme' ),
                '0.0'       => '0.0',
                '0.05'      => '0.05',
                '0.1'       => '0.1',
                '0.2'       => '0.2',
                '0.3'       => '0.3',
                '0.4'       => '0.4',
                '0.5'       => '0.5',
                '0.6'       => '0.6',
                '0.7'       => '0.7',
            ),
            'default'  => 'no-effect',
            'required' => array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
        ),


        array(
            'id'       => 'meta-page-title-parallax-position',
            'type'     => 'select',
            'title'    => esc_html__( 'Parallax vertical position', 'w9sme' ),
            'subtitle' => '',
            'desc'     => '',
            'options'  => array(
                'top'    => esc_html__( 'Top', 'w9sme' ),
                'center' => esc_html__( 'Center', 'w9sme' ),
                'bottom' => esc_html__( 'Bottom', 'w9sme' ),
            ),
            'default'  => '',
            'required' => array(
                array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
                array( 'meta-page-title-parallax-effect', 'not_empty_and', 'no-effect' ),
            ),
        ),

        array(
            'id'       => 'meta-title-random-number-1',
            'type'     => 'info',
            'title'    => esc_html__('Page title style configurations', 'w9sme' ),
            'required' => array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
        ),

        array(
            'id'       => 'meta-page-title-text-transform',
            'type'     => 'select',
            'title'    => esc_html__( 'Title text transform', 'w9sme' ),
            'subtitle' => '',
            'desc'     => '',
            'options'  => array(
                'text-capitalize' => esc_html__( 'Capitalize', 'w9sme' ),
                'text-uppercase'  => esc_html__( 'Uppercase', 'w9sme' ),
            ),
            'default'  => '',
            'required' => array(
                array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
            ),
        ),

        array(
            'id'       => 'meta-page-title-style',
            'type'     => 'select',
            'title'    => esc_html__( 'Style', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose style for the title wrapper.', 'w9sme' ),
            'options'  => array(
                '-1'                  => esc_html__( 'Default', 'w9sme' ),
                'bg-gray-lighter'   => esc_html__( 'Light gray background, dark text', 'w9sme' ),
                'bg-gray'           => esc_html__( 'Gray background, dark text', 'w9sme' ),
                'bg-dark-lighter'   => esc_html__( 'Dark gray background,  light text', 'w9sme' ),
                'bg-dark'           => esc_html__( 'Black background, white text', 'w9sme' ),
                'bg-white'          => esc_html__( 'White background, dark text', 'w9sme' ),
                'bg-dark-alpha-30'  => esc_html__( 'Dark 30%', 'w9sme' ),
                'bg-dark-alpha-50'  => esc_html__( 'Dark 50%', 'w9sme' ),
                'bg-dark-alpha-70'  => esc_html__( 'Dark 70%', 'w9sme' ),
                'bg-light-alpha-30' => esc_html__( 'Light 30%', 'w9sme' ),
                'bg-light-alpha-50' => esc_html__( 'Light 50%', 'w9sme' ),
                'bg-light-alpha-70' => esc_html__( 'Light 70%', 'w9sme' ),
                'bg-custom'         => esc_html__( 'Custom', 'w9sme' ),
            ),
            'default'  => '',
            'required' => array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
        ),

        array(
            'id'       => 'meta-page-title-text-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Text color', 'w9sme' ),
            'subtitle' => esc_html__( 'Pick a color for page title.', 'w9sme' ),
            'default'  => '',
            'validate' => 'color',
            'required' => array(
                array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
                array( 'meta-page-title-style', '=', array( 'bg-custom' ) )
            )
        ),

        array(
            'id'       => 'meta-page-title-bg-color',
            'type'     => 'color_rgba',
            'title'    => esc_html__( 'Background color', 'w9sme' ),
            'subtitle' => esc_html__( 'Pick a background color for page title.', 'w9sme' ),
            'default'  => array(),
            'validate' => 'colorrgba',
            'required' => array(
                array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
                array( 'meta-page-title-style', '=', array( 'bg-custom' ) )
            )
        ),
        
        array(
            'id'       => 'meta-title-random-number-2',
            'type'     => 'info',
            'title'    => 'Breadcrumb configurations',
            'required' => array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
        ),

        array(
            'id'       => 'meta-page-title-breadcrumbs',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Breadcrumbs', 'w9sme' ),
            'subtitle' => esc_html__( 'Enable/disable breadcrumbs in pages title.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                '1' => 'On',
                '-1'  => 'Default',
                '0' => 'Off',
            ),
            'default'  => '',
            'required' => array( 'meta-page-title-enable', '=', array( 'simple', '' ) ),
        ),
    ), // #fields
);