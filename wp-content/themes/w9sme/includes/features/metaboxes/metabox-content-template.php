<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-content-template.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$content_template_list = w9sme_get_content_template_list();

$content_template_list = array_merge( array( '__none__' => esc_html__( '__None__', 'w9sme' ), '-1' => esc_html__( '__Default__', 'w9sme' ) ), $content_template_list );

$sections[] = array(
    'title'  => esc_html__( 'Content Template', 'w9sme' ),
    'desc'   => sprintf( esc_html__( 'Select content template for some specific position in page or %s here.', 'w9sme' ), '<a href="' . admin_url( '/post-new.php?post_type=content-template' ) . '" target="_blank">' . esc_html__( 'create new', 'w9sme' ) . '</a>' ),
    'icon'   => 'dashicons-before dashicons-format-aside',
    'fields' => array(

        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__( 'Header Area', 'w9sme' ),
        ),
        // Before header
        array(
            'id'      => 'meta-content-template-before-header',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for before header area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for before header area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),

        // After header
        array(
            'id'      => 'meta-content-template-after-header',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for after header area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for after header area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),

        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__( 'Page Title Area', 'w9sme' ),
        ),

        // After page title
        array(
            'id'      => 'meta-content-template-after-page-title',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for after page title area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for after page title area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),

        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__( 'Footer Area', 'w9sme' ),
        ),

        // Before footer
        array(
            'id'      => 'meta-content-template-before-footer',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for before footer area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for before footer area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),


        // After footer
        array(
            'id'      => 'meta-content-template-after-footer',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for after footer area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for after footer area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),

    ), // #fields
);