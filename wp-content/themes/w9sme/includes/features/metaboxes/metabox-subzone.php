<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-subzone.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/*
 * General Section
*/

$sections[] = array(
    'title'  => esc_html__( 'Sub Zone Settings', 'w9sme' ),
    'desc'   => esc_html__( 'Configure left, right zone of page settings.', 'w9sme' ),
    'icon'   => 'w9 w9-ico-arrows-expand-horizontal1',
    'fields' => array(
        array(
            'id'   => mt_rand(),
            'type' => 'info',
            'desc' => esc_html__( 'Left Zone Settings', 'w9sme' )
        ),
        
        array(
            'id'       => 'meta-page-leftzone-sidebar',
            'type'     => 'select',
            'title'    => esc_html__( 'Left zone content', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the left zone content from sidebar list.', 'w9sme' ),
            'desc'     => esc_html__( 'Leave empty mean use this setting from "Theme Options".', 'w9sme' ),
            'data'     => 'sidebars',
            'default'  => '',
        ),
        
        array(
            'id'       => 'meta-page-leftzone-position',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Left zone position', 'w9sme' ),
            'subtitle' => esc_html__( 'Left zone position with page.', 'w9sme' ),
            'options'  => array(
                '-1'       => esc_html__( 'Default', 'w9sme' ),
                'static' => esc_html__( 'Static', 'w9sme' ),
                'fixed'  => esc_html__( 'Fixed', 'w9sme' ),
            ),
            'default'  => ''
        ),
        
        array(
            'id'       => 'meta-page-leftzone-default-open',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Left zone open in big screen', 'w9sme' ),
            'subtitle' => esc_html__( 'Left size zone open if window width bigger than "Left Zone Breakpoint".', 'w9sme' ),
            'options'  => array(
                '-1'      => esc_html__( 'Default', 'w9sme' ),
                'open'  => esc_html__( 'Open', 'w9sme' ),
                'close' => esc_html__( 'Close', 'w9sme' ),
            ),
            'default'  => ''
        ),
        
        array(
            'id'       => 'meta-page-leftzone-dismiss-position',
            'type'     => 'button_set',
            'title'    => 'Left zone dismiss position',
            'options'  => array(
                '-1'              => esc_html__( 'Default', 'w9sme' ),
                'dismiss-left'  => esc_html__( 'Left', 'w9sme' ),
                'dismiss-right' => esc_html__( 'Right', 'w9sme' ),
            ),
            'subtitle' => esc_html__( 'Choose your favorite dismiss position.', 'w9sme' ),
            'default'  => '',
        ),
        
        
        array(
            'id'   => mt_rand(),
            'type' => 'info',
            'desc' => esc_html__( 'Right Zone Settings', 'w9sme' )
        ),
        
        array(
            'id'       => 'meta-page-rightzone-sidebar',
            'type'     => 'select',
            'title'    => esc_html__( 'Right zone content', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the Right zone content from sidebar list.', 'w9sme' ),
            'desc'     => esc_html__( 'Leave empty mean use this setting from "Theme Options".', 'w9sme' ),
            'data'     => 'sidebars',
            'default'  => '',
        ),
        
        array(
            'id'       => 'meta-page-rightzone-position',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Right zone position', 'w9sme' ),
            'subtitle' => esc_html__( 'Right zone position with page.', 'w9sme' ),
            'options'  => array(
                '-1'       => esc_html__( 'Default', 'w9sme' ),
                'static' => esc_html__( 'Static', 'w9sme' ),
                'fixed'  => esc_html__( 'Fixed', 'w9sme' ),
            ),
            'default'  => ''
        ),

        array(
            'id'       => 'meta-page-rightzone-default-open',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Right zone open in big screen', 'w9sme' ),
            'subtitle' => esc_html__( 'Right size zone open if window width bigger than "Right Zone Breakpoint".', 'w9sme' ),
            'options'  => array(
                '-1'      => esc_html__( 'Default', 'w9sme' ),
                'open'  => esc_html__( 'Open', 'w9sme' ),
                'close' => esc_html__( 'Close', 'w9sme' ),
            ),
            'default'  => ''
        ),
        
        array(
            'id'       => 'meta-page-rightzone-dismiss-position',
            'type'     => 'button_set',
            'title'    => esc_html__('Right zone dismiss position', 'w9sme' ),
            'options'  => array(
                '-1'              => esc_html__( 'Default', 'w9sme' ),
                'dismiss-left'  => esc_html__( 'Left', 'w9sme' ),
                'dismiss-right' => esc_html__( 'Right', 'w9sme' ),
            ),
            'subtitle' => esc_html__( 'Choose your favorite dismiss position.', 'w9sme' ),
            'default'  => '',
        ),
    ),
);