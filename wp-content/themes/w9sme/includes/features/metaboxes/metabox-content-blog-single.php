<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-content-blog-single.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$sections[] = array(
    'title'  => esc_html__( 'Other Options', 'w9sme' ),
    'desc'   => esc_html__( 'Other options for blog single.', 'w9sme' ),
    'icon'   => 'el el-cogs',
    'fields' => array(
        array(
            'id'       => 'meta-blog-single-resize-image',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Resize post featured image?', 'w9sme' ),
            'subtitle' => esc_html__( 'Resize post featured image for this single post or not?', 'w9sme' ),
            'options'  => array(
                '1' => 'On',
                '-1'  => 'Default',
                '0' => 'Off',
            ),
            'default'  => '',
        ),
    ),
);