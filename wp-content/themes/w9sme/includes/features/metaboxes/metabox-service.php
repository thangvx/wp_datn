<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-service.php
 * @time    : 8/26/16 12:21 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$content_template_list = w9sme_get_content_template_list();

$content_template_list = array_merge( array( '-1' => esc_html__( '__Default__', 'w9sme' ) ), $content_template_list );


$sections[] = array(
	'title'  => esc_html__( 'Service', 'w9sme' ),
	'desc'   => esc_html__( 'Change the service section configuration.', 'w9sme' ),
	'icon'   => 'dashicons-before dashicons-share-alt',
	'fields' => array(
		array(
			'id'      => 'meta-service-single-content-layout',
			'type'    => 'select',
			'title'   => esc_html__( 'Content Layout', 'w9sme' ),
			'desc'    => esc_html__( 'Select content template which made up layout for single service.', 'w9sme' ),
			'options' => $content_template_list,
			'default' => '-1',
		),
		
		array(
			'id'       => 'meta-service-price',
			'type'     => 'text',
			'title'    => esc_html__( 'Price', 'w9sme' ),
			'subtitle' => esc_html__( 'Set service price attribute (must contain a currency symbol).', 'w9sme' )
		),
		
		array(
			'id'       => 'meta-service-time',
			'type'     => 'text',
			'title'    => esc_html__( 'Time/Amount', 'w9sme' ),
			'subtitle' => esc_html__( 'Enter service time/amount attribute.', 'w9sme' )
		),
		
		array(
			'id'       => 'meta-service-addition-info',
			'type'     => 'multi_label_text',
			'title'    => esc_html__( 'Addition info', 'w9sme' ),
			'validate' => 'text',
			'subtitle' => esc_html__( 'Addition info for this label, can be print in shortcode portfolio setting.', 'w9sme' ),
		),
		
		array(
			'id'       => 'meta-service-gallery',
			'type'     => 'gallery',
			'title'    => esc_html__( 'Gallery slider', 'w9sme' ),
			'subtitle' => esc_html__( 'Upload images or add from media library.', 'w9sme' ),
		),
	)
);
