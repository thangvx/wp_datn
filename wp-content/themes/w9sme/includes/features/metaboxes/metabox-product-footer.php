<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: metabox-product-footer.php
 * @time    : 9/28/16 4:27 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

if (!w9sme_is_woocommerce_active()) {
	return;
}

/*
 * Footer Section
*/
//$template = get_template
$content_template_list = w9sme_get_content_template_list();

$sections[] = array(
    'title'  => esc_html__( 'Footer', 'w9sme' ),
    'desc'   => esc_html__( 'Change the footer section configuration.', 'w9sme' ),
    'icon'   => 'fa fa-ellipsis-h',
    'fields' => array(
        array(
            'id'       => 'meta-footer-enable',
            'type'     => 'select',
            'title'    => esc_html__( 'Footer template', 'w9sme' ),
            'subtitle' => esc_html__( 'If on, this layout part will be displayed.', 'w9sme' ),
            'options'  => array(
                '-1'       => esc_html__( 'Default', 'w9sme' ),
                'simple' => esc_html__( 'Simple', 'w9sme' ),
                'custom' => esc_html__( 'Content Template', 'w9sme' ),
                'off'    => esc_html__( 'Off', 'w9sme' )
            ),
            'default'  => '',
        ),

        array(
            'id'       => 'meta-footer-content-template',
            'type'     => 'select',
            'title'    => esc_html__( 'Footer content template', 'w9sme' ),
            'options'  => $content_template_list,
            'desc'     => esc_html__( 'Select content template for page title.', 'w9sme' ),
            'default'  => '',
            'required' => array( 'meta-footer-enable', '=', array( 'custom', '' ) )
        ),
        
        array(
            'id'       => 'meta-footer-copyright-text',
            'type'     => 'textarea',
            'title'    => esc_html__( 'Copyright text', 'w9sme' ),
            'subtitle' => esc_html__( 'Enter the copyright text for the footer.', 'w9sme' ),
            'required' => array( 'meta-footer-enable', '=', array( 'simple', '' ) ),
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'title'    => esc_html__( 'Footer colors', 'w9sme' ),
            'required' => array( 'meta-footer-enable', '=', array( 'simple', '' ) ),
        ),
        array(
            'id'       => 'meta-footer-colors',
            'type'     => 'select',
            'title'    => esc_html__( 'Footer colors', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose footer color style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'gray'   => esc_html__( 'Gray', 'w9sme' ),
                'light'  => esc_html__( 'Light', 'w9sme' ),
                'dark'   => esc_html__( 'Dark', 'w9sme' ),
                'custom' => esc_html__( 'Custom', 'w9sme' ),
            ),
            'default'  => '',
            'compiler' => true,
            'required' => array( 'meta-footer-enable', '=', array( 'simple', '' ) ),
        ),
        
        array(
            'id'       => 'meta-footer-background-color',
            'type'     => 'color_rgba',
            'title'    => esc_html__( 'Footer Background Color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'meta-footer-colors', '=', 'custom'
            ),
            'default'  => array(
                'color' => '#222',
                'alpha' => '1',
                'rgba'  => 'rgba(0, 0, 0, 1)'
            ),
            'compiler' => true
        ),
        
        array(
            'id'       => 'meta-footer-text-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer text color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'meta-footer-colors', '=', 'custom'
            ),
            'default'  => '#868686',
            'compiler' => true
        ),
        
        array(
            'id'       => 'meta-footer-heading-text-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer heading text color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'meta-footer-colors', '=', 'custom'
            ),
            'default'  => '#fff',
            'compiler' => true
        ),
        
        array(
            'id'       => 'meta-footer-link-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer link color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'meta-footer-colors', '=', 'custom'
            ),
            'default'  => '#868686',
            'compiler' => true
        ),
        
        array(
            'id'       => 'meta-footer-link-hover-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer link hover color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'meta-footer-colors', '=', 'custom'
            ),
            'default'  => '#fff',
            'compiler' => true
        )
    
    ) // #fields
);