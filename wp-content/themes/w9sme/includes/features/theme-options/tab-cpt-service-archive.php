<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-cpt-service-archive.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if (!class_exists('W9sme_CPT_Service') || w9sme_get_option('cpt-service-enable') == '0') {
	return;
}

/*-------------------------------------
	service CPT
---------------------------------------*/
$this->sections[] = array(
	'title'      => esc_html__( 'Service Archive', 'w9sme' ),
	'desc'       => esc_html__( 'Service Archive settings section.', 'w9sme' ),
	'icon'       => 'dashicons-before dashicons-share-alt',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'    => mt_rand(),
			'type'  => 'info',
			'title' => esc_html__( 'Template settings', 'w9sme' ),
		),
		array(
			'id'    => 'service-archive-template',
			'title' => esc_html__( 'Override default archive template', 'w9sme' ),
			'type'  => 'select',
			'data'  => 'pages',
		),

		array(
			'id'       => 'service-archive-excerpt-length',
			'type'     => 'text',
			'title'    => esc_html__( 'Excerpt length', 'w9sme' ),
			'subtitle' => esc_html__( 'Limit the excerpt length (words). Default value is 50 words.', 'w9sme' ),
			'validate' => 'numeric',
			'default'  => ''
		),
		
		array(
			'id'    => 'service-booking-page',
			'title' => esc_html__( 'Booking page', 'w9sme' ),
			'subtitle' => esc_html__( 'Select page for booking/reservation', 'w9sme' ),
			'type'  => 'select',
			'data'  => 'pages',
		),
		
		array(
			'id'       => 'service-show-booking-btn',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show booking button?', 'w9sme' ),
			'desc'     => '',
			'default'  => 1
		),
		
		array(
			'id'       => 'service-booking-btn-tx',
			'type'     => 'text',
			'title'    => esc_html__( 'Booking Button Text', 'w9sme' ),
			'subtitle' => esc_html__( 'Set the text for booking button.', 'w9sme' ),
			'default'  => 'BOOK NOW',
			'required' => array( 'service-show-booking-btn', '=', array( '1' ) ),
		),
		
		array(
			'id'       => 'service-archive-show-time',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show time?', 'w9sme' ),
			'subtitle'     => esc_html__( 'Show service time', 'w9sme' ),
			'default'  => 0
		),
		
		array(
			'id'       => 'service-archive-show-price',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show price?', 'w9sme' ),
			'subtitle'     => esc_html__( 'Show service price', 'w9sme' ),
			'default'  => 0
		),
		
		array(
			'id'       => 'service-archive-display-type',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Display type', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive display type.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'grid'    => esc_html__( 'Grid', 'w9sme' ),
				'masonry' => esc_html__( 'Masonry', 'w9sme' ),
			),
			'default'  => 'masonry'
		),
		
		
		array(
			'id'       => 'service-archive-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive listing style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'classic'    => esc_html__( 'Classic', 'w9sme' ),
				'horizontal' => esc_html__( 'Horizontal', 'w9sme' ),
			),
			'default'  => 'default'
		),
		
		array(
			'id'       => 'service-archive-display-columns',
			'type'     => 'select',
			'title'    => esc_html__( 'Display columns', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the number of columns to display on archive pages.', 'w9sme' ),
			'options'  => array(
				'1' => '1',
				'2' => '2',
				'3' => '3',
				'4' => '4',
				'6' => '6',
			),
			'desc'     => esc_html__( 'Note: 6 columns available when screen bigger than 1600px, and should use layout container-xlg or larger.', 'w9sme' ),
			'default'  => '3',
//			'required' => array( 'service-archive-display-type', '=', array( 'masonry', 'grid' ) ),
		),
		
		array(
			'id'       => 'service-archive-gutter',
			'type'     => 'select',
			'title'    => esc_html__( 'Service columns spacing', 'w9sme' ),
			'subtitle' => esc_html__( 'Spacing between service columns.', 'w9sme' ),
			'options'  => array(
				'0'  => '0px',
				'1'  => '1px',
				'2'  => '2px',
				'3'  => '3px',
				'4'  => '4px',
				'5'  => '5px',
				'10' => '10px',
				'15' => '15px',
				'20' => '20px',
				'25' => '25px',
				'30' => '30px',
				'35' => '35px',
				'40' => '40px',
				'45' => '45px',
				'50' => '50px',
				'55' => '55px',
				'60' => '60px',
			),
			'default'  => '30'
		),
		
		array(
			'id'       => 'service-archive-filter-enable',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable filter', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable page filter.', 'w9sme' ),
			'default'  => true,
			'required' => array( 'service-archive-display-type', '=', array( 'masonry', 'grid' ) ),
		),
		
		array(
			'id'       => 'service-archive-filter-align',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Filter align', 'w9sme' ),
			'options'  => array(
				'text-left'   => esc_html__( 'Left', 'w9sme' ),
				'text-center' => esc_html__( 'Center', 'w9sme' ),
				'text-right'  => esc_html__( 'Right', 'w9sme' ),
			),
			'default'  => 'text-center',
			'required' => array( 'service-archive-filter-enable', '=', array( true ) ),
		),
		
		array(
			'id'       => 'service-archive-items-per-page',
			'type'     => 'text',
			'title'    => esc_html__( 'Items per page', 'w9sme' ),
			'subtitle' => esc_html__( 'Default value is 8.', 'w9sme' ),
			'default'  => '8',
			'validate' => 'numeric'
		),
		
		array(
			'id'       => 'service-archive-paging-type',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Paging type', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive paging type.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'default'         => esc_html__( 'Default', 'w9sme' ),
				'load-more'       => esc_html__( 'Load More', 'w9sme' ),
				'infinite-scroll' => esc_html__( 'Infinite Scroll', 'w9sme' )
			),
			'default'  => 'default'
		),
		
		array(
			'id'             => 'service-archive-margin',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
			'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
			'left'           => false,
			'right'          => false,
			'default'        => array(
				'margin-top'    => '0',
				'margin-bottom' => '55px',
				'units'         => 'px',
			),
			'output'         => array( '.site-main-archive.service-archive' )
		),
	
		//Image ratio
		array(
			'id'      => 'service-archive-item-image-ratio',
			'type'    => 'select',
			'title'   => esc_html__( 'Image Ratio', 'w9sme' ),
			'options' => array(
				'auto'          => esc_html__( 'Auto', 'w9sme' ),
				'0.5625'        => '16x9',
				'1.77777777778' => '9x16',
				'0.42857142857' => '21x9',
				'2.33333333333' => '9x21',
				'0.75'          => '4x3',
				'1.33333333333' => '3x4',
				'0.666666667'   => '3x2',
				'1.5'           => '2x3',
				'1'             => '1x1',
				'2'             => '1x2',
				'0.5'           => '2x1',
			),
			'default' => '0.666666667'
		),
		
		array(
			'id'    => mt_rand(),
			'type'  => 'info',
			'title' => esc_html__( 'Layout Settings', 'w9sme' ),
		),
		
		array(
			'id'       => 'service-archive-layout',
			'type'     => 'select',
			'title'    => esc_html__( 'Layout', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive layout.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
				'container'       => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
			),
			'default'  => 'container',
			'validate' => 'not_empty'
		),
		
		array(
			'id'       => 'service-archive-widget-title-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Widget title style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				''        => esc_html__( 'Default', 'w9sme' ),
				'style-1' => esc_html__( 'Style 1', 'w9sme' ),
				'style-2' => esc_html__( 'Style 2', 'w9sme' ),
			),
			'default'  => ''
		),
		
		array(
			'id'       => 'service-archive-sidebar',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
				'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
				'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
				'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
			),
			'default'  => 'none'
		),
		
		array(
			'id'       => 'service-archive-sidebar-width',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
				'large' => esc_html__( 'Large (1/3)', 'w9sme' )
			),
			'default'  => 'small',
			'required' => array( 'service-archive-sidebar', '=', array( 'left', 'both', 'right' ) ),
		),
		
		array(
			'id'       => 'service-archive-sidebar-left',
			'type'     => 'select',
			'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-1',
			'required' => array( 'service-archive-sidebar', '=', array( 'left', 'both' ) ),
		),
		
		array(
			'id'       => 'service-archive-sidebar-right',
			'type'     => 'select',
			'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-2',
			'required' => array( 'service-archive-sidebar', '=', array( 'right', 'both' ) ),
		),
	),
);