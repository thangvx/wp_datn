<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-colors.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}
$most_used_field = array();

if ( w9sme_get_current_preset() == W9SME_THEME_OPTIONS_DEFAULT_NAME ) {
    $preset_list     = w9sme_get_preset_list();
    $most_used_field = array(
        array(
            'id'    => mt_rand(),
            'type'  => 'info',
            'title' => esc_html__( 'Define Most Used Colors', 'w9sme' )
        ),
        array(
            'id'         => 'most-used-color',
            'type'       => 'multi_color',
            'title'      => esc_html__( 'Most used color', 'w9sme' ),
            'subtitle'   => '<strong>' . sprintf( esc_html__( 'Notice 1: This option is only displayed and managed by this preset: %s', 'w9sme' ), $preset_list[W9SME_THEME_OPTIONS_DEFAULT_NAME] ) . '</strong><br/><br/><strong>' . esc_html__( 'Notice 2: After doing add some colors in this option, it only works for the this preset, and other presets haven\'t updated it. Please switch back to other preset and hit the button Save & Generate CSS to make other presets update the new colors.', 'w9sme' ) . '</strong>',
            'desc'       => esc_html__( 'Define your most used colors, which will be listed in the color dropdown in page builder. Notice: Color name should be unique, and different with other name. If some of them have the same name, the valid name and color is the latest one.', 'w9sme' ),
            'show_empty' => false
        ),
//        array(
//            'id'       => mt_rand(),
//            'title'    => esc_html__( 'Regenerate all preset css files', 'w9sme' ),
//            'type'     => 'raw',
//            'markdown' => true,
//            'content'  => w9sme_get_file_contents( w9sme_theme_dir() . 'includes/library/w9sme-options-templates/templates/generate-presets-css.html' )
//        ),
    );
}


$this->sections[] = array(
    'title'  => esc_html__( 'Colors', 'w9sme' ),
    'desc'   => esc_html__( 'Colors settings.', 'w9sme' ),
    'icon'   => 'el el-magic',
    'fields' => array_merge( array(
        array(
            'id'       => 'primary-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Primary color', 'w9sme' ),
            'subtitle' => esc_html__( 'Set primary color.', 'w9sme' ),
            'default'  => '#3b549f',
            'validate' => 'color',
            'compiler' => true
        ),

        array(
            'id'       => 'secondary-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Secondary color', 'w9sme' ),
            'subtitle' => esc_html__( 'Set secondary color.', 'w9sme' ),
            'default'  => '#f8343e',
            'validate' => 'color',
            'compiler' => true
        ),
        array(
            'id'       => 'text-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Text color', 'w9sme' ),
            'subtitle' => esc_html__( 'Set Text Color.', 'w9sme' ),
            'default'  => '#444',
            'validate' => 'color',
            'compiler' => true
        ),

        array(
            'id'       => 'meta-text-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Meta text color', 'w9sme' ),
            'subtitle' => esc_html__( 'Set meta text color.', 'w9sme' ),
            'default'  => '#888888',
            'validate' => 'color',
            'compiler' => true
        ),

        array(
            'id'       => 'border-color',
            'type'     => 'color_rgba',
            'title'    => esc_html__( 'Border color', 'w9sme' ),
            'subtitle' => esc_html__( 'Set border color.', 'w9sme' ),
            'default'  => array(
                'color' => '#808080',
                'alpha' => '0.2',
                'rgba'  => 'rgba(128, 128, 128, 0.2)'
            ),
            'validate' => 'colorrgba',
            'compiler' => true
        ),
    ), $most_used_field )
);