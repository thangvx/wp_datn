<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-general-search.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$this->sections[] = array(
    'title'      => esc_html__( 'Search', 'w9sme' ),
    'desc'       => esc_html__( 'Search page settings.', 'w9sme' ),
    'icon'       => 'el el-search',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => 'search-page-paging-type',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Paging type', 'w9sme' ),
            'subtitle' => esc_html__( 'Select archive paging style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'default'         => esc_html__( 'Default', 'w9sme' ),
                'load-more'       => esc_html__( 'Load More', 'w9sme' ),
                'infinite-scroll' => esc_html__( 'Infinite Scroll', 'w9sme' )
            ),
            'default'  => 'default'
        ),
        array(
            'id'    => mt_rand(),
            'type'  => 'info',
            'title' => 'Layout configurations'
        ),
        array(
            'id'       => 'search-page-layout',
            'type'     => 'select',
            'title'    => esc_html__( 'Layout', 'w9sme' ),
            'subtitle' => esc_html__( 'Select page layout.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
                'container'       => esc_html__( 'Container', 'w9sme' ),
                'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
                'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
            ),
            'default'  => 'container'
        ),
	
	    array(
		    'id'       => 'search-page-widget-title-style',
		    'type'     => 'button_set',
		    'title'    => esc_html__( 'Widget title style', 'w9sme' ),
		    'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
		    'desc'     => '',
		    'options'  => array(
			    ''        => esc_html__( 'Default', 'w9sme' ),
			    'style-1' => esc_html__( 'Style 1', 'w9sme' ),
			    'style-2' => esc_html__( 'Style 2', 'w9sme' ),
		    ),
		    'default'  => ''
	    ),
        
        array(
            'id'       => 'search-page-sidebar',
            'type'     => 'image_select',
            'title'    => esc_html__( 'Sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
                'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
                'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
                'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
            ),
            'default'  => 'none'
        ),
        array(
            'id'       => 'search-page-sidebar-width',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
                'large' => esc_html__( 'Large (1/3)', 'w9sme' )
            ),
            'default'  => 'small',
            'required' => array( 'search-page-sidebar', '=', array( 'left', 'both', 'right' ) ),
        ),
        array(
            'id'       => 'search-page-left-sidebar',
            'type'     => 'select',
            'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-1',
            'required' => array( 'search-page-sidebar', '=', array( 'left', 'both' ) ),
        ),
        array(
            'id'       => 'search-page-right-sidebar',
            'type'     => 'select',
            'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-2',
            'required' => array( 'search-page-sidebar', '=', array( 'right', 'both' ) ),
        ),
    
        array(
            'id'             => 'search-page-margin',
            'type'           => 'spacing',
            'mode'           => 'margin',
            'units'          => 'px',
            'units_extended' => 'false',
            'title'          => esc_html__( 'Page margin', 'w9sme' ),
            'subtitle'       => esc_html__( 'Set page margin.', 'w9sme' ),
            'desc'           => '',
            'left'           => false,
            'right'          => false,
            'default'        => array(
                'margin-top'    => '0',
                'margin-bottom' => '60px',
                'units'         => 'px',
            ),
            'output'         => array( '.search.search-results .site-main, .search.search-no-results .site-main' ),
        )
//        array(
//            'id'    => mt_rand(),
//            'type'  => 'info',
//            'title' => 'Other settings'
//        ),
//        array(
//            'id'       => 'search-page-display-type',
//            'type'     => 'select',
//            'title'    => esc_html__( 'Display type', 'w9sme' ),
//            'subtitle' => esc_html__( 'Select search page display type.', 'w9sme' ),
//            'desc'     => '',
//            'options'  => array(
//                'classic' => esc_html__( 'Classic', 'w9sme' ),
//                'grid'    => esc_html__( 'Grid', 'w9sme' ),
//                'masonry' => esc_html__( 'Masonry', 'w9sme' ),
//            ),
//            'default'  => 'classic',
//            'validate' => 'not_empty'
//        ),
//        array(
//            'id'       => 'search-page-search-form',
//            'type'     => 'switch',
//            'title'    => esc_html__( 'Show search form', 'w9sme' ),
//            'subtitle' => esc_html__( 'Set display or not display the search form.', 'w9sme' ),
//            'desc'     => '',
//            'default'  => '1'
//        )
    )
);