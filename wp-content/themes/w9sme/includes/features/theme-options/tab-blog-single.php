<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-blog-single.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$this->sections[] = array(
	'title'      => esc_html__( 'Blog Single', 'w9sme' ),
	'desc'       => esc_html__( 'Single blog settings.', 'w9sme' ),
	'icon'       => 'el el-file',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'blog-single-post-meta',
			'type'     => 'checkbox',
			'title'    => esc_html__( 'Meta', 'w9sme' ),
			'subtitle' => esc_html__( 'Disable or enable meta options.', 'w9sme' ),
			'options'  => array(
				'date'         => esc_html__( 'Date', 'w9sme' ),
				'author'       => esc_html__( 'Author', 'w9sme' ),
				'categories'   => esc_html__( 'Categories', 'w9sme' ),
				'tags'         => esc_html__( 'Tags', 'w9sme' ),
//				'comments'     => esc_html__( 'Comments', 'w9sme' ),
				'social-share' => esc_html__( 'Social Share', 'w9sme' ),
			),
			'default'  => array(
				'date'         => 1,
				'author'       => 1,
				'categories'   => 1,
				'tags'         => 1,
//				'comments'     => 1,
				'social-share' => 1,
			),
		),
		array(
			'id'       => 'blog-single-post-navigation',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show post navigation', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable/disable post navigation.', 'w9sme' ),
			'desc'     => '',
			'default'  => '1'
		),
		
		array(
			'id'       => 'blog-single-author-info',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show author info', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable/disable author info.', 'w9sme' ),
			'desc'     => '',
			'default'  => '1'
		),

//        array(
//            'id'       => 'blog-single-related-post',
//            'type'     => 'switch',
//            'title'    => esc_html__( 'Show related post', 'w9sme' ),
//            'subtitle' => esc_html__( 'Enable/disable related post.', 'w9sme' ),
//            'desc'     => '',
//            'default'  => '1'
//        ),
//
//        array(
//            'id'       => 'blog-single-related-post-number',
//            'type'     => 'text',
//            'title'    => esc_html__( 'Related post number', 'w9sme' ),
//            'subtitle' => esc_html__( 'Total record of related post.', 'w9sme' ),
//            'validate' => 'number',
//            'default'  => '6',
//            'required' => array( 'blog-single-related-post', '=', array( '1' ) ),
//        ),
//
//        array(
//            'id'       => 'blog-single-related-post-columns',
//            'type'     => 'select',
//            'title'    => esc_html__( 'Related post columns', 'w9sme' ),
//            'default'  => '3',
//            'options'  => array( '2' => '2', '3' => '3', '4' => '4' ),
//            'select2'  => array( 'allowClear' => false ),
//            'required' => array( 'blog-single-related-post', '=', array( '1' ) ),
//        ),
//
//        array(
//            'id'       => 'blog-single-related-post-condition',
//            'type'     => 'checkbox',
//            'title'    => esc_html__( 'Related post condition', 'w9sme' ),
//            'options'  => array(
//                'category' => esc_html__( 'Same Category', 'w9sme' ),
//                'tag'      => esc_html__( 'Same Tag', 'w9sme' ),
//            ),
//            'default'  => array(
//                'category' => '1',
//                'tag'      => '1',
//            ),
//            'required' => array( 'blog-single-related-post', '=', array( '1' ) ),
//        ),
//
		
		array(
			'id'       => 'blog-single-resize-image',
			'type'     => 'switch',
			'title'    => esc_html__( 'Resize post featured image', 'w9sme' ),
			'subtitle' => esc_html__( 'Resize post featured image or not?', 'w9sme' ),
			'default'  => '1'
		),
		
		array(
			'id'             => 'blog-single-margin',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
			'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
			'left'           => false,
			'right'          => false,
			'default'        => array(
				'margin-top'    => '0px',
				'margin-bottom' => '80px',
				'units'         => 'px',
			),
			'output'         => array( '.site-main-single' )
		),
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Layout Settings', 'w9sme' ),
		),
		
		array(
			'id'       => 'blog-single-layout',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Layout', 'w9sme' ),
			'subtitle' => esc_html__( 'Select single blog layout.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
				'container'       => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
			),
			'default'  => 'container'
		),
		
		array(
			'id'       => 'blog-single-widget-title-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Widget title style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				''        => esc_html__( 'Default', 'w9sme' ),
				'style-1' => esc_html__( 'Style 1', 'w9sme' ),
				'style-2' => esc_html__( 'Style 2', 'w9sme' ),
			),
			'default'  => ''
		),
		
		array(
			'id'       => 'blog-single-sidebar',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
				'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
				'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
				'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
			),
			'default'  => 'right'
		),
		
		array(
			'id'       => 'blog-single-sidebar-width',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
				'large' => esc_html__( 'Large (1/3)', 'w9sme' )
			),
			'default'  => 'large',
			'required' => array( 'blog-single-sidebar', '=', array( 'left', 'both', 'right' ) ),
		),
		
		
		array(
			'id'       => 'blog-single-sidebar-left',
			'type'     => 'select',
			'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-1',
			'required' => array( 'blog-single-sidebar', '=', array( 'left', 'both' ) ),
		),
		
		array(
			'id'       => 'blog-single-sidebar-right',
			'type'     => 'select',
			'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-1',
			'required' => array( 'blog-single-sidebar', '=', array( 'right', 'both' ) ),
		),
	)
);