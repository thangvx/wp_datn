<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-cpt.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$this->sections[] = array(
    'title'  => esc_html__( 'Custom Post Type', 'w9sme' ),
    'desc'   => esc_html__( 'Custom post type settings section.', 'w9sme' ),
    'icon'   => 'dashicons-before dashicons-admin-post',
    'fields' => array(
        //Portfolio
        array(
            'id'    => mt_rand(),
            'type'  => 'info',
            'title' => esc_html__('Portfolio Settings', 'w9sme' ),
        ),
        array(
            'id'        => 'cpt-portfolio-enable',
            'type'      => 'switch',
            'title'     => esc_html__( 'Portfolio enable?', 'w9sme' ),
            'subtitle'  => esc_html__( 'Don\'t want to use this custom post type. Just make it off!', 'w9sme' ),
            'default'   => 1,
            'ajax_save' => false
        ),
        array(
            'id'        => 'cpt-portfolio-slug',
            'type'      => 'text',
            'title'     => esc_html__( 'Custom portfolio slug', 'w9sme' ),
            'subtitle'  => esc_html__( 'Using different Portfolio slug.', 'w9sme' ),
            'desc'      => esc_html__( 'No space between each words, using "-" or "_" to separate each words, and all letters should be in lowercase. Default value is "portfolio".', 'w9sme' ),
            'ajax_save' => false,
            'required'  => array( 'cpt-portfolio-enable', '=', 1 )
        ),
        array(
            'id'        => 'cpt-portfolio-name',
            'type'      => 'text',
            'title'     => esc_html__( 'Custom portfolio name', 'w9sme' ),
            'subtitle'  => esc_html__( 'Using different Portfolio name.', 'w9sme' ),
            'desc'      => esc_html__( 'Capitalize first letter of each words. Default value is "Portfolio".', 'w9sme' ),
            'ajax_save' => false,
            'required'  => array( 'cpt-portfolio-enable', '=', 1 )
        ),
        array(
            'id'        => 'cpt-portfolio-tax-slug',
            'type'      => 'text',
            'title'     => esc_html__( 'Custom portfolio category slug', 'w9sme' ),
            'subtitle'  => esc_html__( 'Using different Portfolio Category slug.', 'w9sme' ),
            'desc'      => esc_html__( 'No space between each words, using "-" or "_" to separate each words, and all letters should be in lowercase. Default value is "portfolio-category".', 'w9sme' ),
            'ajax_save' => false,
            'required'  => array( 'cpt-portfolio-enable', '=', 1 )
        ),
        array(
            'id'        => 'cpt-portfolio-tax-name',
            'type'      => 'text',
            'title'     => esc_html__( 'Custom portfolio category name', 'w9sme' ),
            'subtitle'  => esc_html__( 'Using different Portfolio Category name.', 'w9sme' ),
            'desc'      => esc_html__( 'Capitalize first letter of each words. Default value is "Portfolio Category".', 'w9sme' ),
            'ajax_save' => false,
            'required'  => array( 'cpt-portfolio-enable', '=', 1 )
        ),
	    
	    //Service
	
	    array(
		    'id'    => mt_rand(),
		    'type'  => 'info',
		    'title' => esc_html__('Service Settings', 'w9sme' ),
	    ),
	    array(
		    'id'        => 'cpt-service-enable',
		    'type'      => 'switch',
		    'title'     => esc_html__( 'Service enable?', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Don\'t want to use this custom post type. Just make it off!', 'w9sme' ),
		    'default'   => 1,
		    'ajax_save' => false
	    ),
	    array(
		    'id'        => 'cpt-service-slug',
		    'type'      => 'text',
		    'title'     => esc_html__( 'Custom service slug', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Using different Service slug.', 'w9sme' ),
		    'desc'      => esc_html__( 'No space between each words, using "-" or "_" to separate each words, and all letters should be in lowercase. Default value is "service".', 'w9sme' ),
		    'ajax_save' => false,
		    'required'  => array( 'cpt-service-enable', '=', 1 )
	    ),
	    array(
		    'id'        => 'cpt-service-name',
		    'type'      => 'text',
		    'title'     => esc_html__( 'Custom service name', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Using different Service name.', 'w9sme' ),
		    'desc'      => esc_html__( 'Capitalize first letter of each words. Default value is "Service".', 'w9sme' ),
		    'ajax_save' => false,
		    'required'  => array( 'cpt-service-enable', '=', 1 )
	    ),
	    array(
		    'id'        => 'cpt-service-tax-slug',
		    'type'      => 'text',
		    'title'     => esc_html__( 'Custom service category slug', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Using different Service Category slug.', 'w9sme' ),
		    'desc'      => esc_html__( 'No space between each words, using "-" or "_" to separate each words, and all letters should be in lowercase. Default value is "service-category".', 'w9sme' ),
		    'ajax_save' => false,
		    'required'  => array( 'cpt-service-enable', '=', 1 )
	    ),
	    array(
		    'id'        => 'cpt-service-tax-name',
		    'type'      => 'text',
		    'title'     => esc_html__( 'Custom service category name', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Using different Service Category name.', 'w9sme' ),
		    'desc'      => esc_html__( 'Capitalize first letter of each words. Default value is "Service Category".', 'w9sme' ),
		    'ajax_save' => false,
		    'required'  => array( 'cpt-service-enable', '=', 1 )
	    ),
	
	    //
//
//	    array(
//		    'id'    => mt_rand(),
//		    'type'  => 'info',
//		    'title' => esc_html__('Event CPT settings', 'w9sme')
//	    ),
//	    array(
//		    'id'        => 'cpt-event-enable',
//		    'type'      => 'switch',
//		    'title'     => esc_html__( 'Event enable?', 'w9sme' ),
//		    'subtitle'  => esc_html__( 'Don\'t want to use this custom post type. Just make it off!', 'w9sme' ),
//		    'default'   => 1,
//		    'ajax_save' => false
//	    ),
//	    array(
//		    'id'        => 'cpt-event-slug',
//		    'type'      => 'text',
//		    'title'     => esc_html__( 'Custom event slug', 'w9sme' ),
//		    'subtitle'  => esc_html__( 'Using different Event slug.', 'w9sme' ),
//		    'desc'      => esc_html__( 'No space between each words, using "-" or "_" to separate each words, and all letters should be in lowercase. Default value is "event".', 'w9sme' ),
//		    'ajax_save' => false,
//		    'required'  => array( 'cpt-event-enable', '=', 1 )
//	    ),
//	    array(
//		    'id'        => 'cpt-event-name',
//		    'type'      => 'text',
//		    'title'     => esc_html__( 'Custom event name', 'w9sme' ),
//		    'subtitle'  => esc_html__( 'Using different Event name.', 'w9sme' ),
//		    'desc'      => esc_html__( 'Capitalize first letter of each words. Default value is "Event".', 'w9sme' ),
//		    'ajax_save' => false,
//		    'required'  => array( 'cpt-event-enable', '=', 1 )
//	    ),
//	    array(
//		    'id'        => 'cpt-event-tax-slug',
//		    'type'      => 'text',
//		    'title'     => esc_html__( 'Custom event category slug', 'w9sme' ),
//		    'subtitle'  => esc_html__( 'Using different Event Category slug.', 'w9sme' ),
//		    'desc'      => esc_html__( 'No space between each words, using "-" or "_" to separate each words, and all letters should be in lowercase. Default value is "event-category".', 'w9sme' ),
//		    'ajax_save' => false,
//		    'required'  => array( 'cpt-event-enable', '=', 1 )
//	    ),
//	    array(
//		    'id'        => 'cpt-event-tax-name',
//		    'type'      => 'text',
//		    'title'     => esc_html__( 'Custom event category name', 'w9sme' ),
//		    'subtitle'  => esc_html__( 'Using different Event Category name.', 'w9sme' ),
//		    'desc'      => esc_html__( 'Capitalize first letter of each words. Default value is "Event Category".', 'w9sme' ),
//		    'ajax_save' => false,
//		    'required'  => array( 'cpt-event-enable', '=', 1 )
//	    ),
	    
	    //

        array(
            'id'    => mt_rand(),
            'type'  => 'info',
            'title' => esc_html__('Review CPT Settings', 'w9sme')
        ),
        array(
            'id'        => 'review-enable',
            'type'      => 'switch',
            'title'     => esc_html__( 'Review enable?', 'w9sme' ),
            'subtitle'  => esc_html__( 'Don\'t want to use this custom post type. Just make it off!', 'w9sme' ),
            'default'   => 1,
            'ajax_save' => false
        ),
	    
	    //

//        array(
//            'id'    => mt_rand(),
//            'type'  => 'info',
//            'title' => esc_html__('Theme Demo CPT settings', 'w9sme')
//        ),
//        array(
//            'id'        => 'theme-demo-enable',
//            'type'      => 'switch',
//            'title'     => esc_html__( 'Theme demo enable?', 'w9sme' ),
//            'subtitle'  => esc_html__( 'Don\'t want to use this custom post type. Just make it off!', 'w9sme' ),
//            'default'   => 1,
//            'ajax_save' => false
//        ),
	
	    array(
		    'id'    => mt_rand(),
		    'type'  => 'info',
		    'title' => esc_html__('Vacancy Settings', 'w9sme' ),
	    ),
	    array(
		    'id'        => 'cpt-vacancy-enable',
		    'type'      => 'switch',
		    'title'     => esc_html__( 'Vacancy enable?', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Don\'t want to use this custom post type. Just make it off!', 'w9sme' ),
		    'default'   => 1,
		    'ajax_save' => false
	    ),
	    array(
		    'id'        => 'cpt-vacancy-slug',
		    'type'      => 'text',
		    'title'     => esc_html__( 'Custom vacancy slug', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Using different Vacancy slug.', 'w9sme' ),
		    'desc'      => esc_html__( 'No space between each words, using "-" or "_" to separate each words, and all letters should be in lowercase. Default value is "vacancy".', 'w9sme' ),
		    'ajax_save' => false,
		    'required'  => array( 'cpt-vacancy-enable', '=', 1 )
	    ),
	    array(
		    'id'        => 'cpt-vacancy-name',
		    'type'      => 'text',
		    'title'     => esc_html__( 'Custom vacancy name', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Using different Vacancy name.', 'w9sme' ),
		    'desc'      => esc_html__( 'Capitalize first letter of each words. Default value is "Vacancy".', 'w9sme' ),
		    'ajax_save' => false,
		    'required'  => array( 'cpt-vacancy-enable', '=', 1 )
	    ),
	    array(
		    'id'        => 'cpt-vacancy-tax-slug',
		    'type'      => 'text',
		    'title'     => esc_html__( 'Custom vacancy category slug', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Using different Vacancy Category slug.', 'w9sme' ),
		    'desc'      => esc_html__( 'No space between each words, using "-" or "_" to separate each words, and all letters should be in lowercase. Default value is "vacancy-category".', 'w9sme' ),
		    'ajax_save' => false,
		    'required'  => array( 'cpt-vacancy-enable', '=', 1 )
	    ),
	    array(
		    'id'        => 'cpt-vacancy-tax-name',
		    'type'      => 'text',
		    'title'     => esc_html__( 'Custom vacancy category name', 'w9sme' ),
		    'subtitle'  => esc_html__( 'Using different Vacancy Category name.', 'w9sme' ),
		    'desc'      => esc_html__( 'Capitalize first letter of each words. Default value is "Vacancy Category".', 'w9sme' ),
		    'ajax_save' => false,
		    'required'  => array( 'cpt-vacancy-enable', '=', 1 )
	    ),
    ),
);

