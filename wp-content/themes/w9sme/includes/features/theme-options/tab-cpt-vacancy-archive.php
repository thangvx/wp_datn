<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-cpt-vacancy-archive.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! class_exists( 'W9sme_CPT_Vacancy' ) || w9sme_get_option( 'cpt-vacancy-enable' ) == '0' ) {
	return;
}

/*-------------------------------------
	vacancy CPT
---------------------------------------*/
$this->sections[] = array(
	'title'      => esc_html__( 'Vacancy Archive', 'w9sme' ),
	'desc'       => esc_html__( 'Vacancy Archive settings section.', 'w9sme' ),
	'icon'       => 'dashicons-before dashicons-share-alt',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'    => mt_rand(),
			'type'  => 'info',
			'title' => esc_html__( 'Template settings', 'w9sme' ),
		),
		
		array(
			'id'    => 'vacancy-archive-template',
			'title' => esc_html__( 'Override default archive template', 'w9sme' ),
			'type'  => 'select',
			'data'  => 'pages',
		),

		array(
			'id'       => 'vacancy-archive-search-enable',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable search', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable vacancy search.', 'w9sme' ),
			'default'  => true,
		),
		
		array(
			'id'       => 'vacancy-archive-page-length-menu-enable',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable page length menu', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable page length menu, which allow changing the page length.', 'w9sme' ),
			'default'  => true,
		),
		
		array(
			'id'       => 'vacancy-archive-result-info-enable',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable result info', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable result info.', 'w9sme' ),
			'default'  => true,
		),
		
		array(
			'id'       => 'vacancy-archive-items-per-page',
			'type'     => 'text',
			'title'    => esc_html__( 'Items per page', 'w9sme' ),
			'subtitle' => esc_html__( 'Default value is 10.', 'w9sme' ),
			'default'  => '10',
			'validate' => 'numeric'
		),

//		array(
//			'id'       => 'vacancy-archive-paging-type',
//			'type'     => 'button_set',
//			'title'    => esc_html__( 'Paging type', 'w9sme' ),
//			'subtitle' => esc_html__( 'Select archive paging type.', 'w9sme' ),
//			'desc'     => '',
//			'options'  => array(
//				'default'         => esc_html__( 'Default', 'w9sme' ),
//				'load-more'       => esc_html__( 'Load More', 'w9sme' ),
//				'infinite-scroll' => esc_html__( 'Infinite Scroll', 'w9sme' )
//			),
//			'default'  => 'default',
////			'required' => array( 'vacancy-archive-display-type', '=', array( 'grid' ) ),
//		),
		
		array(
			'id'             => 'vacancy-archive-margin',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
			'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
			'left'           => false,
			'right'          => false,
			'default'        => array(
				'margin-top'    => '0',
				'margin-bottom' => '55px',
				'units'         => 'px',
			),
			'output'         => array( '.site-main-archive.vacancy-archive' )
		),

//		//Image ratio
//		array(
//			'id'      => 'vacancy-archive-item-image-ratio',
//			'type'    => 'select',
//			'title'   => esc_html__( 'Image Ratio', 'w9sme' ),
//			'options' => array(
//				'auto'          => esc_html__( 'Auto', 'w9sme' ),
//				'0.5625'        => '16x9',
//				'1.77777777778' => '9x16',
//				'0.42857142857' => '21x9',
//				'2.33333333333' => '9x21',
//				'0.75'          => '4x3',
//				'1.33333333333' => '3x4',
//				'0.666666667'   => '3x2',
//				'1.5'           => '2x3',
//				'1'             => '1x1',
//				'2'             => '1x2',
//				'0.5'           => '2x1',
//			),
//			'default' => '0.666666667'
//		),
	
		/*-------------------------------------
			LAYOUT SETTINGS
		---------------------------------------*/
		array(
			'id'    => mt_rand(),
			'type'  => 'info',
			'title' => esc_html__( 'Layout Settings', 'w9sme' ),
		),
		
		array(
			'id'       => 'vacancy-archive-layout',
			'type'     => 'select',
			'title'    => esc_html__( 'Layout', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive layout.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
				'container'       => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
			),
			'default'  => 'container',
			'validate' => 'not_empty'
		),
		
		array(
			'id'       => 'vacancy-archive-widget-title-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Widget title style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				''        => esc_html__( 'Default', 'w9sme' ),
				'style-1' => esc_html__( 'Style 1', 'w9sme' ),
				'style-2' => esc_html__( 'Style 2', 'w9sme' ),
			),
			'default'  => ''
		),
		
		array(
			'id'       => 'vacancy-archive-sidebar',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
				'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
				'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
				'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
			),
			'default'  => 'none'
		),
		
		array(
			'id'       => 'vacancy-archive-sidebar-width',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
				'large' => esc_html__( 'Large (1/3)', 'w9sme' )
			),
			'default'  => 'small',
			'required' => array( 'vacancy-archive-sidebar', '=', array( 'left', 'both', 'right' ) ),
		),
		
		array(
			'id'       => 'vacancy-archive-sidebar-left',
			'type'     => 'select',
			'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-1',
			'required' => array( 'vacancy-archive-sidebar', '=', array( 'left', 'both' ) ),
		),
		
		array(
			'id'       => 'vacancy-archive-sidebar-right',
			'type'     => 'select',
			'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-2',
			'required' => array( 'vacancy-archive-sidebar', '=', array( 'right', 'both' ) ),
		),
	),
);