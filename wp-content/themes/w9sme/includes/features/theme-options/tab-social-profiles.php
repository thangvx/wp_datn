<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-social-profiles.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$this->sections[] = array(
    'title'  => esc_html__( 'Social Profiles', 'w9sme' ),
    'desc'   => '',
    'icon'   => 'el el-path',
    'fields' => array(
        array(
            'id'   => mt_rand(),
            'type' => 'info',
            'desc' => esc_html__( 'Social Meta Tags', 'w9sme' )
        ),
        array(
            'id'       => 'social-meta-tag',
            'type'     => 'switch',
            'title'    => esc_html__( 'Social meta tags', 'w9sme' ),
            'subtitle' => esc_html__( 'Enable the social meta head tag output.', 'w9sme' ),
            'desc'     => '',
            'default'  => '0'
        ),
        array(
            'id'       => 'social-twitter-author-username',
            'type'     => 'text',
            'title'    => esc_html__( 'Twitter publisher username', 'w9sme' ),
            'subtitle' => esc_html__( 'Enter your twitter username here, to be used for the Twitter Card date. Ensure that you do not include the @ symbol.', 'w9sme' ),
            'desc'     => '',
            'default'  => '',
            'required' => array( 'social-meta-tag', '=', array( '1' ) ),
        ),
        array(
            'id'       => 'social-google-plus-author',
            'type'     => 'text',
            'title'    => esc_html__( 'Google+ Username', 'w9sme' ),
            'subtitle' => esc_html__( 'Enter your Google+ username here, to be used for the authorship meta.', 'w9sme' ),
            'desc'     => '',
            'default'  => '',
            'required' => array( 'social-meta-tag', '=', array( '1' ) ),
        ),
        array(
            'id'   => mt_rand(),
            'type' => 'info',
            'desc' => esc_html__( 'Social Share', 'w9sme' )
        ),
        array(
            'title'    => esc_html__( 'Social Share', 'w9sme' ),
            'id'       => 'social-sharing',
            'type'     => 'checkbox',
            'subtitle' => esc_html__( 'Show the social sharing in blog posts.', 'w9sme' ),

            //Must provide key => value pairs for multi checkbox options
            'options'  => array(
                'facebook'  => 'Facebook',
                'twitter'   => 'Twitter',
                'google'    => 'Google',
                'linkedin'  => 'Linkedin',
                'tumblr'    => 'Tumblr',
                'pinterest' => 'Pinterest'
            ),

            //See how default has changed? you also don't need to specify opts that are 0.
            'default'  => array(
                'twitter'   => '1',
                'facebook'  => '1',
                'google'    => '1',
                'linkedin'  => '1',
                'tumblr'    => '1',
                'pinterest' => '1'
            )
        ),
        array(
            'id'   => mt_rand(),
            'type' => 'info',
            'desc' => esc_html__( 'Social Profiles URL', 'w9sme' )
        ),
        array(
            'id'       => 'social-twitter-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Twitter', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Twitter.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-facebook-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Facebook', 'w9sme' ),
            'subtitle' => esc_html__( 'Your facebook page/profile url.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-dribbble-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Dribbble', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Dribbble.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-vimeo-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Vimeo', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Vimeo.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-tumblr-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Tumblr', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Tumblr.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-skype-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Skype', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Skype username.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-linkedin-url',
            'type'     => 'text',
            'title'    => esc_html__( 'LinkedIn', 'w9sme' ),
            'subtitle' => esc_html__( 'Your LinkedIn page/profile url.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-googleplus-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Google+', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Google+ page/profile URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-flickr-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Flickr', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Flickr page url.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-youtube-url',
            'type'     => 'text',
            'title'    => esc_html__( 'YouTube', 'w9sme' ),
            'subtitle' => esc_html__( 'Your YouTube URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-pinterest-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Pinterest', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Pinterest.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-foursquare-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Foursquare', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Foursqaure URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-instagram-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Instagram', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Instagram.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-github-url',
            'type'     => 'text',
            'title'    => esc_html__( 'GitHub', 'w9sme' ),
            'subtitle' => esc_html__( 'Your GitHub URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-xing-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Xing', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Xing URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-behance-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Behance', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Behance URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-deviantart-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Deviantart', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Deviantart URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-soundcloud-url',
            'type'     => 'text',
            'title'    => esc_html__( 'SoundCloud', 'w9sme' ),
            'subtitle' => esc_html__( 'Your SoundCloud URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-yelp-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Yelp', 'w9sme' ),
            'subtitle' => esc_html__( 'Your Yelp URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-rss-url',
            'type'     => 'text',
            'title'    => esc_html__( 'RSS Feed', 'w9sme' ),
            'subtitle' => esc_html__( 'Your RSS Feed URL.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),
        array(
            'id'       => 'social-email-url',
            'type'     => 'text',
            'title'    => esc_html__( 'Email address', 'w9sme' ),
            'subtitle' => esc_html__( 'Your email address.', 'w9sme' ),
            'desc'     => '',
            'default'  => ''
        ),

    )
);