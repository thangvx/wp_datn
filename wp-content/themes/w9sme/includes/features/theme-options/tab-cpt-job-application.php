<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-cpt-vacancy-single.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! class_exists( 'W9sme_CPT_Vacancy' ) || w9sme_get_option( 'cpt-vacancy-enable' ) == '0' ) {
	return;
}

/*-------------------------------------\
	Vacancy CPT
---------------------------------------*/
$this->sections[] = array(
	'title'      => esc_html__( 'Job Applications Form', 'w9sme' ),
	'desc'       => esc_html__( 'Notice: All setttings in this section only work on reservation page, using the job application form we provided in: {theme_path}/template-html/reservation-template.html', 'w9sme' ),
	'icon'       => 'dashicons-before dashicons-share-alt',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'vacancy-job-applications-page',
			'title'    => esc_html__( 'Job Application Page', 'w9sme' ),
			'subtitle' => esc_html__( 'Select page for job apply', 'w9sme' ),
			'type'     => 'select',
			'data'     => 'pages',
		),
		
		array(
			'id'       => 'vacancy-picker-placeholder',
			'type'     => 'text',
			'title'    => esc_html__( 'Vacancy Picker Placeholder', 'w9sme' ),
			'subtitle' => esc_html__( 'Enter Custom Vacancy Picker Placeholder.', 'w9sme' ),
			'desc'     => 'Default value is "--".',
			'default'  => '--',
		),
		
		array(
			'id'       => 'vacancy-using-vacancy-data',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Using Vacancy Data', 'w9sme' ),
			'subtitle' => esc_html__( 'Using vacancy data in vacancies picker field.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'1' => esc_html__( 'On', 'w9sme' ),
				'0' => esc_html__( 'Off', 'w9sme' )
			),
			'default'  => '0'
		),
		
		array(
			'id'       => 'vacancy-auto-select',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Auto Select Vacancy', 'w9sme' ),
			'subtitle' => esc_html__( 'This will auto select your selected vacancy form vacancy list when hit the button Apply Job.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'1' => esc_html__( 'On', 'w9sme' ),
				'0' => esc_html__( 'Off', 'w9sme' )
			),
			'default'  => '0',
			'required' => array( 'vacancy-using-vacancy-data', '=', array( '1' ) ),
		),
	
	
	),
);