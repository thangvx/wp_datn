<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-footer.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/*
 * Footer Section
*/
$content_template_list = w9sme_get_content_template_list();

$this->sections[] = array(
    'title'  => esc_html__( 'Footer', 'w9sme' ),
    'desc'   => esc_html__( 'Change the footer section configuration.', 'w9sme' ),
    'icon'   => 'fa fa-ellipsis-h',
    'fields' => array(
        array(
            'id'        => 'footer-override-default',
            'type'      => 'select',
            'multi'     => true,
            'title'     => esc_html__( 'Override default footer settings in', 'w9sme' ),
            'subtitle'  => esc_html__( 'Choose which template you need to override the default settings in here.', 'w9sme' ),
            'desc'      => esc_html__( 'The tabs will appear after you save the change.', 'w9sme' ) .
                '<br />  <strong style="color: red;">' . esc_html__( 'Notice 1: If the page doesn\'t auto refresh, please refresh the page after changing and saving this option.', 'w9sme' ) . '</strong> </br> <strong style="color: red;">' .
                esc_html__( 'Notice 2: If an item is removed, the options will be saved automatically after the page auto refresh, please wait a bit.', 'w9sme' ) . '</strong>',
            'options'   => w9sme_get_template_prefix( 'options_field' ),
            'default'   => array(),
            'ajax_save' => false
//            'compiler' => true,
//            'reload_on_change' => true
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__('Default Footer Settings', 'w9sme' ),
        ),
        array(
            'id'       => 'footer-enable',
            'type'     => 'select',
            'title'    => esc_html__( 'Footer template', 'w9sme' ),
            'subtitle' => esc_html__( 'If on, this layout part will be displayed.', 'w9sme' ),
            'options'  => array(
                'simple' => esc_html__( 'Simple', 'w9sme' ),
                'custom' => esc_html__( 'Content Template', 'w9sme' ),
                'off'    => esc_html__( 'Off', 'w9sme' )
            ),
            'default'  => 'simple',
        ),

        array(
            'id'       => 'footer-content-template',
            'type'     => 'select',
            'title'    => esc_html__( 'Footer content template', 'w9sme' ),
            'options'  => $content_template_list,
            'desc'     => esc_html__( 'Select content template for footer.', 'w9sme' ),
            'default'  => '',
            'required' => array( 'footer-enable', '=', array( 'custom' ) )
        ),

        array(
            'id'       => 'footer-copyright-text',
            'type'     => 'textarea',
            'title'    => esc_html__( 'Copyright text', 'w9sme' ),
            'subtitle' => esc_html__( 'Enter the copyright text for the footer.', 'w9sme' ),
            'required' => array( 'footer-enable', '=', 'simple' ),
            'default'  => __( '<span class="light-color s-font fs-italic">WordPress Theme by Thang X. Vu</span>', 'w9sme' )
        ),
        
        array(
            'id'             => 'footer-padding',
            'type'           => 'spacing',
            'mode'           => 'padding',
            'units'          => 'px',
            'units_extended' => 'false',
            'title'          => esc_html__( 'Padding top/bottom', 'w9sme' ),
            'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
            'desc'           => esc_html__( 'If you would like to override the default footer top/bottom padding, then you can do so here.', 'w9sme' ),
            'left'           => false,
            'right'          => false,
            'default'        => array(
                'padding-top'    => '18px',
                'padding-bottom' => '18px',
                'units'          => 'px',
            ),
            'required'       => array( 'footer-enable', '=', 'simple' ),
            'output'         => array( '.site-footer .footer-content-area' )
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'title'    => esc_html__( 'Footer colors', 'w9sme' ),
            'required' => array( 'footer-enable', '=', 'simple' ),
        ),
        array(
            'id'       => 'footer-colors',
            'type'     => 'select',
            'title'    => esc_html__( 'Footer colors style', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose footer color style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'gray'   => esc_html__( 'Gray', 'w9sme' ),
                'light'  => esc_html__( 'Light', 'w9sme' ),
                'dark'   => esc_html__( 'Dark', 'w9sme' ),
                'custom' => esc_html__( 'Custom', 'w9sme' ),
            ),
            'default'  => 'gray',
            'compiler' => true,
            'required' => array( 'footer-enable', '=', 'simple' ),
        ),
        
        array(
            'id'       => 'footer-background-color',
            'type'     => 'color_rgba',
            'title'    => esc_html__( 'Footer background color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'footer-colors', '=', 'custom'
            ),
            'default'  => array(
                'color' => '#222',
                'alpha' => '1',
                'rgba'  => 'rgba(0, 0, 0, 1)'
            ),
            'compiler' => true
        ),
        
        array(
            'id'       => 'footer-text-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer text color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'footer-colors', '=', 'custom'
            ),
            'default'  => '#868686',
            'compiler' => true
        ),
        
        array(
            'id'       => 'footer-heading-text-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer heading text color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'footer-colors', '=', 'custom'
            ),
            'default'  => '#fff',
            'compiler' => true
        ),
        
        array(
            'id'       => 'footer-link-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer link color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'footer-colors', '=', 'custom'
            ),
            'default'  => '#868686',
            'compiler' => true
        ),
        
        array(
            'id'       => 'footer-link-hover-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer link hover color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                'footer-colors', '=', 'custom'
            ),
            'default'  => '#fff',
            'compiler' => true
        )
    
    ) // #fields
);