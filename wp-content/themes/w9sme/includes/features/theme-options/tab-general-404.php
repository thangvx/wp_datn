<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-general-404.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$this->sections[] = array(
    'title'      => esc_html__( '404 Error', 'w9sme' ),
    'desc'       => esc_html__( '404 Error page settings.', 'w9sme' ),
    'icon'       => 'el el-error',
    'subsection' => true,
    'fields'     => array(
//	    array(
//		    'id'       => '-404-image',
//		    'type'     => 'media',
//		    'url'      => true,
//		    'title'    => esc_html__( 'Image source', 'w9sme' ),
//		    'default'  => array(
//			    'url' => w9sme_theme_url(). 'assets/images/404-img.png'
//		    ),
//	    ),
        array(
            'id'      => '-404-title',
            'type'    => 'text',
            'title'   => esc_html__( '404 title', 'w9sme' ),
            'default' => esc_html__('404!', 'w9sme'),
        ),
        array(
            'id'      => '-404-subtitle',
            'type'    => 'textarea',
            'title'   => esc_html__( '404 subtitle', 'w9sme' ),
            'default' => esc_html__('Sorry! Page Not Found!', 'w9sme'),
        ),
	
	    array(
		    'id'      => '-404-go-back-text',
		    'type'    => 'text',
		    'title'   => esc_html__( 'Go back text', 'w9sme' ),
		    'subtitle'    => esc_html__( 'Enter the text of "go back" button.', 'w9sme' ),
		    'default' => esc_html__('Home', 'w9sme'),
	    ),
        
        array(
            'id'      => '-404-go-back-url',
            'type'    => 'text',
            'title'   => esc_html__( 'Go back link', 'w9sme' ),
            'desc'    => esc_html__( 'Default value is your homepage url.', 'w9sme' ),
            'default' => '',
        ),
	
	    array(
		    'id'             => '-404-page-padding',
		    'type'           => 'spacing',
		    'mode'           => 'padding',
		    'units'          => 'px',
		    'units_extended' => 'false',
		    'title'          => esc_html__( 'Page padding', 'w9sme' ),
		    'subtitle'       => esc_html__( 'Set page padding.', 'w9sme' ),
		    'desc'           => '',
		    'left'           => false,
		    'right'          => false,
		    'default'        => array(
			    'padding-top'    => '230px',
			    'padding-bottom' => '300px',
			    'units'         => 'px',
		    ),
		    'output'         => array( '.error404 .site-main-page' ),
	    ),

        array(
            'id'    => mt_rand(),
            'type'  => 'info',
            'title' => esc_html__( '404 colors style configuration', 'w9sme' )
        ),
//        array(
//            'id'       => '-404-page-text-color',
//            'type'     => 'button_set',
//            'title'    => esc_html__( 'Text color', 'w9sme' ),
//            'subtitle' => esc_html__( 'Choose text color for 404 page.', 'w9sme' ),
//            'desc'     => '',
//            'options'  => array(
//                ''            => esc_html__( 'Inherit', 'w9sme' ),
//                'p-color'     => esc_html__( 'Primary', 'w9sme' ),
//                's-color'     => esc_html__( 'Secondary', 'w9sme' ),
//                'light-color' => esc_html__( 'Light', 'w9sme' ),
//                'dark-color'  => esc_html__( 'Dark', 'w9sme' )
//            ),
//            'default'  => '',
//        ),
        array(
            'id'       => '-404-bg-image',
            'type'     => 'media',
            'url'      => true,
            'title'    => esc_html__( 'Background image', 'w9sme' ),
            'subtitle' => esc_html__( 'Upload your background image for 404 site.', 'w9sme' ),
            'desc'     => '',
        ),
        array(
            'id'      => '-404-overlay',
            'type'    => 'button_set',
            'title'   => esc_html__( 'Overlay', 'w9sme' ),
            'desc'    => '',
            'options' => array(
                ''              => esc_html__( 'None', 'w9sme' ),
                'overlay-light' => esc_html__( 'Light', 'w9sme' ),
                'overlay-dark'  => esc_html__( 'Dark', 'w9sme' )
            ),
            'default' => '',
        ),

        array(
            'id'            => '-404-overlay-opacity',
            'type'          => 'slider',
            'title'         => esc_html__( 'Overlay opacity', 'w9sme' ),
            'subtitle'      => esc_html__( 'Set overlay opacity value for 404 site.', 'w9sme' ),
            'desc'          => '',
            'min'           => 0,
            'max'           => 1,
            'step'          => .1,
            'default'       => .5,
            'resolution'    => 0.1,
            'display_value' => 'text'
        ),

//        array(
//            'id'       => '-404-page-overlay-color',
//            'type'     => 'color_rgba',
//            'title'    => esc_html__( 'Page overlay color', 'w9sme' ),
//            'subtitle' => esc_html__( 'Set overlay color for page.', 'w9sme' ),
//            'default'  => array(
//                'color' => '#fff',
//                'alpha' => 0.1,
//                'rgba'  => 'rgba(255, 255, 255, 0.1)'
//            ),
//            'validate' => 'colorrgba',
//        ),
    )
);