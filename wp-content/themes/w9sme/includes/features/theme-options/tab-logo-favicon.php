<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-logo-favicon.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/*
 * Favicon Section
*/

$this->sections[] = array(
	'title' => esc_html__('Logo & Favicon', 'w9sme'),
	'desc' => esc_html__('Configure the logo and favicon in a lot of plataforms. Generate and download your package at http://realfavicongenerator.net/ .', 'w9sme'),
	'icon' => 'el-icon-wrench',
	'fields' => array(
        
        
        array(
            'id' => 'random-general-number',
            'type' => 'info',
            'desc' => esc_html__('Logo', 'w9sme')
        ),
        
        array(
            'id'       => 'logo',
            'type'     => 'media',
            'url'      => true,
            'title'    => esc_html__( 'Logo', 'w9sme' ),
            'subtitle' => esc_html__( 'Upload your logo here.', 'w9sme' ),
            'desc'     => '',
            'default' => array(
                'url' => w9sme_theme_url(). 'assets/images/w9sme-logo.png'
            )
        ),
        
        array(
            'id'       => 'logo-option-1',
            'type'     => 'media',
            'url'      => true,
            'title'    => esc_html__( 'Logo Option 1', 'w9sme' ),
            'subtitle' => esc_html__( 'Upload your logo here.', 'w9sme' ),
            'desc'     => '',
            'default' => array(
                'url' => w9sme_theme_url(). 'assets/images/w9sme-logo.png'
            )
        ),
        
        array(
            'id'       => 'logo-option-2',
            'type'     => 'media',
            'url'      => true,
            'title'    => esc_html__( 'Logo Option 2', 'w9sme' ),
            'subtitle' => esc_html__( 'Upload your logo here.', 'w9sme' ),
            'desc'     => '',
            'default' => array(
                'url' => w9sme_theme_url(). 'assets/images/w9sme-logo.png'
            )
        ),
        
        array(
            'id'       => 'logo-option-3',
            'type'     => 'media',
            'url'      => true,
            'title'    => esc_html__( 'Logo Option 3', 'w9sme' ),
            'subtitle' => esc_html__( 'Upload your logo here.', 'w9sme' ),
            'desc'     => '',
            'default' => array(
                'url' => w9sme_theme_url(). 'assets/images/w9sme-logo.png'
            )
        ),

		array(
			'id' => 'random-general-number',
			'type' => 'info',
			'title' => esc_html__('Favicons', 'w9sme'),
            'desc' => esc_html__('Generate and download your image package at http://realfavicongenerator.net/ .', 'w9sme')
		),

		array(
			'title' => esc_html__('Favicon 16x16', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (16x16).', 'w9sme'),
			'id' => 'favicon-16',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
			'default' => array(
				'url' => w9sme_theme_url(). 'assets/images/w9sme-favicon.png'
			)
		),

		array(
			'title' => esc_html__('Favicon 32x32', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (32x32).', 'w9sme'),
			'id' => 'favicon-32',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 96x96', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (96x96).', 'w9sme'),
			'id' => 'favicon-96',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 160x160', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (160x160).', 'w9sme'),
			'id' => 'favicon-160',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 192x192', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (192x192).', 'w9sme'),
			'id' => 'favicon-192',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'id' => 'random-general-number',
			'type' => 'info',
			'desc' => esc_html__('Apple Favicons', 'w9sme')
		),

		array(
			'title' => esc_html__('Favicon 57x57', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (57x57).', 'w9sme'),
			'id' => 'favicon-a-57',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 114x114', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (114x114).', 'w9sme'),
			'id' => 'favicon-a-114',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 72x72', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (72x72).', 'w9sme'),
			'id' => 'favicon-a-72',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 144x144', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (144x144).', 'w9sme'),
			'id' => 'favicon-a-144',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 60x60', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (60x60).', 'w9sme'),
			'id' => 'favicon-a-60',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 120x120', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (120x120).', 'w9sme'),
			'id' => 'favicon-a-120',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 76x76', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (76x76).', 'w9sme'),
			'id' => 'favicon-a-76',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 152x152', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (152x152).', 'w9sme'),
			'id' => 'favicon-a-152',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Favicon 180x180', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions (180x180).', 'w9sme'),
			'id' => 'favicon-a-180',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'id' => 'random-general-number',
			'type' => 'info',
			'desc' => esc_html__('Windows Metro', 'w9sme')
		),

		array(
		    'id'       => 'favicon-win-color',
		    'type'     => 'color',
		    'title'    => esc_html__('Custom tile background color', 'w9sme'),
		    'subtitle' => esc_html__('Pick a background color for the tile.', 'w9sme'),
		    'validate' => 'color',
		    'transparent' => false,
		    'description' => 'You can see a few recommended tile colors at "Favicon for Windows 8 - Tile" section at http://realfavicongenerator.net/',
		),

		array(
			'title' => esc_html__('Tile image 70x70', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions. Minimum image size: 70x70. Recommended: 128x128.', 'w9sme'),
			'id' => 'favicon-win-70',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Tile image 150x150', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions. Minimum image size: 150x150. Recommended: 270x270.', 'w9sme'),
			'id' => 'favicon-win-150',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Tile image 310x150', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions. Minimum image size: 310x150. Recommended: 558x270.', 'w9sme'),
			'id' => 'favicon-win-310',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

		array(
			'title' => esc_html__('Tile image 310x310', 'w9sme'),
			'desc' => esc_html__('Upload favicon image in the following dimensions. Minimum image size: 310x310. Recommended: 558x558.', 'w9sme'),
			'id' => 'favicon-win-310-quad',
			'type' => 'media',
			'readonly' => false,
			'url'=> true,
		),

	),
);