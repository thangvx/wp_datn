<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: tab-cpt-event-single.php
 * @time    : 4/25/2017 4:26 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if (!class_exists('W9sme_CPT_Event')) {
	return;
}

$this->sections[] = array(
	'title'      => esc_html__( 'Event Single', 'w9sme' ),
	'desc'       => esc_html__( 'Event Single settings section.', 'w9sme' ),
	'icon'       => 'dashicons-before dashicons-calendar-alt',
	'subsection' => true,
	'fields'     => array(
//		array(
//			'id' => 'event-single-default-content',
//			'type' => 'button_set',
//			'title' => esc_html__('Default Content', 'w9sme'),
//			'subtitle' => sprintf(__('Use event single default content if default content of event single is not set.<br>You can set single event content <a href="%s" target="_blank">here</a>', 'w9sme'), admin_url('admin.php?page=vc-general')),
//			'options' => array(
//				'on' => esc_html__('On', 'w9sme'),
//				'off' => esc_html__('Off', 'w9sme'),
//			),
//			'default' => 'on'
//		),
		array(
			'id'       => 'event-single-feature-image',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable feature image ?', 'w9sme' ),
//			'subtitle' => esc_html__( 'Enable event item header.', 'w9sme' ),
			'default'  => 0
		),
		array(
			'id'       => 'event-single-post-meta',
			'type'     => 'checkbox',
			'title'    => esc_html__( 'Meta', 'w9sme' ),
			'subtitle' => esc_html__( 'Disable or enable meta options.', 'w9sme' ),
			'options'  => array(
				'date'       => esc_html__( 'Date', 'w9sme' ),
				'categories' => esc_html__( 'Categories', 'w9sme' ),
				'comments'   => esc_html__( 'Comments', 'w9sme' ),
				'social-share' => esc_html__( 'Social Share', 'w9sme' ),
			),
			'default'  => array(
				'date'       => 1,
				'categories' => 1,
				'comments'   => 1,
				'social-share' => 1,
			),
		),
		array(
			'id'             => 'event-single-margin',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
			'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
			'left'           => false,
			'right'          => false,
			'default'        => array(
				'margin-top'    => '0',
				'margin-bottom' => '0',
				'units'         => 'px',
			),
			'output'         => array( '.site-main-single.event-single' )
		),
		
//		array(
//			'id'       => 'event-single-navigator-enabled',
//			'type'     => 'switch',
//			'title' => esc_html__('Navigator enabled', 'w9sme'),
//			'subtitle' => esc_html__('Enable navigator bottom of single page.', 'w9sme'),
//			'default' => false
//		),
		
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__('Layout Settings', 'w9sme')
		),
		
		array(
			'id'       => 'event-single-layout',
			'type'     => 'select',
			'title'    => esc_html__( 'Layout', 'w9sme' ),
			'subtitle' => esc_html__( 'Select single event layout.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
				'container'       => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
			),
			'default'  => 'container'
		),
		
		array(
			'id'       => 'event-single-widget-title-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Widget title style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				''        => esc_html__( 'Default', 'w9sme' ),
				'style-1' => esc_html__( 'Style 1', 'w9sme' ),
				'style-2' => esc_html__( 'Style 2', 'w9sme' ),
			),
			'default'  => ''
		),
		
		array(
			'id'       => 'event-single-sidebar',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
				'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
				'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
				'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
			),
			'default'  => 'none'
		),
		
		array(
			'id'       => 'event-single-sidebar-width',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
				'large' => esc_html__( 'Large (1/3)', 'w9sme' )
			),
			'default'  => 'small',
			'required' => array( 'event-single-sidebar', '=', array( 'left', 'both', 'right' ) ),
		),
		
		array(
			'id'       => 'event-single-sidebar-left',
			'type'     => 'select',
			'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'event',
			'required' => array( 'event-single-sidebar', '=', array( 'left', 'both' ) ),
		),
		
		array(
			'id'       => 'event-single-sidebar-right',
			'type'     => 'select',
			'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'event',
			'required' => array( 'event-single-sidebar', '=', array( 'right', 'both' ) ),
		),
	),
);