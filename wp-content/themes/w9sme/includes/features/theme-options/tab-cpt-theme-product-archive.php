<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-cpt-theme-product-archive.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/*-------------------------------------
	THEME PRODUCT CPT
---------------------------------------*/
$this->sections[] = array(
    'title'      => esc_html__( 'Theme Product Archive', 'w9sme' ),
    'desc'       => esc_html__( 'Theme Product Archive settings section.', 'w9sme' ),
    'icon'       => 'dashicons-before dashicons-admin-post',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'    => mt_rand(),
            'type'  => 'info',
            'title' => esc_html__('Template settings', 'w9sme' ),
        ),
        
        array(
            'id'    => 'tp-archive-template',
            'title' => esc_html__( 'Override default archive template', 'w9sme' ),
            'type'  => 'select',
            'data'  => 'pages',
        ),
        
        array(
            'id'       => 'tp-archive-items-per-page',
            'type'     => 'text',
            'title'    => esc_html__( 'Items per page', 'w9sme' ),
            'subtitle' => esc_html__( 'Default value is 8.', 'w9sme' ),
            'default'  => '8',
            'validate' => 'numeric'
        ),
        
        array(
            'id'       => 'tp-archive-paging-style',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Paging style', 'w9sme' ),
            'subtitle' => esc_html__( 'Select archive paging style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'default'         => esc_html__( 'Default', 'w9sme' ),
                'load-more'       => esc_html__( 'Load More', 'w9sme' ),
                'infinite-scroll' => esc_html__( 'Infinite Scroll', 'w9sme' )
            ),
            'default'  => 'default'
        ),

        array(
            'id'             => 'tp-archive-margin',
            'type'           => 'spacing',
            'mode'           => 'margin',
            'units'          => 'px',
            'units_extended' => 'false',
            'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
            'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
            'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
            'left'           => false,
            'right'          => false,
            'default'        => array(
                'margin-top'    => '0',
                'margin-bottom' => '0',
                'units'         => 'px',
            ),
            'output'         => array( '.site-main-archive.theme-product-archive' )
        ),

        array(
            'id'    => mt_rand(),
            'type'  => 'info',
            'title' => esc_html__('Layout Settings', 'w9sme' ),
        ),
        array(
            'id'       => 'tp-archive-layout',
            'type'     => 'select',
            'title'    => esc_html__( 'Layout', 'w9sme' ),
            'subtitle' => esc_html__( 'Select archive layout.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
                'container'       => esc_html__( 'Container', 'w9sme' ),
                'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
                'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
            ),
            'default'  => 'container-xlg',
            'validate' => 'not_empty'
        ),

        array(
            'id'       => 'tp-archive-sidebar',
            'type'     => 'image_select',
            'title'    => esc_html__( 'Sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
                'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
                'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
                'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
            ),
            'default'  => 'left'
        ),

        array(
            'id'       => 'tp-archive-sidebar-width',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
            'subtitle' => esc_html__( 'Set Sidebar width.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
                'large' => esc_html__( 'Large (1/3)', 'w9sme' )
            ),
            'default'  => 'small',
            'required' => array( 'tp-archive-sidebar', '=', array( 'left', 'both', 'right' ) ),
        ),

        array(
            'id'       => 'tp-archive-sidebar-left',
            'type'     => 'select',
            'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-1',
            'required' => array( 'tp-archive-sidebar', '=', array( 'left', 'both' ) ),
        ),

        array(
            'id'       => 'tp-archive-sidebar-right',
            'type'     => 'select',
            'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-2',
            'required' => array( 'tp-archive-sidebar', '=', array( 'right', 'both' ) ),
        ),
    ),
);