<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-header-menu.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$this->sections[] = array(
    'title'      => esc_html__( 'Header Menu Settings', 'w9sme' ),
    'desc'       => '',
    'icon'       => 'el el-lines',
    'fields'     => array(
    
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__('Main Menu Setting', 'w9sme')
        ),
	
	    array(
		    'id'       => 'scroll-set-submenu-position',
		    'type'     => 'switch',
		    'title'    => esc_html__( 'Change position (top or bottom) for submenu when scroll', 'w9sme' ),
//		    'subtitle' => esc_html__( 'Enable auto mode .', 'w9sme' ),
		    'default'  => 1,
	    ),

        array(
            'id'          => 'header-menu-font',
            'type'        => 'typography',
            'title'       => esc_html__( 'Menu font', 'w9sme' ),
            'subtitle'    => esc_html__( 'Specify the menu font properties.', 'w9sme' ),
            'google'      => true,
            'fonts'       => w9sme_get_preset_fonts(),
            'line-height' => false,
            'all_styles'  => true, // Enable all Google Font style/weight variations to be added to the page
            'color'       => false,
            'text-align'  => false,
            'font-style'  => true,
            'subsets'     => false,
            'font-size'   => true,
            'font-weight' => true,
            'output' => array('.w9sme-main-nav .w9sme-main-menu-content>li>a'),
            'units'       => 'px', // Defaults to px
            'default'     => array(
                'font-family' => 'Poppins',
                'font-size' => '14px',
                'font-weight' => '500'
            ),
            'compiler' => false
        ),


        array(
            'id'             => 'nav-menu-item-padding',
            'type'           => 'spacing',
            'mode'           => 'padding',
            'top'            => false,
            'bottom'         => false,
            'units'          => 'px',
            'output'         => array( '.w9sme-main-nav .w9sme-main-menu-content>li>a' ),
            'units-extended' => true,
            'title'          => esc_html__( 'Menu item padding', 'w9sme' ),
            'default'        => array(
                'padding-left'  => '20px',
                'padding-right' => '20px',
                'units'         => 'px',
            )
        ),
    
        array(
            'id'             => 'nav-item-height',
            'type'           => 'dimensions',
            'compiler'       => true,
            'width'          => false,
            'units'          => 'px',
            'output'         => array( '.w9sme-main-nav .w9sme-main-menu-content>li>a' ),
            'units-extended' => true,
            'title'          => esc_html__( 'Menu item height', 'w9sme' ),
            'subtitle'       => esc_html__( 'Choose the nav items height.', 'w9sme' ),
            'default'        => array(
                'height' => '80px',
                'units'  => 'px',
            )
        ),
        
        array(
            'id'             => 'nav-sticky-item-height',
            'type'           => 'dimensions',
            'compiler'       => true,
            'width'          => false,
            'units'          => 'px',
            'output'         => array( '.is-sticky .w9sme-main-nav .w9sme-main-menu-content>li>a' ),
            'units-extended' => true,
            'title'          => esc_html__( 'Sticky menu item height', 'w9sme' ),
            'subtitle'       => esc_html__( 'Choose the nav items height.', 'w9sme' ),
            'default'        => array(
                'height' => '60px',
                'units'  => 'px',
            )
        ),
    
        array(
            'id'    => 'info-normal',
            'type'  => 'info',
            'title' => esc_html__( 'Sub Menu Setting', 'w9sme' )
        ),


        array(
            'id'          => 'header-submenu-font',
            'type'        => 'typography',
            'title'       => esc_html__( 'Menu font', 'w9sme' ),
            'subtitle'    => esc_html__( 'Specify the menu font properties.', 'w9sme' ),
            'google'      => true,
            'fonts'       => w9sme_get_preset_fonts(),
            'line-height' => false,
            'all_styles'  => true, // Enable all Google Font style/weight variations to be added to the page
            'color'       => false,
            'text-align'  => false,
            'font-style'  => true,
            'subsets'     => false,
            'font-size'   => true,
            'font-weight' => true,
            'output' => array('.w9sme-main-nav .w9sme-main-menu-content > .menu-item .sub-menu .menu-item > a'),
            'units'       => 'px', // Defaults to px
            'default'     => array(
                'font-family' => 'Poppins',
                'font-size' => '12px',
                'font-weight' => '400'
            ),
            'compiler' => false
        ),


        array(
            'id'       => 'menu-sub-appear-effect',
            'type'     => 'select',
            'title'    => esc_html__( 'Sub menu appear effect', 'w9sme' ),
            'subtitle' => esc_html__( 'Select sub menu appear effect.', 'w9sme' ),
            // Must provide key => value pairs for select options
            'options'  => array(
                'w9sme-effect-none'       => esc_html__( 'None', 'w9sme' ),
                'w9sme-effect-fade'       => esc_html__( 'Fade', 'w9sme' ),
                'w9sme-effect-fade-up'    => esc_html__( 'Fade In Up', 'w9sme' ),
                'w9sme-effect-fade-down'  => esc_html__( 'Fade In Down', 'w9sme' ),
                'w9sme-effect-fade-left'  => esc_html__( 'Fade In Left', 'w9sme' ),
                'w9sme-effect-fade-right' => esc_html__( 'Fade In Right', 'w9sme' ),
                'w9sme-effect-dropdown'   => esc_html__( 'Drop down', 'w9sme' ),
            ),
            'default'  => 'w9sme-effect-fade',
        ),
        
        array(
            'id'      => 'menu-sub-border',
            'type'    => 'border',
            'title'   => esc_html__( 'Sub menu border', 'w9sme' ),
            'output'  => array( '.w9sme-main-menu-content .w9sme-tree-menu .sub-menu, .w9sme-main-menu-content .w9sme-mega-menu >.sub-menu ' ),
            'all'     => false,
            'default' => array(
                'border-color'  => 'transparent',
                'border-style'  => 'solid',
                'border-top'    => '1px',
                'border-right'  => '1px',
                'border-bottom' => '1px',
                'border-left'   => '1px'
            )
        ),
        
        array(
            'id'             => 'menu-sub-padding',
            'type'           => 'spacing',
            'mode'           => 'padding',
            'units'          => 'px',
            'output'         => array( '.w9sme-main-menu-content .sub-menu' ),
            'units-extended' => true,
            'title'          => esc_html__( 'Sub menu padding', 'w9sme' ),
            'default'        => array(
                'padding-left'   => '0px',
                'padding-right'  => '0px',
                'padding-top'    => '15px',
                'padding-bottom' => '15px',
                'units'          => 'px',
            )
        ),
        
        array(
            'id'             => 'menu-sub-li-padding',
            'type'           => 'spacing',
            'mode'           => 'padding',
            'units'          => 'px',
            'output'         => array( '.w9sme-main-menu-content .sub-menu li' ),
            'units-extended' => true,
            'title'          => esc_html__( 'Sub menu item wrapper padding', 'w9sme' ),
            'default'        => array(
                'padding-left'   => '12px',
                'padding-right'  => '12px',
                'padding-top'    => '0',
                'padding-bottom' => '0',
                'units'          => 'px',
            )
        ),
        
        array(
            'id'      => 'menu-sub-background',
            'type'    => 'background',
            'title'   => esc_html__( 'Sub menu background', 'w9sme' ),
            'output'  => array( '.w9sme-main-menu-content .w9sme-tree-menu .sub-menu, .w9sme-main-menu-content .w9sme-mega-menu >.sub-menu ' ),
            'default' => array(
                'background-color' => '#222222',
            )
        ),
        
        array(
            'id'      => 'menu-sub-background-overlay',
            'type'    => 'color_rgba',
            'title'   => esc_html__( 'Sub menu background overlay', 'w9sme' ),
            'output'  => array( 'background-color' => '.w9sme-main-nav .w9sme-tree-menu .sub-menu:before, .w9sme-main-nav .w9sme-mega-menu > .sub-menu:before' ),
            'default' => array(
                'color' => '#000000',
                'alpha' => 0
            ),
        ),
        
        array(
            'id'             => 'menu-sub-item-size',
            'type'           => 'dimensions',
            'compiler'       => true,
            'units'          => 'px',
            'output'         => array( '.w9sme-main-menu-content .w9sme-tree-menu .sub-menu a' ),
            'units-extended' => true,
            'title'          => esc_html__( 'Sub menu item size', 'w9sme' ),
            'subtitle'       => esc_html__( 'Allow your users to choose the sub menu items size.<br/>Note: width of this option not available for mega menu full width items.', 'w9sme' ),
            'default'        => array(
                'width'  => '250px',
                'height' => '30px',
                'units'  => 'px',
            )
        ),
        
        array(
            'id'             => 'menu-sub-item-padding',
            'type'           => 'spacing',
            'mode'           => 'padding',
            'top'            => false,
            'bottom'         => false,
            'units'          => 'px',
            'output'         => array( '.w9sme-main-menu-content .sub-menu a' ),
            'units-extended' => true,
            'title'          => esc_html__( 'Sub menu item padding', 'w9sme' ),
            'default'        => array(
                'padding-left'  => '12px',
                'padding-right' => '12px',
                'units'         => 'px',
            )
        ),
        
        array(
            'id'       => 'menu-sub-item-color',
            'type'     => 'link_color',
            'title'    => esc_html__( 'Sub menu item color', 'w9sme' ),
            'output' => '.w9sme-main-menu-content .sub-menu a',
            'compiler' => true,
            'default'  => array(
                'regular' => '#ffffff',
                'hover'   => '#aaaaaa',
                'active'  => '#aaaaaa',
            )
        ),
        
        array(
            'id'       => 'menu-sub-item-hover-bg-color',
            'type'     => 'color_rgba',
            'compiler' => true,
            'output'   => array( 'background' => '.w9sme-main-menu-content .w9sme-tree-menu .sub-menu .menu-item:hover > a, .w9sme-main-menu-content .w9sme-mega-menu .sub-menu a:hover' ),
            'title'    => esc_html__( 'Sub menu item hover background color', 'w9sme' ),
            'default'  => array(
                'color' => '#ffffff',
                'alpha' => 0.1
            ),
        ),
        
        array(
            'id'       => 'menu-sub-item-hover-effect',
            'type'     => 'select',
            'title'    => esc_html__( 'Sub menu item hover effect', 'w9sme' ),
            'subtitle' => esc_html__( 'Select sub menu item hover effect.', 'w9sme' ),
            // Must provide key => value pairs for select options
            'options'  => array(
                'w9sme-menu-sub-item-hover-none'         => esc_html__( 'None', 'w9sme' ),
                'w9sme-menu-sub-item-hover-move-left'    => esc_html__( 'Move left', 'w9sme' ),
                'w9sme-menu-sub-item-hover-move-right'   => esc_html__( 'Move right', 'w9sme' ),
                'w9sme-menu-sub-item-hover-left-border'  => esc_html__( 'Left border', 'w9sme' ),
                'w9sme-menu-sub-item-hover-right-border' => esc_html__( 'Right border', 'w9sme' ),
            ),
            'default'  => 'w9sme-menu-sub-item-hover-none',
        ),
        
        // Mega menu
        array(
            'id'    => 'info-normal',
            'type'  => 'info',
            'title' => esc_html__( 'Mega Menu Setting', 'w9sme' )
        ),
        
        array(
            'id'       => 'menu-mega-separator-enable',
            'type'     => 'switch',
            'title'    => esc_html__( 'Enable mega menu separator', 'w9sme' ),
            'subtitle' => esc_html__( 'Enable separator between mega menu columns.', 'w9sme' ),
            'default'  => 1,
        ),
        
        array(
            'id'       => 'menu-mega-separator-color',
            'type'     => 'color_rgba',
            'title'    => esc_html__( 'Mega menu separator color', 'w9sme' ),
            'output'   => array( 'border-left-color' => '.w9sme-enable-mega-menu-separator .w9sme-mega-menu > .sub-menu > .menu-item' ),
            'default' => array(
                'color' => '#dddddd',
                'alpha' => 0.4
            ),
        ),
    )
);