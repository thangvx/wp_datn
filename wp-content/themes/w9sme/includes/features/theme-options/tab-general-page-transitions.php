<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-general-page-transitions.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$this->sections[] = array(
    'title'      => esc_html__( 'Transitions', 'w9sme' ),
    'desc'       => '',
    'icon'       => 'fa fa-spinner fa-spin',
    'subsection' => true,
    'fields'     => array(

        array(
            'id'       => 'loading-transitions',
            'type'     => 'switch',
            'title'    => esc_html__( 'Page transitions', 'w9sme' ),
            'subtitle' => esc_html__( 'Enable/disable page transitions.', 'w9sme' ),
            'desc'     => '',
            'default'  => 0
        ),
        array(
            'id'       => 'transition-random-number-1',
            'type'     => 'info',
            'desc'     => esc_html__( 'Loading logo.', 'w9sme' ),
            'required' => array( 'loading-transitions', '!=', array( '0' ) ),
        ),
        array(
            'id'       => 'loading-logo',
            'type'     => 'media',
            'url'      => true,
            'title'    => esc_html__( 'Loading logo', 'w9sme' ),
            'subtitle' => esc_html__( 'Upload loading logo.', 'w9sme' ),
            'required' => array( 'loading-transitions', '!=', array( '0' ) ),
        ),
        //loading text
        array(
            'id'       => 'transition-random-number-2',
            'type'     => 'info',
            'desc'     => esc_html__( 'Loading text.', 'w9sme' ),
            'required' => array( 'loading-transitions', '!=', array( '0' ) ),
        ),
        array(
            'id'       => 'loading-text',
            'type'     => 'text',
            'title'    => esc_html__( 'Loading text', 'w9sme' ),
            'subtitle' => esc_html__( 'Use a loading text.', 'w9sme' ),
            'desc'     => '',
            'required' => array( 'loading-transitions', '!=', array( '0' ) ),
        ),
        array(
            'id'          => 'loading-text-typo',
            'type'        => 'typography',
            'title'       => esc_html__( 'Loading text typography', 'w9sme' ),
            'google'      => true,
            'font-backup' => false,
            'output'      => array( 'h3.loading-text' ),
            'units'       => 'px',
            'subtitle'    => esc_html__( 'Typography option with each property can be called individually.', 'w9sme' ),
            'default'     => array(),
            'required'    => array( 'loading-transitions', '!=', array( '0' ) ),
        ),

        //Loading Animation
        array(
            'id'       => 'transition-random-number-3',
            'type'     => 'info',
            'desc'     => esc_html__( 'Loading animations.', 'w9sme' ),
            'required' => array( 'loading-transitions', '!=', array( '0' ) ),
        ),
        array(
            'id'       => 'loading-animation-bg-color',
            'type'     => 'color_rgba',
            'title'    => esc_html__( 'Loading background color', 'w9sme' ),
            'subtitle' => esc_html__( 'Set loading background color.', 'w9sme' ),
            'default'  => array(
                'color' => '#ffffff',
                'alpha' => 1,
                'rgba'  => 'rgba(255, 255, 255, 1)'
            ),
            'output'   => array( 'background-color' => '.w9sme-page-transitions-wrapper, .w9sme-page-transitions' ),
            'validate' => 'colorrgba',
            'required' => array( 'loading-transitions', '!=', array( '0' ) ),
        ),

        array(
            'id'       => 'loading-animation',
            'type'     => 'select',
            'title'    => esc_html__( 'Loading animation', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose type of preload animation.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'none'          => esc_html__( 'No animation', 'w9sme' ),
                'rotating'      => esc_html__( 'Rotating Plane', 'w9sme' ),
                'double-bounce' => esc_html__( 'Double Bounce', 'w9sme' ),
                'wave'          => esc_html__( 'Wave', 'w9sme' ),
                'cube'          => esc_html__( 'Wandering Cube', 'w9sme' ),
                'pulse'         => esc_html__( 'Pulse', 'w9sme' ),
                'chasing-dots'  => esc_html__( 'Chasing Dots', 'w9sme' ),
                'three-bounce'  => esc_html__( 'Three Bounce', 'w9sme' ),
                'circle'        => esc_html__( 'Circle', 'w9sme' ),
                'folding-cube'  => esc_html__( 'Cube Grid', 'w9sme' ),
                'fading-circle' => esc_html__( 'Fading Circle', 'w9sme' ),
            ),
            'default'  => 'none',
            'required' => array( 'loading-transitions', '!=', array( '0' ) ),
        ),

        //Spinner Color
        array(
            'id'       => 'spinner-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Spinner color', 'w9sme' ),
            'subtitle' => esc_html__( 'Pick a spinner color.', 'w9sme' ),
            'default'  => '',
            'validate' => 'color',
            'output'   => array( 'background-color' => '.sk-spinner-pulse,.sk-rotating-plane,.sk-double-bounce .sk-child,.sk-wave .sk-rect,.sk-chasing-dots .sk-child,.sk-three-bounce .sk-child,.sk-circle .sk-child:before,.sk-fading-circle .sk-circle:before,.sk-folding-cube .sk-cube:before, .circle-2, .circle-3, .circle-4, .circle-5, .circle-6' ),
            'required' => array( 'loading-animation', 'not_empty_and', array( 'none' ) ),
        ),
    )
);