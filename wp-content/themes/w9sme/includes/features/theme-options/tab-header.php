<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-header.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/*
 * Header Section
*/
$this->sections[] = array(
    'title'  => esc_html__( 'Header', 'w9sme' ),
    'desc'   => esc_html__( 'Change the header section configuration.', 'w9sme' ),
    'icon'   => 'el-icon-compass',
    'fields' => array(
        array(
            'id'        => 'header-override-default',
            'type'      => 'select',
            'multi'     => true,
            'title'     => esc_html__( 'Override default header settings in', 'w9sme' ),
            'subtitle'  => esc_html__( 'Choose which template you need to override the default settings in here.', 'w9sme' ),
            'desc'      => esc_html__( 'The tabs will appear after you save the change.', 'w9sme' ) .
                '<br />  <strong style="color: red;">' . esc_html__( 'Notice 1: If the page doesn\'t auto refresh, please refresh the page after changing and saving this option.', 'w9sme' ) . '</strong> </br> <strong style="color: red;">' .
                esc_html__( 'Notice 2: If an item is removed, the options will be saved automatically after the page auto refresh, please wait a bit.', 'w9sme' ) . '</strong>',
            'options'   => w9sme_get_template_prefix( 'options_field' ),
            'default'   => array(),
            'ajax_save' => false
//            'compiler' => true,
//            'reload_on_change' => true
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__('Header Settings', 'w9sme' ),
        ),
        
        array(
            'id'       => 'header-enable',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Enable header?', 'w9sme' ),
            'subtitle' => esc_html__( 'Show or hide header.', 'w9sme' ),
            'options'  => array(
                'on'  => esc_html__( 'On', 'w9sme' ),
                'off' => esc_html__( 'Off', 'w9sme' )
            ),
            'default'  => 'on',
        ),
        
        
        array(
            'id'       => 'nav-content',
            'type'     => 'select',
            'title'    => esc_html__( 'Select menu for header', 'w9sme' ),
            'subtitle' => esc_html__( 'Select menu for page, if you leave it empty it will get primary menu of your site.', 'w9sme' ),
            // Must provide key => value pairs for select options
            'options'  => w9sme_get_menu_list(),
            'default'  => 'primary',
        ),
        
        
        array(
            'id'      => 'nav-module-desktop',
            'type'    => 'sorter',
            'title'   => esc_html__( 'Nav module in desktop', 'w9sme' ),
            'desc'    => esc_html__( 'Select nav module appear in normal desktop nav, you also use this for sorter position of module in nav.', 'w9sme' ),
            'options' => array(
                'enabled'  => array(
                    'logo'      => esc_html__( 'Logo', 'w9sme' ),
                    'main-menu' => esc_html__( 'Main Menu', 'w9sme' ),
                    'search'    => esc_html__( 'Search', 'w9sme' ),
                    'cart'      => esc_html__( 'Cart', 'w9sme' ),
                ),
                'disabled' => array(
                    'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
                    'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
                    'popup'            => esc_html__( 'Popup', 'w9sme' ),
                    'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
                )
            ),
        ),
        
        array(
            'id'       => 'nav-module-desktop-sticky',
            'type'     => 'sorter',
            'title'    => esc_html__( 'Nav module in desktop sticky nav', 'w9sme' ),
            'subtitle' => esc_html__( 'Select nav module appear in sticky nav in desktop screen, you also use this for sorter position of module in nav.', 'w9sme' ),
            'options'  => array(
                'enabled'  => array(
                    'logo'      => esc_html__( 'Logo', 'w9sme' ),
                    'main-menu' => esc_html__( 'Main Menu', 'w9sme' ),
                    'search'    => esc_html__( 'Search', 'w9sme' ),
                    'cart'      => esc_html__( 'Cart', 'w9sme' ),
                ),
                'disabled' => array(
                    'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
                    'popup'            => esc_html__( 'Popup', 'w9sme' ),
                    'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
                    'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
                )
            ),
        ),
        
        array(
            'id'      => 'nav-module-mobile',
            'type'    => 'sorter',
            'title'   => esc_html__( 'Nav module in mobile', 'w9sme' ),
            'desc'    => esc_html__( 'Select nav module appear in sticky nav in desktop screen, you also use this for sorter position of module in nav.', 'w9sme' ),
            'options' => array(
                'enabled'  => array(
                    'logo'            => esc_html__( 'Logo', 'w9sme' ),
                    'toggle-leftzone' => esc_html__( 'Toggle Left Zone', 'w9sme' ),
                ),
                'disabled' => array(
                    'search'           => esc_html__( 'Search', 'w9sme' ),
                    'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
                    'cart'             => esc_html__( 'Cart', 'w9sme' ),
                    'popup'            => esc_html__( 'Popup', 'w9sme' ),
                    'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
                )
            ),
        ),
        
        array(
            'id'      => 'nav-module-desktop-biggest',
            'type'    => 'select',
            'title'   => esc_html__( 'Biggest module in desktop screen', 'w9sme' ),
            'options' => array(
                'logo'             => esc_html__( 'Logo', 'w9sme' ),
                'main-menu'        => esc_html__( 'Main Menu', 'w9sme' ),
                'cart'             => esc_html__( 'Cart', 'w9sme' ),
                'search'           => esc_html__( 'Search', 'w9sme' ),
                'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
                'popup'            => esc_html__( 'Popup', 'w9sme' ),
                'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
                'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
            ),
            'default' => 'main-menu'
        ),
        
        array(
            'id'      => 'nav-module-desktop-biggest-align',
            'type'    => 'button_set',
            'title'   => esc_html__( 'Biggest module in desktop screen align', 'w9sme' ),
            'options' => array(
                'nav-item-desktop-left'   => esc_html__( 'Left', 'w9sme' ),
                'nav-item-desktop-center' => esc_html__( 'Center', 'w9sme' ),
                'nav-item-desktop-right'  => esc_html__( 'Right', 'w9sme' ),
            ),
            'default' => 'nav-item-desktop-right'
        ),
        
        array(
            'id'      => 'nav-module-mobile-biggest',
            'type'    => 'select',
            'title'   => esc_html__( 'Biggest module in mobile screen', 'w9sme' ),
            'options' => array(
                'logo'             => esc_html__( 'Logo', 'w9sme' ),
                'cart'             => esc_html__( 'Cart', 'w9sme' ),
                'search'           => esc_html__( 'Search', 'w9sme' ),
                'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
                'popup'            => esc_html__( 'Popup', 'w9sme' ),
                'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
                'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
            ),
            'default' => 'logo'
        ),
        
        array(
            'id'      => 'nav-module-mobile-biggest-align',
            'type'    => 'button_set',
            'title'   => esc_html__( 'Biggest module in mobile screen align', 'w9sme' ),
            'options' => array(
                'nav-item-mobile-left'   => esc_html__( 'Left', 'w9sme' ),
                'nav-item-mobile-center' => esc_html__( 'Center', 'w9sme' ),
                'nav-item-mobile-right'  => esc_html__( 'Right', 'w9sme' ),
            ),
            'default' => 'nav-item-mobile-left'
        ),
    
    ), // #fields
);