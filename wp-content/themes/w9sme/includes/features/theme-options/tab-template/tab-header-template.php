<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-header.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
//var_dump('.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body .w9sme-main-menu-content>li>a:hover,' . '.main-header.' . $template . ' ' .  '.is-sticky .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a:hover');
$content_template_list = w9sme_get_content_template_list();
$template_name         = w9sme_get_template_prefix( 'name', $template );
$template_name_lower   = strtolower( $template_name );

$this->sections[] = array(
	'title'      => $template_name . esc_html__( ' Header', 'w9sme' ),
	'desc'       => sprintf( esc_html__( 'Change the %s header section configuration.', 'w9sme' ), $template_name_lower ),
	'icon'       => 'fa fa-plus-circle',
	'subsection' => true,
	'fields'     => array(
		
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Header Settings', 'w9sme' ),
		),
		
		array(
			'id'       => $template . 'header-enable',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Enable header?', 'w9sme' ),
			'subtitle' => esc_html__( 'Show or hide header.', 'w9sme' ),
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' )
			),
			'default'  => 'on',
		),
		
		array(
			'id'       => $template . 'nav-content',
			'type'     => 'select',
			'title'    => esc_html__( 'Select menu for header', 'w9sme' ),
			'subtitle' => esc_html__( 'Select menu for page, if you leave it empty it will get primary menu of your site.', 'w9sme' ),
			// Must provide key => value pairs for select options
			'options'  => w9sme_get_menu_list(),
			'default'  => 'primary',
		),
		
		array(
			'id'      => $template . 'nav-module-desktop',
			'type'    => 'sorter',
			'title'   => esc_html__( 'Nav module in desktop', 'w9sme' ),
			'desc'    => esc_html__( 'Select nav module appear in normal desktop nav, you also use this for sorter position of module in nav.', 'w9sme' ),
			'options' => array(
				'enabled'  => array(
					'logo'      => esc_html__( 'Logo', 'w9sme' ),
					'main-menu' => esc_html__( 'Main Menu', 'w9sme' ),
					'search'    => esc_html__( 'Search', 'w9sme' ),
					'cart'      => esc_html__( 'Cart', 'w9sme' ),
				),
				'disabled' => array(
					'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
					'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
					'popup'            => esc_html__( 'Popup', 'w9sme' ),
					'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
				)
			),
		),
		
		array(
			'id'       => $template . 'nav-module-desktop-sticky',
			'type'     => 'sorter',
			'title'    => esc_html__( 'Nav module in desktop sticky nav', 'w9sme' ),
			'subtitle' => esc_html__( 'Select nav module appear in sticky nav in desktop screen, you also use this for sorter position of module in nav.', 'w9sme' ),
			'options'  => array(
				'enabled'  => array(
					'logo'      => esc_html__( 'Logo', 'w9sme' ),
					'main-menu' => esc_html__( 'Main Menu', 'w9sme' ),
					'search'    => esc_html__( 'Search', 'w9sme' ),
					'cart'      => esc_html__( 'Cart', 'w9sme' ),
				),
				'disabled' => array(
					'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
					'popup'            => esc_html__( 'Popup', 'w9sme' ),
					'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
					'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
				)
			),
		),
		
		array(
			'id'      => $template . 'nav-module-mobile',
			'type'    => 'sorter',
			'title'   => esc_html__( 'Nav module in mobile', 'w9sme' ),
			'desc'    => esc_html__( 'Select nav module appear in sticky nav in desktop screen, you also use this for sorter position of module in nav.', 'w9sme' ),
			'options' => array(
				'enabled'  => array(
					'logo'            => esc_html__( 'Logo', 'w9sme' ),
					'toggle-leftzone' => esc_html__( 'Toggle Left Zone', 'w9sme' ),
				),
				'disabled' => array(
					'search'           => esc_html__( 'Search', 'w9sme' ),
					'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
					'cart'             => esc_html__( 'Cart', 'w9sme' ),
					'popup'            => esc_html__( 'Popup', 'w9sme' ),
					'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
				)
			),
		),
		
		array(
			'id'      => $template . 'nav-module-desktop-biggest',
			'type'    => 'select',
			'title'   => esc_html__( 'Biggest module in desktop screen', 'w9sme' ),
			'options' => array(
				'logo'             => esc_html__( 'Logo', 'w9sme' ),
				'main-menu'        => esc_html__( 'Main Menu', 'w9sme' ),
				'cart'             => esc_html__( 'Cart', 'w9sme' ),
				'search'           => esc_html__( 'Search', 'w9sme' ),
				'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
				'popup'            => esc_html__( 'Popup', 'w9sme' ),
				'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
				'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
			),
			'default' => 'main-menu'
		),
		
		array(
			'id'      => $template . 'nav-module-desktop-biggest-align',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Biggest module in desktop screen align', 'w9sme' ),
			'options' => array(
				'nav-item-desktop-left'   => esc_html__( 'Left', 'w9sme' ),
				'nav-item-desktop-center' => esc_html__( 'Center', 'w9sme' ),
				'nav-item-desktop-right'  => esc_html__( 'Right', 'w9sme' ),
			),
			'default' => 'nav-item-desktop-right'
		),
		
		array(
			'id'      => $template . 'nav-module-mobile-biggest',
			'type'    => 'select',
			'title'   => esc_html__( 'Biggest module in mobile screen', 'w9sme' ),
			'options' => array(
				'logo'             => esc_html__( 'Logo', 'w9sme' ),
				'cart'             => esc_html__( 'Cart', 'w9sme' ),
				'search'           => esc_html__( 'Search', 'w9sme' ),
				'custom-content'   => esc_html__( 'Custom Content', 'w9sme' ),
				'popup'            => esc_html__( 'Popup', 'w9sme' ),
				'toggle-leftzone'  => esc_html__( 'Toggle Left Zone', 'w9sme' ),
				'toggle-rightzone' => esc_html__( 'Toggle Right Zone', 'w9sme' ),
			),
			'default' => 'logo'
		),
		
		array(
			'id'      => $template . 'nav-module-mobile-biggest-align',
			'type'    => 'button_set',
			'title'   => esc_html__( 'Biggest module in mobile screen align', 'w9sme' ),
			'options' => array(
				'nav-item-mobile-left'   => esc_html__( 'Left', 'w9sme' ),
				'nav-item-mobile-center' => esc_html__( 'Center', 'w9sme' ),
				'nav-item-mobile-right'  => esc_html__( 'Right', 'w9sme' ),
			),
			'default' => 'nav-item-mobile-left'
		),
		
		// !!!!!!!!!!!!!!!!!!!!
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Header Style.', 'w9sme' )
		),
		
		array(
			'id'       => $template . 'nav-width',
			'type'     => 'select',
			'title'    => esc_html__( 'Header width', 'w9sme' ),
			'subtitle' => esc_html__( 'Select common header width in the list.', 'w9sme' ),
			'output'   => '.main-header.' . $template . ' ' . '.w9sme-nav-body .w9sme-nav-logo-wrapper',
			'options'  => array(
				'container'               => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'           => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid'         => esc_html__( 'Container Fluid', 'w9sme' ),
				'container-nav-fullwidth' => esc_html__( 'Full Width', 'w9sme' ),
			),
			'default'  => 'container',
		),
		
		array(
			'id'       => $template . 'nav-boxed-enabled',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Enable boxed mode', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable boxed mode.', 'w9sme' ),
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'default'  => 'off',
			'required' => array(
				array( $template . 'nav-width', '!=', 'container-fluid' ),
				array( $template . 'nav-width', '!=', 'container-nav-fullwidth' ),
			)
		),
		
		array(
			'id'       => $template . 'nav-occupy-spacing',
			'type'     => 'button_set',
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'title'    => esc_html__( 'Nav occupy space?', 'w9sme' ),
			'subtitle' => esc_html__( 'Nav occupy page space or not.', 'w9sme' ),
			'default'  => 'on',
		),
		
		array(
			'id'             => $template . 'nav-overlay-offset-top',
			'type'           => 'spacing',
			'output'         => array( '.main-header.' . $template . ' ' . '.w9sme-main-header.w9sme-menu-overlay-wrapper' ),
			'mode'           => 'absolute',
			'top'            => true,
			'left'           => false,
			'right'          => false,
			'bottom'         => false,
			'units'          => array( 'px' ),
			'units_extended' => true,
			'title'          => esc_html__( 'Main nav offset top', 'w9sme' ),
			'subtitle'       => esc_html__( 'Main nav offset top in px unit.', 'w9sme' ),
			'default'        => array(),
			'required'       => array(
				$template . 'nav-occupy-spacing',
				'=',
				'off'
			)
		),
		
		array(
			'id'       => $template . 'nav-sticky',
			'type'     => 'button_set',
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'title'    => esc_html__( 'Enable sticky menu', 'w9sme' ),
			'subtitle' => esc_html__( 'Nav will be fixed on top of window when scroll down position bigger than window height.', 'w9sme' ),
			'default'  => 'on',
		),
		
		array(
			'id'       => $template . 'nav-headroom',
			'type'     => 'button_set',
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'title'    => esc_html__( 'Enable sticky headroom', 'w9sme' ),
			'subtitle' => esc_html__( 'Hide sticky menu when scroll down, show it when scroll up.', 'w9sme' ),
			'default'  => 'off',
			'required' => array(
				$template . 'nav-sticky',
				'=',
				'on'
			)
		),
		
		array(
			'id'       => $template . 'header-nav-breakpoint',
			'height'   => false,
			'type'     => 'dimensions',
			'units'    => 'px',
			'default'  => array(
				'width' => '992px',
				'unit'  => 'px'
			),
			'title'    => esc_html__( 'Desktop - mobile breakpoint', 'w9sme' ),
			'subtitle' => esc_html__( 'Desktop nav and desktop nav hide if screen width lower than breakpoint, Mobile nav and mobile module hide if screen greater than breakpoint.', 'w9sme' )
		),
		
		array(
			'id'       => $template . 'nav-item-separator',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Enable nav item separator', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable separator between nav items. This option work best if header width id fullwidth.', 'w9sme' ),
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'default'  => 'off',
		),
		
		array(
			'id'       => $template . 'nav-item-separator-color',
			'title'    => esc_html__( 'Nav item separator color', 'w9sme' ),
			'subtitle' => esc_html__( 'Define nav item separator color.', 'w9sme' ),
			'output'   => array(
				'border-color' => '.main-header.' . $template . ' ' . '.w9sme-nav-enable-separator .w9sme-nav-item, ' . '.main-header.' . $template . ' ' . '.w9sme-nav-enable-separator .w9sme-nav-body-content'
			),
			'type'     => 'color_rgba',
			'default'  => array(
				'color' => '#888',
				'alpha' => '0.4',
			),
			'required' => array(
				$template . 'nav-item-separator',
				'=',
				'on'
			)
		),
		
		array(
			'id'       => $template . 'nav-item-padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => 'px',
			'top'      => false,
			'bottom'   => false,
			'title'    => esc_html__( 'Nav item padding', 'w9sme' ),
			'subtitle' => esc_html__( 'Define nav item padding if have separator.', 'w9sme' ),
			'output'   => array( '.main-header.' . $template . ' ' . '.w9sme-nav-enable-separator .w9sme-nav-item' ),
			'default'  => array(
				'padding-left'  => '20px',
				'padding-right' => '20px',
				'unit'          => 'px'
			),
			'required' => array(
				$template . 'nav-item-separator',
				'=',
				'on'
			)
		),
		
		array(
			'id'       => $template . 'nav-item-color',
			'output'   => array( 'color' => '.main-header.' . $template . ' ' . '.w9sme-nav-body' ),
			'type'     => 'color',
			'title'    => esc_html__( 'Nav item color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav color.', 'w9sme' ),
			'default'  => '#ffffff',
		),
		
		array(
			'id'       => $template . 'nav-item-link-color',
			'output'   => array( '.main-header.' . $template . ' ' . '.w9sme-nav-body .w9sme-main-menu-content>li>a, ' . '.main-header.' . $template . ' ' . '.w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a' ),
			'type'     => 'link_color',
			'title'    => esc_html__( 'Nav item link color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav link color.', 'w9sme' ),
			'desc'     => esc_html__( 'Hover, active color available for link in nav only.', 'w9sme' ),
			'default'  => array(
				'regular' => '#222',
				'hover'   => '#EB6C6C',
				'active'  => '#EB6C6C',
			)
		),
		
		array(
			'id'       => $template . 'nav-link-hover-background',
			'type'     => 'color_rgba',
			'output'   => array( 'background-color' => '.main-header.' . $template . ' ' . '.w9sme-nav-body .w9sme-main-menu-content>li>a:hover, ' . '.main-header.' . $template . ' ' . '.w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a:hover' ),
			'title'    => esc_html__( 'Main nav link hover background', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose link background color when hover.', 'w9sme' ),
			'default'  => '',
			'required' => array(
				$template . 'nav-menu-item-hover-effect',
				'=',
				array(
					'w9sme-menu-item-hover-none',
					'w9sme-menu-item-hover-underline',
					'w9sme-menu-item-hover-bracket'
				)
			),
		),
		
		array(
			'id'       => $template . 'nav-menu-item-hover-effect',
			'type'     => 'select',
			'title'    => esc_html__( 'Nav menu item hover effect', 'w9sme' ),
			'subtitle' => esc_html__( 'Select menu item (link in menu only) hover effect.', 'w9sme' ),
			'options'  => array(
				'w9sme-menu-item-hover-none'              => esc_html__( 'None', 'w9sme' ),
				'w9sme-menu-item-hover-underline'         => esc_html__( 'Underline', 'w9sme' ),
				'w9sme-menu-item-hover-bracket'           => esc_html__( 'Brackets', 'w9sme' ),
				'w9sme-menu-item-hover-magic-line-top'    => esc_html__( 'Magic Line Top', 'w9sme' ),
				'w9sme-menu-item-hover-magic-line-bottom' => esc_html__( 'Magic Line Bottom', 'w9sme' ),
			),
			'default'  => 'w9sme-menu-item-hover-underline',
		),
		
		array(
			'id'      => $template . 'nav-background',
			'type'    => 'background',
			'output'  => array( '.main-header.' . $template . ' ' . '.w9sme-nav-body' ),
			'title'   => esc_html__( 'Main nav background', 'w9sme' ),
			'default' => array(
				'background-color' => 'transparent',
			)
		),
		
		array(
			'id'      => $template . 'nav-background-overlay',
			'type'    => 'color_rgba',
			'title'   => esc_html__( 'Main nav background overlay', 'w9sme' ),
			'output'  => array( 'background-color' => '.main-header.' . $template . ' ' . '.w9sme-nav-body:before' ),
			// See Notes below about these lines.
			//'output'    => array('background-color' => '.site-header'),
			'default' => array(
				'color' => '#f2f2f2',
				'alpha' => 1
			),
		),
		
		array(
			'id'      => $template . 'nav-border',
			'type'    => 'border',
			'title'   => esc_html__( 'Nav border', 'w9sme' ),
			'output'  => array( '.main-header.' . $template . ' ' . '.w9sme-nav-body' ),
			'all'     => false,
			'color'   => false,
			'default' => array(
				'border-style'  => 'solid',
				'border-top'    => '0',
				'border-right'  => '0',
				'border-bottom' => '1px',
				'border-left'   => '0'
			)
		),
		
		array(
			'id'       => $template . 'nav-border-color',
			'title'    => esc_html__( 'Nav border color', 'w9sme' ),
			'subtitle' => esc_html__( 'Specify the nav border color.', 'w9sme' ),
			'output'   => array(
				'border-color' => '.main-header.' . $template . ' ' . '.w9sme-nav-body'
			),
			'type'     => 'color_rgba',
			'default'  => array(
				'color' => '#888',
				'alpha' => '0.4',
			),
		),
		
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Header Sticky Style.', 'w9sme' )
		),
		
		array(
			'id'       => $template . 'nav-sticky-item-color',
			'output'   => array( 'color' => '.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body' ),
			'type'     => 'color',
			'title'    => esc_html__( 'Sticky nav item color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav color.', 'w9sme' ),
			'default'  => '#ffffff',
		),
		
		array(
			'id'       => $template . 'nav-sticky-item-link-color',
			'type'     => 'link_color',
			'title'    => esc_html__( 'Sticky nav item link color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav link color.', 'w9sme' ),
			'output'   => array( '.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body .w9sme-main-menu-content>li>a, ' . '.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a' ),
			'desc'     => esc_html__( 'Hover, active color available for link in nav only.', 'w9sme' ),
			'default'  => array(
				'regular' => '#222',
				'hover'   => '#EB6C6C',
				'active'  => '#EB6C6C',
			)
		),
		
		array(
			'id'       => $template . 'nav-sticky-link-hover-background',
			'type'     => 'color_rgba',
			'output'   => array( 'background-color' => '.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body .w9sme-main-menu-content>li>a:hover,' . '.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a:hover' ),
			'title'    => esc_html__( 'Sticky nav link hover background', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose link background color when hover.', 'w9sme' ),
			'default'  => '',
//		    'validate' => 'color',
		),
		
		array(
			'id'      => $template . 'nav-sticky-background',
			'type'    => 'background',
			'output'  => array( '.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body' ),
			'title'   => esc_html__( 'Sticky nav background', 'w9sme' ),
			'default' => array(
				'background-color' => 'transparent',
			)
		),
		
		array(
			'id'      => $template . 'nav-sticky-background-overlay',
			'type'    => 'color_rgba',
			'title'   => esc_html__( 'Sticky nav background overlay', 'w9sme' ),
			'output'  => array( 'background-color' => '.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body:before' ),
			'default' => array(
				'color' => '#f2f2f2',
				'alpha' => 1
			),
		),
		
		array(
			'id'      => $template . 'nav-sticky-border',
			'type'    => 'border',
			'title'   => esc_html__( 'Sticky nav border', 'w9sme' ),
			'output'  => array( '.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body' ),
			'all'     => false,
			'color'   => false,
			'default' => array(
				'border-style'  => 'solid',
				'border-top'    => '0',
				'border-right'  => '0',
				'border-bottom' => '0',
				'border-left'   => '0'
			)
		),
		
		array(
			'id'       => $template . 'nav-sticky-border-color',
			'title'    => esc_html__( 'Nav border color', 'w9sme' ),
			'subtitle' => esc_html__( 'Specify the sticky nav border color.', 'w9sme' ),
			'output'   => array(
				'border-color' => '.main-header.' . $template . ' ' . '.is-sticky .w9sme-nav-body'
			),
			'type'     => 'color_rgba',
			'default'  => array(
				'color' => '#888',
				'alpha' => '0.4',
			),
		),
		
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Logo', 'w9sme' )
		),
		
		array(
			'id'       => $template . 'nav-logo-select',
			'type'     => 'select',
			'compiler' => false,
			'title'    => esc_html__( 'Select logo', 'w9sme' ),
			'subtitle' => esc_html__( 'Select logo from logo list in "Logo & Favicon" tab.', 'w9sme' ),
			'options'  => array(
				'logo'          => esc_html__( 'Basic logo', 'w9sme' ),
				'logo-option-1' => esc_html__( 'Logo option 1', 'w9sme' ),
				'logo-option-2' => esc_html__( 'Logo option 2', 'w9sme' ),
				'logo-option-3' => esc_html__( 'Logo option 3', 'w9sme' ),
			),
			'default'  => 'logo',
		),
		
		array(
			'id'             => $template . 'nav-logo-height',
			'type'           => 'dimensions',
			'compiler'       => true,
			'width'          => false,
			'units'          => 'px',
			'output'         => array( '.main-header.' . $template . ' ' . '.w9sme-main-nav .w9sme-nav-logo-wrapper img' ),
			'units-extended' => true,
			'title'          => esc_html__( 'Nav logo height', 'w9sme' ),
			'subtitle'       => esc_html__( 'Allow your users to choose the nav items height.', 'w9sme' ),
			'default'        => array(
				'height' => '40px',
				'units'  => 'px',
			)
		),
		
		array(
			'id'       => $template . 'nav-sticky-logo-select',
			'type'     => 'select',
			'compiler' => false,
			'title'    => esc_html__( 'Select logo for sticky nav', 'w9sme' ),
			'subtitle' => esc_html__( 'Select logo from logo list in "Logo & Favicon" tab.', 'w9sme' ),
			'options'  => array(
				'logo'          => esc_html__( 'Basic logo', 'w9sme' ),
				'logo-option-1' => esc_html__( 'Logo option 1', 'w9sme' ),
				'logo-option-2' => esc_html__( 'Logo option 2', 'w9sme' ),
				'logo-option-3' => esc_html__( 'Logo option 3', 'w9sme' ),
			),
			'default'  => 'logo',
		),
		
		array(
			'id'             => $template . 'nav-sticky-logo-height',
			'type'           => 'dimensions',
			'compiler'       => true,
			'width'          => false,
			'units'          => 'px',
			'output'         => array( '.main-header.' . $template . ' ' . '.is-sticky .w9sme-main-nav .w9sme-nav-logo-wrapper img' ),
			'units-extended' => true,
			'title'          => esc_html__( 'Sticky nav logo height', 'w9sme' ),
			'subtitle'       => esc_html__( 'Allow your users to choose the nav items height.', 'w9sme' ),
			'default'        => array(
				'height' => '40px',
				'units'  => 'px',
			)
		),
	
	
	), // #fields
);