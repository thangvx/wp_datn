<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-footer-template.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$content_template_list = w9sme_get_content_template_list();
$template_name         = w9sme_get_template_prefix( 'name', $template );
$template_name_lower   = strtolower( $template_name );

$this->sections[] = array(
    'title'      => $template_name . esc_html__( ' Footer', 'w9sme' ),
    'desc'       => sprintf( esc_html__( 'Change the %s footer section configuration.', 'w9sme' ), $template_name_lower ),
    'icon'       => 'fa fa-plus-circle',
    'subsection' => true,
    'fields'     => array(
        array(
            'id'       => $template . 'footer-enable',
            'type'     => 'select',
            'title'    => esc_html__( 'Footer template', 'w9sme' ),
            'subtitle' => esc_html__( 'If on, this layout part will be displayed.', 'w9sme' ),
            'options'  => array(
                'simple' => esc_html__( 'Simple', 'w9sme' ),
                'custom' => esc_html__( 'Content Template', 'w9sme' ),
                'off'    => esc_html__( 'Off', 'w9sme' )
            ),
            'default'  => 'simple',
        ),

        array(
            'id'       => $template . 'footer-content-template',
            'type'     => 'select',
            'title'    => esc_html__( 'Footer content template', 'w9sme' ),
            'options'  => $content_template_list,
            'desc'     => esc_html__( 'Select content template for footer.', 'w9sme' ),
            'default'  => '',
            'required' => array( $template . 'footer-enable', '=', array( 'custom' ) )
        ),
        
        array(
            'id'       => $template . 'footer-copyright-text',
            'type'     => 'textarea',
            'title'    => esc_html__( 'Copyright text', 'w9sme' ),
            'subtitle' => esc_html__( 'Enter the copyright text for the footer.', 'w9sme' ),
            'required' => array( $template . 'footer-enable', '=', 'simple' ),
            'default'  => __( '<span class="light-color s-font fs-italic">Creative WordPress Theme made by 9WPThemes</span>', 'w9sme' )
        ),
        
        array(
            'id'             => $template . 'footer-padding',
            'type'           => 'spacing',
            'mode'           => 'padding',
            'units'          => 'px',
            'units_extended' => 'false',
            'title'          => esc_html__( 'Padding top/bottom', 'w9sme' ),
            'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
            'desc'           => esc_html__( 'If you would like to override the default footer top/bottom padding, then you can do so here.', 'w9sme' ),
            'left'           => false,
            'right'          => false,
            'default'        => array(
	            'padding-top'    => '18px',
	            'padding-bottom' => '18px',
                'units'          => 'px',
            ),
            'required'       => array( $template . 'footer-enable', '=', 'simple' ),
            'output'         => array( '.site-footer.' . $template . ' .footer-content-area' )
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'title'    => esc_html__( 'Footer Colors', 'w9sme' ),
            'required' => array( $template . 'footer-enable', '=', 'simple' ),
        ),
        array(
            'id'       => $template . 'footer-colors',
            'type'     => 'select',
            'title'    => esc_html__( 'Footer colors style', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose footer color style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'gray'   => esc_html__( 'Gray', 'w9sme' ),
                'light'  => esc_html__( 'Light', 'w9sme' ),
                'dark'   => esc_html__( 'Dark', 'w9sme' ),
                'custom' => esc_html__( 'Custom', 'w9sme' ),
            ),
            'default'  => 'gray',
            'compiler' => true,
            'required' => array( $template . 'footer-enable', '=', 'simple' ),
        ),
        
        array(
            'id'       => $template . 'footer-background-color',
            'type'     => 'color_rgba',
            'title'    => esc_html__( 'Footer background color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                $template . 'footer-colors', '=', 'custom'
            ),
            'default'  => array(
                'color' => '#222',
                'alpha' => '1',
                'rgba'  => 'rgba(0, 0, 0, 1)'
            ),
            'compiler' => true
        ),
        
        array(
            'id'       => $template . 'footer-text-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer text color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                $template . 'footer-colors', '=', 'custom'
            ),
            'default'  => '#868686',
            'compiler' => true
        ),
        
        array(
            'id'       => $template . 'footer-heading-text-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer heading text color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                $template . 'footer-colors', '=', 'custom'
            ),
            'default'  => '#fff',
            'compiler' => true
        ),
        
        array(
            'id'       => $template . 'footer-link-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer link color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                $template . 'footer-colors', '=', 'custom'
            ),
            'default'  => '#868686',
            'compiler' => true
        ),
        
        array(
            'id'       => $template . 'footer-link-hover-color',
            'type'     => 'color',
            'title'    => esc_html__( 'Footer link hover color', 'w9sme' ),
            'subtitle' => '',
            'required' => array(
                $template . 'footer-colors', '=', 'custom'
            ),
            'default'  => '#fff',
            'compiler' => true
        )
    
    ) // #fields
);