<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-title-template.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$content_template_list = w9sme_get_content_template_list();
$template_name         = w9sme_get_template_prefix( 'name', $template );
$template_name_lower   = strtolower( $template_name );

$this->sections[] = array(
	'title'      => $template_name . esc_html__( ' Page Title', 'w9sme' ),
	'desc'       => sprintf( esc_html__( 'Change the %s title section configuration.', 'w9sme' ), $template_name_lower ),
	'icon'       => 'fa fa-plus-circle',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => $template_name . esc_html__( ' Title Settings', 'w9sme' ),
		),

		array(
			'id'       => $template . 'page-title-enable',
			'type'     => 'select',
			'title'    => esc_html__( 'Page title template', 'w9sme' ),
			'subtitle' => esc_html__( 'If on, this layout part will be displayed.', 'w9sme' ),
			'options'  => array(
				'simple' => esc_html__( 'Simple', 'w9sme' ),
				'custom' => esc_html__( 'Content Template', 'w9sme' ),
				'off'    => esc_html__( 'Off', 'w9sme' )
			),
			'default'  => 'simple',
		),

		array(
			'id'       => $template . 'page-title-content-template',
			'type'     => 'select',
			'title'    => esc_html__( 'Page title content template', 'w9sme' ),
			'options'  => $content_template_list,
			'desc'     => esc_html__( 'Select content template for page title.', 'w9sme' ),
			'default'  => '',
			'required' => array( $template . 'page-title-enable', '=', array( 'custom' ) )
		),


		array(
			'id'       => $template . 'page-title-custom',
			'type'     => 'text',
			'title'    => esc_html__( 'Custom title', 'w9sme' ),
			'subtitle' => '',
			'desc'     => '',
			'default'  => ( $template === 'blog-single-' ) ? esc_html__( 'Blog', 'w9sme' ) : '',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple', 'custom' ) ),
		),


		array(
			'id'       => $template . 'page-title-subtitle',
			'type'     => 'text',
			'title'    => esc_html__( 'Sub title', 'w9sme' ),
			'subtitle' => '',
			'desc'     => '',
			'default'  => '',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple', 'custom' ) ),
		),

		array(
			'id'       => $template . 'page-title-layout',
			'type'     => 'select',
			'title'    => esc_html__( 'Layout', 'w9sme' ),
			'subtitle' => esc_html__( 'Select page title layout', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'fullwidth'         => esc_html__( 'Full Width (overflow - hidden)', 'w9sme' ),
				'fullwidth-visible' => esc_html__( 'Full Width (overflow - visible)', 'w9sme' ),
				'container'         => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'     => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid'   => esc_html__( 'Container Fluid', 'w9sme' )
			),
			'default'  => 'container',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
		),

		array(
			'id'             => $template . 'page-title-inner-padding',
			'type'           => 'spacing',
			'mode'           => 'padding',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Page title inner padding', 'w9sme' ),
			'subtitle'       => esc_html__( 'Set page title inner top/bottom padding.', 'w9sme' ),
			'desc'           => '',
			'left'           => false,
			'right'          => false,
			'default'        => array(
				'padding-top'    => '95px',
				'padding-bottom' => '70px',
				'units'          => 'px',
			),
			'required'       => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
			'output'         => array( '.site-title.' . $template . ' .site-title-inner' )
		),

		array(
			'id'             => $template . 'page-title-inner-wrapper-padding',
			'type'           => 'spacing',
			'mode'           => 'padding',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Page title inner wrapper padding', 'w9sme' ),
			'subtitle'       => esc_html__( 'Set page title inner wrapper left/right padding.', 'w9sme' ),
			'desc'           => '',
			'top'            => false,
			'bottom'         => false,
			'required'       => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
			'output'         => array( '.site-title.' . $template . ' .site-title-inner-wrapper' )
		),

		array(
			'id'             => $template . 'page-title-margin-bottom',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Margin bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'Set page title bottom margin.', 'w9sme' ),
			'desc'           => '',
			'left'           => false,
			'right'          => false,
			'top'            => false,
			'default'        => array(
				'margin-bottom' => '80px',
				'units'         => 'px',
			),
			'required'       => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
			'output'         => array( '.site-title.' . $template )
		),

		array(
			'id'       => $template . 'page-title-background',
			'type'     => 'media',
			'url'      => true,
			'title'    => esc_html__( 'Title background', 'w9sme' ),
			'compiler' => 'true',
			'subtitle' => esc_html__( 'Upload any media using the WordPress native uploader.', 'w9sme' ),
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
			'default'  => array(
				'url' => w9sme_theme_url() . 'assets/images/title-bg-default.jpeg'
			)
		),

		array(
			'id'       => $template . 'page-title-parallax-effect',
			'type'     => 'select',
			'title'    => esc_html__( 'Parallax effect', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose parallax effect for the background image.', 'w9sme' ),
			'options'  => array(
				'no-effect' => esc_html__( 'No effect', 'w9sme' ),
				'0.0'       => '0.0',
				'0.05'      => '0.05',
				'0.1'       => '0.1',
				'0.2'       => '0.2',
				'0.3'       => '0.3',
				'0.4'       => '0.4',
				'0.5'       => '0.5',
				'0.6'       => '0.6',
				'0.7'       => '0.7',
			),
			'default'  => 'no-effect',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
		),


		array(
			'id'       => $template . 'page-title-parallax-position',
			'type'     => 'select',
			'title'    => esc_html__( 'Parallax vertical position', 'w9sme' ),
			'subtitle' => '',
			'desc'     => '',
			'options'  => array(
				'top'    => esc_html__( 'Top', 'w9sme' ),
				'center' => esc_html__( 'Center', 'w9sme' ),
				'bottom' => esc_html__( 'Bottom', 'w9sme' ),
			),
			'default'  => 'center',
			'required' => array(
				array( $template . 'page-title-enable', '=', array( 'simple' ) ),
				array( $template . 'page-title-parallax-effect', 'not_empty_and', 'no-effect' ),
			),
		),

		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => $template_name . esc_html__( ' Title Style Configurations', 'w9sme' ),
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
		),

		//


		array(
			'id'       => $template . 'page-title-text-align',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Text align', 'w9sme' ),
			'subtitle' => esc_html__( 'Set Page Title Text Align', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'text-left'   => esc_html__( 'Left', 'w9sme' ),
				'text-center' => esc_html__( 'Center', 'w9sme' ),
				'text-right'  => esc_html__( 'Right', 'w9sme' )
			),
			'default'  => 'text-left',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
		),

		array(
			'id'       => $template . 'title-subtitle-custom-style',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable custom style for title and subtitle ?', 'w9sme' ),
//		    'subtitle' => esc_html__( 'Smooth animation when scrolling the page.', 'w9sme' ),
			'desc'     => '',
			'default'  => 0,
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
		),

		//--- Title and subtitle option ---

		array(
			'id'          => $template . 'title-text-typo',
			'type'        => 'typography',
			'title'       => esc_html__( 'Title text typo', 'w9sme' ),
			'subtitle'    => esc_html__( 'Config typography for title text.', 'w9sme' ),
			'desc'        => '',
			'units'       => 'px',
//            'font-family' => false,
			'subsets'     => false,
			'font-backup' => false,
			'fonts'       => false,
			'text-align'  => false,
			'color'       => false,
			'font-style'  => false,
			'preview'     => false,
			'line-height' => false,
			'default'     => array(
				'font-family' => 'Poppins',
				'font-weight' => '600',
				'font-size'   => '48px',
			),
			'required'    => array( $template . 'title-subtitle-custom-style', '=', array( '1' ) ),
			'output'      => array( '.site-title.' . $template . ' .site-title-inner h1.title' )
		),

		array(
			'id'       => $template . 'title-font-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Title font style', 'w9sme' ),
			'options'  => array(
				''           => esc_html__( 'Default', 'w9sme' ),
				'fs-inherit' => esc_html__( 'Inherit', 'w9sme' ),
				'fs-normal'  => esc_html__( 'Normal', 'w9sme' ),
				'fs-italic'  => esc_html__( 'Italic', 'w9sme' ),
			),
			'default'  => '',
			'required' => array( $template . 'title-subtitle-custom-style', '=', array( '1' ) ),
		),

		array(
			'id'       => $template . 'title-text-transform',
			'type'     => 'select',
			'title'    => esc_html__( 'Title text transform', 'w9sme' ),
			'subtitle' => '',
			'desc'     => '',
			'options'  => array(
				'text-transform-__' => esc_html__( 'Normal', 'w9sme' ),
				'text-capitalize'   => esc_html__( 'Capitalize', 'w9sme' ),
				'text-uppercase'    => esc_html__( 'Uppercase', 'w9sme' ),
			),
			'default'  => 'text-uppercase',
			'validate' => 'not_empty',
			'required' => array( $template . 'title-subtitle-custom-style', '=', array( '1' ) ),
		),

		array(
			'id'             => $template . 'title-margin-bottom',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Title margin bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'Set title bottom margin.', 'w9sme' ),
			'desc'           => '',
			'left'           => false,
			'right'          => false,
			'top'            => false,
			'default'        => array(
				'margin-bottom' => '10px',
				'units'         => 'px',
			),
			'required'       => array( $template . 'title-subtitle-custom-style', '=', array( '1' ) ),
			'output'         => array( '.site-title.' . $template . ' .site-title-inner h1.title' )
		),

		array(
			'id'          => $template . 'subtitle-text-typo',
			'type'        => 'typography',
			'title'       => esc_html__( 'Subtitle text typo', 'w9sme' ),
			'subtitle'    => esc_html__( 'Config typography for subtitle text.', 'w9sme' ),
			'desc'        => '',
			'units'       => 'px',
//            'font-family' => false,
			'subsets'     => false,
			'font-backup' => false,
			'fonts'       => false,
			'text-align'  => false,
			'color'       => false,
			'font-style'  => false,
			'preview'     => false,
			'line-height' => false,
			'default'     => array(
				'font-family' => 'Playfair Display',
				'font-weight' => '400',
				'font-size'   => '18px',
			),
			'required'    => array( $template . 'title-subtitle-custom-style', '=', array( '1' ) ),
			'output'      => array( '.site-title.' . $template . ' .site-title-inner p.sub-title' )
		),

		array(
			'id'       => $template . 'subtitle-font-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Subtitle font style', 'w9sme' ),
			'options'  => array(
				'fs-__'      => esc_html__( 'Default', 'w9sme' ),
				'fs-inherit' => esc_html__( 'Inherit', 'w9sme' ),
				'fs-normal'  => esc_html__( 'Normal', 'w9sme' ),
				'fs-italic'  => esc_html__( 'Italic', 'w9sme' ),
			),
			'default'  => 'fs-italic',
			'required' => array( $template . 'title-subtitle-custom-style', '=', array( '1' ) ),
		),

		array(
			'id'       => $template . 'subtitle-text-transform',
			'type'     => 'select',
			'title'    => esc_html__( 'Subtitle text transform', 'w9sme' ),
			'subtitle' => '',
			'desc'     => '',
			'options'  => array(
				'text-transform-__' => esc_html__( 'Normal', 'w9sme' ),
				'text-capitalize'   => esc_html__( 'Capitalize', 'w9sme' ),
				'text-uppercase'    => esc_html__( 'Uppercase', 'w9sme' ),
			),
			'default'  => 'text-transform-__',
			'validate' => 'not_empty',
			'required' => array( $template . 'title-subtitle-custom-style', '=', array( '1' ) ),
		),


		//

		array(
			'id'       => $template . 'page-title-style',
			'type'     => 'select',
			'title'    => esc_html__( 'Color style', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose style for the title wrapper.', 'w9sme' ),
			'options'  => array(
				'bg-gray-lighter'   => esc_html__( 'Light gray background, dark text', 'w9sme' ),
				'bg-gray'           => esc_html__( 'Gray background, dark text', 'w9sme' ),
				'bg-dark-lighter'   => esc_html__( 'Dark gray background,  light text', 'w9sme' ),
				'bg-dark'           => esc_html__( 'Black background, white text', 'w9sme' ),
				'bg-white'          => esc_html__( 'White background, dark text', 'w9sme' ),
				'bg-dark-alpha-30'  => esc_html__( 'Dark 30%', 'w9sme' ),
				'bg-dark-alpha-50'  => esc_html__( 'Dark 50%', 'w9sme' ),
				'bg-dark-alpha-70'  => esc_html__( 'Dark 70%', 'w9sme' ),
				'bg-light-alpha-30' => esc_html__( 'Light 30%', 'w9sme' ),
				'bg-light-alpha-50' => esc_html__( 'Light 50%', 'w9sme' ),
				'bg-light-alpha-70' => esc_html__( 'Light 70%', 'w9sme' ),
				'bg-custom'         => esc_html__( 'Custom', 'w9sme' ),
			),
			'default'  => 'bg-dark-alpha-50',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) )
		),

		array(
			'id'       => $template . 'page-title-text-color',
			'type'     => 'color',
			'title'    => esc_html__( 'Text color', 'w9sme' ),
			'subtitle' => esc_html__( 'Pick a color for page title.', 'w9sme' ),
			'default'  => '#444',
			'required' => array(
				array( $template . 'page-title-enable', '=', array( 'simple' ) ),
				array( $template . 'page-title-style', '=', array( 'bg-custom' ) )
			)
		),

		array(
			'id'       => $template . 'page-title-bg-color',
			'type'     => 'color_rgba',
			'title'    => esc_html__( 'Background color', 'w9sme' ),
			'subtitle' => esc_html__( 'Pick a background color for page title.', 'w9sme' ),
			'default'  => array(
				'color' => '#fff',
				'alpha' => 0.7,
				'rgba'  => 'rgba(255, 255, 255, 0.7)'
			),
			'required' => array(
				array( $template . 'page-title-enable', '=', array( 'simple' ) ),
				array( $template . 'page-title-style', '=', array( 'bg-custom' ) )
			)
		),

		array(
			'id'       => $template . 'page-title-top-line',
			'type'     => 'switch',
			'title'    => esc_html__( 'Top-line support', 'w9sme' ),
			'desc'     => '',
			'default'  => '0',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) )
		),

		array(
			'id'          => $template . 'page-title-top-line-color',
			'type'        => 'color',
			'title'       => esc_html__( 'Top-line color', 'w9sme' ),
			'subtitle'    => esc_html__( 'Pick a color for page title top-line.', 'w9sme' ),
			'transparent' => false,
			'default'     => '#fff',
			'validate'    => 'color',
			'required'    => array(
				array( $template . 'page-title-enable', '=', array( 'simple' ) ),
				array( $template . 'page-title-top-line', '=', array( '1' ) )
			),
			'compiler'    => true,
		),

		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => 'Breadcrumb Configurations',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
		),

		array(
			'id'       => $template . 'page-title-breadcrumbs',
			'type'     => 'switch',
			'title'    => esc_html__( 'Breadcrumbs', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable/disable breadcrumbs in pages title.', 'w9sme' ),
			'desc'     => '',
			'default'  => '1',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
		),

		//--- Breadcrumbs option ----

//	    array(
//		    'id'       => $template . 'breadcrumbs-prepended-text',
//		    'type'     => 'text',
//		    'title'    => esc_html__( 'Prepended text', 'w9sme' ),
//		    'subtitle'     => esc_html__('Breadcrumbs prepended text', 'w9sme'),
//		    'default'  =>  esc_html__( 'You are here:', 'w9sme' ),
//		    'required' => array( $template . 'page-title-breadcrumbs', '=', array( '1' ) ),
//	    ),

		array(
			'id'       => $template . 'breadcrumbs-position',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Breadcrumbs position', 'w9sme' ),
			'options'  => array(
				'left'  => esc_html__( 'Left', 'w9sme' ),
				'right' => esc_html__( 'Right', 'w9sme' ),
			),
			'default'  => 'right',
			'required' => array( $template . 'page-title-breadcrumbs', '=', array( '1' ) ),
		),
		
		//==============================================
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Call To Action (CTA) Button', 'w9sme' ),
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
		),
		
		array(
			'id'       => $template . 'page-title-cta',
			'type'     => 'button_set',
			'title'    => esc_html__( 'CTA Button', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable/disable CTA button in page title.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'0'       => esc_html__( 'Off', 'w9sme' ),
				'simple' => esc_html__( 'Simple', 'w9sme' ),
				'custom'  => esc_html__( 'Custom Shortcode', 'w9sme' ),
			),
			'default'  => '0',
			'required' => array( $template . 'page-title-enable', '=', array( 'simple' ) ),
		),
		
		array(
			'id'       => $template . 'page-title-cta-btn-text',
			'type'     => 'text',
			'title'    => esc_html__( 'CTA Button Text', 'w9sme' ),
			'subtitle' => esc_html__( 'CTA button text', 'w9sme' ),
			'default'  => '',
			'required' => array(
				array( $template . 'page-title-enable', '=', array( 'simple' ) ),
				array( $template . 'page-title-cta', '=', array( 'simple', 'custom' ) )
			),
		),
		
		array(
			'id'       => $template . 'page-title-cta-btn-shortcode',
			'type'     => 'textarea',
			'title'    => esc_html__( 'CTA Button Shortcode', 'w9sme' ),
			'subtitle' => esc_html__( 'Define the CTA button', 'w9sme' ),
			'desc'     => esc_html__( 'Content can be raw HTML, shortcode string or mix of them. Use the {{cta-btn-text}} to reference to the CTA Button Text.', 'w9sme' ),
			'default'  => '',
			'required' => array(
				array( $template . 'page-title-enable', '=', array( 'simple' ) ),
				array( $template . 'page-title-cta', '=', array( 'custom' ) )
			),
		)
		
	), // #fields
);