<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-page-settings.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$this->sections[] = array(
    'title'  => esc_html__( 'Pages', 'w9sme' ),
    'desc'   => esc_html__( 'Pages settings.', 'w9sme' ),
    'icon'   => 'dashicons-before dashicons-media-document',
    'fields' => array(
        array(
            'id'    => mt_rand(),
            'type'  => 'info',
            'title' => esc_html__('Layout configurations', 'w9sme' ),
        ),
        array(
            'id'       => 'page-layout',
            'type'     => 'select',
            'title'    => esc_html__( 'Layout', 'w9sme' ),
            'subtitle' => esc_html__( 'Select page layout.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
                'container'       => esc_html__( 'Container', 'w9sme' ),
                'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
                'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
            ),
            'default'  => 'fullwidth'
        ),
	
	    array(
		    'id'       => 'page-widget-title-style',
		    'type'     => 'button_set',
		    'title'    => esc_html__( 'Widget title style', 'w9sme' ),
		    'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
		    'desc'     => '',
		    'options'  => array(
			    ''        => esc_html__( 'Default', 'w9sme' ),
			    'style-1' => esc_html__( 'Style 1', 'w9sme' ),
			    'style-2' => esc_html__( 'Style 2', 'w9sme' ),
		    ),
		    'default'  => ''
	    ),
        
        array(
            'id'       => 'page-sidebar',
            'type'     => 'image_select',
            'title'    => esc_html__( 'Sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
                'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
                'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
                'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
            ),
            'default'  => 'none'
        ),
        array(
            'id'       => 'page-sidebar-width',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
                'large' => esc_html__( 'Large (1/3)', 'w9sme' )
            ),
            'default'  => 'small',
            'required' => array( 'page-sidebar', '=', array( 'left', 'both', 'right' ) ),
        ),
        array(
            'id'       => 'page-sidebar-left',
            'type'     => 'select',
            'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-1',
            'required' => array( 'page-sidebar', '=', array( 'left', 'both' ) ),
        ),
        array(
            'id'       => 'page-sidebar-right',
            'type'     => 'select',
            'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-2',
            'required' => array( 'page-sidebar', '=', array( 'right', 'both' ) ),
        ),
        array(
            'id'    => mt_rand(),
            'type'  => 'info',
            'title' => esc_html__('Other settings', 'w9sme' ),
        ),
        array(
            'id'       => 'page-comment',
            'type'     => 'switch',
            'title'    => esc_html__( 'Page comment', 'w9sme' ),
            'subtitle' => esc_html__( 'Enable/Disable page comment.', 'w9sme' ),
            'desc'     => '',
            'default'  => '1'
        ),
        array(
            'id'             => 'page-margin',
            'type'           => 'spacing',
            'mode'           => 'margin',
            'units'          => 'px',
            'units_extended' => 'false',
            'title'          => esc_html__( 'Page margin', 'w9sme' ),
            'subtitle'       => esc_html__( 'Set page margin.', 'w9sme' ),
            'desc'           => '',
            'left'           => false,
            'right'          => false,
            'default'        => array(
                'margin-bottom' => '100px',
                'margin-top'    => '',
                'units'         => 'px',
            ),
            'output'         => array( '.site-main-page' ),
        )
    )
);