<?php
/**
 * Copyright 2016, 9WPThemes
 * @filename: tab-cpt-event-archive.php
 * @time    : 4/25/2017 4:27 PM
 * @author  : 9WPThemes Team
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if (!class_exists('W9sme_CPT_Event')) {
	return;
}

$this->sections[] = array(
	'title'      => esc_html__( 'Event Archive', 'w9sme' ),
	'desc'       => esc_html__( 'Event Archive settings.', 'w9sme' ),
	'icon'       => 'dashicons-before dashicons-calendar-alt',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'    => 'event-archive-template',
			'title' => esc_html__( 'Override default archive template', 'w9sme' ),
			'type'  => 'select',
			'data'  => 'pages',
		),
		//---s
		array(
			'id'       => 'event-archive-item-header',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable event item header', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable event item header.', 'w9sme' ),
			'default'  => 1
		),
		
		array(
			'id'       => 'event-archive-discount-tag',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable discount tag ?', 'w9sme' ),
//			'subtitle' => esc_html__( 'Enable event item header.', 'w9sme' ),
			'default'  => 1,
			'required' => array( 'event-archive-discount-tag', '=', array( 1 ) ),
		),
		//---e
		
		array(
			'id'      => 'event-archive-item-image-ratio',
			'type'    => 'select',
			'title'   => esc_html__( 'Event item image ratio', 'w9sme' ),
			'desc'    => '',
			'options' => array(
				'0.5625'        => '16x9',
				'1.77777777778' => '9x16',
				'0.42857142857' => '21x9',
				'2.33333333333' => '9x21',
				'0.75'          => '4x3',
				'1.33333333333' => '3x4',
				'0.666666667'   => '3x2',
				'1.5'           => '2x3',
				'1'             => '1x1',
				'0.5'           => '2x1',
				'2'             => '1x2',
				'original'      => esc_html__( 'Original', 'w9sme' ),
			),
			'default' => 'original',
		),
		
//		array(
//			'id'       => 'event-archive-item-image-action',
//			'type'     => 'button_set',
//			'title'    => esc_html__( 'Blog item feature image action', 'w9sme' ),
//			'subtitle' => esc_html__( 'Action with the feature image of the blog item.', 'w9sme' ),
//			'desc'     => '',
//			'options'  => array(
//				'none' => esc_html__( 'None', 'w9sme' ),
//				'link' => esc_html__( 'Link to post', 'w9sme' ),
//			),
//			'default'  => 'link',
//		),
		
		array(
			'id'       => 'event-archive-post-meta',
			'type'     => 'checkbox',
			'title'    => esc_html__( 'Meta', 'w9sme' ),
			'subtitle' => esc_html__( 'Disable or enable meta options.', 'w9sme' ),
			'options'  => array(
				'date'         => esc_html__( 'Date', 'w9sme' ),
//				'author'       => esc_html__( 'Author', 'w9sme' ),
				'categories'   => esc_html__( 'Categories', 'w9sme' ),
//				'tags'         => esc_html__( 'Tags', 'w9sme' ),
				'comments'     => esc_html__( 'Comments', 'w9sme' ),
				'social-share' => esc_html__( 'Social Share', 'w9sme' ),
			),
			'default'  => array(
				'date'         => 1,
//				'author'       => 1,
				'categories'   => 1,
//				'tags'         => 0,
				'comments'     => 1,
				'social-share' => 1,
			),
		),
		
		array(
			'id'       => 'event-archive-items-per-page',
			'type'     => 'text',
			'title'    => esc_html__( 'Items per page', 'w9sme' ),
			'subtitle' => esc_html__( 'Default value is 8.', 'w9sme' ),
			'default'  => '8',
			'validate' => 'numeric'
		),
		
		array(
			'id'       => 'event-archive-paging-type',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Paging type', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive paging type.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'default'         => esc_html__( 'Default', 'w9sme' ),
				'load-more'       => esc_html__( 'Load More', 'w9sme' ),
				'infinite-scroll' => esc_html__( 'Infinite Scroll', 'w9sme' )
			),
			'default'  => 'default'
		),
		
		array(
			'id'       => 'event-archive-paging-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Paging style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive paging style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'style-1' => esc_html__( 'Classic', 'w9sme' ),
				'style-2' => esc_html__( 'Modern', 'w9sme' ),
			),
			'required' => array( 'event-archive-paging-type', '=', array( 'default' ) ),
			'default'  => 'style-2'
		),
		
		array(
			'id'       => 'event-archive-paging-align',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Paging align', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive paging align.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'left'   => esc_html__( 'left', 'w9sme' ),
				'center' => esc_html__( 'Center', 'w9sme' ),
				'right'  => esc_html__( 'Right', 'w9sme' ),
			),
			'required' => array( 'event-archive-paging-type', '=', array( 'default' ) ),
			'default'  => 'left'
		),
		
		array(
			'id'             => 'event-archive-margin',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
			'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
			'left'           => false,
			'right'          => false,
			'default'        => array(
				'margin-top'    => '0',
				'margin-bottom' => '0',
				'units'         => 'px',
			),
			'output'         => array( '.site-main-archive.event-archive' )
		),
		
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Layout Settings', 'w9sme' ),
		),
		array(
			'id'       => 'event-archive-layout',
			'type'     => 'select',
			'title'    => esc_html__( 'Layout', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive layout.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
				'container'       => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
			),
			'default'  => 'container',
			'validate' => 'not_empty'
		),
		
		array(
			'id'       => 'event-archive-widget-title-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Widget title style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				''        => esc_html__( 'Default', 'w9sme' ),
				'style-1' => esc_html__( 'Style 1', 'w9sme' ),
				'style-2' => esc_html__( 'Style 2', 'w9sme' ),
			),
			'default'  => ''
		),
		
		array(
			'id'       => 'event-archive-sidebar',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
				'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
				'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
				'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
			),
			'default'  => 'right'
		),
		
		array(
			'id'       => 'event-archive-sidebar-width',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
				'large' => esc_html__( 'Large (1/3)', 'w9sme' )
			),
			'default'  => 'large',
			'required' => array( 'event-archive-sidebar', '=', array( 'left', 'both', 'right' ) ),
		),
		
		
		array(
			'id'       => 'event-archive-sidebar-left',
			'type'     => 'select',
			'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-1',
			'required' => array( 'event-archive-sidebar', '=', array( 'left', 'both' ) ),
		),
		
		array(
			'id'       => 'event-archive-sidebar-right',
			'type'     => 'select',
			'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-1',
			'required' => array( 'event-archive-sidebar', '=', array( 'right', 'both' ) ),
		),
	)
);