<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-cpt-service-single.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! class_exists( 'W9sme_CPT_Service' ) || w9sme_get_option( 'cpt-service-enable' ) == '0' ) {
	return;
}

$content_template_list = w9sme_get_content_template_list();
/*-------------------------------------\
	SERVICE CPT
---------------------------------------*/
$this->sections[] = array(
	'title'      => esc_html__( 'Service Single', 'w9sme' ),
	'desc'       => esc_html__( 'Service Single settings section.', 'w9sme' ),
	'icon'       => 'dashicons-before dashicons-share-alt',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'      => 'service-single-content-layout',
			'type'    => 'select',
			'title'   => esc_html__( 'Content Layout', 'w9sme' ),
			'desc'    => esc_html__( 'Select content template which made up layout for single service.', 'w9sme' ),
			'options' => $content_template_list,
			'default' => '',
		),
//		array(
//			'id'      => 'service-single-feature-image',
//			'type'    => 'switch',
//			'title'   => esc_html__( 'Enable feature image ?', 'w9sme' ),
////			'subtitle' => esc_html__( 'Enable service item header.', 'w9sme' ),
//			'default' => 0
//		),
//		array(
//			'id'       => 'service-single-post-meta',
//			'type'     => 'checkbox',
//			'title'    => esc_html__( 'Meta', 'w9sme' ),
//			'subtitle' => esc_html__( 'Disable or enable meta options.', 'w9sme' ),
//			'options'  => array(
//				'date'         => esc_html__( 'Date', 'w9sme' ),
//				'categories'   => esc_html__( 'Categories', 'w9sme' ),
//				'price'        => esc_html__( 'Price', 'w9sme' ),
//				'time'         => esc_html__( 'Time', 'w9sme' ),
//				'social-share' => esc_html__( 'Social Share', 'w9sme' ),
//			),
//			'default'  => array(
//				'date'         => 1,
//				'categories'   => 1,
//				'price'        => 1,
//				'time'         => 1,
//				'social-share' => 1
//			),
//		),
		
//		array(
//			'id'       => 'service-single-show-booking-btn',
//			'type'     => 'switch',
//			'title'    => esc_html__( 'Show service booking button?', 'w9sme' ),
//			'desc'     => '',
//			'default'  => 0
//		),
		
		array(
			'id'       => 'service-single-booking-btn-tx',
			'type'     => 'text',
			'title'    => esc_html__( 'Booking Button Text', 'w9sme' ),
			'subtitle' => esc_html__( 'Set the text for booking button.', 'w9sme' ),
			'default'  => 'BOOK NOW'
		),
		
		array(
			'id'       => 'service-single-related',
			'type'     => 'switch',
			'title'    => esc_html__( 'Related services', 'w9sme' ),
			'subtitle' => esc_html__( 'Show or hide related services.', 'w9sme' ),
			'default'  => 1
		),
		
		array(
			'id'       => 'service-single-related-amount',
			'type'     => 'text',
			'title'    => esc_html__( 'Amount of related services', 'w9sme' ),
			'subtitle' => esc_html__( 'Number of related services to show.', 'w9sme' ),
			'default'  => '4',
			'validate' => 'numeric',
			'required' => array( 'service-single-related', '=', array( 1 ) ),
		),
		
		array(
			'id'       => 'service-single-related-col',
			'type'     => 'select',
			'title'    => esc_html__( 'Related services - display columns', 'w9sme' ),
			'options'  => array(
				'1' => '1',
				'2' => '2',
				'3' => '3',
				'4' => '4',
			),
//		    'desc'     => esc_html__( 'Note: 6 columns available when screen bigger than 1600px, and should use layout container-xlg or larger.', 'w9sme' ),
			'default'  => '4',
			'required' => array( 'service-single-related', '=', array( 1 ) ),
//			'required' => array( 'service-archive-display-type', '=', array( 'masonry', 'grid' ) ),
		),
		
		array(
			'id'       => 'service-single-related-show-booking-btn',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show related service booking button?', 'w9sme' ),
			'desc'     => '',
			'required' => array( 'service-single-related', '=', array( 1 ) ),
			'default'  => 0
		),
		
		array(
			'id'       => 'service-single-related-booking-btn-tx',
			'type'     => 'text',
			'title'    => esc_html__( 'Related Service Booking Button Text', 'w9sme' ),
			'subtitle' => esc_html__( 'Set the text for booking button.', 'w9sme' ),
			'default'  => 'BOOK NOW',
			'required' => array( 'service-single-related-show-booking-btn', '=', array( '1' ) ),
		),
		
		array(
			'id'       => 'service-single-related-show-time',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show related service time?', 'w9sme' ),
			'subtitle' => esc_html__( 'Show service time', 'w9sme' ),
			'required' => array( 'service-single-related', '=', array( 1 ) ),
			'default'  => 0
		),
		
		array(
			'id'       => 'service-single-related-show-price',
			'type'     => 'switch',
			'title'    => esc_html__( 'Show related service price?', 'w9sme' ),
			'required' => array( 'service-single-related', '=', array( 1 ) ),
			'subtitle' => esc_html__( 'Show service price', 'w9sme' ),
			'default'  => 0
		),
		
		array(
			'id'             => 'service-single-margin',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
			'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
			'left'           => false,
			'right'          => false,
			'default'        => array(
				'margin-top'    => '0',
				'margin-bottom' => '0',
				'units'         => 'px',
			),
			'output'         => array( '.site-main-single.service-single' )
		),

//        array(
//            'id'       => 'service-single-navigator-enabled',
//            'type'     => 'switch',
//            'title' => esc_html__('Navigator enabled', 'w9sme'),
//            'subtitle' => esc_html__('Enable navigator bottom of single page.', 'w9sme'),
//            'default' => false
//        ),
		
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Layout Settings', 'w9sme' )
		),
		
		array(
			'id'       => 'service-single-layout',
			'type'     => 'select',
			'title'    => esc_html__( 'Layout', 'w9sme' ),
			'subtitle' => esc_html__( 'Select single service layout.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
				'container'       => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
			),
			'default'  => 'container'
		),
		
		array(
			'id'       => 'service-single-widget-title-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Widget title style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				''        => esc_html__( 'Default', 'w9sme' ),
				'style-1' => esc_html__( 'Style 1', 'w9sme' ),
				'style-2' => esc_html__( 'Style 2', 'w9sme' ),
			),
			'default'  => ''
		),
		
		array(
			'id'       => 'service-single-sidebar',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
				'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
				'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
				'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
			),
			'default'  => 'none'
		),
		
		array(
			'id'       => 'service-single-sidebar-width',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
				'large' => esc_html__( 'Large (1/3)', 'w9sme' )
			),
			'default'  => 'small',
			'required' => array( 'service-single-sidebar', '=', array( 'left', 'both', 'right' ) ),
		),
		
		array(
			'id'       => 'service-single-sidebar-left',
			'type'     => 'select',
			'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'service',
			'required' => array( 'service-single-sidebar', '=', array( 'left', 'both' ) ),
		),
		
		array(
			'id'       => 'service-single-sidebar-right',
			'type'     => 'select',
			'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'service',
			'required' => array( 'service-single-sidebar', '=', array( 'right', 'both' ) ),
		),
	),
);