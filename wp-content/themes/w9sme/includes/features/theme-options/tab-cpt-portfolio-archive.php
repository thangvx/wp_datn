<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-cpt-portfolio-archive.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/*-------------------------------------
	PORTFOLIO CPT
---------------------------------------*/
$this->sections[] = array(
	'title'      => esc_html__( 'Portfolio Archive', 'w9sme' ),
	'desc'       => esc_html__( 'Portfolio Archive settings section.', 'w9sme' ),
	'icon'       => 'dashicons-before dashicons-awards',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'    => mt_rand(),
			'type'  => 'info',
			'title' => esc_html__( 'Template settings', 'w9sme' ),
		),
		array(
			'id'    => 'portfolio-archive-template',
			'title' => esc_html__( 'Override default archive template', 'w9sme' ),
			'type'  => 'select',
			'data'  => 'pages',
		),
		array(
			'id'       => 'portfolio-archive-display-type',
			'type'     => 'select',
			'title'    => esc_html__( 'Display type', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive display type.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'grid'    => esc_html__( 'Grid', 'w9sme' ),
				'masonry' => esc_html__( 'Masonry', 'w9sme' ),
			),
			'default'  => 'grid',
			'validate' => 'not_empty'
		),
		
		array(
			'id'       => 'portfolio-archive-display-columns',
			'type'     => 'select',
			'title'    => esc_html__( 'Display columns', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the number of columns to display on archive pages.', 'w9sme' ),
			'options'  => array(
				'1' => '1',
				'2' => '2',
				'3' => '3',
				'4' => '4',
				'6' => '6',
			),
			'desc'     => esc_html__( 'Note: 6 columns available when screen bigger than 1600px, and should use layout container-xlg or larger.', 'w9sme' ),
			'default'  => '4',
			'required' => array( 'portfolio-archive-display-type', '=', array( 'masonry', 'grid' ) ),
		),
		
		array(
			'id'       => 'portfolio-archive-gutter',
			'type'     => 'select',
			'title'    => esc_html__( 'Portfolio columns spacing', 'w9sme' ),
			'subtitle' => esc_html__( 'Spacing between portfolio columns.', 'w9sme' ),
			'options'  => array(
				'0'  => '0px',
				'1'  => '1px',
				'2'  => '2px',
				'3'  => '3px',
				'4'  => '4px',
				'5'  => '5px',
				'10' => '10px',
				'15' => '15px',
				'20' => '20px',
				'25' => '25px',
				'30' => '30px',
				'35' => '35px',
				'40' => '40px',
				'45' => '45px',
				'50' => '50px',
				'55' => '55px',
				'60' => '60px',
			),
			'default'  => '30'
		),
		
		array(
			'id'       => 'portfolio-archive-filter-enable',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable filter', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable page filter.', 'w9sme' ),
			'default'  => true,
			'required' => array( 'portfolio-archive-display-type', '=', array( 'masonry', 'grid' ) ),
		),
		
		array(
			'id'       => 'portfolio-archive-filter-align',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Filter align', 'w9sme' ),
			'options'  => array(
				'text-left'   => esc_html__( 'Left', 'w9sme' ),
				'text-center' => esc_html__( 'Center', 'w9sme' ),
				'text-right'  => esc_html__( 'Right', 'w9sme' ),
			),
			'default'  => 'text-left',
			'required' => array( 'portfolio-archive-filter-enable', '=', array( true ) ),
		),
		
		array(
			'id'       => 'portfolio-archive-items-per-page',
			'type'     => 'text',
			'title'    => esc_html__( 'Items per page', 'w9sme' ),
			'subtitle' => esc_html__( 'Default value is 8.', 'w9sme' ),
			'default'  => '8',
			'validate' => 'numeric'
		),
		
		array(
			'id'       => 'portfolio-archive-paging-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Paging style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive paging style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'default'         => esc_html__( 'Default', 'w9sme' ),
				'load-more'       => esc_html__( 'Load More', 'w9sme' ),
				'infinite-scroll' => esc_html__( 'Infinite Scroll', 'w9sme' )
			),
			'default'  => 'infinite-scroll'
		),
		
		array(
			'id'             => 'portfolio-archive-margin',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
			'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
			'left'           => false,
			'right'          => false,
			'default'        => array(
				'margin-top'    => '0',
				'margin-bottom' => '80px',
				'units'         => 'px',
			),
			'output'         => array( '.site-main-archive.portfolio-archive' )
		),
		
		array(
			'id'    => mt_rand(),
			'type'  => 'info',
			'title' => esc_html__( 'Item Design', 'w9sme' ),
		),
		
		//Item type
		array(
			'id'       => 'portfolio-archive-item-type',
			'type'     => 'select',
			'title'    => esc_html__( 'Archive item type', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive item type.', 'w9sme' ),
			'options'  => array(
				'simple'  => esc_html__( 'Simple', 'w9sme' ),
				'overlay' => esc_html__( 'Overlay', 'w9sme' )
			),
			'default'  => 'simple',
			'validate' => 'not_empty'
		),
		
		//Overlay color
		array(
			'id'       => 'portfolio-archive-item-color',
			'type'     => 'color_rgba',
			'title'    => esc_html__( 'Portolfio item color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose portfolio color.', 'w9sme' ),
			'default'  => array(
				'color' => '#ffffff',
				'alpha' => '1',
			),
			'required' => array(
				'portfolio-archive-item-type',
				'=',
				'overlay'
			)
		),
		array(
			'id'       => 'portfolio-archive-item-overlay-color',
			'type'     => 'color_rgba',
			'title'    => esc_html__( 'Portolfio item overlay color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose portfolio overlay color.', 'w9sme' ),
			'default'  => array(
				'color' => '#000000',
				'alpha' => '0.8',
			),
			'required' => array(
				'portfolio-archive-item-type',
				'=',
				'overlay'
			)
		),
		
		array(
			'id'             => 'portfolio-archive-item-overlay-offset',
			'type'           => 'spacing',
			'output'         => array( '.w9sme-portfolio-overlay-default .entry-content' ),
			'mode'           => 'padding',
			'units'          => 'px',
			'all'            => true,
			'units_extended' => 'false',
			'title'          => esc_html__( 'Overlay offset', 'w9sme' ),
			'subtitle'       => esc_html__( 'Choose portfolio overlay offset in px.', 'w9sme' ),
			'default'        => array(
				'padding' => '30px',
				'units'   => 'px',
			),
			
			'required' => array(
				'portfolio-archive-item-type',
				'=',
				'overlay'
			)
		),
		
		//Overlay Effect
		array(
			'id'       => 'portfolio-archive-item-overlay-effect',
			'type'     => 'select',
			'title'    => esc_html__( 'Portfolio overlay appear effect', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose overlay appear effect', 'w9sme' ),
			'options'  => array(
				'none'                       => esc_html__( 'None', 'w9sme' ),
				'__hoverdir'                 => esc_html__( 'Hover Dir', 'w9sme' ),
				'__animation-fade-in'        => esc_html__( 'Fade In', 'w9sme' ),
				'__animation-fade-in-left'   => esc_html__( 'Fade In Left', 'w9sme' ),
				'__animation-fade-in-right'  => esc_html__( 'Fade In Right', 'w9sme' ),
				'__animation-fade-in-top'    => esc_html__( 'Fade In Top', 'w9sme' ),
				'__animation-fade-in-bottom' => esc_html__( 'Fade In Bottom', 'w9sme' ),
				'__animation-zoom-in'        => esc_html__( 'Zoom In', 'w9sme' ),
				'__animation-flip-in-x'      => esc_html__( 'Flip In X', 'w9sme' ),
				'__animation-flip-in-y'      => esc_html__( 'Flip In Y', 'w9sme' ),
			),
			'default'  => '__hoverdir',
			'required' => array(
				'portfolio-archive-item-type',
				'=',
				'overlay'
			)
		),
		
		// Show Title
		array(
			'id'       => 'portfolio-archive-show-title',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Show item title ?', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'yes' => esc_html__( 'Yes', 'w9sme' ),
				'no'  => esc_html__( 'No', 'w9sme' ),
			),
			'required' => array(
				array( 'portfolio-archive-item-type', '=', 'simple' ),
			),
			'default'  => 'yes'
		),
		
		//Image ratio
		array(
			'id'      => 'portfolio-archive-item-image-ratio',
			'type'    => 'select',
			'title'   => esc_html__( 'Image Ratio', 'w9sme' ),
			'options' => array(
				'auto'          => esc_html__( 'Auto', 'w9sme' ),
				'0.5625'        => '16x9',
				'1.77777777778' => '9x16',
				'0.42857142857' => '21x9',
				'2.33333333333' => '9x21',
				'0.75'          => '4x3',
				'1.33333333333' => '3x4',
				'0.666666667'   => '3x2',
				'1.5'           => '2x3',
				'1'             => '1x1',
				'2'             => '1x2',
				'0.5'           => '2x1',
			),
			'default' => '0.666666667'
		),
		
		array(
			'id'    => mt_rand(),
			'type'  => 'info',
			'title' => esc_html__( 'Layout Settings', 'w9sme' ),
		),
		
		array(
			'id'       => 'portfolio-archive-layout',
			'type'     => 'select',
			'title'    => esc_html__( 'Layout', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive layout.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
				'container'       => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
			),
			'default'  => 'container',
			'validate' => 'not_empty'
		),
		
		array(
			'id'       => 'portfolio-archive-sidebar',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
				'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
				'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
				'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
			),
			'default'  => 'none'
		),
		
		array(
			'id'       => 'portfolio-archive-sidebar-width',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
				'large' => esc_html__( 'Large (1/3)', 'w9sme' )
			),
			'default'  => 'small',
			'required' => array( 'portfolio-archive-sidebar', '=', array( 'left', 'both', 'right' ) ),
		),
		
		array(
			'id'       => 'portfolio-archive-sidebar-left',
			'type'     => 'select',
			'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-1',
			'required' => array( 'portfolio-archive-sidebar', '=', array( 'left', 'both' ) ),
		),
		
		array(
			'id'       => 'portfolio-archive-sidebar-right',
			'type'     => 'select',
			'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-2',
			'required' => array( 'portfolio-archive-sidebar', '=', array( 'right', 'both' ) ),
		),
	),
);