<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-header-style.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$this->sections[] = array(
	'title'  => esc_html__( 'Header Style', 'w9sme' ),
	'desc'   => esc_html__( 'Change the header section configuration.', 'w9sme' ),
	'icon'   => 'el-icon-star',
	'fields' => array(
		array(
			'id'       => 'nav-width',
			'type'     => 'select',
			'title'    => esc_html__( 'Header width', 'w9sme' ),
			'subtitle' => esc_html__( 'Select common header width in the list.', 'w9sme' ),
			'output'   => '.w9sme-nav-body .w9sme-nav-logo-wrapper',
			'options'  => array(
				'container'               => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'           => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid'         => esc_html__( 'Container Fluid', 'w9sme' ),
				'container-nav-fullwidth' => esc_html__( 'Full Width', 'w9sme' ),
			),
			'default'  => 'container',
		),
		
		array(
			'id'       => 'nav-boxed-enabled',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Enable boxed mode', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable boxed mode.', 'w9sme' ),
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'default'  => 'off',
			'required' => array(
				array( 'nav-width', '!=', 'container-fluid' ),
				array( 'nav-width', '!=', 'container-nav-fullwidth' ),
			)
		),
		
		array(
			'id'       => 'nav-occupy-spacing',
			'type'     => 'button_set',
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'title'    => esc_html__( 'Nav occupy space?', 'w9sme' ),
			'subtitle' => esc_html__( 'Nav occupy page space or not.', 'w9sme' ),
			'default'  => 'on',
		),
		
		array(
			'id'             => 'nav-overlay-offset-top',
			'type'           => 'spacing',
			'output'         => array( '.w9sme-main-header.w9sme-menu-overlay-wrapper' ),
			'mode'           => 'absolute',
			'top'            => true,
			'left'           => false,
			'right'          => false,
			'bottom'         => false,
			'units'          => array( 'px' ),
			'units_extended' => true,
			'title'          => esc_html__( 'Main nav offset top', 'w9sme' ),
			'subtitle'       => esc_html__( 'Main nav offset top in px unit.', 'w9sme' ),
			'default'        => array(),
			'required'       => array(
				'nav-occupy-spacing',
				'=',
				'off'
			)
		),
		
		array(
			'id'       => 'nav-sticky',
			'type'     => 'button_set',
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'title'    => esc_html__( 'Enable sticky menu', 'w9sme' ),
			'subtitle' => esc_html__( 'Nav will be fixed on top of window when scroll down position bigger than window height.', 'w9sme' ),
			'default'  => 'on',
		),
		
		array(
			'id'             => 'nav-sticky-show-up',
			'type'           => 'spacing',
			'mode'           => 'absolute',
			'top'            => true,
			'left'           => false,
			'right'          => false,
			'bottom'         => false,
			'units'          => '',
			'units_extended' => true,
			'title'          => esc_html__( 'When will the sticky menu show up ?', 'w9sme' ),
			'subtitle'       => esc_html__( 'Unit: px. If you leave the field blank, the default value will be the height of your browser screen.', 'w9sme' ),
			'default'        => array(),
			'required'       => array(
				'nav-sticky',
				'=',
				'on'
			)
		),
		
		array(
			'id'       => 'nav-headroom',
			'type'     => 'button_set',
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'title'    => esc_html__( 'Enable sticky headroom', 'w9sme' ),
			'subtitle' => esc_html__( 'Hide sticky menu when scroll down, show it when scroll up.', 'w9sme' ),
			'default'  => 'off',
			'required' => array(
				'nav-sticky',
				'=',
				'on'
			)
		),
		
		array(
			'id'       => 'header-nav-breakpoint',
			'height'   => false,
			'type'     => 'dimensions',
			'units'    => 'px',
			'default'  => array(
				'width' => '992px',
				'unit'  => 'px'
			),
			'title'    => esc_html__( 'Desktop - mobile breakpoint', 'w9sme' ),
			'subtitle' => esc_html__( 'Desktop nav and desktop nav hide if screen width lower than breakpoint, Mobile nav and mobile module hide if screen greater than breakpoint.', 'w9sme' )
		),
		
		array(
			'id'       => 'nav-item-separator',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Enable nav item separator', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable separator between nav items. This option work best if header width id fullwidth.', 'w9sme' ),
			'options'  => array(
				'on'  => esc_html__( 'On', 'w9sme' ),
				'off' => esc_html__( 'Off', 'w9sme' ),
			),
			'default'  => 'off',
		),
		
		array(
			'id'       => 'nav-item-separator-color',
			'title'    => esc_html__( 'Nav item separator color', 'w9sme' ),
			'subtitle' => esc_html__( 'Define nav item separator color.', 'w9sme' ),
			'output'   => array(
				'border-color' => '.w9sme-nav-enable-separator .w9sme-nav-item, .w9sme-nav-enable-separator .w9sme-nav-body-content'
			),
			'type'     => 'color_rgba',
			'default'  => array(
				'color' => '#888',
				'alpha' => '0.4',
			),
			'required' => array(
				'nav-item-separator',
				'=',
				'on'
			)
		),
		
		array(
			'id'       => 'nav-item-padding',
			'type'     => 'spacing',
			'mode'     => 'padding',
			'units'    => 'px',
			'top'      => false,
			'bottom'   => false,
			'title'    => esc_html__( 'Nav item padding', 'w9sme' ),
			'subtitle' => esc_html__( 'Define nav item padding if have separator.', 'w9sme' ),
			'output'   => array( '.w9sme-nav-enable-separator .w9sme-nav-item' ),
			'default'  => array(
				'padding-left'  => '20px',
				'padding-right' => '20px',
				'unit'          => 'px'
			),
			'required' => array(
				'nav-item-separator',
				'=',
				'on'
			)
		),
		
		array(
			'id'       => 'nav-item-color',
			'output'   => array( 'color' => '.w9sme-nav-body' ),
			'type'     => 'color',
			'title'    => esc_html__( 'Nav item color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav color.', 'w9sme' ),
			'default'  => '#ffffff',
		),
		
		array(
			'id'       => 'nav-item-link-color',
			'output'   => array( '.w9sme-nav-body .w9sme-main-menu-content>li>a, .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a' ),
			'type'     => 'link_color',
			'title'    => esc_html__( 'Nav item link color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav link color.', 'w9sme' ),
			'desc'     => esc_html__( 'Hover, active color available for link in nav only.', 'w9sme' ),
			'default'  => array(
				'regular' => '#222',
				'hover'   => '#EB6C6C',
				'active'  => '#EB6C6C',
			)
		),
		
		array(
			'id'       => 'nav-link-hover-background',
			'type'     => 'color_rgba',
			'output'   => array( 'background-color' => '.w9sme-nav-body .w9sme-main-menu-content>li>a:hover, .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a:hover' ),
			'title'    => esc_html__( 'Main nav link hover background', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose link background color when hover.', 'w9sme' ),
			'default'  => '',
			'required' => array(
				'nav-menu-item-hover-effect',
				'=',
				array(
					'w9sme-menu-item-hover-none',
					'w9sme-menu-item-hover-underline',
					'w9sme-menu-item-hover-bracket'
				)
			),
		),
		
		array(
			'id'       => 'nav-menu-item-hover-effect',
			'type'     => 'select',
			'title'    => esc_html__( 'Nav menu item hover effect', 'w9sme' ),
			'subtitle' => esc_html__( 'Select menu item (link in menu only) hover effect.', 'w9sme' ),
			'options'  => array(
				'w9sme-menu-item-hover-none'              => esc_html__( 'None', 'w9sme' ),
				'w9sme-menu-item-hover-underline'         => esc_html__( 'Underline', 'w9sme' ),
				'w9sme-menu-item-hover-bracket'           => esc_html__( 'Brackets', 'w9sme' ),
				'w9sme-menu-item-hover-magic-line-top'    => esc_html__( 'Magic Line Top', 'w9sme' ),
				'w9sme-menu-item-hover-magic-line-bottom' => esc_html__( 'Magic Line Bottom', 'w9sme' ),
			),
			'default'  => 'w9sme-menu-item-hover-underline',
		),
		
		array(
			'id'      => 'nav-background',
			'type'    => 'background',
			'output'  => array( '.w9sme-nav-body' ),
			'title'   => esc_html__( 'Main nav background', 'w9sme' ),
			'default' => array(
				'background-color' => 'transparent',
			)
		),
		
		array(
			'id'      => 'nav-background-overlay',
			'type'    => 'color_rgba',
			'title'   => esc_html__( 'Main nav background overlay', 'w9sme' ),
			'output'  => array( 'background-color' => '.w9sme-nav-body:before' ),
			// See Notes below about these lines.
			//'output'    => array('background-color' => '.site-header'),
			'default' => array(
				'color' => '#F2f2f2',
				'alpha' => 1
			),
		),
		
		array(
			'id'      => 'nav-border',
			'type'    => 'border',
			'title'   => esc_html__( 'Nav border', 'w9sme' ),
			'output'  => array( '.w9sme-nav-body' ),
			'all'     => false,
			'color'   => false,
			'default' => array(
				'border-style'  => 'solid',
				'border-top'    => '0',
				'border-right'  => '0',
				'border-bottom' => '1px',
				'border-left'   => '0'
			)
		),
		
		array(
			'id'       => 'nav-border-color',
			'title'    => esc_html__( 'Nav border color', 'w9sme' ),
			'subtitle' => esc_html__( 'Specify the nav border color.', 'w9sme' ),
			'output'   => array(
				'border-color' => '.w9sme-nav-body'
			),
			'type'     => 'color_rgba',
			'default'  => array(
				'color' => '#888',
				'alpha' => '0.4',
			),
		),
		
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Header Sticky Style.', 'w9sme' )
		),
		
		array(
			'id'       => 'nav-sticky-item-color',
			'output'   => array( 'color' => '.is-sticky .w9sme-nav-body' ),
			'type'     => 'color',
			'title'    => esc_html__( 'Sticky nav item color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav color.', 'w9sme' ),
			'default'  => '#ffffff',
		),
		
		array(
			'id'       => 'nav-sticky-item-link-color',
			'type'     => 'link_color',
			'title'    => esc_html__( 'Sticky nav item link color', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose nav link color.', 'w9sme' ),
			'output'   => array( '.is-sticky .w9sme-nav-body .w9sme-main-menu-content>li>a,.is-sticky .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a' ),
			'desc'     => esc_html__( 'Hover, active color available for link in nav only.', 'w9sme' ),
			'default'  => array(
				'regular' => '#222',
				'hover'   => '#EB6C6C',
				'active'  => '#EB6C6C',
			)
		),
		
		array(
			'id'       => 'nav-sticky-link-hover-background',
			'type'     => 'color_rgba',
			'output'   => array( 'background-color' => '.is-sticky .w9sme-nav-body .w9sme-main-menu-content>li>a:hover, .is-sticky .w9sme-nav-body .w9sme-nav-item:not(.w9sme-nav-main-menu-wrapper) a:hover' ),
			'title'    => esc_html__( 'Sticky nav link hover background', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose link background color when hover.', 'w9sme' ),
			'default'  => '',
//            'validate' => 'color',
		),
		
		array(
			'id'      => 'nav-sticky-background',
			'type'    => 'background',
			'output'  => array( '.is-sticky .w9sme-nav-body' ),
			'title'   => esc_html__( 'Sticky nav background', 'w9sme' ),
			'default' => array(
				'background-color' => 'transparent',
			)
		),
		
		array(
			'id'      => 'nav-sticky-background-overlay',
			'type'    => 'color_rgba',
			'title'   => esc_html__( 'Sticky nav background overlay', 'w9sme' ),
			'output'  => array( 'background-color' => '.is-sticky .w9sme-nav-body:before' ),
			'default' => array(
				'color' => '#f2f2f2',
				'alpha' => 1
			),
		),
		
		array(
			'id'      => 'nav-sticky-border',
			'type'    => 'border',
			'title'   => esc_html__( 'Sticky nav border', 'w9sme' ),
			'output'  => array( '.is-sticky .w9sme-nav-body' ),
			'all'     => false,
			'color'   => false,
			'default' => array(
				'border-style'  => 'solid',
				'border-top'    => '0',
				'border-right'  => '0',
				'border-bottom' => '0',
				'border-left'   => '0'
			)
		),
		
		array(
			'id'       => 'nav-sticky-border-color',
			'title'    => esc_html__( 'Nav border color', 'w9sme' ),
			'subtitle' => esc_html__( 'Specify the sticky nav border color.', 'w9sme' ),
			'output'   => array(
				'border-color' => '.is-sticky .w9sme-nav-body'
			),
			'type'     => 'color_rgba',
			'default'  => array(
				'color' => '#888',
				'alpha' => '0.4',
			),
		),
	)
);
