<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-general.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$maintenance_mode = array();
if ( w9sme_get_current_preset() === W9SME_THEME_OPTIONS_DEFAULT_NAME ) {
	$maintenance_mode = array(
		array(
			'id'   => 'general-random-number-4',
			'type' => 'info',
			'desc' => esc_html__( 'Maintenance Mode', 'w9sme' )
		),
		array(
			'id'       => 'maintenance-mode',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Maintenance mode', 'w9sme' ),
			'subtitle' => '',
			'desc'     => '',
			'options'  => array( '2' => 'On (Custom Page)', '1' => 'On (Standard)', '0' => 'Off', ),
			'default'  => '0'
		),
		array(
			'id'       => 'maintenance-mode-page',
			'type'     => 'select',
			'data'     => 'pages',
			'title'    => esc_html__( 'Custom maintenance mode page', 'w9sme' ),
			'subtitle' => esc_html__( 'if you would like to show a custom page instead of the standard WordPress message, select the page that is your maintenance page.', 'w9sme' ),
			'desc'     => '',
			'default'  => '',
			'required' => array( 'maintenance-mode', '=', '2' )
		),
		array(
			'id'   => 'general-random-number-5',
			'type' => 'info',
			'desc' => esc_html__( 'Shared Info Settings', 'w9sme' )
		),
		array(
			'id'       => 'shared-info',
			'type'     => 'multi_label_text',
			'title'    => esc_html__( 'Shared Info', 'w9sme' ),
			'subtitle' => __( 'Shared info only work in Theme Options, Widgets and Post Content.<br/> Just use the syntax <b>{{label}}</b> to reference to the <b>content</b>.', 'w9sme' ),
		)
	);
}

$this->sections[] = array(
	'title'  => esc_html__( 'General Settings', 'w9sme' ),
	'desc'   => esc_html__( 'Configure easily the basic theme\'s settings.', 'w9sme' ),
	'icon'   => 'el-icon-adjust-alt',
	'fields' => array_merge(
		array(
			array(
				'id'   => 'general-random-number-1',
				'type' => 'info',
				'desc' => esc_html__( 'General Settings', 'w9sme' )
			),
			array(
				'id'       => 'smooth-scroll',
				'type'     => 'switch',
				'title'    => esc_html__( 'Smooth scroll', 'w9sme' ),
				'subtitle' => esc_html__( 'Smooth animation when scrolling the page.', 'w9sme' ),
				'desc'     => '',
				'default'  => 0
			),
			
			array(
				'id'       => 'custom-scroll',
				'type'     => 'switch',
				'title'    => esc_html__( 'Custom scroll bar', 'w9sme' ),
				'subtitle' => esc_html__( 'Enable or disable custom scroll bar.', 'w9sme' ),
				'desc'     => '',
				'default'  => 0
			),
			
			
			array(
				'id'       => 'custom-scroll-width',
				'type'     => 'dimensions',
				'title'    => esc_html__( 'Custom scroll bar width', 'w9sme' ),
				'desc'     => esc_html__( 'This must be numeric (no px) or empty.', 'w9sme' ),
				'units'    => 'px',
				'height'   => false,
				'default'  => array(
					'width' => '10'
				),
				'validate' => 'not_empty',
				'output'   => array( 'body::-webkit-scrollbar' ),
				'required' => array( 'custom-scroll', '=', array( '1' ) ),
			),
			
			array(
				'id'                    => 'custom-scroll-color',
				'type'                  => 'background',
				'title'                 => esc_html__( 'Custom scroll bar color', 'w9sme' ),
				'subtitle'              => esc_html__( 'Set custom scroll bar color.', 'w9sme' ),
				'background-repeat'     => false,
				'background-attachment' => false,
				'background-position'   => false,
				'background-image'      => false,
				'preview'               => false,
				'transparent'           => false,
				'background-size'       => false,
				'validate'              => 'color',
				'required'              => array( 'custom-scroll', '=', array( '1' ) ),
				'default'               => array(
					'background-color' => '#eee',
				),
				'output'                => array( 'body::-webkit-scrollbar' ),
			),
			
			array(
				'id'                    => 'custom-scroll-thumb-color',
				'type'                  => 'background',
				'title'                 => esc_html__( 'Custom scroll bar thumb color', 'w9sme' ),
				'subtitle'              => esc_html__( 'Set custom scroll bar thumb color.', 'w9sme' ),
				'background-repeat'     => false,
				'background-attachment' => false,
				'background-position'   => false,
				'background-image'      => false,
				'preview'               => false,
				'transparent'           => false,
				'background-size'       => false,
				'validate'              => 'color',
				'default'               => array(
					'background-color' => '#fff',
				),
				'required'              => array( 'custom-scroll', '=', array( '1' ) ),
				'output'                => array( 'body::-webkit-scrollbar-thumb' ),
			),
			
			array(
				'id'       => 'back-to-top',
				'type'     => 'switch',
				'title'    => esc_html__( 'Back to top', 'w9sme' ),
				'subtitle' => esc_html__( 'Enable or disable back to top button.', 'w9sme' ),
				'desc'     => '',
				'default'  => '1'
			),

//        array(
//            'id'       => 'panel-selector',
//            'type'     => 'switch',
//            'title'    => esc_html__( 'Panel selector', 'w9sme' ),
//            'subtitle' => esc_html__( 'Enable or disable panel selector.', 'w9sme' ),
//            'desc'     => '',
//            'default'  => '0'
//        ),
//			array(
//				'id'       => 'rtl-mode',
//				'type'     => 'switch',
//				'title'    => esc_html__( 'Force RTL mode', 'w9sme' ),
//				'subtitle' => esc_html__( 'Force enable rtl mode.', 'w9sme' ),
//				'desc'     => '',
//				'default'  => '0'
//			),
//			array(
//				'id'       => 'input-style',
//				'type'     => 'button_set',
//				'title'    => esc_html__( 'Input style', 'w9sme' ),
//				'subtitle' => esc_html__( 'Select global input style.', 'w9sme' ),
//				'desc'     => '',
//				'options'  => array(
//					'normal'        => esc_html__( 'Normal', 'w9sme' ),
//					'border-bottom' => esc_html__( 'Bottom Line', 'w9sme' ),
//				),
//				'compiler' => true,
//				'default'  => 'normal'
//			),
			
			array(
				'id'       => 'widget-title-style',
				'type'     => 'button_set',
				'title'    => esc_html__( 'Widget title style', 'w9sme' ),
				'subtitle' => esc_html__( 'Select general widget title style.', 'w9sme' ),
				'desc'     => '',
				'options'  => array(
					'style-1' => esc_html__( 'Style 1', 'w9sme' ),
					'style-2' => esc_html__( 'Style 2', 'w9sme' ),
				),
				'default'  => 'style-2'
			),
			
			array(
				'id'      => 'google-map-api',
				'type'    => 'text',
				'title'   => esc_html__( 'Google map API key', 'w9sme' ),
				'desc'    => sprintf( esc_html__( 'Enter your google map api key, very important option if you want to use W9sme Google Maps shortcode. %s', 'w9sme' ), '<br/><a href="https://developers.google.com/maps/documentation/javascript/get-api-key" target="_blank">' . esc_html__( 'Read the google map documentation for more information.', 'w9sme' ) . '</a>' ),
				'default' => '',
			),
			array(
				'id'   => 'general-random-number-2',
				'type' => 'info',
				'desc' => esc_html__( 'Performances', 'w9sme' )
			),
			array(
				'id'       => 'use-min-files',
				'type'     => 'switch',
				'title'    => esc_html__( 'Use compressed CSS & Script files', 'w9sme' ),
				'subtitle' => esc_html__( 'Enable this option will speed up your page performance.', 'w9sme' ),
				'desc'     => '',
				'default'  => '0',
				'compiler' => true,
			),
			
			array(
				'id'   => 'general-random-number-3',
				'type' => 'info',
				'desc' => esc_html__( 'Body Settings', 'w9sme' )
			),
			array(
				'id'       => 'body-layout',
				'type'     => 'image_select',
				'title'    => esc_html__( 'Body layout', 'w9sme' ),
				'subtitle' => esc_html__( 'Select the layout style of page body.', 'w9sme' ),
				'desc'     => '',
				'options'  => array(
					'wide'     => array(
						'title' => 'Wide',
						'img'   => w9sme_theme_url() . 'assets/images/layout-wide.png'
					),
					'boxed'    => array(
						'title' => 'Boxed',
						'img'   => w9sme_theme_url() . 'assets/images/layout-boxed.png'
					),
					'extended' => array(
						'title' => 'Extended',
						'img'   => w9sme_theme_url() . 'assets/images/layout-extended.png'
					),
					'float'    => array(
						'title' => 'Float',
						'img'   => w9sme_theme_url() . 'assets/images/layout-float.png'
					)
				),
				'default'  => 'wide'
			),
			array(
				'id'       => 'body-background',
				'type'     => 'background',
				'title'    => esc_html__( 'Body background', 'w9sme' ),
				'subtitle' => esc_html__( 'Config the style for body background.', 'w9sme' ),
				'output'   => array( 'Body' ), // An array of CSS selectors to apply this font style to dynamically
				'default'  => array(
					'background-color'      => '',
					'background-repeat'     => 'no-repeat',
					'background-position'   => 'center center',
					'background-attachment' => 'fixed',
					'background-size'       => 'cover'
				),
				'required' => array( 'body-layout', '!=', 'wide' )
			),
			array(
				'id'       => 'site-content-background',
				'type'     => 'background',
				'title'    => esc_html__( 'Site content background', 'w9sme' ),
				'subtitle' => esc_html__( 'Config the style for site content background.', 'w9sme' ),
				'output'   => array( '#page.site' ),
				// An array of CSS selectors to apply this font style to dynamically
				'default'  => array(
					'background-color'      => '',
					'background-repeat'     => 'no-repeat',
					'background-position'   => 'center center',
					'background-attachment' => 'fixed',
					'background-size'       => 'cover'
				),
			)
		),
		$maintenance_mode
	),
);