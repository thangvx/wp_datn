<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-cpt-service-single.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! class_exists( 'W9sme_CPT_Service' ) || w9sme_get_option( 'cpt-service-enable' ) == '0' ) {
	return;
}

/*-------------------------------------\
	SERVICE CPT
---------------------------------------*/
$this->sections[] = array(
	'title'      => esc_html__( 'Service Booking', 'w9sme' ),
	'desc'       => esc_html__( 'Notice: All setttings in this section only work on reservation page, using the reservation form we provided in: {theme_path}/template-html/reservation-template.html', 'w9sme' ),
	'icon'       => 'dashicons-before dashicons-share-alt',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'service-booking-date-format',
			'type'     => 'text',
			'title'    => esc_html__( 'Date Format', 'w9sme' ),
			'subtitle' => esc_html__( 'Enter Custom Date Format in DatePicker Field.', 'w9sme' ),
			'desc'     => sprintf(__('Default date format is yyyy/M/dd.<br>Date format reference <a href="%s" target="_blank">here</a>', 'w9sme'), 'https://bootstrap-datepicker.readthedocs.io/en/latest/options.html#format'),
			'default'  => 'yyyy/M/dd',
		),
		
		array(
			'id'       => 'service-booking-service-picker-placeholder',
			'type'     => 'text',
			'title'    => esc_html__( 'Service Picker Placeholder', 'w9sme' ),
			'subtitle' => esc_html__( 'Enter Custom Service Picker Placeholder.', 'w9sme' ),
			'desc'     => 'Default value is "--".',
			'default'  => '--',
		),
		
		array(
			'id'       => 'service-booking-using-service-data',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Using Service Data', 'w9sme' ),
			'subtitle' => esc_html__( 'Using service data in Services picker field.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'1' => esc_html__( 'On', 'w9sme' ),
				'0' => esc_html__( 'Off', 'w9sme' )
			),
			'default'  => '0'
		),
		
		array(
			'id'       => 'service-booking-auto-select-service',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Auto Select Service', 'w9sme' ),
			'subtitle' => esc_html__( 'This will auto select your selected service form service list.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'1' => esc_html__( 'On', 'w9sme' ),
				'0' => esc_html__( 'Off', 'w9sme' )
			),
			'default'  => '0',
			'required' => array( 'service-booking-using-service-data', '=', array( '1' ) ),
		),
	),
);