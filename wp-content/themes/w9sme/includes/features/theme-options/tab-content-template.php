<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-content-template.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

/*
 * Content Template
*/

$content_template_list = w9sme_get_content_template_list();

$this->sections[] = array(
    'title'  => esc_html__( 'Content Template', 'w9sme' ),
    'desc'   => sprintf( esc_html__( 'Select content template for some specific position in page or %s here.', 'w9sme' ), '<a href="' . admin_url( '/post-new.php?post_type=content-template' ) . '" target="_blank">' . esc_html__( 'create new', 'w9sme' ) . '</a>' ),
    'icon'   => 'dashicons-before dashicons-format-aside',
    'fields' => array(
        
        array(
            'id'        => 'content-template-override-default',
            'type'      => 'select',
            'multi'     => true,
            'title'     => esc_html__( 'Override default content template settings in', 'w9sme' ),
            'subtitle'  => esc_html__( 'Choose which template you need to override the default settings in here.', 'w9sme' ),
            'desc'      => esc_html__( 'The tabs will appear after you save the change.', 'w9sme' ) .
                '<br />  <strong style="color: red;">' . esc_html__( 'Notice 1: If the page doesn\'t auto refresh, please refresh the page after changing and saving this option.', 'w9sme' ) . '</strong> </br> <strong style="color: red;">' .
                esc_html__( 'Notice 2: If an item is removed, the options will be saved automatically after the page auto refresh, please wait a bit.', 'w9sme' ) . '</strong>',
            'options'   => w9sme_get_template_prefix( 'options_field' ),
            'default'   => array(),
            'ajax_save' => false
//            'compiler' => true,
//            'reload_on_change' => true
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__( 'Header Area', 'w9sme' ),
        ),
        // Before header
        array(
            'id'      => 'content-template-before-header',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for before header area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for before header area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),
        
        // After header
        array(
            'id'      => 'content-template-after-header',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for after header area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for after header area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__( 'Page Title Area', 'w9sme' ),
        ),
        
        // After page title
        array(
            'id'      => 'content-template-after-page-title',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for after page title area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for after page title area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__( 'Footer Area', 'w9sme' ),
        ),
        
        // Before footer
        array(
            'id'      => 'content-template-before-footer',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for before footer area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for before footer area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),
        
        
        // After footer
        array(
            'id'      => 'content-template-after-footer',
            'type'    => 'select',
            'title'   => esc_html__( 'Content template for after footer area', 'w9sme' ),
            'desc'    => esc_html__( 'Select content template for after footer area.', 'w9sme' ),
            'options' => $content_template_list,
            'default' => '',
        ),
    
    ), // #fields
);