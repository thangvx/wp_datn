<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-product-archive.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

if (!w9sme_is_woocommerce_active()) {
	return;
}

$this->sections[] = array(
    'title'  => esc_html__( 'Shop', 'w9sme' ),
    'desc'   => esc_html__( 'Shop (Woocommerce) settings.', 'w9sme' ),
    'icon'   => 'w9 w9-ico-shopper29',
    'fields' => array(
        // cart icon
        array(
            'id'       => 'shop-cart-icon',
            'type'     => 'button_set',
            'title'    => esc_html__('Cart icon', 'w9sme' ),
            'options'  => array(
                'flor-ico flor-ico-icon-bag'   => '<i class="flor-ico flor-ico-icon-bag"></i>',
                'flor-ico flor-ico-icon-bag-alt'   => '<i class="flor-ico flor-ico-icon-bag-alt"></i>',
                'w9 w9-ico-shopper29'   => '<i class="w9 w9-ico-shopper29"></i>',
                'w9 w9-ico-svg-icon-02' => '<i class="w9 w9-ico-svg-icon-02"></i>',
                'flor-ico flor-ico-icon-cart-alt' => '<i class="flor-ico flor-ico-icon-cart-alt"></i>',
                'flor-ico flor-ico-icon-cart'    => '<i class="flor-ico flor-ico-icon-cart"></i>',
//                'w9 w9-ico-svg-icon-16' => '<i class="w9 w9-ico-svg-icon-16"></i>',
            ),
            'subtitle' => esc_html__( 'Choose your favorite cart icon.', 'w9sme' ),
            'default'  => 'flor-ico flor-ico-icon-bag',
            'compiler' => true
        ),
        
        array(
            'id'       => 'shop-quick-view',
            'type'     => 'switch',
            'title'    => esc_html__( 'Quick view', 'w9sme' ),
            'subtitle' => esc_html__( 'Turn on or off product quick view feature.', 'w9sme' ),
            'default'  => 1
        ),

//        array(
//            'id'       => 'shop-quick-view-nav',
//            'type'     => 'switch',
//            'title'    => esc_html__( 'Quick view navigation', 'w9sme' ),
//            'subtitle' => esc_html__( 'Turn on or off product quick view navigation feature.', 'w9sme' ),
//            'default'  => 0,
//            'required' => array(
//                'shop-quick-view',
//                '=',
//                1
//            )
//        ),

        /*-------------------------------------
        	MINI CART
        ---------------------------------------*/
        array(
            'id'       => 'shop-mini-cart-ajax-actions',
            'type'     => 'switch',
            'title'    => esc_html__( 'Mini cart ajax actions', 'w9sme' ),
            'subtitle' => esc_html__( 'Using ajax for actions in mini cart.', 'w9sme' ),
            'default'  => 0,
        ),

        array(
            'id'       => 'shop-mini-cart-scrollbar-start',
            'type'     => 'text',
            'title'    => esc_html__( 'Mini cart scrollbar start', 'w9sme' ),
            'subtitle' => esc_html__( 'Enter amount of item show be shown in the mini cart, if number of item in the cart greater than this value, the scroll bar will be appeared. Default value is 3.', 'w9sme' ),
            'desc'     => esc_html__( 'If you find that changing this option does not work, please try to add an item to the cart to refresh the session storage.', 'w9sme' ),
            'validate' => 'numeric',
            'default'  => '3'
        ),
	    
	    /*-------------------------------------
	    	IMAGES
	    ---------------------------------------*/
	    array(
		    'id'       => 'shop-image-padding',
		    'type'     => 'switch',
		    'title'    => esc_html__( 'Display Image Padding', 'w9sme' ),
		    'subtitle' => esc_html__( 'Add some image padding in shop for visibility', 'w9sme' ),
		    'default'  => '0'
	    ),
    )
);

$this->sections[] = array(
    'title'      => esc_html__( 'Product Archive', 'w9sme' ),
    'desc'       => esc_html__( 'Product archive settings.', 'w9sme' ),
    'icon'       => 'el el-folder-close',
    'subsection' => true,
    'fields'     => array(
//        array(
//            'id'       => 'product-archive-display-type',
//            'type'     => 'select',
//            'title'    => esc_html__( 'Display type', 'w9sme' ),
//            'subtitle' => esc_html__( 'Select archive display type.', 'w9sme' ),
//            'desc'     => '',
//            'options'  => array(
//                'simple' => esc_html__( 'Simple', 'w9sme' ),
//                'classic' => esc_html__( 'Classic', 'w9sme' ),
//                'modern'  => esc_html__( 'Modern', 'w9sme' ),
//            ),
//            'default'  => 'simple'
//        ),
        
        array(
            'id'       => 'product-archive-display-columns',
            'type'     => 'select',
            'title'    => esc_html__( 'Display columns', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the number of columns to display on archive pages.', 'w9sme' ),
            'options'  => array(
                '2' => '2',
                '3' => '3',
                '4' => '4'
            ),
            'desc'     => '',
            'default'  => '3'
        ),
        
        array(
            'id'       => 'product-archive-item-per-page',
            'type'     => 'text',
            'title'    => esc_html__( 'Item per page', 'w9sme' ),
            'subtitle' => esc_html__( 'Enter amount of item per page. Default value is 12.', 'w9sme' ),
            'validate' => 'numeric',
            'default'  => '12'
        ),
        array(
            'id'       => 'product-archive-paging-type',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Paging type', 'w9sme' ),
            'subtitle' => esc_html__( 'Select archive paging type.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'default'         => esc_html__( 'Default', 'w9sme' ),
                'load-more'       => esc_html__( 'Load More', 'w9sme' ),
                'infinite-scroll' => esc_html__( 'Infinite Scroll', 'w9sme' )
            ),
            'default'  => 'default'
        ),
        
        array(
            'id'             => 'product-archive-margin',
            'type'           => 'spacing',
            'mode'           => 'margin',
            'units'          => 'px',
            'units_extended' => 'false',
            'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
            'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
            'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
            'left'           => false,
            'right'          => false,
            'default'        => array(
                'margin-top'    => '80px',
                'margin-bottom' => '0',
                'units'         => 'px',
            ),
            'output'         => array( '.site-main-archive.product-archive' )
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__('Layout Settings', 'w9sme' ),
        ),
        array(
            'id'       => 'product-archive-layout',
            'type'     => 'select',
            'title'    => esc_html__( 'Layout', 'w9sme' ),
            'subtitle' => esc_html__( 'Select archive layout.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
                'container'       => esc_html__( 'Container', 'w9sme' ),
                'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
                'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
            ),
            'default'  => 'container-xlg',
            'validate' => 'not_empty'
        ),
	
	    array(
		    'id'       => 'product-archive-widget-title-style',
		    'type'     => 'button_set',
		    'title'    => esc_html__( 'Widget title style', 'w9sme' ),
		    'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
		    'desc'     => '',
		    'options'  => array(
			    ''        => esc_html__( 'Default', 'w9sme' ),
			    'style-1' => esc_html__( 'Style 1', 'w9sme' ),
			    'style-2' => esc_html__( 'Style 2', 'w9sme' ),
		    ),
		    'default'  => ''
	    ),
        
        array(
            'id'       => 'product-archive-sidebar',
            'type'     => 'image_select',
            'title'    => esc_html__( 'Sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
                'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
                'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
                'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
            ),
            'default'  => 'none'
        ),
        
        array(
            'id'       => 'product-archive-sidebar-width',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
                'large' => esc_html__( 'Large (1/3)', 'w9sme' )
            ),
            'default'  => 'small',
            'required' => array( 'product-archive-sidebar', '=', array( 'left', 'both', 'right' ) ),
        ),
        
        array(
            'id'       => 'product-archive-sidebar-left',
            'type'     => 'select',
            'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-1',
            'required' => array( 'product-archive-sidebar', '=', array( 'left', 'both' ) ),
        ),
        
        array(
            'id'       => 'product-archive-sidebar-right',
            'type'     => 'select',
            'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-2',
            'required' => array( 'product-archive-sidebar', '=', array( 'right', 'both' ) ),
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__('Other Settings', 'w9sme' )
        ),
        array(
            'id'       => 'product-archive-sale-flash',
            'type'     => 'switch',
            'title'    => esc_html__('Sale flash', 'w9sme' ),
            'subtitle' => esc_html__( 'Show or hide sale flash in product archive page.', 'w9sme' ),
            'default'  => '1'
        ),
        array(
            'id'       => 'product-archive-add-to-cart-btn',
            'type'     => 'switch',
            'title'    => esc_html__( 'Add-to-cart button', 'w9sme' ),
            'subtitle' => esc_html__( 'Show or hide the add-to-cart button in product archive page.', 'w9sme' ),
            'default'  => 1
        ),
        array(
            'id'       => 'product-archive-show-result-count',
            'type'     => 'switch',
            'title'    => esc_html__( 'Result count', 'w9sme' ),
            'subtitle' => esc_html__( 'Show/hide result count in product archive page.', 'w9sme' ),
            'desc'     => '',
            'default'  => '1'
        ),
        array(
            'id'       => 'product-archive-show-catalog-ordering',
            'type'     => 'switch',
            'title'    => esc_html__( 'Catalog ordering', 'w9sme' ),
            'subtitle' => esc_html__( 'Show/hide catalog ordering in product archive page.', 'w9sme' ),
            'desc'     => '',
            'default'  => '1'
        ),
        array(
            'id'       => 'product-archive-show-rating',
            'type'     => 'switch',
            'title'    => esc_html__( 'Product rating', 'w9sme' ),
            'subtitle' => esc_html__( 'Show or hide product rating in product archive page.', 'w9sme' ),
            'desc'     => '',
            'default'  => '1'
        ),
        array(
            'id'       => 'product-archive-secondary-image-on-hover',
            'type'     => 'switch',
            'title'    => esc_html__( 'Product secondary image on hover', 'w9sme' ),
            'subtitle' => esc_html__( 'Show or hide secondary image when hover on the product image in archive product page.', 'w9sme' ),
            'desc'     => esc_html__( 'This feature will get the first item of Product Gallery, if your product does not have any image in Product Gallery, this feature will not work for that product.', 'w9sme' ),
            'default'  => '1'
        ),
    )
);