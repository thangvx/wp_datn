<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-cpt-portfolio-single.php
 * @time    : 8/26/16 12:35 PM
 * @author  : 9WPThemes Team
 */
if ( !defined( 'ABSPATH' ) ) {
    die( '-1' );
}

$content_template_list = w9sme_get_content_template_list();
/*-------------------------------------\
	PORTFOLIO CPT
---------------------------------------*/
$this->sections[] = array(
    'title'      => esc_html__( 'Portfolio Single', 'w9sme' ),
    'desc'       => esc_html__( 'Portfolio Single settings section.', 'w9sme' ),
    'icon'       => 'dashicons-before dashicons-awards',
    'subsection' => true,
    'fields'     => array(
	    array(
		    'id'      => 'portfolio-single-content-layout',
		    'type'    => 'select',
		    'title'   => esc_html__( 'Content Layout', 'w9sme' ),
		    'desc'    => esc_html__( 'Select content template which made up layout for single portfolio.', 'w9sme' ),
		    'options' => $content_template_list,
		    'default' => '',
	    ),
        
        array(
            'id'             => 'portfolio-single-margin',
            'type'           => 'spacing',
            'mode'           => 'margin',
            'units'          => 'px',
            'units_extended' => 'false',
            'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
            'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
            'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
            'left'           => false,
            'right'          => false,
            'default'        => array(
                'margin-top'    => '0',
                'margin-bottom' => '0',
                'units'         => 'px',
            ),
            'output'         => array( '.site-main-single.portfolio-single' )
        ),
        
        array(
            'id'       => 'portfolio-single-navigator-enabled',
            'type'     => 'switch',
            'title' => esc_html__('Navigator enabled', 'w9sme'),
            'subtitle' => esc_html__('Enable navigator bottom of single page.', 'w9sme'),
            'default' => false
        ),
        
        array(
            'id'       => mt_rand(),
            'type'     => 'info',
            'subtitle' => esc_html__('Layout Settings', 'w9sme')
        ),

        array(
            'id'       => 'portfolio-single-layout',
            'type'     => 'select',
            'title'    => esc_html__( 'Layout', 'w9sme' ),
            'subtitle' => esc_html__( 'Select single portfolio layout.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
                'container'       => esc_html__( 'Container', 'w9sme' ),
                'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
                'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
            ),
            'default'  => 'fullwidth'
        ),

        array(
            'id'       => 'portfolio-single-sidebar',
            'type'     => 'image_select',
            'title'    => esc_html__( 'Sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
                'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
                'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
                'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
            ),
            'default'  => 'none'
        ),

        array(
            'id'       => 'portfolio-single-sidebar-width',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
            'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
            'desc'     => '',
            'options'  => array(
                'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
                'large' => esc_html__( 'Large (1/3)', 'w9sme' )
            ),
            'default'  => 'small',
            'required' => array( 'portfolio-single-sidebar', '=', array( 'left', 'both', 'right' ) ),
        ),
        
        array(
            'id'       => 'portfolio-single-sidebar-left',
            'type'     => 'select',
            'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-1',
            'required' => array( 'portfolio-single-sidebar', '=', array( 'left', 'both' ) ),
        ),

        array(
            'id'       => 'portfolio-single-sidebar-right',
            'type'     => 'select',
            'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
            'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
            'data'     => 'sidebars',
            'desc'     => '',
            'default'  => 'sidebar-1',
            'required' => array( 'portfolio-single-sidebar', '=', array( 'right', 'both' ) ),
        ),
    ),
);