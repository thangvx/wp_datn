<?php
/**
 * Copyright(c) 2016, 9WPThemes
 * @filename: tab-blog-archive.php
 * @time    : 8/26/16 12:07 PM
 * @author  : 9WPThemes Team
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$this->sections[] = array(
	'title'  => esc_html__( 'Blog Settings', 'w9sme' ),
	'desc'   => esc_html__( 'Archive settings.', 'w9sme' ),
	'icon'   => 'el el-list-alt',
	'fields' => array()
);

$this->sections[] = array(
	'title'      => esc_html__( 'Blog Archive', 'w9sme' ),
	'desc'       => esc_html__( 'Archive settings.', 'w9sme' ),
	'icon'       => 'el el-folder-close',
	'subsection' => true,
	'fields'     => array(
		array(
			'id'       => 'blog-archive-display-type',
			'type'     => 'select',
			'title'    => esc_html__( 'Display type', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive display type.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'wide'    => esc_html__( 'Wide', 'w9sme' ),
				'grid'    => esc_html__( 'Grid', 'w9sme' ),
				'masonry' => esc_html__( 'Masonry', 'w9sme' ),
			),
			'default'  => 'wide',
			'validate' => 'not_empty'
		),
		
		array(
			'id'       => 'blog-archive-display-style',
			'type'     => 'select',
			'title'    => esc_html__( 'Display style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive display style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'style-1' => esc_html__( 'Classic', 'w9sme' ),
				'style-2' => esc_html__( 'Modern 1', 'w9sme' ),
				'style-3' => esc_html__( 'Modern 2', 'w9sme' ),
			),
			'default'  => 'style-1'
		),
		
		array(
			'id'       => 'blog-archive-display-columns',
			'type'     => 'select',
			'title'    => esc_html__( 'Display columns', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the number of columns to display on archive pages.', 'w9sme' ),
			'options'  => array(
				'2' => '2',
				'3' => '3',
				'4' => '4'
			),
			'desc'     => '',
			'default'  => '3',
			'required' => array( 'blog-archive-display-type', '=', array( 'masonry', 'grid' ) ),
		),

		array(
			'id'       => 'blog-archive-item-header',
			'type'     => 'switch',
			'title'    => esc_html__( 'Enable blog item header', 'w9sme' ),
			'subtitle' => esc_html__( 'Enable blog item header.', 'w9sme' ),
			'default'  => 1
		),
		
		array(
			'id'      => 'blog-archive-item-image-ratio',
			'type'    => 'select',
			'title'   => esc_html__( 'Blog item image ratio', 'w9sme' ),
			'desc'    => '',
			'options' => array(
				'0.5625'        => '16x9',
				'1.77777777778' => '9x16',
				'0.42857142857' => '21x9',
				'2.33333333333' => '9x21',
				'0.75'          => '4x3',
				'1.33333333333' => '3x4',
				'0.666666667'   => '3x2',
				'1.5'           => '2x3',
				'1'             => '1x1',
				'0.5'           => '2x1',
				'2'             => '1x2',
				'original'      => esc_html__( 'Original', 'w9sme' ),
			),
			'default' => 'original',
		),
		
		array(
			'id'       => 'blog-archive-item-image-action',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Blog item feature image action', 'w9sme' ),
			'subtitle' => esc_html__( 'Action with the feature image of the blog item.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'none' => esc_html__( 'None', 'w9sme' ),
				'link' => esc_html__( 'Link to post', 'w9sme' ),
			),
			'default'  => 'link',
		),

		array(
			'id'       => 'blog-archive-post-meta',
			'type'     => 'checkbox',
			'title'    => esc_html__( 'Meta', 'w9sme' ),
			'subtitle' => esc_html__( 'Disable or enable meta options.', 'w9sme' ),
			'options'  => array(
				'date'         => esc_html__( 'Date', 'w9sme' ),
				'author'       => esc_html__( 'Author', 'w9sme' ),
				'categories'   => esc_html__( 'Categories', 'w9sme' ),
				'tags'         => esc_html__( 'Tags', 'w9sme' ),
				'comments'     => esc_html__( 'Comments', 'w9sme' ),
				'social-share' => esc_html__( 'Social Share', 'w9sme' ),
			),
			'default'  => array(
				'date'         => 1,
				'author'       => 1,
				'categories'   => 1,
				'tags'         => 0,
				'comments'     => 1,
				'social-share' => 1,
			),
		),
		
		array(
			'id'       => 'blog-archive-excerpt-length',
			'type'     => 'text',
			'title'    => esc_html__( 'Excerpt length', 'w9sme' ),
			'subtitle' => esc_html__( 'Limit the excerpt length (words). Default value is 50 words.', 'w9sme' ),
			'validate' => 'numeric',
			'default'  => ''
		),
		
		array(
			'id'       => 'blog-archive-paging-type',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Paging type', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive paging type.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'default'         => esc_html__( 'Default', 'w9sme' ),
				'load-more'       => esc_html__( 'Load More', 'w9sme' ),
				'infinite-scroll' => esc_html__( 'Infinite Scroll', 'w9sme' )
			),
			'default'  => 'default'
		),
		
		array(
			'id'       => 'blog-archive-paging-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Paging style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive paging style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'style-1' => esc_html__( 'Classic', 'w9sme' ),
				'style-2' => esc_html__( 'Modern', 'w9sme' ),
			),
			'required' => array( 'blog-archive-paging-type', '=', array( 'default' ) ),
			'default'  => 'style-2'
		),
		
		array(
			'id'       => 'blog-archive-paging-align',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Paging align', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive paging align.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'left'   => esc_html__( 'left', 'w9sme' ),
				'center' => esc_html__( 'Center', 'w9sme' ),
				'right'  => esc_html__( 'Right', 'w9sme' ),
			),
			'required' => array( 'blog-archive-paging-type', '=', array( 'default' ) ),
			'default'  => 'center'
		),
		
		array(
			'id'             => 'blog-archive-margin',
			'type'           => 'spacing',
			'mode'           => 'margin',
			'units'          => 'px',
			'units_extended' => 'false',
			'title'          => esc_html__( 'Margin top/bottom', 'w9sme' ),
			'subtitle'       => esc_html__( 'This must be numeric (no px). Leave blank for default.', 'w9sme' ),
			'desc'           => esc_html__( 'If you would like to override the default footer top/bottom margin, then you can do so here.', 'w9sme' ),
			'left'           => false,
			'right'          => false,
			'default'        => array(
				'margin-top'    => '0',
				'margin-bottom' => '0',
				'units'         => 'px',
			),
			'output'         => array( '.site-main-archive' )
		),
		
		array(
			'id'       => mt_rand(),
			'type'     => 'info',
			'subtitle' => esc_html__( 'Layout Settings', 'w9sme' ),
		),
		array(
			'id'       => 'blog-archive-layout',
			'type'     => 'select',
			'title'    => esc_html__( 'Layout', 'w9sme' ),
			'subtitle' => esc_html__( 'Select archive layout.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'fullwidth'       => esc_html__( 'Full Width', 'w9sme' ),
				'container'       => esc_html__( 'Container', 'w9sme' ),
				'container-xlg'   => esc_html__( 'Container Extended', 'w9sme' ),
				'container-fluid' => esc_html__( 'Container Fluid', 'w9sme' )
			),
			'default'  => 'container',
			'validate' => 'not_empty'
		),
		
		array(
			'id'       => 'blog-archive-widget-title-style',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Widget title style', 'w9sme' ),
			'subtitle' => esc_html__( 'Select widget title style. If this field is set to default, the same one on "General Tab" will take control', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				''        => esc_html__( 'Default', 'w9sme' ),
				'style-1' => esc_html__( 'Style 1', 'w9sme' ),
				'style-2' => esc_html__( 'Style 2', 'w9sme' ),
			),
			'default'  => ''
		),
		
		array(
			'id'       => 'blog-archive-sidebar',
			'type'     => 'image_select',
			'title'    => esc_html__( 'Sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar style.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'none'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-none.png' ),
				'left'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-left.png' ),
				'right' => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-right.png' ),
				'both'  => array( 'title' => '', 'img' => w9sme_theme_url() . 'assets/images/sidebar-both.png' ),
			),
			'default'  => 'right'
		),
		
		array(
			'id'       => 'blog-archive-sidebar-width',
			'type'     => 'button_set',
			'title'    => esc_html__( 'Sidebar width', 'w9sme' ),
			'subtitle' => esc_html__( 'Set sidebar width.', 'w9sme' ),
			'desc'     => '',
			'options'  => array(
				'small' => esc_html__( 'Small (1/4)', 'w9sme' ),
				'large' => esc_html__( 'Large (1/3)', 'w9sme' )
			),
			'default'  => 'large',
			'required' => array( 'blog-archive-sidebar', '=', array( 'left', 'both', 'right' ) ),
		),
		
		
		array(
			'id'       => 'blog-archive-sidebar-left',
			'type'     => 'select',
			'title'    => esc_html__( 'Left sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default left sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-1',
			'required' => array( 'blog-archive-sidebar', '=', array( 'left', 'both' ) ),
		),
		
		array(
			'id'       => 'blog-archive-sidebar-right',
			'type'     => 'select',
			'title'    => esc_html__( 'Right sidebar', 'w9sme' ),
			'subtitle' => esc_html__( 'Choose the default right sidebar.', 'w9sme' ),
			'data'     => 'sidebars',
			'desc'     => '',
			'default'  => 'sidebar-1',
			'required' => array( 'blog-archive-sidebar', '=', array( 'right', 'both' ) ),
		),
	)
);