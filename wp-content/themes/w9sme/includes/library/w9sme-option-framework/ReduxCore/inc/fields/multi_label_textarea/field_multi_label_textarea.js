/*global redux_change, redux*/

(function ($) {
	"use strict";

	redux.field_objects = redux.field_objects || {};
	redux.field_objects.multi_label_textarea = redux.field_objects.multi_label_textarea || {};

	$(document).ready(
		function () {
			//redux.field_objects.multi_label_textarea.init();
		}
	);

	redux.field_objects.multi_label_textarea.init = function (selector) {

		if (!selector) {
			selector = $(document).find('.redux-container-multi_label_textarea:visible');
		}

		$(selector).each(
			function () {
				var el = $(this);
				var parent = el;
				if (!el.hasClass('redux-field-container')) {
					parent = el.parents('.redux-field-container:first');
				}
				if (parent.is(":hidden")) { // Skip hidden fields
					return;
				}
				if (parent.hasClass('redux-field-init')) {
					parent.removeClass('redux-field-init');
				} else {
					return;
				}

				el.find('.redux-multi-label-textarea-remove').live(
					'click', function () {
						redux_change($(this));
						$(this).prev('input[type="text"]').val('');
						$(this).parent().slideUp(
							'medium', function () {
								$(this).remove();
							}
						);
					}
				);

				var tinyMceOptions = {
					selector        : '.redux-container-multi_label_textarea .visible-item textarea',
					menubar         : false,
					plugins         : 'image lists textcolor',
					toolbar         : "undo redo | styleselect | bold italic | fontsizeselect | " +
					"forecolor backcolor | alignleft aligncenter alignright alignjustify " +
					"| bullist numlist outdent indent | blockquote",
					fontsize_formats: "inherit medium 8pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 18pt 24pt 28pt 36pt"
				};


				el.find('.redux-multi-label-textarea-add').click(
					function () {
						var number = parseInt($(this).attr('data-add_number'));
						var id = $(this).attr('data-id');
						var name = $(this).attr('data-name');
						var count = el.find('#' + id + ' li').length;

						for (var i = 0; i < number; i++) {
							var new_input = $('#' + id + ' li:first-child').clone();
							new_input.find('.reserved').removeClass('reserved');
							el.find('#' + id).append(new_input);
							el.find('#' + id + ' li:last-child').removeAttr('style');
							el.find('#' + id + ' li:last-child input.regular-text').val('');
							el.find('#' + id + ' li:last-child input.regular-text').attr('name', name + '[' + count + '][label]');
							el.find('#' + id + ' li:last-child input.regular-text').attr('id', name + '-' + count + '-label');
							el.find('#' + id + ' li:last-child textarea.regular-text-content').val('');
							el.find('#' + id + ' li:last-child textarea.regular-text-content').attr('name', name + '[' + count + '][content]');
							el.find('#' + id + ' li:last-child textarea.regular-text-content').attr('id', name + '-' + count + '-content');
							el.find('#' + id + ' li:last-child').addClass('new-item-' + count);

							tinyMceOptions['selector'] = '.redux-container-multi_label_textarea .new-item-' + count + ' textarea';
							tinymce.init(tinyMceOptions);
						}
					}
				);

				tinymce.init(tinyMceOptions);
			}
		);
	};
})(jQuery);